package eu.accept.tiekinetix.nexus.profiles.util;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.concurrent.Executors;

/**
 * Created by vchepegin on 05/05/2017.
 */
@Nested
@DisplayName("Test of tests")
class CoreUtilityServiceTest {
    @Test
    void isNumericR() {
    }

    @Test
    void isNumeric() {
    }

    @Test
    void bestDaysBetween() {
    }

    @Test
    void simpleDaysBetween() {
    }

    class MockData {
        private Date date;

        public Date getDate() {
            return date;
        }

        public void setDate(Date date) {
            this.date = date;
        }
    }

    @Test
    void simpleDateComparison () {
        Date d = new Date (17, 4, 8);
        MockData se = new MockData();
        se.setDate(d);

        d.setYear(322);
        //d = new Date (322, 11, 30);
        //se.setEndDate(d);
        System.out.println(se.getDate());

        Executors.newCachedThreadPool();
    }

//    @Test
//    void dateAfterDaysTest() {
//        Date startDate = new Date(117, 3, 13);
//        Date expectedDate = new Date(117, 4, 11);;
//        assertEquals(expectedDate, dateAfterDays(startDate, 28L));
//
//        startDate = new Date(117, 3, 13);
//        expectedDate = new Date(117, 3, 20);;
//        assertEquals(expectedDate, dateAfterDays(startDate, 7L));
//    }
//
//    @Test
//    void dateAfterWorkingDaysTest() {
//        Date startDate = new Date(117, 2, 6);
//        Date expectedDate = new Date(117, 2, 6);;
//        assertEquals(expectedDate, dateAfterWorkingDays(startDate, 0L));
//
//        //start is set to Saturday thus end has to be Monday
//        startDate = new Date(117, 2, 4);
//        expectedDate = new Date(117, 2, 6);;
//        assertEquals(expectedDate, dateAfterWorkingDays(startDate, 1L));
//
//        startDate = new Date(117, 2, 6);
//        expectedDate = new Date(117, 2, 7);
//        assertEquals(expectedDate, dateAfterWorkingDays(startDate, 2L));
//
//        startDate = new Date(117, 2, 6);
//        expectedDate = new Date(117, 2, 21);
//        assertEquals(expectedDate, dateAfterWorkingDays(startDate, 12L));
//
//        startDate = new Date(117, 2, 22);
//        expectedDate = new Date(117, 2, 31);;
//        assertEquals(expectedDate, dateAfterWorkingDays(startDate, 8L));
//
//        startDate = new Date(117, 2, 22);
//        expectedDate = new Date(117, 2, 30);;
//        assertEquals(expectedDate, dateAfterWorkingDays(startDate, 7L));
//
//        startDate = new Date(117, 2, 22);
//        expectedDate = new Date(117, 2, 29);;
//        assertEquals(expectedDate, dateAfterWorkingDays(startDate, 6L));
//
//        startDate = new Date(117, 2, 22);
//        expectedDate = new Date(117, 2, 28);;
//        assertEquals(expectedDate, dateAfterWorkingDays(startDate, 5L));
//
//        startDate = new Date(117, 2, 6);
//        expectedDate = new Date(117, 2, 10);;
//        assertEquals(expectedDate, dateAfterWorkingDays(startDate, 5L));
//
//        startDate = new Date(117, 4, 8);
//        expectedDate = new Date(117, 4, 11);;
//        assertEquals(expectedDate, dateAfterWorkingDays(startDate, 4L));
//
//    }


}