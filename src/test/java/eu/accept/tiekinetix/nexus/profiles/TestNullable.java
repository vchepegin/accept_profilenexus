package eu.accept.tiekinetix.nexus.profiles;

import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Activity;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Quantity;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * Created by vchepegin on 13/07/2017.
 */
public class TestNullable {

    @Test
    public void testNullable () {
        Activity activity =  new Activity();
        Optional<Quantity> oldQuantity = Optional.ofNullable(activity.getConsumptionRate());
        oldQuantity.ifPresent((x)->System.out.println("The value in oldQuantity is present"));

        activity.addConsumptionRate(new Quantity("id1", "kw/h", 45));
        Optional<Quantity> oldQuantity2 = Optional.ofNullable(activity.getConsumptionRate());
        Set<Quantity> removable = new HashSet<>();
        oldQuantity2.ifPresent((x)-> removable.add(x));
    }

}
