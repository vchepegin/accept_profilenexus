package eu.accept.tiekinetix.nexus.profiles.util;

/**
 * Created by vchepegin on 27/06/2017.
 */
public class ExtractionTest {

    private final static String input = "{\"name\": \"Construction Formwork\", \"taskType\": \"Activity\", " +
            "\"tier\": \"TIER_3\", \"scheduleStatus\": \"NOT_STARTED\", \"completionStatus\": " +
            "\"TO_DO\", \"workingHoursPerDay\" : \"8\", \"numberOfAssignedCrews\": \"1\", \"quantity\": " +
            "{\"unit\":\"m2\", \"amount\":\"383\"}, \"consumptionRate\": {\"unit\":\"wh/m2\", " +
            "\"amount\":\"0.235\"}, \"wbsCode\":\"1\"}";

    public static void main(String[] args) {
        int position = input.indexOf("taskType");
        int position2 = input.indexOf(",", position);
        position = input.indexOf(":", position);
        position = input.indexOf("\"", position);
        String type = input.substring(position+1, position2-1);

        System.out.println(type);
    }


}
