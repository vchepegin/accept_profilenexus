package eu.accept.tiekinetix.nexus.profiles.controllers;

import org.junit.jupiter.api.*;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Transaction;
import org.neo4j.test.TestGraphDatabaseFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.number.OrderingComparison.greaterThan;

/**
 * Created by vchepegin on 03/05/2017.
 */

@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
@WebAppConfiguration
@AutoConfigureMockMvc
@Nested
@DisplayName("Test of tests")
public class TaskOperationsTest {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private WebApplicationContext wac;

    @LocalServerPort
    private int port;

    private URL base;

    @Autowired
    private TestRestTemplate template;

    private static final File DB_PATH = new File( "neo4j-test-db" );
    private GraphDatabaseService graphDb;



//    @BeforeAll
//    public static void  setUpBeforeALL () throws IOException {
//        graphDb = new GraphDatabaseFactory().newEmbeddedDatabase( DB_PATH );
//        registerShutdownFunction( graphDb );
//    }

    @AfterAll
    public static void tearDown () throws IOException {
//        graphDb.shutdown();
//        FileUtils.deleteRecursively( DB_PATH );
    }

    @BeforeEach
    public void setUp() throws Exception {
        graphDb = new TestGraphDatabaseFactory().newImpermanentDatabase();
        registerShutdownFunction( graphDb );
//        this.base = new URL("http://localhost:" + port + "/");
//        this.mvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }

    private static void registerShutdownFunction( final GraphDatabaseService graphDb )
    {
        // Registers a shutdown hook for the Neo4j instance so that it
        // shuts down nicely when the VM exits (even if you "Ctrl-C" the
        // running application).
        Runtime.getRuntime().addShutdownHook( new Thread()
        {
            @Override
            public void run()
            {
                graphDb.shutdown();
            }
        } );
    }

    @Test
    @DisplayName("should do with embedded DB")
    public void embeddedDBTest() throws Exception {
        Node n = null;
        try ( Transaction tx = graphDb.beginTx() )
        {
            n = graphDb.createNode();
            n.setProperty( "name", "Nancy" );
            tx.success();
        }

        // The node should have a valid id
        assertThat( n.getId(), is( greaterThan( -1L ) ) );

        // Retrieve a node by using the id of the created node. The id's and
        // property should match.
        try ( Transaction tx = graphDb.beginTx() )
        {
            Node foundNode = graphDb.getNodeById( n.getId() );
            assertThat( foundNode.getId(), is( n.getId() ) );
            assertThat(foundNode.getProperty( "name" ), is( "Nancy" ) );
        }
    }

//    @Test
//    @DisplayName("should do with MVC Mock Builder")
//    public void sampleTest() throws Exception {
//        mvc.perform(MockMvcRequestBuilders.get("/").accept(MediaType.APPLICATION_JSON))
//                .andExpect(status().isOk())
//                .andExpect(content().string(equalTo("Hello ACCEPT PN")));
//    }
//
//    @Test
//    @DisplayName("should do with a simple response body")
//    public void createTask() throws Exception {
//        ResponseEntity<String> response = template.getForEntity(base.toString(),
//                String.class);
//        assertThat(response.getBody(), equalTo("task JSON"));
//    }

}