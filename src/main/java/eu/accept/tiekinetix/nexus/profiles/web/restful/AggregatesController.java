package eu.accept.tiekinetix.nexus.profiles.web.restful;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vchepegin on 07/03/2017.
 *
 * Controller for aggregates, e.g. give me all skills per specific crew
 */
@CrossOrigin
@RestController
public class AggregatesController {

    @Autowired
    private TaskRepository taskRepository;

    private final ObjectMapper mapper = new ObjectMapper();

}
