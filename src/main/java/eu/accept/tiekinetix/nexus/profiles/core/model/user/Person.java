package eu.accept.tiekinetix.nexus.profiles.core.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.PersonRole;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.PersonStatus;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Location;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Task;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.TimeFrame;
import eu.accept.tiekinetix.nexus.profiles.core.model.user.Specialisation;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by vchepegin on 27/01/2017.
 */

@NodeEntity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Person implements Serializable {

    private static final long serialVersionUID = 894105848152511257L;
    @GraphId private Long id;

    private String personID; //= SPC-CLIP ID, si the same as registred by the ACCEPT authentication service
    private String tikkiUserID; // = User ID from the TIKKI incident management system
    private String acceptUserName;
	private PersonStatus personStatus;
    private String lastName;
    private String firstName;
    private PersonRole role;
    private String languageCode; //an according to ISO 639-1
    private int hoursPerDay;
//    private String crewID; //TODO is this a part of this object?

    public Person(String personID, String acceptUserName,
                  PersonStatus personStatus, String lastName, String firstName,
                  PersonRole role, String languageCode) {
        this.personID = personID;
        this.acceptUserName = acceptUserName;
        this.personStatus = personStatus;
        this.lastName = lastName;
        this.firstName = firstName;
        this.role = role;
        this.languageCode = languageCode;
    }

    public Set<String> skills;

    @JsonProperty("skills")
    public Set<String> getSkills() { return this.skills;}

    public Person addSkill (String skills) {
        if (this.skills == null) {
            this.skills = new HashSet<>();
        }
        this.skills.add(skills.toLowerCase());
        return this;
    }

    public void removeSkill(String skills) {
        this.skills.remove(skills.toLowerCase());
    }


    public Set<String> specialisations;

    @JsonProperty("specialisations")
    public Set<String> getSpecialisations() { return this.specialisations;}

    public Person addSpecialisation (String specialisation) {
        if (this.specialisations == null) {
            this.specialisations = new HashSet<>();
        }
        this.specialisations.add(specialisation.toLowerCase());
        return this;
    }

    public void removeSpecialisation(String specialisation) {
        this.specialisations.remove(specialisation.toLowerCase());
    }


    @Relationship(type="onLeave", direction = Relationship.OUTGOING)
    public Set<TimeFrame> onLeaveEvents;

    /**
     * Add new onLeave event to the person
     * @param timeFrame
     * @return Person for the fluent interface
     */
    public Person addOnLeaveEvent (TimeFrame timeFrame) {
        if (onLeaveEvents == null) {
            onLeaveEvents = new HashSet<>();
        }
        onLeaveEvents.add(timeFrame);

        return this;
    }

    @Relationship(type="hasContactInformation", direction = Relationship.OUTGOING)
    public ContactDetails contactDetails;

    /**
     * Add new task event to the person
     * @param contactDetails
     * @return Person for the fluent interface
     */
    public Person setContactDetails (ContactDetails contactDetails) {
        this.contactDetails = contactDetails;
        return this;
    }

    @Relationship(type="hasJobRelationData", direction = Relationship.OUTGOING)
    public Set<JobData> jobDatas;

    /**
     * Add new task event to the person
     * @param jobData
     * @return Person for the fluent interface
     */
    public Person addPerformanceIndicator (JobData jobData) {
        if (jobDatas == null) {
            jobDatas = new HashSet<>();
        }
        jobDatas.add(jobData);

        return this;
    }

    @Relationship(type="atLocation", direction = Relationship.OUTGOING)
    public Location location;

    /**
     * Add new location to the person
     * @param location
     * @return Person for the fluent interface
     */
    public Person setLocation (Location location) {
        this.location = location;

        return this;
    }

    @Relationship(type="hasEquipment", direction = Relationship.OUTGOING)
    public Set<Equipment> equipment;

    /**
     * Add new equipment to the task
     * @param newEquipment
     * @return Task for the fluent interface
     */
    public Person addEquipment (Equipment newEquipment) {
        if (equipment == null) {
            equipment = new HashSet<>();
        }
        equipment.add(newEquipment);

        return this;
    }
/*
    @JsonProperty("equipment")
    public Set<Equipment> getEquipment() {
        return equipment;
    }

    @JsonProperty("equipment")
    public void setEquipment(Set<Equipment> equipment) {
        this.equipment = equipment;
    }
*/
    private Person() {}

    @JsonProperty("personID")
    public String getPersonID() {
        return personID;
    }

    @JsonProperty("personID")
    public void setPersonID(String personID) {
        this.personID = personID;
    }

    @JsonProperty("tikkiUserID")
    public String getTikkiUserID() {
        return tikkiUserID;
    }

    @JsonProperty("tikkiUserID")
    public void setTikkiUserID(String tikkiUserID) {
        this.tikkiUserID = tikkiUserID;
    }

    @JsonProperty("personStatus")
    public PersonStatus getPersonStatus() {
        return personStatus;
    }

    public void setPersonStatus(PersonStatus personStatus) {
        this.personStatus = personStatus;
    }

    @JsonProperty("lastName")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonProperty("firstName")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty("role")
    public PersonRole getRole() {
        return role;
    }

    public void setRole(PersonRole role) {
        this.role = role;
    }

    @JsonProperty("languageCode")
    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }
/*
    @JsonProperty("crewID")
    public String getCrewID() {
        return crewID;
    }

    //TODO set ID of the crew when it is returned
    private void setCrewID(String crewID) {
        this.crewID = crewID;
    }
*/
    @JsonProperty("acceptUserName")
    public String getAcceptUserName() {
        return acceptUserName;
    }

    @JsonProperty("acceptUserName")
    public void setAcceptUserName(String acceptUserName) {
        this.acceptUserName = acceptUserName;
    }

    @JsonProperty("hoursPerDay")
    public int getHoursPerDay() {
        return hoursPerDay;
    }

    @JsonProperty("hoursPerDay")
    public void setHoursPerDay(int hoursPerDay) {
        this.hoursPerDay = hoursPerDay;
    }
/*
    public Set<Task> getAssignedTasks() {
        return assignedTasks;
    }
*/
}
