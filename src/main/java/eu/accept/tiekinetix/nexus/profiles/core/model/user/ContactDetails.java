package eu.accept.tiekinetix.nexus.profiles.core.model.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

import java.io.Serializable;

/**
 * Created by vchepegin on 30/01/2017.
 */
@NodeEntity
public class ContactDetails implements Serializable {

    private static final long serialVersionUID = -606561210785269383L;
    @GraphId
    private Long id;

    private String contactDetailsID;
    private String countryCode; //ISO 3166-1 - basically a country GUID
    private String country; //could be in the local language
    private String city; //recommended to use IATA codes http://www.iata.org/publications/Pages/code-search.aspx
    private String street;
    private String telephone;
    private String email;

    public ContactDetails(String contactDetailsID, String countryCode, String country,
                          String city, String street, String telephone, String email) {
        this.contactDetailsID = contactDetailsID;
        this.countryCode = countryCode;
        this.country = country;
        this.city = city;
        this.street = street;
        this.telephone = telephone;
        this.email = email;
    }

    public ContactDetails () {}

    @JsonProperty("contactDetailsID")
    public String getContactDetailsID() {
        return contactDetailsID;
    }

    @JsonProperty("contactDetailsID")
    public void setContactDetailsID(String contactDetailsID) {
        this.contactDetailsID = contactDetailsID;
    }

    @JsonProperty("countryCode")
    public String getCountryCode() {
        return countryCode;
    }

    @JsonProperty("countryCode")
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    @JsonProperty("city")
    public String getCity() {
        return city;
    }

    @JsonProperty("city")
    public void setCity(String city) {
        this.city = city;
    }

    @JsonProperty("street")
    public String getStreet() {
        return street;
    }

    @JsonProperty("street")
    public void setStreet(String street) {
        this.street = street;
    }

    @JsonProperty("telephone")
    public String getTelephone() {
        return telephone;
    }

    @JsonProperty("telephone")
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }
}
