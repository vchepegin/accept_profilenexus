package eu.accept.tiekinetix.nexus.profiles.bim.model;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Project;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.springframework.web.bind.annotation.ResponseBody;

@Getter
@RequiredArgsConstructor
@NodeEntity
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
@ResponseBody
public abstract class BimEntity {

    @GraphId
    @JsonIgnore
    protected Long neoId;

    /**
     * We need a unique id to create constraints in Neo4j (see also {@link eu.accept.tiekinetix.bimimport.controller.NeoIndexController}.
     * Hence we create the id by concatenating projectId, type (= Neo4j label) and bimId.
     * This combination is unique for all entities apart from BimParameters. For these we use the field guid as "primary key".
     *
     */
    protected String entityId;

    @Relationship(type = "PROJECT")
    protected Project project;

    @Relationship(type = "SCHEDULEDENTITY")
    protected Set<ScheduledEntity> scheduledEntities = new HashSet<>();

    @JacksonXmlProperty(localName = "Guid")
    protected String guid;

    @JacksonXmlProperty(localName = "Id")
    protected Long bimId;

    protected void initEntityId() {
        if (project == null || project.getProjectID() == null || bimId == null) {
            throw new IllegalArgumentException();
        }
        entityId = project.getProjectID() + ":" + getClass().getSimpleName() + ":" + bimId;
    }

    public void setBimId(Long bimId) {
        this.bimId = bimId;
        if (project != null) {
            initEntityId();
        }
    }

    public void setNeoId(Long neoId) {
        this.neoId = neoId;
    }

    public Long getNeoId() {
        return this.neoId;
    }
    public Long getBimId() {
        return this.bimId;
    }
    public String getBimEntityId() {
        return this.entityId;
    }
    public String getGuid() {
        return this.guid;
    }


    public void setProject(Project project) {
        this.project = project;
        if (bimId != null) {
            initEntityId();
        }
    }

    public Set<ScheduledEntity> getScheduledEntities() {
        return scheduledEntities;
    }

    public void addScheduledEntity(ScheduledEntity se) {
         scheduledEntities.add(se);
    }

}
