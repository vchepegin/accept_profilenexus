package eu.accept.tiekinetix.nexus.profiles.bim.controller;

import java.io.IOException;
import java.util.List;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import eu.accept.tiekinetix.nexus.profiles.bim.model.*;
import eu.accept.tiekinetix.nexus.profiles.bim.repository.BimEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by akuschki on 12/06/2017.
 */
@CrossOrigin
@RestController
//@RequestMapping(value = "deprecated/{bimType}/{bimId}",produces = {"application/json"}, consumes = {"*/*"})
public class BimEntityController extends AbstractBimController {

    @Autowired
    private BimEntityRepository bimEntityRepository;
/*
    @Transactional
    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@Valid @PathVariable("projectID") String projectId, @Valid @PathVariable("bimType") String bimType, @Valid @PathVariable("bimId") Long bimId) throws IOException {
        bimEntityRepository.delete(bimEntityRepository.find(projectId, null, bimId, bimType, 10000)
                .stream()
                .findAny()
                .orElseThrow(() -> new ResourceNotFoundException())
                .getNeoId());
    }
    */
/*
    @Transactional
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET, produces = {"application/json"})
    public BimEntity get(@Valid @PathVariable("projectID") String projectId, @Valid @PathVariable("bimType") String bimType, @Valid @PathVariable("bimId") Long bimId, @PathParam("depth") Integer depth) throws IOException {
        return bimEntityRepository.findOne(bimEntityRepository.find(projectId, null, bimId, null, bimType)
                                                              .stream()
                                                              .findAny()
                                                              .orElseThrow(() -> new ResourceNotFoundException())
                                                              .getNeoId(), depth != null ? depth : 1);
    }
*/
/*
    @Transactional
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.GET, produces = {"application/json"})
//    public List<BimEntity> get(@Valid @PathVariable("projectID") String projectId, @Valid @PathVariable("bimType") String bimType, @Valid @PathVariable("bimId") Long bimId, @PathParam("depth") Integer depth) throws IOException {

//    public List<BimEntity> get(@PathParam("projectID") String projectId, @PathParam("bimType") String bimType, @PathParam("bimId") Long bimId, @PathParam("depth") Integer depth) throws IOException {
//        return bimEntityRepository.find(projectId, null, bimId, bimType);
//                                                              .stream()
//                                                              .findAny()
//                                                              .orElseThrow(() -> new ResourceNotFoundException());
//                                                              .getNeoId(), depth != null ? depth : 1);
    public List<BimEntity> get (@PathParam(value="projectId") String projectId,
                                @PathParam(value="scheduledEntityId") String scheduledEntityId,
                                @PathParam("bimType") String bimType,
                                @PathParam("bimId") Long bimId,
                                @PathParam("limit") Integer limit)
            throws IOException {
        Integer limitExpr = 1000;
        if (limit!= null && limit >0) {
            limitExpr = limit;
        }
        return bimEntityRepository.find(projectId, scheduledEntityId, bimId, bimType,limitExpr);

    }
*/


//    @RequestMapping(value = "/bimEntitiesPaged", method = RequestMethod.GET)
//    HttpEntity<PagedResources<BimEntity>> getEntitiesPaged(Pageable pageable, PagedResourcesAssembler assembler) {
//
//        Page<BimEntity> entities = bimEntityRepository.findAll(pageable);
//        final PagedResources pagedResources = assembler.toResource(entities);
//        return new ResponseEntity<PagedResources<BimEntity>>(pagedResources, HttpStatus.OK);
//    }


    @CrossOrigin
//    @Transactional
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/bimEntities/search/find",method = RequestMethod.GET, produces = {"application/json"})
    public List<BimEntity> bimEntities (@PathParam(value="projectId") String projectId,
                                        @PathParam(value="scheduledEntityId") String scheduledEntityId,
                                        @PathParam("bimType") String bimType,
                                        @PathParam("bimId") Long bimId,
                                        @PathParam("limit") Integer limit)
            throws IOException {
        Integer limitExpr = 1000;
        if (limit!= null && limit >0) {
            limitExpr = limit;
        }
        return bimEntityRepository.find(projectId, scheduledEntityId, bimId, bimType, limitExpr);
    }


    @CrossOrigin
//    @Transactional
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/bimElements",method = RequestMethod.GET, produces = {"application/json"})
    public List<BimElement> bimElements (@PathParam(value="projectId") String projectId,
                                         @PathParam(value="scheduledEntityId") String scheduledEntityId,
                                         @PathParam("bimId") Long bimId,
                                         @PathParam("limit") Integer limit)
            throws IOException {
        Integer limitExpr = 1000;
        if (limit!= null && limit >0) {
            limitExpr = limit;
        }
        return bimEntityRepository.findBimElement(projectId, scheduledEntityId, bimId, limitExpr);
    }

    @CrossOrigin
//    @Transactional
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/bimMaterials",method = RequestMethod.GET, produces = {"application/json"})
    public List<BimMaterial> bimMaterials (@PathParam(value="projectId") String projectId,
                                          @PathParam(value="scheduledEntityId") String scheduledEntityId,
                                          @PathParam("bimId") Long bimId,
                                          @PathParam("limit") Integer limit)
            throws IOException {
        Integer limitExpr = 1000;
        if (limit!= null && limit >0) {
            limitExpr = limit;
        }
        return bimEntityRepository.findBimMaterial(projectId, scheduledEntityId, bimId, limitExpr);
    }

    @CrossOrigin
//    @Transactional
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/bimTypes",method = RequestMethod.GET, produces = {"application/json"})
    public List<BimType> bimTypes (@PathParam(value="projectId") String projectId,
                                   @PathParam(value="scheduledEntityId") String scheduledEntityId,
                                   @PathParam("bimId") Long bimId,
                                   @PathParam("limit") Integer limit)
            throws IOException {
        Integer limitExpr = 1000;
        if (limit!= null && limit >0) {
            limitExpr = limit;
        }
        return bimEntityRepository.findBimType(projectId, scheduledEntityId, bimId, limitExpr);
    }

    @CrossOrigin
//    @Transactional
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/bimRooms",method = RequestMethod.GET, produces = {"application/json"})
    public List<BimRoom> bimRooms (@PathParam(value="projectId") String projectId,
                                   @PathParam(value="scheduledEntityId") String scheduledEntityId,
                                   @PathParam("bimId") Long bimId,
                                   @PathParam("limit") Integer limit)
            throws IOException {
        Integer limitExpr = 1000;
        if (limit!= null && limit >0) {
            limitExpr = limit;
        }
        return bimEntityRepository.findBimRoom(projectId, scheduledEntityId, bimId, limitExpr);
    }

    @CrossOrigin
//    @Transactional
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/bimFamilies",method = RequestMethod.GET, produces = {"application/json"})
    public List<BimFamily> bimFamilies (@PathParam(value="projectId") String projectId,
                                   @PathParam("bimId") Long bimId,
                                   @PathParam("limit") Integer limit)
            throws IOException {
        Integer limitExpr = 1000;
        if (limit!= null && limit >0) {
            limitExpr = limit;
        }
        return bimEntityRepository.findBimFamily(projectId, bimId, limitExpr);
    }

    @CrossOrigin
//    @Transactional
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/bimCategories",method = RequestMethod.GET, produces = {"application/json"})
    public List<BimCategory> bimCategories (@PathParam(value="projectId") String projectId,
                                        @PathParam("bimId") Long bimId,
                                        @PathParam("limit") Integer limit)
            throws IOException {
        Integer limitExpr = 1000;
        if (limit!= null && limit >0) {
            limitExpr = limit;
        }
        return bimEntityRepository.findBimCategory(projectId, bimId, limitExpr);
    }
}
