package eu.accept.tiekinetix.nexus.profiles.web.restful;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import eu.accept.tiekinetix.nexus.messaging.ChangedEntitiesLoggingService;
import eu.accept.tiekinetix.nexus.profiles.core.exceptions.EntityAlreadyExistsException;
import eu.accept.tiekinetix.nexus.profiles.core.exceptions.EntityDoesNotExist;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.Constructable;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.*;
import eu.accept.tiekinetix.nexus.profiles.reporting.processing.KPIProcessorService;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.ProjectRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.ScheduledEntityRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.TaskRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.WorkflowRepository;
import org.neo4j.ogm.response.model.RelationshipModel;
import org.neo4j.ogm.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

import static eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.Intention.SCHEDULED_WORKING_DAYS_CALCULATION;
/**
 * Created by vchepegin on 20/04/2017.
 */
@CrossOrigin
@RestController
public class WorkflowController {

    @Autowired
    private WorkflowRepository  workflowRepository;
    @Autowired
    private ScheduledEntityRepository scheduledEntityRepository;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    Session session;
    @Autowired
    KPIProcessorService kpiProcessor;
    @Autowired
    ChangedEntitiesLoggingService loggingService;

    private final ObjectMapper mapper = new ObjectMapper();

    final static Logger logger = LoggerFactory.getLogger(WorkflowController.class);

    @CacheEvict(value = "workflow", allEntries = true)
    @CrossOrigin
    @RequestMapping(value="/cloneWorkflowToProject/{workflowID}/{projectID}", method = RequestMethod.POST)
    public String cloneWorkflowToProject (@RequestBody List<Map<String, Object>> payload,
                                            @PathVariable(value="workflowID") String workflowID,
                                            @PathVariable(value="projectID") String projectID) throws Exception {

        Project parent = projectRepository.findByProjectID(projectID);
        if (parent == null) {
            throw new EntityDoesNotExist("Project with ID: " + projectID + " does not exist.");
        }

        Workflow workflow = workflowRepository.findByWorkflowID(workflowID);
        if (workflow == null) {
            throw new EntityDoesNotExist("Workflow with ID: " + workflowID + " does not exist.");
        }

        Map<String, Map<String, Object>> params  = new HashMap();
        for (Map<String, Object> item : payload) {
            params.put(item.get("wbsCode").toString(), item);
        }

        Workflow newWorkflow = new Workflow();
        newWorkflow.setWorkflowID(java.util.UUID.randomUUID().toString());
        newWorkflow.setName(workflow.getName());


        String nQuery = "match (a:Workflow {workflowID: {workflowID}})-[r*..21]-(b) return collect(distinct b);";

        String rQuery = "match (a:Workflow {workflowID: {workflowID}})-[r*..21]->(b) WITH b as nds  match" +
                " (nds)<-[rel]-(d) return distinct rel;";

//        Map<String, String> criteria = new HashMap<>(8);
//        criteria.put("workflowID", workflowID);

        Iterable<Map<String, Object>> nodeResults = session.query(nQuery,
                ImmutableMap.<String, String>builder().put("workflowID", workflowID).build()).queryResults();

        Iterable<Map<String, Object>> relResults = session.query(rQuery,
                ImmutableMap.<String, String>builder().put("workflowID", workflowID).build()).queryResults();

        HashMap<Long, String> newUID = new HashMap<>(50);

        //create all entities
        HashMap<String, Object> entities = new HashMap<>(50);

        Map hMap = nodeResults.iterator().next();
        List nodes = (ArrayList) hMap.get("collect(distinct b)");

        for ( Object node : nodes) {

            String guid = java.util.UUID.randomUUID().toString();

            if (node instanceof Activity) {
                Activity newActivity = new Activity((Activity)node);

                newUID.put(((Activity) node).getTaskID(), guid);
                newActivity.setEntityID(guid);

                Map<String, Object> p = params.get(newActivity.getWbsCode());
                if (p != null) {
                    //this method also creates quantity and consumptionRate if in JSON
                    WorkflowUtils.updateWithNewParameters(newActivity, p);
                }

                entities.put(guid, newActivity);

            }

            if (node instanceof QualityGate) {
                QualityGate newQG = new QualityGate((QualityGate)node);

                newUID.put(((QualityGate) node).getQualityGateID(), guid);
                newQG.setEntityID(guid);

                Map<String, Object> p = params.get(newQG.getWbsCode());
                if (p != null) {
                    WorkflowUtils.updateWithNewParameters(newQG, p);
                }

                entities.put(guid, newQG);
            }

//            if (node instanceof Quantity) {
//                Quantity newQuantity = new Quantity((Quantity)node);
//
//                newUID.put(((Quantity) node).getId(), guid);
//                newQuantity.setQuantityID(guid);
//
//                Map<String, Object> p = params.get(newQuantity.getWbsCode());
//                if (p != null) {
//                    updateWithNewParameters(newQuantity, p);
//                }
//
//                entities.put(guid, newQuantity);
//            }
        }

        //inverse index of the entities with hasSpecialScheduling relationships

        //link entities to each other - creation of the graph
        for (Map<String, Object> map: relResults) {
            RelationshipModel rel = (RelationshipModel) map.get("rel");

            if (rel.getType().equals("hasStart")) {
                ScheduledEntity start = (ScheduledEntity) entities.get(newUID.get(rel.getEndNode()));
                newWorkflow.addStart(start);
            }

            if (rel.getType().equals("hasSuccessor")) {
                ScheduledEntity pre = (ScheduledEntity) entities.get(newUID.get(rel.getStartNode()));
                ScheduledEntity post = (ScheduledEntity) entities.get(newUID.get(rel.getEndNode()));
                pre.addSuccessor(post);
            }

//            if (rel.getType().equals("hasQuantity")) {
//                Activity activity = (Activity) entities.get(newUID.get(rel.getStartNode()));
//                if (activity.getQuantity() == null) {
//                    Quantity quantity = (Quantity) entities.get(newUID.get(rel.getEndNode()));
//                    activity.addQuantity(quantity);
//                }
//            }
//
//            if (rel.getType().equals("hasConsumptionRate")) {
//                Activity activity = (Activity) entities.get(newUID.get(rel.getStartNode()));
//                if (activity.getConsumptionRate() == null) {
//                    Quantity quantity = (Quantity) entities.get(newUID.get(rel.getEndNode()));
//                    activity.addConsumptionRate(quantity);
//                }
//            }

            if (rel.getType().equals("hasSpecialScheduling")) {
                ScheduledEntity pre = (ScheduledEntity) entities.get(newUID.get(rel.getStartNode()));
                ScheduledEntity post = (ScheduledEntity) entities.get(newUID.get(rel.getEndNode()));
                pre.addSpecialSchedulingRelation(post);

            }
        }

        parent.addWorkflow(newWorkflow);
        projectRepository.save(parent);

        return newWorkflow.getWorkflowID();
    }

    @CacheEvict(value = "workflow", allEntries = true)
    @CrossOrigin
    @RequestMapping(value="/addNewWorkflow/{workflowID}/{taskID}", method = RequestMethod.POST)
    public void addNewWorkflow (@PathVariable(value="workflowID") String workflowID,
                                  @PathVariable(value="taskID") String taskID) throws Exception {

        Task parent = taskRepository.findByEntityID(taskID);
        if (parent == null) {
            throw new EntityDoesNotExist("Task with ID: " + taskID + " does not exist.");
        }

        Workflow workflow = workflowRepository.findByWorkflowID(workflowID);
        if (workflow == null) {
            throw new EntityDoesNotExist("Workflow with ID: " + workflowID + " does not exist.");
        }

        Map params = new HashMap();
        params.put("startDate", parent.getStartDate());
        params.put("wbsCode", parent.getWbsCode());
        params.put("lbsCode", parent.getLbsCode());

        //removed based on the requirements from issue #109
        //The quantity HAS TO be set in the task that starts a workflow
//        if (parent.getQuantity() == null || parent.getQuantity().getAmount() == 0) {
//            throw new IllegalArgumentException("The quantity variable has to be set in the Task that tries " +
//                    "to create a workflow.");
//        }
//
        //issue #109 - we still pass the variable around if available
//        if (parent.getQuantity() != null) {
//            params.put("quantity", parent.getQuantity().getAmount());
//        } else {
//            params.put("quantity", 1d);
//        }

        String nQuery = "match (a:Workflow {workflowID: {workflowID}})-[r*..21]-(b) return collect(distinct b);";

        String RQuery = "match (a:Workflow {workflowID: {workflowID}})-[r*..21]->(b) WITH b as nds  match" +
                " (nds)<-[rel]-(d) return distinct rel;";

        //the result of this query is not convenient for the processing
        String query = "match (a:Workflow {workflowID: {workflowID}})-[r*]-(b) WITH b as nds " +
                "match (nds)<-[rel]-(d) return distinct rel, collect(distinct d);";

        Map<String, String> criteria = new HashMap<>(1);
        criteria.put("workflowID", workflowID);

        Iterable<Map<String, Object>> nodeResults = session.query(nQuery, criteria).queryResults();

        Iterable<Map<String, Object>> relResults = session.query(RQuery, criteria).queryResults();

        HashMap<Long, String> newUID = new HashMap<>(35);

        //create all entities
        HashMap<String, Object> entities = new HashMap<>(35);

        Map hMap = nodeResults.iterator().next();
        List nodes = (ArrayList) hMap.get("collect(distinct b)");

        //array of Constructable-s for the further calculations
        Task falseParent = new Task (parent);
//        HashMap<String, Constructable> constructables = new HashMap(30);
//        ArrayList<ScheduledEntity> cons = new ArrayList<>(25);

        for ( Object node : nodes) {

            String guid = java.util.UUID.randomUUID().toString();

            if (node instanceof Activity) {
                Activity newActivity = new Activity((Activity)node);

                newUID.put(((Activity) node).getTaskID(), guid);
                newActivity.setEntityID(guid);
//                constructables.put(newActivity.getWbsCode(), newActivity);
//                cons.add(newActivity);
                entities.put(guid, newActivity);
                loggingService.logChangedEntity(newActivity.getEntityID());
            }

            if (node instanceof QualityGate) {
                QualityGate newQG = new QualityGate((QualityGate)node);

                newUID.put(((QualityGate) node).getQualityGateID(), guid);
                newQG.setEntityID(guid);
//                constructables.put(newQG.getWbsCode(), newQG);
//                cons.add(newQG);
                entities.put(guid, newQG);
            }

            if (node instanceof Quantity) {
                Quantity newQuantity = new Quantity((Quantity)node);

                newUID.put(((Quantity) node).getId(), guid);
                newQuantity.setQuantityID(guid);
                entities.put(guid, newQuantity);
            }
        }

        //inverse index of the entities with hasSpecialScheduling relationships
        HashMap<String, ScheduledEntity> specialSchedulingRelations = new HashMap();

        //link entities to each other - creation of the graph
        for (Map<String, Object> map: relResults) {
            RelationshipModel rel = (RelationshipModel) map.get("rel");

            if (rel.getType().equals("hasStart")) {
                ScheduledEntity start = (ScheduledEntity) entities.get(newUID.get(rel.getEndNode()));
                /**
                 *  necessary to adjust for the Activity since they start next day after finishing previous step
                 *  BUT NOT for the Activities that starts a Workflow, i.e. direct children of the Task
                 */
                LocalDate adjustedDate = parent.getStartDate().toInstant().atZone
                        (ZoneId.systemDefault()).toLocalDate();
/*                if (start instanceof Activity) {
////////////////////////////////////////////////////////////////////// we should remove that, as startNodes have 1 day before startDate of parent-task
                    adjustedDate = adjustedDate.minusDays(1);
                }
*/
                //instantiate the start date with the start date of a parent task
                Date sDate = Date.from(adjustedDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
                sDate.setHours(9);
                start.setStartDate(sDate);

                parent.addChild(start);

                //this is to deal with the diversity of different relationships and uncertainty with the start nodes
                falseParent.addSuccessor(start);
            }

            if (rel.getType().equals("hasSuccessor")) {
                ScheduledEntity pre = (ScheduledEntity) entities.get(newUID.get(rel.getStartNode()));
                ScheduledEntity post = (ScheduledEntity) entities.get(newUID.get(rel.getEndNode()));

                parent.addChild(pre);
                parent.addChild(post);
                pre.addSuccessor(post);
            }

            if (rel.getType().equals("hasQuantity")) {
                Task task = (Task) entities.get(newUID.get(rel.getStartNode()));
                Quantity quantity = (Quantity) entities.get(newUID.get(rel.getEndNode()));
                task.addQuantity(quantity);
            }

            if (rel.getType().equals("hasSpecialScheduling")) {
                ScheduledEntity startNode = (ScheduledEntity) entities.get(newUID.get(rel.getStartNode()));
                String startNodeID = newUID.get(rel.getEndNode()).toString();
                specialSchedulingRelations.put(startNodeID, startNode);
            }
        }

        //TODO: use just params Map for this purpose
        params.putAll(specialSchedulingRelations);

        /*
         *  Since we know the order of the steps (WBS codes of the template) we can use the
         *  iteration starting from the initial steps towards the end.
         *  Strictly speaking this ordering is not necessary.
         */
        Set<ScheduledEntity> currentStartNodes =  falseParent.getSuccessors();
        for (ScheduledEntity c :  currentStartNodes) {
            Constructable asConstructable = (Constructable) c;
            asConstructable.construct(params);
        }

        while (!currentStartNodes.isEmpty()) {
            //TODO: TreeSet does not give a better traverse so far
            Set<ScheduledEntity> nextStartNodes = new HashSet<>();
            for (ScheduledEntity currentParent : currentStartNodes) {
                nextStartNodes.addAll(WorkflowUtils.doConstruction(currentParent, params));
            }
            currentStartNodes = new HashSet<>();
            currentStartNodes.addAll(nextStartNodes);
        }

        //attach to the parent task the newly created workflow and save
        scheduledEntityRepository.save(parent);

        kpiProcessor.propagateUpwards(SCHEDULED_WORKING_DAYS_CALCULATION);
    }

    @CacheEvict(value = "workflow", allEntries = true)
    @CrossOrigin
    @RequestMapping(value="/createWorkflow", method = RequestMethod.POST)
    public String createWorkflow (@RequestBody String payload) throws Exception {

        Workflow newWorkflow = mapper.readValue(payload, Workflow.class);

        if (newWorkflow == null) {
            throw new IllegalArgumentException("Workflow cannot be created: " +
                    "check the taskType definition and spelling.");
        }

        String guid = newWorkflow.getWorkflowID();
        Workflow existingWorkflow = null;

        if (guid != null && !guid.isEmpty()) {
            existingWorkflow = workflowRepository.findByWorkflowID(guid);
        }
        if (existingWorkflow != null) {
            throw new EntityAlreadyExistsException("The workflow with ID = " + guid + " already exists.");
        }

        if (guid == null ||  guid.isEmpty()) {
            guid = java.util.UUID.randomUUID().toString();
        }

        newWorkflow.setWorkflowID(guid);
        workflowRepository.save(newWorkflow);

        return guid;
    }

    @CacheEvict(value = "workflow", allEntries = true)
    @CrossOrigin
    @RequestMapping(value="/assignStartToWorkflow/{workflowID}/{entityID}", method = RequestMethod.POST)
    public void assignStartToWorkflow (@PathVariable(value="workflowID") String workflowID,
                                  @PathVariable(value="entityID") String entityID) throws Exception {

        Workflow workflow = workflowRepository.findByWorkflowID(workflowID);

        if (workflow == null) {
            throw new EntityDoesNotExist("The workflow with ID = " + workflowID + " does not exist.");
        }

        ScheduledEntity entity = scheduledEntityRepository.findByEntityID(entityID);

        if (entity == null) {
            throw new EntityDoesNotExist("The entity with ID = " + entityID + " does not exist.");
        }

        workflow.addStart(entity);
        workflowRepository.save(workflow);
    }

    @CacheEvict(value = "workflow", allEntries = true)
    @CrossOrigin
    @RequestMapping(value="/addSpecialScheduling/{sourceID}/{destinationID}", method = RequestMethod.POST)
    public void addSpecialScheduling (@PathVariable(value="sourceID") String sourceID,
                                       @PathVariable(value="destinationID") String destinationID) throws Exception {

        ScheduledEntity source = scheduledEntityRepository.findByEntityID(sourceID);
        if (source == null) {
            throw new EntityDoesNotExist("Entity with ID: " + sourceID + " does not exist.");
        }

        ScheduledEntity destination = scheduledEntityRepository.findByEntityID(destinationID);
        if (destination == null) {
            throw new EntityDoesNotExist("Entity with ID: " + destinationID + " does not exist.");
        }

        source.addSpecialSchedulingRelation(destination);
        scheduledEntityRepository.save(source);

    }

    @CrossOrigin
    @RequestMapping(value="/getWorkflows/{projectID}", method = RequestMethod.GET)
    @Cacheable("workflow")
    public List<Workflow> getWorkflows (@PathVariable(value="projectID") String projectID) throws Exception {

        return workflowRepository.findWorkflowsByProject(projectID);
    }

    @CrossOrigin
    @RequestMapping(value="/getWorkflowTemplates", method = RequestMethod.GET)
    @Cacheable("workflow")
    public List<Workflow> getWorkflowTemplates () throws Exception {

        return workflowRepository.findWorkflowTemplates();
    }

    @CrossOrigin
    @RequestMapping(value="/getWorkflow/{workflowID}", method = RequestMethod.GET)
    @Cacheable("workflow")
    public List<ScheduledEntity> getWorkflowObjects (@PathVariable(value="workflowID") String workflowID)
            throws Exception {

        return workflowRepository.findWorkflow(workflowID);
    }

}
