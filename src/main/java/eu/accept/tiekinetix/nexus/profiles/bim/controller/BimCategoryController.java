package eu.accept.tiekinetix.nexus.profiles.bim.controller;

import java.io.IOException;
import java.util.Optional;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.accept.tiekinetix.nexus.profiles.bim.model.BimCategory;
import eu.accept.tiekinetix.nexus.profiles.bim.model.BimEntity;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by akuschki on 12/06/2017.
 */

@CrossOrigin
@RestController
@RequestMapping(value = "/projects/{projectID}/bimCategories/{bimId}",produces = {"application/json"}, consumes = {"application/json", "*/*"})
public class BimCategoryController extends AbstractBimController {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Transactional
    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId) throws IOException {
        categoryRepository.delete(findOne(projectID, bimId));
    }

    @RequestMapping(method = RequestMethod.GET)
    public BimCategory get(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId) throws IOException {
        return findOne(projectID, bimId);
    }

    @Transactional
    @RequestMapping(method = RequestMethod.PUT)
//    public BimEntity put(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId, @Valid @RequestBody BimCategory bimCategory, @PathParam("scheduledEntity") Optional<String> scheduledEntity) throws IOException {
    public BimEntity put(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId, @Valid @RequestBody String payload) throws IOException {
        BimCategory bimCategory = mapper.readValue(payload, BimCategory.class);
        validatePayload(bimId, bimCategory);
        return processAndSave(projectID, bimId, bimCategory, null);
    }

    private BimCategory findOne(String projectID, Long bimId) {
        return categoryRepository.findByProjectProjectIDAndBimId(projectID, bimId)
                .stream()
                .findFirst()
                .orElseThrow(ResourceNotFoundException::new);
    }
}

