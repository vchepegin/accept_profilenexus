package eu.accept.tiekinetix.nexus.profiles.web.restful;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.accept.tiekinetix.nexus.profiles.core.exceptions.EntityDoesNotExist;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Checklist;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.QualityGate;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.ChecklistRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.QualityGateRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.ScheduledEntityRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * Created by vchepegin on 23/02/2017.
 */
@CrossOrigin
@RestController
public class ChecklistController {

    @Autowired
    private ChecklistRepository checklistRepository;
    @Autowired
    private QualityGateRepository qualityGateRepository;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private ScheduledEntityRepository scheduledEntityRepository;

    private final ObjectMapper mapper = new ObjectMapper();

    @CrossOrigin
    @RequestMapping(value="/deleteChecklist/{checklistNameID}", method = RequestMethod.DELETE)
    public void deleteComponent (@PathVariable(value="checklistID") String checklistID)
            throws Exception {
        Checklist checklist = checklistRepository.findByChecklistID(checklistID);
        checklistRepository.delete(checklist);
    }

    @CrossOrigin
    @RequestMapping(value="/getChecklistsByTask/{taskID}", method = RequestMethod.GET)
    public Set<String> getChecklistsByTask (
            @PathVariable(value="taskID") String taskID) throws Exception {

//        ScheduledEntity entity = scheduledEntityRepository.findByEntityID(taskID);
//        if (entity == null) {
//            throw new EntityDoesNotExist("Entity with ID = " + taskID + " does not exist.");
//        }
        //Empty set wil be returned if entity does not exist - no exception is thrown but faster processing
        return checklistRepository.getChecklistsByTask(taskID);
    }

//    @CrossOrigin
//    @RequestMapping(value="/addChecklistToTask/{taskID}", method= RequestMethod.POST)
//    public String addChecklistToTask (@RequestBody String payload,
//                                      @PathVariable(value="taskID") String taskID) throws Exception {
//
//        ScheduledEntity entity = scheduledEntityRepository.findByEntityID(taskID);
//        if (entity == null) {
//            throw new EntityDoesNotExist("Task with ID " + taskID + " does not exist.");
//        }
//
//        Checklist checklist = null;
//        checklist = mapper.readValue(payload, Checklist.class);
//
//        String guid = checklist.getChecklistID();
//        if (guid == null ||  guid.isEmpty()) {
//            guid = java.util.UUID.randomUUID().toString();
//            checklist.setChecklistID(guid);
//        }
//        entity.addCheckList(checklist);
//        scheduledEntityRepository.save(entity);
//
//        return guid;
//    }

    @CrossOrigin
    @RequestMapping(value="/addChecklistToTask/{taskID}/{checklistID}", method= RequestMethod.PUT)
    public void addChecklistToTask2 (@PathVariable(value="taskID") String taskID,
                                      @PathVariable(value="checklistID") String checklistID) throws Exception {

        ScheduledEntity entity = scheduledEntityRepository.findByEntityID(taskID);
        if (entity == null) {
            throw new EntityDoesNotExist("Task with ID " + taskID + " does not exist.");
        }

        Checklist checklist = new Checklist();
        checklist.setChecklistID(checklistID);

        entity.addCheckList(checklist);
        scheduledEntityRepository.save(entity);

    }

//    @CrossOrigin
//    @RequestMapping(value="/getChecklistTemplatesOfTask/{taskID}", method = RequestMethod.GET)
//    public Set<String> getChecklistTemplatesOfTask (
//            @PathVariable(value="taskID") String taskID) throws Exception {
//
////        ScheduledEntity entity = scheduledEntityRepository.findByEntityID(taskID);
////        if (entity == null) {
////            throw new EntityDoesNotExist("Entity with ID = " + taskID + " does not exist.");
////        }
//        //Empty set wil be returned if entity does not exist - no exception is thrown but faster processing
//        return checklistRepository.getChecklistTemplatesByTask(taskID);
//    }

//    @CrossOrigin
//    @RequestMapping(value="/addChecklistTemplate/{qgID}", method= RequestMethod.POST)
//    public String addChecklistTemplateToTask (@RequestBody String payload,
//                                      @PathVariable(value="qgID") String qgID) throws Exception {
//
//        ScheduledEntity entity = scheduledEntityRepository.findByEntityID(qgID);
//        if (entity == null && ! (entity instanceof QualityGate)) {
//            throw new EntityDoesNotExist("Quality Gate with ID " + qgID + " does not exist.");
//        }
//
//        QualityGate qg = (QualityGate) entity;
//        Checklist checklist = null;
//        checklist = mapper.readValue(payload, Checklist.class);
//
//        String guid = checklist.getChecklistID();
//        if (guid == null ||  guid.isEmpty()) {
//            guid = java.util.UUID.randomUUID().toString();
//            checklist.setChecklistID(guid);
//        }
//
//        checklistRepository.save(checklist);
//
//        qg.setCheklistTemplateID(checklist.getChecklistID());
//        scheduledEntityRepository.save(entity);
//
//        return guid;
//    }

}
