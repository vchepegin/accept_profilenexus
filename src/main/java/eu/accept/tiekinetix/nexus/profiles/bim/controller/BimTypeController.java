package eu.accept.tiekinetix.nexus.profiles.bim.controller;

import java.io.IOException;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.accept.tiekinetix.nexus.profiles.bim.model.BimParameter;
import eu.accept.tiekinetix.nexus.profiles.bim.model.BimType;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by akuschki on 12/06/2017.
 */

@CrossOrigin
@RestController
@RequestMapping(value = "/projects/{projectID}/bimTypes/{bimId}", produces = {"application/json"}, consumes = {"application/json", "*/*"})
public class BimTypeController extends AbstractBimController {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Transactional
    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId) throws IOException {
        typeRepository.delete(findOne(projectID, bimId));
    }

    @RequestMapping(method = RequestMethod.GET)
    public BimType get(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId) throws IOException {
        return findOne(projectID, bimId);
    }

    @Transactional
    @RequestMapping(method = RequestMethod.PUT)
//    public void put(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId, @Valid @RequestBody BimType type, @PathParam("setScheduledEntity") Optional<String> scheduledEntity) throws IOException {
    public void put(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId, @Valid @RequestBody String payload) throws IOException {
        BimType type = mapper.readValue(payload, BimType.class);
        validatePayload(bimId, type);
        deduplicateEmbeddedParameters(type, projectID);
        setCategoryLink(type, projectID);
        setFamilyLink(type, projectID);
        setMaterialsLink(type, projectID);
        processAndSave(projectID, bimId, type, null);
        buildRelationsToTasks(type, projectID);
    }

    private BimType findOne(String projectID, Long bimId) {
        return typeRepository.findByProjectProjectIDAndBimId(projectID, bimId)
                .stream()
                .findFirst()
                .orElseThrow(ResourceNotFoundException::new);
    }

    private void setFamilyLink(BimType type, String projectID) {
        type.setFamilies(type.getParamsByName("Family Name")
                .stream()
                .map(BimParameter::getValue)
                .map(familyName -> familyRepository.findByProjectProjectIDAndName(projectID, familyName)
                        .stream()
                        .findFirst()
                        .orElse(null))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet()));
    }

    private void setMaterialsLink(BimType type, String projectID) {
        type.getMaterialsIds()
                .stream()
                .forEach(p -> materialRepository.findByProjectProjectIDAndBimId(projectID, p.getBimId())
                        .stream()
                        .findFirst()
                        .ifPresent(material -> type.getMaterials()
                                .add(material)));
    }
}
