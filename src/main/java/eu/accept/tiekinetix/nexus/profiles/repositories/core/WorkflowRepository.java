package eu.accept.tiekinetix.nexus.profiles.repositories.core;

import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Workflow;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by vchepegin on 20/04/2017.
 */
public interface WorkflowRepository extends GraphRepository<Workflow> {

    Workflow findByName (String name);

    Workflow findByWorkflowID (String workflowID);

    @Query("match (x:Project {projectID: {0}})-[r:hasWorkflow]-(a:Workflow) return a;")
    List<Workflow> findWorkflowsByProject (@Param("0")String projectID);

    @Query("match (a:Workflow) where NOT (a) <- [:hasWorkflow] - () return a")
    List<Workflow> findWorkflowTemplates ();

    @Query("match (b:Workflow {workflowID: {0}})-[r*..21]->(z) " +
            "WHERE z:QualityGate OR z:Activity " +
            "WITH z AS a " +
            "OPTIONAL MATCH p = (a)-[r*0..1]-(d:Quantity) " +
            "return  a, nodes(p), rels(p)")
    List<ScheduledEntity> findWorkflow (@Param("0")String workflowID);

}
