package eu.accept.tiekinetix.nexus.profiles.repositories.core;

import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Checklist;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

import java.util.Set;

/**
 * Created by vchepegin on 23/02/2017.
 */
public interface ChecklistRepository extends GraphRepository<Checklist> {

    Checklist findByChecklistID (String checklistID);

    @Query("match (b:ScheduledEntity {entityID: {0}} )-[r:hasCheckList*]->(c:Checklist) return c.checklistID")
    Set<String> getChecklistsByTask (@Param("0")String entityID);

    @Query("match (b:ScheduledEntity {entityID: {0}} )-[r:hasCheckListTemplate*]->(c:Checklist) return c.checklistID")
    Set<String> getChecklistTemplatesByTask (@Param("0")String entityID);
}
