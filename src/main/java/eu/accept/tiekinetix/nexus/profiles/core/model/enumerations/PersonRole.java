package eu.accept.tiekinetix.nexus.profiles.core.model.enumerations;

/**
 * Created by vchepegin on 27/01/2017.
 */
public enum PersonRole {
    WORKER ("Worker"),
    FOREMAN ("Foreman"),
    SITE_MANAGER ("Site Manager"),
    PROJECT_MANAGER ("Project Manager"),
    GENERAL_CONTRACTOR ("General Contractor"),
    CLIENT ("Client"),
    ENGINEER ("Engineers"),
    QUALITY_MANAGER ("Quality Manager"),
    ARCHITECT ("Architect"),
    EXTERNAL_EXPERT ("External Expert"),
    DIRECTOR ("Director"),
    MANUFACTURER("Manufacturer"),
    CONTRACTOR("Contractor");

    private final String stakeholderCode;

    PersonRole(String code) {
        this.stakeholderCode = code;
    }

    private String getStakeholderCode() {
        return stakeholderCode;
    }

    @Override
    public String toString () {
        return getStakeholderCode();
    }
}
