package eu.accept.tiekinetix.nexus.profiles.web.restful;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import eu.accept.tiekinetix.nexus.messaging.ChangedEntitiesLoggingService;
import eu.accept.tiekinetix.nexus.profiles.core.exceptions.EntityAlreadyExistsException;
import eu.accept.tiekinetix.nexus.profiles.core.exceptions.EntityDoesNotExist;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.CompletionStatus;
import eu.accept.tiekinetix.nexus.profiles.core.model.user.Crew;
import eu.accept.tiekinetix.nexus.profiles.core.model.user.Equipment;
import eu.accept.tiekinetix.nexus.profiles.core.model.user.Person;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.*;
import eu.accept.tiekinetix.nexus.profiles.reporting.processing.KPIProcessorService;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.*;
import eu.accept.tiekinetix.nexus.profiles.util.Asserts;
import eu.accept.tiekinetix.nexus.profiles.util.CoreUtilities;
import eu.accept.tiekinetix.nexus.profiles.util.TikkiQueue;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.neo4j.ogm.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.expression.ParseException;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.lang.reflect.Field;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.Intention.SCHEDULED_WORKING_DAYS_CALCULATION;
import static eu.accept.tiekinetix.nexus.profiles.util.DateUtils.dateAfterWorkingDays;

/**
 * Created by vchepegin on 08/02/2017.
 */
@CrossOrigin
@RestController
public class TaskController {

    @Autowired //don't forget the setter
    private TaskRepository taskRepository;
    @Autowired
    private CrewRepository crewRepository;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private ActivityRepository activityRepository;
    @Autowired
    private QuantityRepository quantityRepository;
    @Autowired
    private ScheduledEntityRepository scheduledEntityRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private CoreUtilities coreUtilities;
    @Autowired
    private KPIProcessorService kpiProcessor;
    @Autowired
    private ChangedEntitiesLoggingService loggingService;
    @Autowired
    private StringRedisTemplate template;


    @Autowired
    Session session;

    private static final ObjectMapper mapper = new ObjectMapper();

    private static final CoreUtilities coreUtils = new CoreUtilities();

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/createTaskWithParent/{parentID}/{taskID}", method = RequestMethod.POST)
    public String createTaskWithParent(@RequestBody String payload,
                                       @PathVariable(value = "parentID") String parentID,
                                       @PathVariable(value = "taskID") String taskID) throws Exception {

        Task parent = taskRepository.findByEntityID(parentID);
        Asserts.notNull(parent, "Task with ID = " + parentID);

        final Pattern pattern = Pattern.compile("@(?<id>.*)");
        final Matcher matcher = pattern.matcher(parentID);
        matcher.find();
        String entityID = taskID + '@' + matcher.group("id");

        if (scheduledEntityRepository.isEntityPresent(entityID)) {
            throw new EntityAlreadyExistsException("The entity with ID " + entityID + " already exist.");
        }
        //RDI Issue#87
        Task child = Task.getInstanceFromTemplate(mapper.readValue(payload, Task.class),
                entityID);

        parent.addChild(child);

        taskRepository.save(parent);

        return entityID;
    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/createTaskByID/{taskID}", method = RequestMethod.POST)
    public ScheduledEntity createTaskByID(@RequestBody String payload,
                                          @PathVariable(value = "taskID") String taskID) throws Exception {
        return createTaskByIDCommon(payload, taskID, scheduledEntityRepository);
    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/createTaskByID/{taskID}/{projectID}", method = RequestMethod.POST)
    public String createTaskByProject(@RequestBody String payload,
                                      @PathVariable(value = "taskID") String taskID,
                                      @PathVariable(value = "projectID") String projectID) throws Exception {

        Project project = projectRepository.findByProjectID(projectID);
        Asserts.notNull(project, "Project with ID = " + projectID);

        //RDI Issue#87
        final String guid = taskID + "@" + projectID;
        Task newTask = Task.getInstanceFromTemplate(mapper.readValue(payload, Task.class),
                guid);

        if (scheduledEntityRepository.isEntityPresent(taskID)) {
            throw new EntityAlreadyExistsException("The entity with ID " + taskID + " already exist.");
        }

        project.addTask(newTask);
        projectRepository.save(project);
        //taskRepository.save(newTask);

        return guid;
    }

//    //RDI-3044 - remocved as agreed since not really necessary and even harmful
//    @CacheEvict(value = "scheduledentity", allEntries = true)
//    @CrossOrigin
//    @RequestMapping(value="/createTaskWithParentAndProjectByID/{taskID}/{parentID}/{projectID}", method = RequestMethod.POST)
//    @Deprecated //ruins the hierarchy of tasks and thus was not provided
//    public void createTaskWithParentAndProjectByID (@RequestBody String payload,
//                                     @PathVariable(value="taskID") String taskID,
//                                     @PathVariable(value="parentID") String parentID,
//                                     @PathVariable(value="projectID") String projectID) throws Exception {
//
//        Task parent = taskRepository.findByEntityID(parentID);
//        Asserts.notNull(parent, "Task with ID = " + parentID);
//
//        Project project = projectRepository.findByProjectID(projectID);
//        Asserts.notNull(project, "Project with ID = " + projectID);
//
//        Task child = Task.getInstanceFromTemplate(mapper.readValue(payload, Task.class),
//                taskID);
//
//        //VERY BAD MOVE TO CONNECT A CHILD DIRECTLY TO the Project
//        project.addTask(child);
//        parent.addChild(child);
//
//        projectRepository.save(project);
//    }
//
//    //RDI-3044
//    @CacheEvict(value = "scheduledentity", allEntries = true)
//    @CrossOrigin
//    @RequestMapping(value="/createTaskWithParentAndProject/{parentID}/{projectID}", method = RequestMethod.POST)
//    @Deprecated //ruins the hierarchy of tasks and thus was not provided
//    public String createTaskWithParentAndProject (@RequestBody String payload,
//                                                @PathVariable(value="parentID") String parentID,
//                                                @PathVariable(value="projectID") String projectID) throws Exception {
//
//        final String guid = projectID + '@' + java.util.UUID.randomUUID().toString();
//        createTaskWithParentAndProjectByID(payload, guid, parentID, projectID);
//        return guid;
//    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/createTask/{parentTaskID}/{taskType}", method = RequestMethod.POST)
    public String createTaskByTypeAndParent(@RequestBody String payload,
                                            @PathVariable(value = "parentTaskID") String parentTaskID,
                                            @PathVariable(value = "taskType") String taskType)
            throws Exception {

        ScheduledEntity parent = scheduledEntityRepository.findByEntityID(parentTaskID);

        if (parent == null) {
            throw new EntityDoesNotExist("The parent entity with ID = "
                    + parentTaskID + "does not exists.");
        }


        int index = payload.indexOf("entityID");
        StringBuilder params = new StringBuilder(payload);
        if (index < 0) {
            params.insert(1, "\"entityID\":" + "\"" + java.util.UUID.randomUUID().toString() + "\",");
        }

        ScheduledEntity newEntity = coreUtilities.createScheduledEntity(params.toString(), taskType);

        if (index >= 0 && scheduledEntityRepository.isEntityPresent(newEntity.getEntityID())) {
            throw new EntityAlreadyExistsException("The task with ID = " + newEntity.getEntityID() + "already exists.");
        }

        newEntity.setTaskType(taskType);
        newEntity.wrapUp();
        parent.addChild(newEntity);
        loggingService.logChangedEntity(newEntity.getEntityID());
        scheduledEntityRepository.save(parent);

        if (newEntity instanceof Activity) {
            kpiProcessor.propagateUpwards(SCHEDULED_WORKING_DAYS_CALCULATION);
        }

        return newEntity.getEntityID();
    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/createTask/{taskType}", method = RequestMethod.POST)
    public String createTaskByType(@RequestBody String payload,
                                   @PathVariable(value = "taskType") String taskType) throws Exception {


        int index = payload.indexOf("entityID");
        StringBuilder params = new StringBuilder(payload);
        if (index < 0) {
            params.insert(1, "\"entityID\":" + "\"" + java.util.UUID.randomUUID().toString() + "\",");
        }


        ScheduledEntity newEntity = coreUtilities.createScheduledEntity(params.toString(), taskType);

        ScheduledEntity existingEntity = null;
        if (index >= 0 && scheduledEntityRepository.isEntityPresent(newEntity.getEntityID())) {
            throw new EntityAlreadyExistsException("The task with ID = " + newEntity.getEntityID() + "already exists.");
        }

        newEntity.setTaskType(taskType);
        newEntity.wrapUp();
        scheduledEntityRepository.save(newEntity);

        return newEntity.getEntityID();
    }

    @CrossOrigin
    @RequestMapping(value = "/findTask/{limit}", method = RequestMethod.POST)
    @Cacheable("scheduledentity")
    public List<ScheduledEntity> findTask(@RequestBody Map<String, Object> payload,
                                          @PathVariable(value = "limit") String limit) throws Exception {

        final String template = "match (a:ScheduledEntity) %1 return a limit " + limit;

        String cypher = coreUtilities.prepareCypher(template, payload);

        return coreUtilities.getObjectsByCriteria(session, cypher, payload, ScheduledEntity.class);

    }

    @CrossOrigin
    @RequestMapping(value = "/findTaskByProject/{projectID}", method = RequestMethod.POST)
    @Cacheable("scheduledentity")
    public List<ScheduledEntity> findTaskByProject(@RequestBody Map<String, Object> payload,
                                                   @PathVariable(value = "projectID") String projectID) throws Exception {

        String template = "match (b:Project {projectID : \"%2\"})-[r*]->(a:ScheduledEntity) %1 return " +
                "distinct a";

        template = template.replaceAll("%2", projectID);

        String cypher = coreUtilities.prepareCypher(template.toString(), payload);

        return coreUtilities.getObjectsByCriteria(session, cypher, payload, ScheduledEntity.class);

    }

    @CrossOrigin
    @RequestMapping(value = "/findTaskByOwner/{ownerID:.+}", method = RequestMethod.POST)
    public List<ScheduledEntity> findTaskByOwner(@RequestBody Map<String, Object> payload,
                                                 @PathVariable(value = "ownerID") String ownerID)
            throws Exception {

        if (!coreUtilities.datesAreInOrder(payload)) {
            throw new IllegalArgumentException("A starting date cannot be later than an end date.");
        }

       /*
        *  Example:
        *  MATCH (a:ScheduledEntity)-[r:hasOwner]-(b:Person )
        *  WHERE a.startDate >= "2017-03-06" AND a.endDate <= "2017-05-10"
        *  RETURN a;
        */
        final String template = "match (a:ScheduledEntity)-[r:hasOwner]->" +
                "(b:Person {personID : {id} } ) %1 return a";


        String cypher = coreUtilities.prepareCypher(template, payload);

        payload.put("id", ownerID);

        return coreUtilities.getObjectsByCriteria(session, cypher, payload, ScheduledEntity.class);
    }


    @CrossOrigin
    @RequestMapping(value = "/findTaskByCrew/{crewID}", method = RequestMethod.POST)
    public List<Activity> findTaskByCrew(@RequestBody Map<String, Object> payload,
                                         @PathVariable(value = "crewID") String crewID)
            throws Exception {

        if (!coreUtilities.datesAreInOrder(payload)) {
            throw new IllegalArgumentException("A starting date cannot be later than an end date.");
        }

        final String template = "match (a:Activity)-[r:hasCrew]-" +
                "(b:Crew {crewID : {id} } ) %1 return a";

        String cypher = coreUtilities.prepareCypher(template, payload);

        payload.put("id", crewID);

        return coreUtilities.getObjectsByCriteria(session, cypher, payload, Activity.class);
    }

    @CrossOrigin
    @RequestMapping(value = "/findTaskByCrewMember/{personID:.+}", method = RequestMethod.POST)
    public List<Activity> findTaskByCrewMember(@RequestBody Map<String, Object> payload,
                                               @PathVariable(value = "personID") String personID)
            throws Exception {

        if (!coreUtilities.datesAreInOrder(payload)) {
            throw new IllegalArgumentException("A starting date cannot be later than an end date.");
        }

        final String template = "match (a:Activity)-[r1:hasCrew]-(b:Crew)-[r2:hasWorker]" +
                "-(p:Person {personID : {id} }) %1 return a;";

        String cypher = coreUtilities.prepareCypher(template, payload);

        payload.put("id", personID);


        return coreUtilities.getObjectsByCriteria(session, cypher, payload, Activity.class);
    }

    @CrossOrigin
    @RequestMapping(value = "/getTaskByID/{taskID}", method = RequestMethod.GET)
    @Cacheable("scheduledentity")
    public ScheduledEntity getTaskByID(@PathVariable(value = "taskID") String taskID) throws Exception {
        ScheduledEntity entity2 = scheduledEntityRepository.findByEntityID2(taskID);
        if (entity2 == null) {
            throw new EntityDoesNotExist("Task with ID = " + taskID + " does not exist.");
        }
        return entity2;
    }

    @CrossOrigin
    @RequestMapping(value = "/getParentTaskID/{entityID}", method = RequestMethod.GET)
    public String getParentTaskID(@PathVariable(value = "entityID") String entityID)
            throws Exception {

        return scheduledEntityRepository.getParentTask(entityID).getEntityID();
    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/getSuccessorTasks/{entityID}", method = RequestMethod.GET)
    public Set<ScheduledEntity> getSuccessorTasks(
            @PathVariable(value = "entityID") String entityID) throws Exception {

        return scheduledEntityRepository.findSuccessors(entityID);
    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/getSubTasks/{entityID}", method = RequestMethod.GET)
    public Set<ScheduledEntity> getSubTasks(
            @PathVariable(value = "entityID") String entityID) throws Exception {
        Set<ScheduledEntity> children = scheduledEntityRepository.findChildren(entityID);

        return children;
    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/getSubTasksWithCompletionStatus/{entityID}/{completionStatus}", method = RequestMethod.GET)
    public Set<ScheduledEntity> getSubTasksWithCompletionStatus(
            @PathVariable(value = "entityID") String entityID, @PathVariable(value = "completionStatus") String completionStatus)
            throws Exception {
        Set<ScheduledEntity> children = scheduledEntityRepository.findChildrenWithCompletionStatus(entityID, completionStatus);

        return children;
    }

    /*
    @CrossOrigin
    @RequestMapping(value="/getComponentsOfTask/{taskID}", method = RequestMethod.GET)
    public List<Component> getComponentsOfTask (
            @PathVariable(value="taskID") String taskID) throws Exception {

        StringBuilder query = new StringBuilder("match (a:Task {entityID : {taskID} })" +
                "-[r]->(o:Component) return o") ;

        Map <String, String> params = new LinkedHashMap<String, String>();
        params.put("taskID", taskID);
        Iterable<Map<String, Object>> iterableResult = session.query(query.toString(),
                params).queryResults();

        ArrayList<Component> components = new ArrayList<>();

        for (Map<String, Object> map : iterableResult) {
            Component component = mapper.convertValue(map.get("o"), Component.class);
            components.add(component);
        }

        return components;
    }
*/
    @CrossOrigin
    @RequestMapping(value = "/getDocumentsOfTask/{taskID}/{option}", method = RequestMethod.GET)
//    public ResponseEntity<String> getDocumentsOfTask (
    public String getDocumentsOfTask(
            @PathVariable(value = "taskID") String taskID,
            @PathVariable(value = "option") String option) throws Exception {

        ScheduledEntity task = scheduledEntityRepository.findByEntityID(taskID);
        if (task == null) {
            throw new EntityDoesNotExist("Task with ID = " + taskID + " does not exist.");
        }

        final String url = "http://demo-viki.answare-tech.com:3001/api/EDAs/filter";

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        StringBuilder requestJson = new StringBuilder("{ \"contains\" : { \"attachedTo.project\": \"" +
                projectRepository.findProjectIdByChild(taskID) + "\", %1}}");
        String optional = " ";
        if (option.equals("1")) {
            optional = "\"attachedTo.location\": \"" + task.getLbsCode() + "\"";
        }
        if (option.equals("2")) {
            optional = "\"attachedTo.task\": \"" + task.getWbsCode() + "\"";
        }
        if (option.equals("3")) {
            optional = "\"attachedTo.task\": \"" + task.getWbsCode() + "\"," +
                    "\"attachedTo.location\": \"" + task.getLbsCode() + "\"";
        }
        int index = requestJson.indexOf("%1");
        requestJson.replace(index, index + 2, optional);
/*
        HttpEntity<String> httpEntity = new HttpEntity<String>(requestJson.toString(), headers);
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.POST,
                httpEntity, String.class);
*/
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);

        String jsonPayload = requestJson.toString();
        StringEntity entity = new StringEntity(jsonPayload);
        httpPost.setEntity(entity);
        httpPost.setHeader("Content-type", "application/json");

        CloseableHttpResponse response = client.execute(httpPost);
        String responseString = new BasicResponseHandler().handleResponse(response);
        return responseString;

    }

    @CrossOrigin
    @RequestMapping(value = "/getEquipmentsOfTask/{taskID}", method = RequestMethod.GET)
    public Set<Equipment> getEquipmentsOfTask(
            @PathVariable(value = "taskID") String taskID) throws Exception {

        Task task = taskRepository.findByEntityID(taskID);
        if (task == null) {
            throw new EntityDoesNotExist("Task with ID = " + taskID + " does not exist.");
        }

        return task.getEquipment();

    }

    @CrossOrigin
    @RequestMapping(value = "/getPersonnel/{activityID}", method = RequestMethod.GET)
    public Set<Person> getPersonnel(@PathVariable(value = "activityID") String activityID) throws Exception {

        return activityRepository.findCrewMembers(activityID);

    }

    @CrossOrigin
    @RequestMapping(value = "/getOwner/{entityID}", method = RequestMethod.GET)
    public Person getOwner(@PathVariable(value = "entityID") String entityID) throws Exception {

        ScheduledEntity entity = scheduledEntityRepository.findByEntityID2(entityID);
        if (entity == null) {
            throw new EntityDoesNotExist("Entity with ID " + entityID + " does not exist");
        }
        return entity.getOwner();
    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/addEquipmentToTask/{taskID}", method = RequestMethod.POST)
    public String addEquipmentToTask(@RequestBody String payload,
                                     @PathVariable(value = "taskID") String taskID) throws Exception {

        Task task = taskRepository.findByEntityID(taskID);
        if (task == null) {
            throw new EntityDoesNotExist("Task with ID " + taskID + " does not exist.");
        }

        Equipment equipment = null;
        equipment = mapper.readValue(payload, Equipment.class);

        String guid = equipment.getEquipmentID();
        if (guid == null || guid.isEmpty()) {
            guid = java.util.UUID.randomUUID().toString();
            equipment.setEquipmentID(guid);
        }
        task.addEquipment(equipment);
        taskRepository.save(task);

        return guid;
    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/addSchedulingRelationship/{sID}/{eID}", method = RequestMethod.POST)
    public void addSchedulingRelationship(@PathVariable(value = "sID") String sID,
                                          @PathVariable(value = "eID") String eID) throws Exception {

        if (sID != null && !sID.isEmpty() && eID != null && !eID.isEmpty()) {
            ScheduledEntity start = scheduledEntityRepository.findByEntityID(sID);
            if (start == null) {
                throw new EntityDoesNotExist("Entity with ID = " + sID + " does not exist.");
            }
            ScheduledEntity end = scheduledEntityRepository.findByEntityID(eID);
            if (end == null) {
                throw new EntityDoesNotExist("Entity with ID = " + eID + " does not exist.");
            }

            start.addSpecialSchedulingRelation(end);

            scheduledEntityRepository.save(start);
        }
    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/addSuccessorTask/{predecessorID}/{successorID}", method = RequestMethod.POST)
    public void addSuccessorTask(@PathVariable(value = "predecessorID") String predecessorID,
                                 @PathVariable(value = "successorID") String successorID) throws Exception {

        if (predecessorID != null && !predecessorID.isEmpty() &&
                successorID != null && !successorID.isEmpty()) {
            ScheduledEntity predecessor = scheduledEntityRepository.findByEntityID(predecessorID);
            if (predecessor == null) {
                throw new EntityDoesNotExist("Entity with ID = " + predecessorID + " does not exist.");
            }
            ScheduledEntity successor = scheduledEntityRepository.findByEntityID(successorID);
            if (successor == null) {
                throw new EntityDoesNotExist("Entity with ID = " + successorID + " does not exist.");
            }

            predecessor.addSuccessor(successor);

            scheduledEntityRepository.save(predecessor);
        }
    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/addSubtask/{parentID}/{childID}", method = RequestMethod.POST)
    public void addSubtask(@PathVariable(value = "parentID") String parentID,
                           @PathVariable(value = "childID") String childID) throws Exception {

        if (parentID != null && !parentID.isEmpty() &&
                childID != null && !childID.isEmpty()) {
            ScheduledEntity parent = scheduledEntityRepository.findByEntityID2(parentID);
            if (parent == null) {
                throw new EntityDoesNotExist("Entity with ID = " + parentID + " does not exist.");
            }
            ScheduledEntity child = scheduledEntityRepository.findByEntityID(childID);
            if (child == null) {
                throw new EntityDoesNotExist("Entity with ID = " + childID + " does not exist.");
            }

            parent.addChild(child);

            scheduledEntityRepository.save(parent);

            kpiProcessor.propagateUpwards(SCHEDULED_WORKING_DAYS_CALCULATION);
        }
    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/setOwner/{entityID}/{ownerID:.+}", method = RequestMethod.POST)
    public void setOwner(@PathVariable(value = "entityID") String entityID,
                         @PathVariable(value = "ownerID") String ownerID) throws Exception {

        ScheduledEntity entity = scheduledEntityRepository.findByEntityID(entityID);
        if (entity == null) {
            throw new EntityDoesNotExist("An Entity with ID = " + entityID + " does not exist.");
        }

        Person owner = personRepository.findByPersonID(ownerID);
        if (owner == null) {
            throw new EntityDoesNotExist("A Person with ID = " + ownerID + " does not exist.");
        }
//        if (entity.getOwner() == owner) { return; }
        entity.addOwner(owner);
        Project theProject = projectRepository.findProjectByChild(entityID);
        if (entity.getTikkiQueueID() == null || entity.getTikkiQueueID().isEmpty()) {
            String queueID = TikkiQueue.createTikkiQueueForOwner(owner.getTikkiUserID(), entity.getCode(), theProject.getProjectName());
            entity.setTikkiQueueID(queueID);
        } else {
            TikkiQueue.updateTikkiQueue(owner.getTikkiUserID(), entity.getTikkiQueueID());
        }

        scheduledEntityRepository.save(entity);
    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/setCompletionStatus/{entityID}", method = RequestMethod.PUT)
    public void setCompletionStatus(@RequestBody Map<String, Object> payload,
                                    @PathVariable(value = "entityID") String entityID) {

        ScheduledEntity entity = scheduledEntityRepository.findByEntityID(entityID);
        Asserts.notNull(entity, "The entity with ID " + entityID);

        CompletionStatus newStatus = Enum.valueOf(CompletionStatus.class,
                payload.get("completionStatus").toString());
        CompletionStatus oldStatus = entity.getCompletionStatus();
        if (oldStatus != newStatus) {
            entity.setCompletionStatus(newStatus);
            scheduledEntityRepository.save(entity);
            kpiProcessor.propagateCompletionStatusDone(entity);
        }
    }

    /*
        public void propagateCompletionStatusDone(ScheduledEntity entity) {
            if (entity.getCompletionStatus() == CompletionStatus.DONE) {

                //notification
                final String message = entity.getTaskType() + " with name  " + entity.getName() +
                        " is now in status DONE. Code: WBS = " +
                        entity.getWbsCode() + ", LBS = " + entity.getLbsCode() + " with id = " + entity.getEntityID();
                coreUtilities.sendCommand("CompletionStatusChanged",
                        entity.getEntityID(), message);

                markTasksDone(entity.getEntityID());
            }

        }
        private void markTasksDone (String taskID) {
            //horizontal
            List<String> ids = taskRepository.find2bReady(taskID);
            for (String id : ids) {
                if (!Strings.isNullOrEmpty(id)) {
                    taskRepository.setStatus(id, CompletionStatus.IN_PROGRESS.toString());
                    taskRepository.setInitialSubTasks2Ready(id); //vertical down
                    //notification
                    ScheduledEntity se = scheduledEntityRepository.findByEntityID(id);
                    final String mesaage = se.getTaskType() + " with name  " + se.getName() +
                            " is now in status IN_PROGRESS. Code: WBS = " +
                            se.getWbsCode() + ", LBS = " + se.getLbsCode() + " with id = " + id;
                    coreUtilities.sendCommand("CompletionStatusChanged", id, mesaage);
                }
            }

            //vertical up
            String tID = taskRepository.ifAllChildrenComplete(taskID);
            if ( !Strings.isNullOrEmpty(tID) ) {
                taskRepository.setStatus(tID, CompletionStatus.DONE.toString());
                //notification
                ScheduledEntity se = scheduledEntityRepository.findByEntityID(tID);
                final String mesaage = se.getTaskType() + " with name  " + se.getName() +
                        " is now in status DONE. Code: WBS = " +
                        se.getWbsCode() + ", LBS = " + se.getLbsCode() + " with id = " + tID;
                coreUtilities.sendCommand("CompletionStatusChanged", tID, mesaage);

                markTasksDone(tID);
            }

        }
    */
    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/moveEndDateByWorkingDays/{entityID}/{nDays}", method = RequestMethod.PUT)
    public void moveEndDateByWorkingDays(@PathVariable(value = "entityID") String entityID,
                                         @PathVariable(value = "nDays") double nDays) throws Exception {

        ScheduledEntity entity = scheduledEntityRepository.findByEntityID(entityID);
        Asserts.notNull(entity, "The entity with ID " + entityID);

        //add and swap if different
        Date currentEndDate = entity.getEndDate();
        entity.setEndDate(dateAfterWorkingDays(currentEndDate, nDays));
        entity.setDuration(rescheduleDuration(entity));
        scheduledEntityRepository.save(entity);

        Set<ScheduledEntity> nodes = scheduledEntityRepository.findAllSuccessors(entity.getEntityID());
//        nodes.add(entity);
//        nDays++; //this is necessary for the right behaviour of dateAfterWorkingDays function
        for (ScheduledEntity node : nodes) {
            node.setStartDate(dateAfterWorkingDays(node.getStartDate(), nDays));
            node.setEndDate(dateAfterWorkingDays(node.getEndDate(), nDays));
            node.setDuration(rescheduleDuration(node));
        }

        scheduledEntityRepository.save(nodes);

    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/moveTimeFrameByWorkingDays/{entityID}/{nDays}", method = RequestMethod.PUT)
    public void moveTimeFrameByWorkingDays(@PathVariable(value = "entityID") String entityID,
                                           @PathVariable(value = "nDays") double nDays) throws Exception {

        ScheduledEntity entity = scheduledEntityRepository.findByEntityID(entityID);
        Asserts.notNull(entity, "The entity with ID " + entityID);

        //add and swap if different
        entity.setStartDate(dateAfterWorkingDays(entity.getStartDate(), nDays));
        entity.setEndDate(dateAfterWorkingDays(entity.getEndDate(), nDays));
        entity.setDuration(rescheduleDuration(entity));
        scheduledEntityRepository.save(entity);

        Set<ScheduledEntity> nodes = scheduledEntityRepository.findAllSuccessors(entity.getEntityID());
//        nodes.add(entity);
//        nDays++; //this is necessary for the right behaviour of dateAfterWorkingDays function
        for (ScheduledEntity node : nodes) {
            node.setStartDate(dateAfterWorkingDays(node.getStartDate(), nDays));
            node.setEndDate(dateAfterWorkingDays(node.getEndDate(), nDays));
            node.setDuration(rescheduleDuration(node));
        }

        scheduledEntityRepository.save(nodes);

    }

    private double rescheduleDuration(ScheduledEntity se) {
        LocalDate startDate = se.getStartDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate endDate = se.getEndDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        double duration = ChronoUnit.DAYS.between(startDate, endDate);
        return (duration + 1d);
    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/updateTask/{entityID}", method = RequestMethod.PUT)
    public void updateTask(@RequestBody Map<String, Object> payload,
                           @PathVariable(value = "entityID") String entityID) throws Exception {

        ScheduledEntity entity = scheduledEntityRepository.findByEntityID2(entityID);
        Asserts.notNull(entity, "The entity with ID " + entityID);

        ScheduledEntity delta = null;

        if (entity instanceof QualityGate) {
            delta = mapper.convertValue(payload, QualityGate.class);
        } else if (entity instanceof Activity) {
            delta = mapper.convertValue(payload, Activity.class);
        } else if (entity instanceof Task) {
            delta = mapper.convertValue(payload, Task.class);
        }

        boolean kpiIsEffected = false;
        Set<Quantity> removable = new HashSet<>();
        for (Map.Entry<String, Object> entry : payload.entrySet()) {

            try {
                Class<?> c = entity.getClass();
                Field t = null;
                //search in the whole hierarchy upwards
                while (t == null && c.getSuperclass() != null) {
                    try {
                        t = c.getDeclaredField(entry.getKey());
                    } catch (NoSuchFieldException nsEx) {
                        //TODO: it just swollows it for now
//                        nsEx.printStackTrace();
                    }
                    c = c.getSuperclass();
                }

                if (t != null) {
                    t.setAccessible(true);
                    if ("consumptionRate".equalsIgnoreCase(entry.getKey())) {
                        Quantity quantity = Quantity.fromObject(entry.getValue());

                        Activity activity = (Activity) entity;
                        Optional<Quantity> oldQuantity = Optional.ofNullable(activity.getConsumptionRate());
                        oldQuantity.ifPresent((q) -> removable.add(q));

                        activity.addConsumptionRate(quantity);

                        kpiIsEffected = true;
                    } else if ("quantity".equalsIgnoreCase(entry.getKey())) {
                        Quantity quantity = Quantity.fromObject(entry.getValue());

                        Task task = (Task) entity;
                        Optional<Quantity> oldQuantity = Optional.ofNullable(task.getQuantity());
                        oldQuantity.ifPresent((q) -> removable.add(q));

                        task.addQuantity(quantity);

                        kpiIsEffected = true;
                    } else {
                        if (t.getType().isEnum()) {
                            t.set(entity, Enum.valueOf((Class<Enum>) t.getType(),
                                    entry.getValue().toString()));
                        } else {
                            Object value = t.get(delta);
                            t.set(entity, value);
                            if ("workingHoursPerDay".equalsIgnoreCase(entry.getKey())) {
                                kpiIsEffected = true;
                            }
                            //                            t.set(task, entry.getValue());
                        }
                    }
                } else {
                    throw new IllegalArgumentException(entry.getKey() + " field does not exist in this class hierarchy");
                }

            } catch (IllegalAccessException ex) {
                //just print is but continue
                ex.printStackTrace();
            }
        }

        if (kpiIsEffected && entity instanceof Activity) {
            ((Activity) entity).calcScheduledWork();
            loggingService.logChangedEntity(entity.getEntityID());
//            template.convertAndSend("CHANGED_ACTIVITY",
//                    mapper.writeValueAsString(new Message("ModifiedActivity",
//                            entity.getEntityID())));

        }

        entity.wrapUp();
        scheduledEntityRepository.save(entity);

        Task parent = scheduledEntityRepository.getParentTask(entityID);
        if (kpiIsEffected && entity instanceof Activity && parent != null) {
            kpiProcessor.propagateUpwards(SCHEDULED_WORKING_DAYS_CALCULATION);
        }

        //remove replaced Quantity objects
        removable.parallelStream().forEach((q) -> quantityRepository.delete(q));
//        for (Quantity q : removable) {
//            quantityRepository.delete(q);
//        }
    }


    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/updateTask", method = RequestMethod.PUT)
    @Deprecated
    /**
     * This method is deprecated, please, use {@code /updateTask/{entityID}} instead.
     */
    public void updateTask(@RequestBody Map<String, Object> payload) throws Exception {

        Optional<Object> taskID = Optional.ofNullable(payload.get("entityID") != null ?
                payload.get("entityID") : payload.get("taskID"));

        if (!taskID.isPresent() || Strings.isNullOrEmpty(taskID.get().toString())) {
            throw new IllegalStateException("Missing an obligatory parameter entityID.");
        }

        payload.remove("entityID");
        payload.remove("taskID");

        updateTask(payload, taskID.get().toString());
    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/deleteTask/{entityID}", method = RequestMethod.DELETE)
    public void deleteTask(@PathVariable(value = "entityID") String entityID)
            throws Exception {

        if (!scheduledEntityRepository.isEntityPresent(entityID)) {
            throw new EntityDoesNotExist("The entity with ID " + entityID + " does not exist.");
        }

        scheduledEntityRepository.delete(entityID);

        loggingService.logChangedEntity(entityID);
        kpiProcessor.propagateUpwards(SCHEDULED_WORKING_DAYS_CALCULATION);

    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/deleteSubTasks/{entityID}", method = RequestMethod.DELETE)
    public void deleteSubTasks(@PathVariable(value = "entityID") String entityID)
            throws Exception {

        if (!scheduledEntityRepository.isEntityPresent(entityID)) {
            throw new EntityDoesNotExist("The entity with ID " + entityID + " does not exist.");
        }

        scheduledEntityRepository.deleteSubTasks(entityID);

        loggingService.logChangedEntity(entityID);
        kpiProcessor.propagateUpwards(SCHEDULED_WORKING_DAYS_CALCULATION);

    }

    @CrossOrigin
    @RequestMapping(value = "/getWithNeighbours/{entityID}", method = RequestMethod.GET)
    public Set<ScheduledEntity> getWithNeighbours(@PathVariable(value = "entityID") String entityID)
            throws Exception {

        return scheduledEntityRepository.findTaskWithSubAndSuccessors(entityID);

    }

    @CrossOrigin
    @RequestMapping(value = "/getProjectOfTask/{entityID}", method = RequestMethod.GET)
    public Project getProjectOfTask(@PathVariable(value = "entityID") String entityID)
            throws Exception {

        return projectRepository.findProjectByChild(entityID);
    }


    @CrossOrigin
    @RequestMapping(value = "/getToDoList/{projectID}", method = RequestMethod.POST)
    public List<ScheduledEntity> getToDoList(@RequestBody Map<String, Object> payload,
                                             @PathVariable(value = "projectID") String projectID)
            throws Exception {

        Project project = projectRepository.findByProjectID(projectID);
        if (project == null) {
            throw new EntityDoesNotExist("Project with ID " + projectID + " does not exist.");
        }
/*
        if (payload.get("startDate") ==  null) {
            throw new IllegalArgumentException("Start date has to be defined in the request body.");
        }
*/
        if (payload.get("tier") == null) {
            payload.put("tier", "TIER_3");
        }

        String template = "MATCH (n:Project {projectID: \"%2\" })-[r*]->(a:ScheduledEntity) %1 RETURN DISTINCT a";
        template = template.replaceAll("%2", projectID);
        payload.put("completionStatus", "IN_PROGRESS");

        String cypher = prepareCypher(template, payload);


        return coreUtilities.getObjectsByCriteria(session, cypher, payload, ScheduledEntity.class);

    }


    @CrossOrigin
    @RequestMapping(value = "/getTasksOfPerson/{projectID}/{personID:.+}", method = RequestMethod.POST)
    public List<ScheduledEntity> getTasksOfPerson(@RequestBody Map<String, Object> payload,
                                                  @PathVariable(value = "projectID") String projectID,
                                                  @PathVariable(value = "personID") String personID)
            throws Exception {

        Project project = projectRepository.findByProjectID(projectID);
        if (project == null) {
            throw new EntityDoesNotExist("Project with ID " + projectID + " does not exist.");
        }
        String template = "MATCH (n:Project {projectID: \"%3\" })-[r*]->(s:ScheduledEntity) " +
                "MATCH (s) - [o:hasOwner] -> (p:Person {personID: \"%2\" }) " +
                "OPTIONAL MATCH (s) -[c:hasChild*0..3] -> (a:ScheduledEntity) %1 RETURN DISTINCT a ORDER BY a.wbsCode ASC";
        template = template.replaceAll("%3", projectID);
        template = template.replaceAll("%2", personID);

        Boolean noSubTasks = false;
        List<ScheduledEntity> result = null;
        String cypher = prepareCypher(template, payload);
        if (payload.get("noSubTasks") != null) {
            payload.remove("noSubTasks");
            noSubTasks = true;
        }

        // first check for owner of parent-tasks
        if (noSubTasks == false) {
            result = coreUtilities.getObjectsByCriteria(session, cypher, payload, ScheduledEntity.class);
        }

        if (result == null || result.isEmpty()) {
            // check for owner of Activity or QualityGate
            template = "MATCH (n:Project {projectID: \"%3\" })-[r*]->(a:ScheduledEntity) " +
                    "MATCH (a) - [o:hasOwner] -> (p:Person {personID: \"%2\" }) %1 RETURN DISTINCT a  ORDER BY a.wbsCode ASC";

            template = template.replaceAll("%3", projectID);
            template = template.replaceAll("%2", personID);
            cypher = prepareCypher(template, payload);
            result = coreUtilities.getObjectsByCriteria(session, cypher, payload, ScheduledEntity.class);
        }
        if (result.isEmpty()) {
            // check for member of crew, which has been assigned to Activity
            template = "MATCH (n:Project {projectID: \"%3\" })-[r*]->(a:ScheduledEntity) " +
                    "match (a:ScheduledEntity)-[r1:hasCrew]-(b:Crew)-[r2:hasWorker]" +
                    "-(p:Person {personID : \"%2\" }) %1 return DISTINCT a ORDER BY a.wbsCode ASC";

            template = template.replaceAll("%3", projectID);
            template = template.replaceAll("%2", personID);
            cypher = prepareCypher(template, payload);
            result = coreUtilities.getObjectsByCriteria(session, cypher, payload, ScheduledEntity.class);

        }
        return result;
    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/resetTask/{taskID}", method = RequestMethod.PUT)
    public ScheduledEntity resetTask(@PathVariable(value = "taskID") String taskID) throws Exception {
        ScheduledEntity entity2 = scheduledEntityRepository.findByEntityID2(taskID);
        if (entity2 == null) {
            throw new EntityDoesNotExist("Task with ID = " + taskID + " does not exist.");
        } else {
            entity2.reset();
            scheduledEntityRepository.save(entity2);
        }
        return entity2;
    }

    public String prepareCypher(String template, Map<String, Object> payload) {
        StringBuilder criteria = new StringBuilder("");
        boolean isFirst = true;
        for (Map.Entry<String, Object> entry : payload.entrySet()) {

            String param = entry.getKey();

            if (!param.toUpperCase().equals("STARTDATE") && !param.toUpperCase().equals("ENDDATE")) {
                if (!isFirst) {
                    criteria.append(" AND ");
                }
                criteria.append("a." + param + " = {" + param + "}");
                isFirst = false;
            }

            switch (param.toUpperCase()) {
                case "STARTDATE":
                    if (!isFirst) {
                        criteria.append(" AND ");
                    }
                    String str = payload.get(param).toString();
                    payload.put(param, str + "T23:59:59");
                    criteria.append(" NOT a.startDate > {" + param + "}");
                    isFirst = false;
                    break;
                case "ENDDATE":
                    if (!isFirst) {
                        criteria.append(" AND ");
                    }
                    str = payload.get(param).toString();
                    payload.put(param, str + "T23:59:59");
                    criteria.append(" a.endDate <= {" + param + "}");
                    isFirst = false;
                    break;
                default:
            }
        }

        String query = "";
        if (!"".equals(criteria.toString())) {
            criteria.insert(0, " WHERE ");
            query = template.replace("%1", criteria.toString());
            //query.insert(71, criteria.toString());
        } else {
            query = template.replace("%1", "");
        }

        return query;
    }


    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/assignCrewToActivity/{crewID}/{activityID}", method = RequestMethod.POST)
    public void assignCrewToActivity(@PathVariable(value = "crewID") String crewID,
                                     @PathVariable(value = "activityID") String activityID) throws Exception {

        Activity activity = activityRepository.findByEntityID(activityID);
        if (activity == null) {
            throw new EntityDoesNotExist("Activity with ID " + activityID + " does not exist.");
        }

        Crew crew = crewRepository.findByCrewID(crewID);
        if (crew == null) {
            throw new EntityDoesNotExist("Crew with ID " + crewID + " does not exist.");
        }

        activity.addCrew(crew);
        activityRepository.save(activity);
        if (activity.getTikkiQueueID() == null || activity.getTikkiQueueID().isEmpty()) {
            Project theProject = projectRepository.findProjectByChild(activityID);
            String projectName = theProject.getProjectName();
            TikkiQueue.createTikkiQueueForCrew(activity.getCrews(), activity.getCode(), projectName);
        } else {
            TikkiQueue.updateTikkiQueueForCrew(activity.getCrews(), activity.getTikkiQueueID());
        }

        loggingService.logChangedEntity(activity.getEntityID());
        kpiProcessor.propagateUpwards(SCHEDULED_WORKING_DAYS_CALCULATION);


    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/removeCrewFromActivity/{crewID}/{activityID}", method = RequestMethod.POST)
    public void removeCrewFromActivity(@PathVariable(value = "crewID") String crewID,
                                       @PathVariable(value = "activityID") String activityID)
            throws Exception {


        Activity activity = activityRepository.findByEntityID(activityID);
        if (activity == null) {
            throw new EntityDoesNotExist("Activity with ID " + activityID + " does not exist.");
        }

        for (Crew crew : activity.getCrews()) {
            if (crew.getCrewID().equals(crewID)) {
                activity.removeCrew(crew);
                break;
            }
        }

        activityRepository.save(activity);
        if (activity.getTikkiQueueID() != null && !activity.getTikkiQueueID().isEmpty()) {
            TikkiQueue.updateTikkiQueueForCrew(activity.getCrews(), activity.getTikkiQueueID());
        }

        loggingService.logChangedEntity(activity.getEntityID());
        kpiProcessor.propagateUpwards(SCHEDULED_WORKING_DAYS_CALCULATION);
    }

    public static ScheduledEntity createTaskByIDCommon(String payload, String taskID,
                                                 ScheduledEntityRepository scheduledEntityRepository) throws Exception {
        if (scheduledEntityRepository.isEntityPresent(taskID)) {
            throw new EntityAlreadyExistsException("The entity with ID " + taskID + " already exist.");
        }

        StringBuilder params = new StringBuilder(payload);
        params.insert(1, "\"entityID\":" + "\"" + taskID + "\",");

        ScheduledEntity newSE = coreUtils.createScheduledEntity(params.toString(),
                coreUtils.getTypeFromPayload(payload));

        //newSE.setEntityID(taskID);

        newSE.wrapUp();
        scheduledEntityRepository.save(newSE);

        return newSE;
    }

    public static String createTaskByIdWithProjectID(String payload, String taskID, String projectID,
                                                     ScheduledEntityRepository scheduledEntityRepository, ProjectRepository projectRepository) {
        final ObjectMapper mapper = new ObjectMapper();

        Project project = projectRepository.findByProjectID(projectID);
        Asserts.notNull(project, "Project with ID = " + projectID);

        //RDI Issue#87
        final String guid = taskID + "@" + projectID;
        try {
            if (scheduledEntityRepository.isEntityPresent(guid)) {
                throw new EntityAlreadyExistsException("The entity with ID " + guid + " already exist.");
            }
            Task newTask = Task.getInstanceFromTemplate(mapper.readValue(payload, Task.class), guid);
            project.addTask(newTask);
            projectRepository.save(project);
            //taskRepository.save(newTask);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return guid;
    }

    public static String createTaskByIdWithParentID(String payload, String taskID, String parentID,
                                                    ScheduledEntityRepository scheduledEntityRepository, TaskRepository taskRepository) {
        Task parent = taskRepository.findByEntityID(parentID);
        Asserts.notNull(parent, "Task with ID = " + parentID);

        final Pattern pattern = Pattern.compile("@(?<id>.*)");
        final Matcher matcher = pattern.matcher(parentID);
        matcher.find();
        String entityID = taskID + '@' + matcher.group("id");

        if (scheduledEntityRepository.isEntityPresent(entityID)) {
//            throw new EntityAlreadyExistsException("The entity with ID " + entityID + " already exist.");
            return entityID;
        }
        //RDI Issue#87
        try {
            Task child = Task.getInstanceFromTemplate(mapper.readValue(payload, Task.class), entityID);
            parent.addChild(child);
            taskRepository.save(parent);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entityID;

    }

    public static void addSuccessorTask(String predecessorID, String successorID,
                                                    ScheduledEntityRepository scheduledEntityRepository)
    {
        if (predecessorID != null && !predecessorID.isEmpty() && successorID != null && !successorID.isEmpty()) {
            ScheduledEntity predecessor = scheduledEntityRepository.findByEntityID(predecessorID);
            if (predecessor == null) {
                throw new EntityDoesNotExist("Entity with ID = " + predecessorID + " does not exist.");
            }
            ScheduledEntity successor = scheduledEntityRepository.findByEntityID(successorID);
            if (successor == null) {
                throw new EntityDoesNotExist("Entity with ID = " + successorID + " does not exist.");
            }

            predecessor.addSuccessor(successor);

            scheduledEntityRepository.save(predecessor);
        }
    }
}