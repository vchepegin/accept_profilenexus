package eu.accept.tiekinetix.nexus.profiles.reporting.processing;

import com.google.common.base.Strings;
import eu.accept.tiekinetix.nexus.messaging.ChangedEntitiesLoggingService;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.CompletionStatus;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.Intention;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.ScheduleStatus;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.*;
import eu.accept.tiekinetix.nexus.profiles.reporting.vo.ActivityReport;
import eu.accept.tiekinetix.nexus.profiles.reporting.vo.BaseReport;
import eu.accept.tiekinetix.nexus.profiles.reporting.vo.ChangedObjects;
import eu.accept.tiekinetix.nexus.profiles.reporting.vo.QualityGateReport;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.ProjectRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.ScheduledEntityRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.TaskRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.reporting.BaseReportRepository;
import eu.accept.tiekinetix.nexus.profiles.util.CoreUtilities;
import static eu.accept.tiekinetix.nexus.profiles.util.DateUtils.trimDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;


/**
 * At the moment it is one service that does the job but if necessary it can be split into
 * multiple services each executed in parallel when necessary and doing the sub-tasks.
 * Created by vchepegin on 06/06/2017.
 */
@Service
public class KPIProcessorService {

    @Autowired
    private BaseReportRepository baseReportRepository;
    @Autowired
    private ChangedEntitiesLoggingService loggingService;
    @Autowired
    private ScheduledEntityRepository scheduledEntityRepository;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private CoreUtilities coreUtilities;

    /**
     * Create the first level of parents
     * @param intent
     */
    @Async
    public void propagateUpwards (Intention intent) { //TODO Enum with intentions
        ChangedObjects co2 = loggingService.poll();
        Set<Task> parents = new HashSet<>();
        Set<String> wbsCodes = new HashSet<>();
        String projectID = null;
        Date dateMonitored = null;
        if (co2 != null && co2.getEntityIDs() != null) {
            for (String id : co2.getEntityIDs()) {
                //create a set of all effected parents
                Task task = scheduledEntityRepository.getParentTask(id);
                if (task != null) {
                    parents.add(task);
                }
                if (projectID == null) {
                    projectID = projectRepository.findProjectIdByChild(id);
                }
                Task anActivity = taskRepository.findByEntityID(id);
                if (anActivity != null) {
                    wbsCodes.add(anActivity.getWbsCode());
                    if (dateMonitored == null) {
                        dateMonitored = anActivity.getDateMonitored();
                    }
                }
            }
            if (!parents.isEmpty()) {
                bfs(parents, intent); // calculate KPI for all involved Tasks
            }
/* ------------------------------------------------ */
            if (!wbsCodes.isEmpty() && dateMonitored != null) {

                // calculate KPI for all Activities/Tasks with same wbs / all locations
                calcKpiWbsAllLocation(wbsCodes, projectID, dateMonitored);
            }
/* ------------------------------------------------ */

        }
    }

    private void calcKpiWbsAllLocation(Set<String> differentWbsActivities, String projectID, Date dateMonitored) {
        Set<String> tier2WbsCodes = new HashSet<>();
        for (String actualWbsCode : differentWbsActivities) {
            int endIndex = actualWbsCode.lastIndexOf(".");
            if (endIndex != -1)
            {
                tier2WbsCodes.add(actualWbsCode.substring(0, endIndex));
            }
            calculateKPIallLocationsActivities(actualWbsCode, projectID, dateMonitored);
        }
        calculateKPIallLocationsTasks(tier2WbsCodes, projectID, dateMonitored, "TIER_2"); // TIER_2
        Set<String> tier1WbsCodes = new HashSet<>();
        for (String actualWbsCode : tier2WbsCodes) {
            int endIndex = actualWbsCode.lastIndexOf(".");
            if (endIndex != -1)
            {
                tier1WbsCodes.add(actualWbsCode.substring(0, endIndex));
            }
        }
        calculateKPIallLocationsTasks(tier1WbsCodes, projectID, dateMonitored, "TIER_1"); // TIER_1
    }

    private void calculateKPIallLocationsActivities(String actualWbsCode, String projectID, Date dateMonitored) {
        Set<Task> wbsSiblings = new HashSet<>();
        wbsSiblings = taskRepository.findTaskWithSameWbsCode(projectID, actualWbsCode);
        if (wbsSiblings == null || wbsSiblings.isEmpty()) {
            return;
        }
        Iterator<Task> iter = wbsSiblings.iterator();
        Task firstElement = iter.next();
        String name = firstElement.getName();
        double progress = 0;
        double ppc = 0;
        double par = 0;
        double scheduledWorkingDays = 0;
        double remainingWorkingDays = 0;
        double progressTimesScheduledWorkingDays = 0;
        double cumulativeGoalAchieved = 0;
        double cumulativeDaysMonitored = 0;
        double parTimesScheduledWorkingDays = 0;
        double extraEffort = 0;
        int numberOfTasks = 0;
        for (Task aTask : wbsSiblings) {
            scheduledWorkingDays += aTask.getScheduledWorkingDays();
            remainingWorkingDays += aTask.getRemainingWorkingDays();
            progressTimesScheduledWorkingDays += (aTask.getProgress() * aTask.getScheduledWorkingDays());
            cumulativeGoalAchieved += aTask.getCumulativeGoalAchieved();
            cumulativeDaysMonitored += aTask.getCumulativeDaysMonitored();
            parTimesScheduledWorkingDays += (aTask.getPAR() * aTask.getScheduledWorkingDays());
            extraEffort += aTask.getExtraEffort();
            numberOfTasks++;
        }
//        ppc = cumulativeGoalAchieved / cumulativeDaysMonitored / numberOfTasks;
        if (cumulativeDaysMonitored != 0) {
            ppc = cumulativeGoalAchieved / cumulativeDaysMonitored * 100;
        }
        if (scheduledWorkingDays != 0) {
            par = parTimesScheduledWorkingDays / scheduledWorkingDays;
            progress = progressTimesScheduledWorkingDays / scheduledWorkingDays;
        }
        BaseReport kpiReport = new BaseReport(name, projectID, actualWbsCode, "all locations", dateMonitored,
                ppc, par, progress, extraEffort, "TIER_3", "Activity", scheduledWorkingDays, remainingWorkingDays);
        // save KPI-Record for Activities with same wbs-Code
        saveUpdateBaseReport(kpiReport, dateMonitored);
    }

    private void calculateKPIallLocationsTasks(Set<String> wbsCodes, String projectID, Date dateMonitored, String tier) {
        double progress = 0;
        double ppc = 0;
        double par = 0;
        double scheduledWorkingDays = 0;
        double remainingWorkingDays = 0;
        double progressTimesScheduledWorkingDays = 0;
        double cumulativeGoalAchieved = 0;
        double cumulativeDaysMonitored = 0;
        double parTimesScheduledWorkingDays = 0;
        double extraEffort = 0;
        int numberOfTasks = 0;
        for (String actualWbsCode : wbsCodes) {
            Set<Task> wbsSiblings = new HashSet<>();
            wbsSiblings = taskRepository.findTaskWithSameWbsCode(projectID, actualWbsCode);
            Iterator<Task> iter = wbsSiblings.iterator();
            Task firstElement = iter.next();
            String name = firstElement.getName();
            for (Task aTask : wbsSiblings) {
                Set<Task> subTasks = (Set<Task>) taskRepository.findSubTasks(aTask.getEntityID());
                for (Task aSubTask : subTasks) {
                    scheduledWorkingDays += aSubTask.getScheduledWorkingDays();
                    remainingWorkingDays += aSubTask.getRemainingWorkingDays();
                    progressTimesScheduledWorkingDays += (aSubTask.getProgress() * aSubTask.getScheduledWorkingDays());
                    cumulativeGoalAchieved += aSubTask.getCumulativeGoalAchieved();
                    cumulativeDaysMonitored += aSubTask.getCumulativeDaysMonitored();
                    parTimesScheduledWorkingDays += (aSubTask.getPAR() * aSubTask.getScheduledWorkingDays());
                    extraEffort += aSubTask.getExtraEffort();
                    numberOfTasks++;
                }
            }
//            ppc = cumulativeGoalAchieved / cumulativeDaysMonitored / numberOfTasks;
            if (cumulativeDaysMonitored != 0) {
                ppc = cumulativeGoalAchieved / cumulativeDaysMonitored * 100;
            }
            if (scheduledWorkingDays!= 0) {
                par = parTimesScheduledWorkingDays / scheduledWorkingDays;
                progress = progressTimesScheduledWorkingDays / scheduledWorkingDays;
            }
            BaseReport kpiReport = new BaseReport(name, projectID, actualWbsCode,"all locations",
                    dateMonitored, ppc, par, progress, extraEffort, tier, "Task", scheduledWorkingDays, remainingWorkingDays);
            // save KPI-Record for Tasks with same wbs-Code
            saveUpdateBaseReport(kpiReport, dateMonitored);

            scheduledWorkingDays = 0;
            remainingWorkingDays = 0;
            progressTimesScheduledWorkingDays = 0;
            cumulativeGoalAchieved = 0;
            cumulativeDaysMonitored = 0;
            parTimesScheduledWorkingDays = 0;
            extraEffort = 0;
        }
    }

        /**
         * This method use breadth-first traverse fo the Project->Task tree graph in a recursive manner
         * @param parents
         * @param intent
         */
    private void bfs (Set<Task> parents, Intention intent) {
        Set<Task> upperLevel = new HashSet<>();
        for (Task task : parents) {
            Task upperTask = scheduledEntityRepository.getParentTask(task.getEntityID());
            if (upperTask != null) {
                upperLevel.add(upperTask);
            }
            Set<Task> ses = scheduledEntityRepository.findTasksBelow(task.getEntityID());

            double aggregateS = 0;
            double aggregateR = 0;
            double remainings = 0;
            for (Task e : ses) {
                aggregateS += e.getScheduledWorkingDays();
                aggregateR += e.getRemainingWorkingDays();
                remainings += e.getRemainingToDosAbsolute();
            }
            task.setScheduledWorkingDays(aggregateS);
            task.setRemainingWorkingDays(aggregateR);
            task.setRemainingToDosAbsolute(remainings);

            if (intent == Intention.KPI_CALCULATION) {
                processKPI(task);
            }

            scheduledEntityRepository.save(task);
        }
        if (upperLevel.size() != 0) {
            bfs(upperLevel, intent);
        }
    }


    /**
     * A dispatch method to start the process
     * @param task
     */
    public void processKPI (Task task)  {

        // match (a:Task {entityID: {0}})-[:hasChild]->(b:Task) return MAX(b.dateMonitored)

        Set<Task> subTasks = scheduledEntityRepository.findTasksBelow(task.getEntityID());
        //Nothing can be done then
        if (subTasks == null || subTasks.isEmpty()) {
            return;
        }

        int cumulativeGoalAchieved = 0;
        int cumulativeDaysMonitored = 0;
        double progress = 0;
        double par = 0;
        double extraEfforts = 0;
        double remainingWorkingDays = 0;

        Date dateMonitored = new Date(0, 0, 1);
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date lastMonitoredDate = null;
        try {
            lastMonitoredDate = sdf.parse( scheduledEntityRepository.
                        findLastMonitoredTaskDate(task.getEntityID()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        int tasksMonitoredToday = 0;
        double activitiesToday = 0;
        for (Task subTask :  subTasks) {
            cumulativeGoalAchieved += subTask.getCumulativeGoalAchieved();
            cumulativeDaysMonitored += subTask.getCumulativeDaysMonitored();
            progress += subTask.getProgress() * subTask.getScheduledWorkingDays();
            par += subTask.getPAR() * subTask.getScheduledWorkingDays();
            extraEfforts += subTask.getExtraEffort();
            remainingWorkingDays += subTask.getRemainingWorkingDays();
            Date subTaskDate = subTask.getDateMonitored();
            if (subTaskDate != null) {
                dateMonitored = dateMonitored.after(subTaskDate) ? dateMonitored : subTaskDate;
            }
            if (lastMonitoredDate.equals(dateMonitored)) {
                activitiesToday += task.getpPCofDay();
                tasksMonitoredToday++;
            }
         }

        //Nothing can be done then if 0 or less
        if (cumulativeDaysMonitored > 0) {
            task.setCumulativeGoalAchieved(cumulativeGoalAchieved);
            task.setCumulativeDaysMonitored(cumulativeDaysMonitored);
            task.setExtraEffort(extraEfforts);
            task.setRemainingWorkingDays(remainingWorkingDays);
            if (task.getScheduledWorkingDays() != 0) {
                task.setProgress(progress / task.getScheduledWorkingDays());
                task.setPAR(par / task.getScheduledWorkingDays());
                task.setPlannedProgress((task.getScheduledWorkingDays() - task.getRemainingWorkingDays()) /
                        task.getScheduledWorkingDays());
            }
            if (task.getCumulativeDaysMonitored() != 0) {
                task.setPPC(((double) task.getCumulativeGoalAchieved()) / task.getCumulativeDaysMonitored() * 100);
            }
            double delay = (task.getPlannedProgress() - task.getProgress()) * task.getScheduledWorkingDays();

            task.setExpectedEndDate(new Date(task.getEndDate().getTime() +
                    (long) (extraEfforts * 86400000))); // delay = days as a unit
//                    (long) (delay * 86400000))); // delay = days as a unit

//          task.setScheduleStatus(scheduledStatus(task));
            dateMonitored = trimDate(dateMonitored);
            task.setDateMonitored(dateMonitored);
            if (tasksMonitoredToday > 0) {
                task.setpPCofDay(activitiesToday / tasksMonitoredToday);
            } else {
                task.setpPCofDay(0d);
            }

            //RDI: "Additional attributes in Task and Activity for KPI" story
            task.addInitialState(new InitialState(task.getScheduledWorkingDays(),
                    task.getStartDate(), task.getEndDate()));

            task.setScheduleStatus(scheduledStatus(task));


            taskRepository.save(task);

            BaseReport taskReport = BaseReport.getInstance(task);
            taskReport.setProjectID(projectRepository.findProjectIdByChild(task.getEntityID()));
//            List<BaseReport> aBaseReportList = baseReportRepository.findIfAlreadyExists(taskReport.getProjectID(), taskReport.getWbsCode(),
//                    taskReport.getLbsCode(), taskReport.getEntityID(), dateMonitoredStr);
/*            List<BaseReport> aBaseReportList = baseReportRepository.findIfAlreadyExists(taskReport.getProjectID(), taskReport.getWbsCode(),
                    taskReport.getLbsCode(), dateMonitoredStr);
            if (aBaseReportList.isEmpty()) {
                baseReportRepository.save(taskReport);
            } else {
                baseReportRepository.delete(aBaseReportList.get(0).getEntityID());
                baseReportRepository.save(taskReport);
            }
*/
            saveUpdateBaseReport(taskReport, dateMonitored);
//            sendNotification(task, "");

        }
    }

    void saveUpdateBaseReport(BaseReport aBaseReport, Date dateMonitored) {
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        String dateMonitoredStr = sdf.format(dateMonitored).substring(0, 11) + "00:00:00.000Z";
        List<BaseReport> aBaseReportList = baseReportRepository.findIfAlreadyExists(aBaseReport.getProjectID(),
                aBaseReport.getWbsCode(), aBaseReport.getLbsCode(), aBaseReport.getEntityID(), dateMonitoredStr);
        if (aBaseReportList.isEmpty()) {
            baseReportRepository.save(aBaseReport);
        } else {
            baseReportRepository.delete(aBaseReportList.get(0).getEntityID());
            baseReportRepository.save(aBaseReport);
        }

    }

    /**
     * A dispatch method to start the process
     * @param activity
     */
    public void processKPI (Activity activity)  {

        if (activity.getRemainingToDosAbsolute() == 0 && activity.getCumulativeDaysMonitored() > 0) return;

        if (activity.getRemainingWorkingDays() > 0 ) {
            activity.setDynamicDailyGoal(activity.getRemainingToDosAbsolute() /
                    activity.getRemainingWorkingDays());
        } else {
            activity.setDynamicDailyGoal(activity.getRemainingToDosAbsolute());
        }
        activity.setCumulativeDaysMonitored(activity.getCumulativeDaysMonitored() + 1);


//        if (activity.getDidPercent() == 0 && activity.getDidAbsolute() == 0) {
//            // PM _______________________________________________________________________________
//            // PM: of course, that's possible. Nothing has been done that day for the Activity;
//            // but cumulativeScheduledWorkingDays will be incremented and remainingWorkingDays will be decremented
//            throw new IllegalArgumentException("Absolute value and % cannot be zero at the same time.");
//        }

        if (activity.getDidPercent() == 0 && activity.getDidAbsolute() != 0) {
            activity.setDidPercent((int) (activity.getDidAbsolute() * 100 / activity.getDynamicDailyGoal()));
        }

        if (activity.getDidPercent() != 0 && activity.getDidAbsolute() == 0) {
            activity.setDidAbsolute(activity.getDynamicDailyGoal() * activity.getDidPercent()  / 100);
        }

        if (activity.getDidPercent() >= 100) {
            activity.setGoalAchieved(true);
            activity.setCumulativeGoalAchieved(activity.getCumulativeGoalAchieved() + 1);
        } else {
            activity.setGoalAchieved(false);
        }

        if (activity.isGoalAchieved()) {
            activity.setpPCofDay(100d);
        } else {
            activity.setpPCofDay(0d);
        }

        if ((activity.getDidAbsolute() > 0) && (activity.getRemainingToDosAbsolute() / activity.getDidAbsolute() < 0.05)
                && activity.getDidPercent() >= 100) {
            activity.setDidAbsolute(activity.getRemainingToDosAbsolute());
        }

        if (activity.getDynamicDailyGoal() > 0) {
            activity.setCumulativeDidGoals(activity.getCumulativeDidGoals() +
                    activity.getDidAbsolute());
        }

        activity.setCumulativeDailyGoals(activity.getCumulativeDailyGoals() +
                        activity.getDailyGoal());

        if (activity.getQuantity() == null) {
            throw new IllegalArgumentException("Activity MUST have quantity in order to perform KPI calculations");
        }

        activity.setRemainingToDosAbsolute(Math.max(
                activity.getQuantity().getAmount() - activity.getCumulativeDidGoals(),
                0));

        activity.setRemainingWorkingDays(Math.max(
                activity.getScheduledWorkingDays() - activity.getCumulativeDaysMonitored(),
                0
        ));

        if (activity.getDidAbsolute()  == 0 ) {
            activity.setPAR_monitored(0);
        } else {
            activity.setPAR_monitored(activity.getDynamicDailyGoal() / activity.getDidAbsolute());
        }

        if (activity.getCumulativeDidGoals() == 0 ) {
            activity.setPAR(0);
        } else {
//            activity.setPAR(activity.getCumulativeDailyGoals() / activity.getCumulativeDidGoals());
            activity.setPAR((double) Math.round(activity.getCumulativeDailyGoals() / activity.getCumulativeDidGoals() * 100) / 100);
        }


        activity.setCumulativeDailyPAR(activity.getCumulativeDailyPAR() + activity.getPAR_monitored());
        if (activity.getCumulativeDaysMonitored() > 0) {
            activity.setPAR_average(activity.getCumulativeDailyPAR() / activity.getCumulativeDaysMonitored());
        }

        if ((activity.getRemainingWorkingDays() > 0) &&
                (activity.getPAR_average() >= 2) &&
                ((activity.getScheduledWorkingDays() / activity.getRemainingWorkingDays()) >= 2) ) {
            activity.setDailyCheckCritical(true);
        } else {
            activity.setDailyCheckCritical(false);
        }
        if (activity.getQuantity().getAmount() > 0) {
            activity.setContributionToActivityCompletion(activity.getDidAbsolute() / activity.getQuantity().getAmount());
            activity.setProgress(activity.getCumulativeDidGoals() / activity.getQuantity().getAmount());
        }
        activity.setProgress(Math.min(activity.getProgress(), 1));
        if (activity.getScheduledWorkingDays() > 0) {
            activity.setPlannedProgress((activity.getScheduledWorkingDays() - activity.getRemainingWorkingDays()) /
                    activity.getScheduledWorkingDays());
        }
        if (activity.isGoalAchieved()) {
            activity.setPPC(100d);
        } else {
            activity.setPPC(activity.getCumulativeGoalAchieved() / activity.getCumulativeDaysMonitored() * 100);
        }
        if (activity.getWorkingHoursPerDay() > 0) {
            activity.setDelayIndicator((activity.getRemainingToDosAbsolute() * activity.getConsumptionRate().getAmount() /
                    activity.getWorkingHoursPerDay()) - activity.getRemainingWorkingDays());
        }
        activity.setExtraEffort(activity.getDelayIndicator());


        //Issue #113
        if (activity.getInitialState() == null) {
            //RDI: "Additional attributes in Task and Activity for KPI" story
            activity.addInitialState(new InitialState(activity.getScheduledWorkingDays(),
                    activity.getStartDate(), activity.getEndDate()));
        }

        double calcExtraEffortDays = activity.getCumulativeDaysMonitored() -
                activity.getInitialState().getPlannedScheduledWorkingDays();
        long tmp = 1;
        if (activity.getRemainingToDosAbsolute() <= 0.1 ) {
            tmp = 0;
        }
            activity.setExpectedEndDate(new Date(activity.getEndDate().getTime() +
                (long) (activity.getDelayIndicator() * 86400000) +
                (tmp + (long) calcExtraEffortDays) * 86400000)); // delay = days as a unit


        activity.setScheduleStatus(scheduledStatus(activity));

        if (activity.getRemainingToDosAbsolute() <= 0.1 ) {
            activity.setCompletionStatus(CompletionStatus.DONE);
            activity.setExtraEffort(calcExtraEffortDays);
//            propagateCompletionStatusDone(activity);
        }

        ActivityReport activityReport = ActivityReport.getInstance(activity);
        activityReport.setProjectID(projectRepository.findProjectIdByChild(activity.getEntityID()));
//        baseReportRepository.save(activityReport);
        final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

        Date dateMonitored = activity.getDateMonitored();
        saveUpdateBaseReport(activityReport, dateMonitored);
/*
        String dateMonitoredStr = sdf.format(dateMonitored).substring(0, 11) + "00:00:00.000Z";
//        List<BaseReport> aBaseReportList = baseReportRepository.findIfAlreadyExists(activityReport.getProjectID(),
//                activityReport.getWbsCode(), activityReport.getLbsCode(), activityReport.getEntityID(), dateMonitoredStr);
        List<BaseReport> aBaseReportList = baseReportRepository.findIfAlreadyExists(activityReport.getProjectID(),
                activityReport.getWbsCode(), activityReport.getLbsCode(), dateMonitoredStr);
        if (aBaseReportList.isEmpty()) {
            baseReportRepository.save(activityReport);
        } else {
            baseReportRepository.delete(aBaseReportList.get(0).getEntityID());
            baseReportRepository.save(activityReport);
        }
*/

        if (activity.getRemainingWorkingDays() > 0 ) {
            activity.setDynamicDailyGoal(activity.getRemainingToDosAbsolute() /
                    activity.getRemainingWorkingDays());
//            activity.setCumulativeDaysScheduled(activity.getCumulativeDaysScheduled() + 1);
        } else {
            activity.setDynamicDailyGoal(activity.getRemainingToDosAbsolute());
        }

        taskRepository.save(activity);
        String dynGoalStr = String.format( "%.2f", activity.getDynamicDailyGoal());
        String remainingStr = String.format( "%.2f", activity.getRemainingToDosAbsolute());
        String addInfo = "Your next daily goal will be " + dynGoalStr + " " + activity.getQuantity().getUnit() +
                " and the overall remaining to-do is " + remainingStr + " " + activity.getQuantity().getUnit();

        sendNotification(activity, "CompletionStatusChanged", addInfo);
    }

    /**
     * Return the status based on the logic for Tasks and Acitvities
     * @param se
     * @return
     */
    private ScheduleStatus scheduledStatus (Task se) {
        ScheduleStatus formerScheduleStatus = se.getScheduleStatus();
        double scheduleStatus = se.getRemainingWorkingDays() == 0 ?
                -1d : se.getExtraEffort() / se.getRemainingWorkingDays();
        ScheduleStatus returnVal = ScheduleStatus.ON_SCHEDULE;
 /*
        final String mesaage = se.getName() + ": ScheduledStatus changed to BEHIND_SCHEDULE for the " +
                se.getTaskType() + " with id = " + se.getEntityID() + ". Code: WBS = " +
                se.getWbsCode() + ", LBS = " + se.getLbsCode();
*/

        if (se.getRemainingWorkingDays() > 0) {
            if (scheduleStatus > 0.05) {
                //notification
                 returnVal = ScheduleStatus.BEHIND_SCHEDULE ;
            } else if (scheduleStatus < -0.05) {
                returnVal = ScheduleStatus.AHEAD_OF_SCHEDULE;
            } else {
                returnVal = ScheduleStatus.ON_SCHEDULE;
            }
        } else {
            if (se.getRemainingToDosAbsolute() > 0) {
                // activity is not finished
                //notification
                returnVal = ScheduleStatus.BEHIND_SCHEDULE;
            } else {
                // activity is finished
                if ((se.getProgress() - se.getPlannedProgress()) > 0.05) {
                    returnVal = ScheduleStatus.AHEAD_OF_SCHEDULE;
                } else if (Math.abs((se.getInitialState().getPlannedScheduledWorkingDays() - se.getCumulativeDaysMonitored()) /
                        se.getInitialState().getPlannedScheduledWorkingDays()) <= 0.125) {
                    returnVal = ScheduleStatus.ON_SCHEDULE;
                } else {
                    //notification
                    returnVal = ScheduleStatus.BEHIND_SCHEDULE;
                }
            }
        }
        if (formerScheduleStatus != returnVal) {
            String addInfo = "ScheduledStatus of this " + se.getTaskType() + " changed to " + " " +
                    se.getScheduleStatus().toString();

 //           sendNotification(se, "ScheduledStatusChanged", addInfo);
            sendNotification(se, "CompletionStatusChanged", addInfo);  // we don't have and don't need an own ScheduledStatusChangedHandler

        }

        return returnVal;
    }

    private void sendNotification(ScheduledEntity se, String msgType, String additionalInfo)  {
        final String messageHead = se.getName() + ": ";
        final String messageEnd =  ".\n\nCode: WBS = " + se.getWbsCode() + ", LBS = " + se.getLbsCode();
        String messageComplete = messageHead + additionalInfo + messageEnd +
                  "\nID of " + se.getTaskType() + " = " + se.getEntityID();
        coreUtilities.sendCommand(msgType, se.getEntityID(), messageComplete);
    }
    /**
     * A dispatch method to start the process
     * @param qGate
     */
    public void processKPI (QualityGate qGate)  {
        if (qGate.isVerified()) {
            //PM____________________________________________________
            // user in that case has to call explicitly qualityGate.setCompletionStatus(DONE);
            // so, at the moment nothing to do for the QualityGate
                        //           qGate.setCompletionStatus(CompletionStatus.DONE);
        }

        QualityGateReport qgr = QualityGateReport.getInstance(qGate);
        baseReportRepository.save(qgr);
    }

    @Async
    public void propagateCompletionStatusDone(ScheduledEntity entity) {
        String addInfo = "CompletionStatus of this " + entity.getTaskType() + " changed to " +
                entity.getCompletionStatus().toString();
        sendNotification(entity, "CompletionStatusChanged", addInfo);
        if (entity.getCompletionStatus() == CompletionStatus.DONE) {
            markTasksDone(entity.getEntityID());
        }

    }
    private void markTasksDone (String taskID) {
        //horizontal
        Set<String> ids = taskRepository.find2bReady(taskID);
        for (String id : ids) {
            if (!Strings.isNullOrEmpty(id)) {
                taskRepository.setStatus(id, CompletionStatus.IN_PROGRESS.toString());
                taskRepository.setInitialSubTasks2Ready(id); //vertical down
                //notification
                ScheduledEntity se = scheduledEntityRepository.findByEntityID(id);
                String addInfo = "CompletionStatus of this " + se.getTaskType() + " changed to " + " " +
                        se.getCompletionStatus().toString();
                sendNotification(se, "CompletionStatusChanged", addInfo);
            }
        }

        //vertical up
        String tID = taskRepository.ifAllChildrenComplete(taskID);
        if ( !Strings.isNullOrEmpty(tID) ) {
            taskRepository.setStatus(tID, CompletionStatus.DONE.toString());
            //notification
            ScheduledEntity se = scheduledEntityRepository.findByEntityID(tID);
            String addInfo = "CompletionStatus of this " + se.getTaskType() + " changed to " + " " +
                    se.getCompletionStatus().toString();
            sendNotification(se, "CompletionStatusChanged", addInfo);

            markTasksDone(tID);
        }

    }

}
