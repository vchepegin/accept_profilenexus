package eu.accept.tiekinetix.nexus.profiles.web.restful;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.accept.tiekinetix.nexus.profiles.core.exceptions.EntityAlreadyExistsException;
import eu.accept.tiekinetix.nexus.profiles.core.exceptions.EntityDoesNotExist;
import eu.accept.tiekinetix.nexus.profiles.core.model.user.Crew;
import eu.accept.tiekinetix.nexus.profiles.core.model.user.Equipment;
import eu.accept.tiekinetix.nexus.profiles.core.model.user.Person;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.CrewRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.EquipmentRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.PersonRepository;
import eu.accept.tiekinetix.nexus.profiles.util.CoreUtilities;
import org.neo4j.ogm.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by vchepegin on 30/01/2017.
 */
@CrossOrigin
@RestController
public class CrewController {

    @Autowired //don't forget the setter
    private CrewRepository crewRepository;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private EquipmentRepository equipmentRepository;
    @Autowired
    private CoreUtilities coreUtilities;
    @Autowired
    Session session;

    private final ObjectMapper mapper = new ObjectMapper();

    @CrossOrigin
    @RequestMapping(value="/getActivitiesOfCrew/{crewID}", method= RequestMethod.GET)
    public Set<String> getActivitiesOfCrew (@PathVariable(value="crewID") String crewID)
            throws Exception  {
        return crewRepository.findActivitiesByCrewId(crewID);
    }

    @CrossOrigin
    @RequestMapping(value="/getCrewByID/{crewID}", method= RequestMethod.GET)
    public Crew getCrewByID (@PathVariable(value="crewID") String crewID)
            throws Exception  {
        Crew crew = crewRepository.findByCrewID(crewID);
        return crew;
    }

    @CrossOrigin
    @RequestMapping(value="/getCrews", method= RequestMethod.GET)
    public Set<Crew> getCrews ()
            throws Exception  {
        Set<Crew> crews = crewRepository.getCrews();
        return crews;
    }

    @CrossOrigin
    @RequestMapping(value="/getCrewsWorkerIsAssignedTo/{personID}", method= RequestMethod.GET)
    public Set<Crew> getCrewsWorkerIsAssignedTo (@PathVariable(value="personID") String personID)
            throws Exception  {
        Set<Crew> crews = crewRepository.getCrewsOfWorker(personID);
        return crews;
    }
    @CrossOrigin
    @RequestMapping(value="/findCrew/{limit}", method = RequestMethod.POST)
    public List<Crew> findCrew (@RequestBody Map<String, Object> payload,
                                @PathVariable(value="limit") String limit) throws Exception {

        final String template = "match (a:Crew) %1 return a limit " + limit;

        String cypher = coreUtilities.prepareCypher(template, payload);

        return coreUtilities.getObjectsByCriteria(session, cypher, payload, Crew.class);
    }

    @CrossOrigin
    @RequestMapping(value="/createCrew", method = RequestMethod.POST)
    public String createCrew(@RequestBody String payload) throws Exception {

        //TODO check if such crew exists already, if NOT create else throw EntityAlreadyExistsException

        //introduce Optionals
        Crew newCrew = null;
        newCrew = mapper.readValue(payload, Crew.class);


        String guid = newCrew.getCrewID();

        Crew crew = crewRepository.findByCrewID(guid);
        if (crew != null) {
            throw new EntityAlreadyExistsException("The person with ID = " + guid + "already exists.");
        }

        if (guid == null ||  guid.isEmpty()) {
            guid = java.util.UUID.randomUUID().toString();
            newCrew.setCrewID(guid);
        }
        crewRepository.save(newCrew);

        return guid;
    }

    //TODO REFACTOR: duplicate code
    @CrossOrigin
    @RequestMapping(value="/createCrew/{crewID}", method = RequestMethod.POST)
    public String createCrew(@RequestBody String payload,
                             @PathVariable(value="crewID") String crewID) throws Exception {

        Crew oldCrew = crewRepository.findByCrewID(crewID);
        if (oldCrew != null) {
            throw new EntityAlreadyExistsException("The crew with ID = " + crewID + "already exists.");
        }

        Crew newCrew = null;
        newCrew = mapper.readValue(payload, Crew.class);


        newCrew.setCrewID(crewID);
        crewRepository.save(newCrew);

        return crewID;
    }

    @CrossOrigin
    @RequestMapping(value="/deleteCrew/{crewID}", method = RequestMethod.DELETE)
    public void deleteComponent (@PathVariable(value="crewID") String crewID)
            throws Exception {
        //TODO: whar are the consequences of this step in terms of links to other entities?!
        crewRepository.delete(crewID);
    }

    @CrossOrigin
    @RequestMapping(value="/updateCrew", method = RequestMethod.PUT)
    public void updateCrew (@RequestBody Map<String, Object> payload) {

        Crew crew = crewRepository.findByCrewID
                (payload.get("crewID").toString());

        for (Map.Entry<String, Object> entry : payload.entrySet()) {

            try {
                Class<?> c = crew.getClass();
                Field t = c.getDeclaredField(entry.getKey());
                t.setAccessible(true);
                if (t.getType().isEnum()) {
                    t.set(crew, Enum.valueOf((Class<Enum>)t.getType(),
                            entry.getValue().toString()));
                } else {
                    t.set(crew, entry.getValue());
                }
            } catch (NoSuchFieldException | IllegalAccessException ex) {
                ex.printStackTrace();
            }
        }

        crewRepository.save(crew);
    }

    @CrossOrigin
    @RequestMapping(value="/setCrewOwner/{crewID}/{personID:.+}", method = RequestMethod.PUT)
    public String setCrewOwner(@PathVariable(value="crewID") String crewID,
                             @PathVariable(value="personID") String personID) throws Exception {

        Crew crew = crewRepository.findByCrewID(crewID);
        if (crew == null) {
            throw new EntityDoesNotExist("The crew with ID = " + crewID + "does not exist.");
        }

        Person person = personRepository.findByPersonID(personID);
        if (person == null) {
            throw new EntityDoesNotExist("The person with ID = " + personID + "does not exist.");
        }

        String status;
        if (crew.getOwner() != null) {
            status = "replaced";
        } else {
            status = "added";
        }

        crew.setOwner(person);
        crewRepository.save(crew);

        return "{ \"status\":\""  + status + "\"}";


    }

    @CrossOrigin
    @RequestMapping(value="/removeEquipment/{crewID}/{equipmentID}", method = RequestMethod.DELETE)
    public void removeEquipment ( @PathVariable(value="crewID") String crewID,
            @PathVariable(value="equipmentID") String equipmentID)
            throws Exception {
        Crew crew = crewRepository.findByCrewID(crewID);
        for (Equipment e : crew.equipment) {
            if (e.getEquipmentID().equals(equipmentID)) {
                crew.removeEquipment(e);
                break;
            }
        }
        equipmentRepository.delete(equipmentID);
        crewRepository.save(crew);
    }


}
