package eu.accept.tiekinetix.nexus.profiles.web.restful;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import eu.accept.tiekinetix.nexus.notifications.Notification;
import eu.accept.tiekinetix.nexus.notifications.NotificationProperties;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Project;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.PersonRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.ProjectRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.ScheduledEntityRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.reporting.NotificationRepository;
import org.neo4j.ogm.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

import static eu.accept.tiekinetix.nexus.notifications.EmailProvider.sendEmail;
//import static eu.accept.tiekinetix.nexus.notifications.SMSProvider.sendNotification;
import static eu.accept.tiekinetix.nexus.profiles.util.TikkiQueue.sendTikkiMessage;

/**
 * Created by pmerz on 20/10/2017.
 */
@CrossOrigin
@RestController
public class NotificationController {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    NotificationRepository notificationRepository;
    @Autowired //don't forget the setter
    private PersonRepository personRepository;
    @Autowired //don't forget the setter
    ScheduledEntityRepository seRepository;
    @Autowired //don't forget the setter
    ProjectRepository projectRepository;
    @Autowired
    Session session;

    @Autowired
    private ApplicationContext applicationContext;


    @CrossOrigin
    @RequestMapping(value="/addNotification", method = RequestMethod.POST)
    public void addNotification (@RequestBody Map<String, Object> payload) throws Exception {
//        public void addNotification (@RequestBody String payload) throws Exception {
        Object dat = payload.get("dateTime");
        String dateTime = dat.toString();
        dateTime = dateTime.replace('T', ' ');
        dateTime = dateTime.replace(':', '.');
        payload.replace("dateTime", dateTime);

//        Notification newNotification = mapper.readValue(payload, Notification.class);
        Notification newNotification = mapper.convertValue(payload, Notification.class);

        notificationRepository.save(newNotification);
    //TODO: add phone number
        //find the actual owner phone number
 //       sendNotification(newNotification.getSubject(), "");
        Set<String> receiverIDs = new HashSet<String>();
        receiverIDs.add(newNotification.getReceiverID());
        sendNotification(newNotification, receiverIDs, null);
    }

    @CrossOrigin
    @RequestMapping(value="/getNotifications", method = RequestMethod.POST)
    public List<Notification> getNotifications (@RequestBody Map<String, Object> payload) throws Exception {

//        NotificationFilter filter = mapper.readValue(payload, NotificationFilter.class);

//        return notificationRepository.findNotificationsByFilter(filter.getProjectID(),
//                filter.getReceiverID(),
//                String.valueOf(filter.getTimestamp().getTime()));

        final String template = "match (a:Notification) %1 return a";
        ArrayList<Notification> notifications = new ArrayList<>();

        String cypher = prepareCypher4Notification(template, payload);
        if (cypher == null || cypher.isEmpty()) {
            return notifications;
        }
        Iterable<Map<String, Object>> iterableResult = session.query(cypher, payload).queryResults();

        ObjectMapper mapper = new ObjectMapper();
        for (Map map : iterableResult) {
            Notification notification = mapper.convertValue(map.get("a"), Notification.class);
            notifications.add(notification);
        }

        return notifications;

    }

    private String prepareCypher4Notification (String template, Map<String, Object> payload) {
        StringBuilder criteria = new StringBuilder("");
        boolean isFirst = true;
        for (Map.Entry<String, Object> entry : payload.entrySet()) {

            String param = entry.getKey();
            if (!param.isEmpty()) {
                if (!isFirst) {
                    criteria.append(" AND ");
                }
                if (param.equalsIgnoreCase("DATETIME")) {
                    String dateTime = param;
                    dateTime.replace(" ", "T");
                    dateTime.replace(".", ":");
                    criteria.append("a.timestamp >= {" + dateTime + "}");
                } else if (param.equalsIgnoreCase("RECEIVERID")) {
                    String receiverID = param;
                    criteria.append("a.receiverID CONTAINS {" + receiverID + "}");
                } else {
                    criteria.append("a." + param + " = {" + param + "}");
                }
                isFirst = false;
            }
        }

        String query = "";
        if (!"".equals(criteria.toString())) {
            criteria.insert(0, " WHERE ");
            query = template.replace("%1", criteria.toString());
            //query.insert(71, criteria.toString());
        } else {
            query = template.replace("%1", "");
        }

        return query;
    }


    @CrossOrigin
    @RequestMapping(value="/addBroadcastNotification", method = RequestMethod.POST)
    public void addBroadcastNotification (@RequestBody Map<String, Object> payload) throws Exception {
        Object taskID = payload.get("taskID");
        Object projectID = payload.get("projectID");
//        String nullPersonID = new String();
        Set<String> personIDs = null;
        Set<String> receiverIDs = new HashSet<String>();

        Object dat = payload.get("dateTime");
        String dateTime = dat.toString();
        dateTime = dateTime.replace("T", " ");
        dateTime = dateTime.replace(":", ".");

        payload.replace("dateTime", dateTime);
        if (taskID != null) {
            payload.remove("taskID");
//            payload.put("receiverID", nullPersonID);
            personIDs = personRepository.getAllPersonsInvolvedInTask(taskID.toString());
        }
        else if (projectID != null){
            personIDs = personRepository.getAllPersonsInvolvedInProject(projectID.toString());
        }
        if (!personIDs.isEmpty()) {
/*            for (String personID : personIDs) {
                Object thePersonIDObj = personID;
                payload.replace("receiverID", thePersonIDObj);
                Notification notification = mapper.convertValue(payload, Notification.class);
                notificationRepository.save(notification);
                String personIString = thePersonIDObj.toString();
                receiverIDs.add(personIString);
                sendNotification(notification, receiverIDs);
             }
*/
            Boolean isFirst = true;
            String receiverIDConcatenated = new String();
            for (String personID : personIDs) {
                if (isFirst == false) {
                    receiverIDConcatenated = receiverIDConcatenated + ", ";
                } else {
                    isFirst = false;
                }
                receiverIDConcatenated = receiverIDConcatenated + personID;
                receiverIDs.add(personID);
            }
//            payload.replace("receiverID", receiverIDConcatenated);
            payload.put("receiverID", receiverIDConcatenated);

            Notification notification = mapper.convertValue(payload, Notification.class);
            notificationRepository.save(notification);
            sendNotification(notification, receiverIDs, taskID.toString());

        }
    }
    private void sendNotification(Notification notification, Set<String> recipientIDs, String taskID) {

        Set<String> emailsOfReceiver = new HashSet<String>();
        Set<String> emails = new HashSet<String>();
        for (String receiverID : recipientIDs) {
            emailsOfReceiver = personRepository.getEmailAddress(receiverID);
            for (String email : emailsOfReceiver) {
                if (email != null && !email.isEmpty()) {
                    emails.add(email);
                }
            }
        }

        Project project = projectRepository.findByProjectID(notification.getProjectID());
        String projectName = new String();
        if (project != null) {
            projectName = project.getProjectName();
        }
        String subject = new String("ACCEPT.EU: new message from ") + notification.getSender();
        String body = new String("Dear owner, \n\nYour project  ") + projectName +
                " has an update: \n\n" + notification.getSubject() + ". \n\nYours sincerely,\n ACCEPT Crew";
        if (!emails.isEmpty()) {
            NotificationProperties properties = applicationContext.getBean(NotificationProperties.class);
            sendEmail(properties, subject,body, emails.toArray(new String[emails.size()]));
        }
        if (taskID != null && !taskID.isEmpty() ) {
            ScheduledEntity se = seRepository.findByEntityID(taskID);
            if (se != null) {
                String tikkiQueueID = se.getTikkiQueueID();
                try {
                    sendTikkiMessage(emails, tikkiQueueID, subject, body);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
        }
    }

    @CrossOrigin
    @RequestMapping(value="/deleteNotificationsOlderThanDate/{timestamp}", method = RequestMethod.DELETE)
    public void deleteNotificationsOlderThanDate (@PathVariable(value="timestamp") String timestamp) throws Exception {
        timestamp = timestamp.replace('T', ' ');
        timestamp = timestamp.replace(':', '.');
        notificationRepository.deleteNotificationsOlderThanDate(timestamp);
    }

}
