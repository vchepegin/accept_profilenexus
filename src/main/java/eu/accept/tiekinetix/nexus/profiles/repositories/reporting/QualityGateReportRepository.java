package eu.accept.tiekinetix.nexus.profiles.repositories.reporting;

import eu.accept.tiekinetix.nexus.profiles.reporting.vo.QualityGateReport;
import org.springframework.data.neo4j.repository.GraphRepository;

/**
 * Created by vchepegin on 12/06/2017.
 */
public interface QualityGateReportRepository extends GraphRepository<QualityGateReport> {

    QualityGateReport findByEntityID (String entityID);

}