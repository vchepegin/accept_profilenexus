package eu.accept.tiekinetix.nexus.profiles.repositories.core;

import eu.accept.tiekinetix.nexus.profiles.core.model.user.Equipment;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by vchepegin on 24/02/2017.
 */
public interface EquipmentRepository extends GraphRepository<Equipment> {

    Equipment findByEquipmentID (String equipmentID);

    @Query("match (a:Equipment {equipmentID: {0} })-[r]-(b) DETACH delete a")
    void delete(@Param("0")String equipmentID);
}
