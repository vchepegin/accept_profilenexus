package eu.accept.tiekinetix.nexus.profiles.repositories.reporting;

import eu.accept.tiekinetix.nexus.profiles.reporting.vo.BaseReport;
import eu.accept.tiekinetix.nexus.profiles.reporting.vo.MinMaxReportingDates;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

/**
 * Created by vchepegin on 15/06/2017.
 */
public interface BaseReportRepository extends GraphRepository<BaseReport> {

    List<BaseReport> findByProjectID (String id);

    @Query("match (a:BaseReport) WHERE a.projectID = {0} AND a.dateMonitored > {1}" +
            " AND a.dateMonitored < {2} return a")
    List<BaseReport> findByProjectIDAndFilter (@Param("0")String id, @Param("1")String start, @Param("2")String end);

    @Query("MATCH (n:BaseReport {projectID : {0} }) return MIN(n.dateMonitored), MAX(n.dateMonitored)")
    MinMaxReportingDates findMinMaxMonitoringDates (@Param("0")String projectID);

    @Query("match (a:BaseReport) WHERE a.projectID = {0} AND a.wbsCode = {1} AND a.lbsCode = {2}" +
            " AND a.entityID = {3} AND a.dateMonitored = {4} return a")
    List<BaseReport> findIfAlreadyExists(@Param("0")String projectID, @Param("1")String wbsCode,
                                         @Param("2")String lbsCode, @Param("3")String entityID,
                                         @Param("4")String dateMonitored);
/*
@Query("match (a:BaseReport) WHERE a.projectID = {0} AND a.wbsCode = {1} AND a.lbsCode = {2}" +
        " AND a.dateMonitored = {3} return a")
List<BaseReport> findIfAlreadyExists(String projectID, String wbsCode, String lbsCode, String dateMonitored);
*/

    @Query("match (a:BaseReport) WHERE a.projectID = {0} AND a.wbsCode = {1} AND a.lbsCode = {2}" +
            " AND a.entityID = {3} return a")
    List<BaseReport> findIfAlreadyExistsWithoutDate(@Param("0")String projectID, @Param("1")String wbsCode,
                                                    @Param("2")String lbsCode, @Param("3")String entityID);

    @Query("match (a:BaseReport {entityID: {0} }) DETACH delete a")
    void delete(@Param("0")String entityID);

    @Query("match (b:BaseReport {projectID: {0}}) delete b")
    void deleteAllReportsOfProject(@Param("0")String projectID);
}