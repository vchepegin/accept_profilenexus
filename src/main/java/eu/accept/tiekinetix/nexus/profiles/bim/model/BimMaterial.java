package eu.accept.tiekinetix.nexus.profiles.bim.model;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import static java.util.stream.Collectors.toSet;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

/**
 * Created by arian.kuschki on 12/06/17.
 */
@Getter
@Setter
@NodeEntity
@XmlRootElement(name = "Material")
@ToString(callSuper = true)
public class BimMaterial extends BimEntity implements HasCategories, HasEmbeddedParameters {

    @JacksonXmlProperty(localName = "Class")
    @JsonProperty("class")
    private String clazz;
    @JacksonXmlProperty(localName = "Category")
    private String categoryName;
    @JacksonXmlProperty(localName = "Guid")
    private String guid;
    @JacksonXmlProperty(localName = "Density")
    private Double density;
    @JacksonXmlProperty(localName = "KeyNote")
    private String keyNote;
    @JacksonXmlProperty(localName = "Parameter")
    @JacksonXmlElementWrapper(localName = "Parameters")
    private Set<BimParameter> parameters = new HashSet<>();
    @JacksonXmlProperty(localName = "Name")
    private String name;

    @Relationship
    private Set<BimCategory> categories;

    @Override
    public Set<BimParameter> getParameters() {
        return parameters;
    }

    @Override
    public  void setParameters(Set<BimParameter> parameters) {
        this.parameters = parameters;
    }

    @Override
    public Set<BimCategory> getCategories() {
        return categories;
    }

    @Override
    public void setCategories(Set<BimCategory> categories) {
        this.categories = categories;
    }

    @JsonIgnoreProperties
    @Override
    public Set<Long> getCategoryIds() {
        return parameters.stream()
                .filter(p -> "Category".equals(p.getName()))
                .map(BimParameter::getValue)
                .map(Long::parseLong)
                .collect(toSet());
    }

    @Override
    public Set<Long> getParamIds(final String name) {
        return getParamsByName(name).stream()
                .map(BimParameter::getValue)
                .map(Long::parseLong)
                .collect(toSet());
    }

    @Override
    public Set<BimParameter> getParamsByName(String name) {
        return parameters.stream()
                .filter(p -> name.equals(p.getName()))
                .distinct()
                .collect(toSet());
    }

    @JsonProperty("Density")
    public Double getDensity() {
        return density;
    }

    @JsonProperty("Density")
    public void setDensity(Double density) {
        this.density = density;
    }

    @JsonProperty("KeyNote")
    public String getKeyNote() {
        return keyNote;
    }

    @JsonProperty("KeyNote")
    public void setKeyNote(String keyNote) {
        this.keyNote = keyNote;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }

}
