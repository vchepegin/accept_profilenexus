package eu.accept.tiekinetix.nexus.profiles.core.model.workflow.behaviour;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * Aspect ot deal with changes happening when the completion status of any ScheduledEntity occurs.
 * Created by vchepegin on 23/02/2017
 */
@Aspect
@Component
public class CompletionStatusAspect {

    @Pointcut("execution(* eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity.setCompletionStatus " +
            "(eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.CompletionStatus) )")
//    +
//            " && args(completionStatus)")
    public void pcSetCompletionStatus () { }


//    @Before("execution(* eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity.setCompletionStatus(..) )")
//    +
//            "(eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.CompletionStatus))" +
//            " && args(status)")
    @Before("pcSetCompletionStatus ()")
    public void beforeSetCompletionStatus (JoinPoint joinPoint) {
        System.out.println("Completion status ADVICE ");

    }

}
