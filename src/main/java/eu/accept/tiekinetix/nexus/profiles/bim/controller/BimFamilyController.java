package eu.accept.tiekinetix.nexus.profiles.bim.controller;

import java.io.IOException;
import java.util.Optional;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.accept.tiekinetix.nexus.profiles.bim.model.BimFamily;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by akuschki on 12/06/2017.
 */

@CrossOrigin
@RestController
@RequestMapping(value = "/projects/{projectID}/bimFamilies/{bimId}",produces = {"application/json"}, consumes = {"application/json", "*/*"})
public class BimFamilyController extends AbstractBimController {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Transactional
    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId) throws IOException {
        familyRepository.delete(findOne(projectID, bimId));
    }

    @RequestMapping(method = RequestMethod.GET)
    public BimFamily get(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId) throws IOException {
        return findOne(projectID, bimId);
    }

    @Transactional
    @RequestMapping(method = RequestMethod.PUT)
//    public void put(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId, @Valid @RequestBody BimFamily family, @PathParam("setScheduledEntity") Optional<String> scheduledEntity) throws IOException {
    public void put(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId, @Valid @RequestBody String payload) throws IOException {
        BimFamily family = mapper.readValue(payload, BimFamily.class);
        validatePayload(bimId, family);
        setCategoryLink(family, projectID);
        processAndSave(projectID, bimId, family, null);
    }

    private BimFamily findOne(String projectID, Long bimId) {
        return familyRepository.findByProjectProjectIDAndBimId(projectID, bimId)
                .stream()
                .findFirst()
                .orElseThrow(ResourceNotFoundException::new);
    }
}
