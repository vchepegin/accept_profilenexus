package eu.accept.tiekinetix.nexus.profiles.util;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by vchepegin on 13/03/2017.
 */
public abstract class DateUtils {

    //Only check arabic numbers which is good enough here
    public static boolean isNumericR (String str) {
        return str.matches("-?\\d+(\\.\\d+)?");
    }

    /**
     * just in case also non-arabic numbers need to be tested
     * @param str
     * @return
     */
    public static boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    /**
     * @see <a href="http://pom.nu/2o5gJGR">Origin of this method</a>
     * @param start
     * @param end
     * @param ignore
     * @return
     */
    public static int bestDaysBetween(final LocalDate start,
                                      final LocalDate end, final List<DayOfWeek> ignore) {
        int days = simpleDaysBetween(start, end);

        if (days == 0) {
            return 0;
        }

        if (!ignore.isEmpty()) {
            int weeks = days / 7;
            int startDay = start.getDayOfWeek().getValue();
            int endDay = end.getDayOfWeek().getValue();
            int diff = weeks * ignore.size();

            for (DayOfWeek day : ignore) {
                int currDay = day.getValue();
                if (startDay <= currDay) {
                    diff++;
                }
                if (endDay > currDay) {
                    diff++;
                }
            }

            if (endDay > startDay) {
                diff -= endDay - startDay;
            }

            return days - diff;
        }

        return days;
    }

    /**
     * @see <a href="http://pom.nu/2o5gJGR">Origin of this method</a>
     *
     * @param start
     * @param end
     * @return
     */
    public static int simpleDaysBetween(final LocalDate start,
                                        final LocalDate end) {
        return (int) ChronoUnit.DAYS.between(start, end);
    }

    /**
     * Calculate the date after the start date and N working days excluding weekends (Sat-Sun).
     * This function assumes 1 as the same day, i.e. 2 is the next day, etc.
     * @param startDate
     * @param nWorkingDays
     * @return
     */
    public static Date dateAfterWorkingDays (Date startDate, double nWorkingDays) {

        //this function works only with the future
        if (nWorkingDays <= 0) {
            return dateBeforeWorkingDays (startDate, nWorkingDays);
        }

        LocalDate counter = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        double i = nWorkingDays;
        while ( nWorkingDays >= 1 && i >= 1 ) {
            counter =  counter.plusDays(1);
            if (counter.getDayOfWeek() != DayOfWeek.SATURDAY &&  counter.getDayOfWeek() != DayOfWeek.SUNDAY) {
                i--;
            }
        }

        if (counter.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
            counter =  counter.plusDays(1);
        }

        if (counter.getDayOfWeek().equals(DayOfWeek.SATURDAY)) {
            counter =  counter.plusDays(2);
        }

        Date date2set = Date.from(counter.atStartOfDay(ZoneId.systemDefault()).toInstant());
        date2set.setHours(9);
        return date2set;
    }

    public static Date dateBeforeWorkingDays (Date startDate, double nWorkingDays) {
        LocalDate counter = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        double i = Math.abs(nWorkingDays);

        while ( i > 0 ) {
            counter =  counter.minusDays(1);
            if (counter.getDayOfWeek() != DayOfWeek.SATURDAY &&  counter.getDayOfWeek() != DayOfWeek.SUNDAY) {
                i--;
            }
        }

//        if (counter.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
//            counter =  counter.minusDays(2);
//        }
//
//        if (counter.getDayOfWeek().equals(DayOfWeek.SATURDAY)) {
//            counter =  counter.minusDays(1);
//        }

        Date date2set = Date.from(counter.atStartOfDay(ZoneId.systemDefault()).toInstant());
        date2set.setHours(9);
        return date2set;
    }

    /**
     * NOTE: 0 is the same date
     * @param startDate
     * @param nDays
     * @return
     */
    public static Date dateAfterDays (Date startDate, long nDays) {
        //this function works only with the future
        LocalDate counter = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        if (nDays == 0) {
            return startDate;
        }
        Date date2set = new Date();
        if (nDays > 0) {
            counter = counter.plusDays(nDays);
            if (counter.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
                counter = counter.plusDays(1);
            }
            if (counter.getDayOfWeek().equals(DayOfWeek.SATURDAY)) {
                counter = counter.plusDays(2);
            }
            date2set = Date.from(counter.atStartOfDay(ZoneId.systemDefault()).toInstant());
            date2set.setHours(9);
        } else {
            counter = counter.plusDays(nDays);
            if (counter.getDayOfWeek().equals(DayOfWeek.SUNDAY)) {
                counter = counter.minusDays(2);
            }
            if (counter.getDayOfWeek().equals(DayOfWeek.SATURDAY)) {
                counter = counter.minusDays(1);
            }
            date2set = Date.from(counter.atStartOfDay(ZoneId.systemDefault()).toInstant());
            date2set.setHours(9);

        }
        return date2set;
    }

    /**
     * Return the last part of the WBS code, used for getting the WBS code od the workflow step
     * when it is created from the existing real workflow, i.e. not from the template aka Workflow.
     * @param wbs
     * @return real WBS code of the workflow step
     */
    public static String getPureWbs (String wbs) {

        if (wbs == null) {
            throw new IllegalArgumentException("WBS code cannot ne null");
        }

        int li = wbs.lastIndexOf(".");
        if (li != -1) {
            wbs = wbs.substring(li, wbs.length()-1);
        }
        return wbs;
    }

    public static Date trimDate(Date date) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("CEST"));
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR, 0);

        return calendar.getTime();
    }
}
