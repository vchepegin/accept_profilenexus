package eu.accept.tiekinetix.nexus.profiles.core.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by vchepegin on 15/03/2017.
 */
@ResponseStatus(value= HttpStatus.EXPECTATION_FAILED, reason="Entity with such ID does not exist.") //409
public class EntityDoesNotExist extends RuntimeException {

    public EntityDoesNotExist (String entityID) {
        super(entityID + " does not exist.");
    }
}
