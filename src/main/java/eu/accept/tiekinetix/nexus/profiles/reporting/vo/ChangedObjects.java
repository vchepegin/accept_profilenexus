package eu.accept.tiekinetix.nexus.profiles.reporting.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * This object is used to save IDs of chenged objects that can effect the state fo the rest of the model.
 * The typical example is a set of Activities that being changed effect the state of the tasks and projects.
 *
 * This is temporal solution. The good solution is to send the IDs of activities as immutable messages via any
 * messaging system, e.g. Redis or any other persistent queues for further processing.
 *
 * Created by vchepegin on 04/07/2017.
 */
@NodeEntity
public class ChangedObjects {

    @GraphId
    private Long id;

    Date validUntil;

    //the size of this array is limited to Integer.MAX_VALUE = 2,147,483,647, which is enough for this service now
    Set<String> entityIDs;

    public ChangedObjects () {}

    public ChangedObjects addID (String id) {
        if (entityIDs == null) {
            entityIDs = new HashSet<>();
        }
        entityIDs.add(id);
        return this;
    }

    public ChangedObjects addIDs (Set<String> ids) {
        if (entityIDs == null) {
            entityIDs = new HashSet<>();
        }
        entityIDs.addAll(ids);
        return this;
    }

    @JsonProperty("validUntil")
    public Date getValidUntil() {
        if (this.validUntil == null) return null;
        return new Date(this.validUntil.getTime());
    }

    @JsonProperty("validUntil")
    public void setValidUntil(Date validUntil) {
        if (validUntil != null) {
            this.validUntil = new Date (validUntil.getTime());
        }
    }

    @JsonProperty("entityIDs")
    public Set<String> getEntityIDs() {
        return entityIDs;
    }

    @JsonProperty("entityIDs")
    public void setEntityIDs(Set<String> entityIDs) {
        this.entityIDs = entityIDs;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ChangedObjects)) return false;

        ChangedObjects that = (ChangedObjects) o;

        if (getValidUntil() != null ? !getValidUntil().equals(that.getValidUntil()) : that.getValidUntil() != null)
            return false;
        return getEntityIDs() != null ? getEntityIDs().equals(that.getEntityIDs()) : that.getEntityIDs() == null;
    }

    @Override
    public int hashCode() {
        int result = getValidUntil() != null ? getValidUntil().hashCode() : 0;
        result = 31 * result + (getEntityIDs() != null ? getEntityIDs().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("ChangedObjects{");
        if (entityIDs != null) {
            for (String e : entityIDs) {
                result.append(e + ", ");
            }
        }
        result.append('}');
        return result.toString();
    }
}
