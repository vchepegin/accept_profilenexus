package eu.accept.tiekinetix.nexus.profiles.bim.repository;

import java.util.List;

import eu.accept.tiekinetix.nexus.profiles.bim.model.*;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface BimEntityRepository extends GraphRepository<BimEntity> {

//    @Query("MATCH (a:BimEntity) DETACH DELETE a;")
//    void deleteByProjectBimId(long projectBimId);

//    @Query("MATCH (a:BimEntity {bimId: {bimId}}) DETACH DELETE a")
    @Query("MATCH (a:BimEntity {bimId: {0}}) DETACH DELETE a")
    void detachDelete(@Param("0")long bimId);

//    @Query("MATCH (a:BimEntity) --> (p:Project {projectID: $projectId) DETACH DELETE a")
    @Query("MATCH (a:BimEntity) --> (p:Project {projectID: {0}}) DETACH DELETE a")
    void detachDeleteByProject(@Param("0")String projectID);

//    @Query("MATCH (scheduledEntity:ScheduledEntity) <-- (entity:BimEntity) --> (project:Project)" + " WHERE (entity.bimId = $bimId OR $bimId IS NULL)" + " AND (project.projectID = $projectId OR $projectId IS NULL)" + " AND (scheduledEntity.entityID = $scheduledEntityId OR $scheduledEntityId IS NULL)" + " AND (entity.guid = $bimGuid OR $bimGuid IS NULL)" + " AND ($bimType IN labels(entity) OR $bimType IS NULL)" + " RETURN entity")
    @Query("MATCH (entity:BimEntity) -[r1:PROJECT]-> (project:Project) " +
           "WHERE (entity.bimId = {2} OR {2} IS null) AND (project.projectID = {0} OR {0} IS null) " +
           "AND ({3} IN labels(entity) OR {3} IS null) " +
           "OPTIONAL MATCH (parameter:BimParameter) <-[r2:PARAMETERS]-(entity) " +
           "OPTIONAL MATCH (scheduledEntity:ScheduledEntity) <-[r3:SCHEDULEDENTITY]- (entity) " +
           "WHERE (scheduledEntity.entityID = {1} OR {1} IS null) AND (entity.bimId = {2}) " +
           "AND ( {3} IN labels(entity) OR {3} IS null) RETURN DISTINCT entity, r3, scheduledEntity, r2, parameter LIMIT {4}")
    List<BimEntity> find(@Param("0")String projectId, @Param("1")String scheduledEntityId, @Param("2")Long bimId,
                         @Param("3")String bimType,@Param("4") Integer limit);
/*
    @Query("MATCH (scheduledEntity:ScheduledEntity) <-[r1]- (entity:BimElement) -[r2]-> (project:Project)" +
            " WHERE (entity.bimId = {2} OR {2} IS null)" + " AND (project.projectID = {0} OR {0} IS null)" +
            " AND (scheduledEntity.entityID = {1} OR {1} IS null)" + " RETURN entity, r1, scheduledEntity")
*/
    @Query("MATCH (parameter:BimParameter) <-[r3]-(entity:BimElement) -[r2]-> (project:Project)" +
        " WHERE (entity.bimId = {2} OR {2} IS null)" + " AND (project.projectID = {0} OR {0} IS null)" +
        " OPTIONAL MATCH (scheduledEntity:ScheduledEntity) <-[r1]- (entity:BimElement)" +
        " WHERE (scheduledEntity.entityID = {1} OR {1} IS null)" +
        " RETURN DISTINCT entity, r1, scheduledEntity, r3, parameter LIMIT {3}")
    List<BimElement> findBimElement(@Param("0")String projectId, @Param("1")String scheduledEntityId,
                                    @Param("2")Long bimId, @Param("3")Integer limit);

    @Query("MATCH (parameter:BimParameter) <-[r3]-(entity:BimMaterial) -[r2]-> (project:Project)" +
            " WHERE (entity.bimId = {2} OR {2} IS null)" + " AND (project.projectID = {0} OR {0} IS null)" +
            " OPTIONAL MATCH (scheduledEntity:ScheduledEntity) <-[r1]- (entity:BimMaterial)" +
            " WHERE (scheduledEntity.entityID = {1} OR {1} IS null)" +
            " RETURN DISTINCT entity, r1, scheduledEntity, r3, parameter LIMIT {3}")
    List<BimMaterial> findBimMaterial(@Param("0")String projectId, @Param("1")String scheduledEntityId,
                                      @Param("2")Long bimId, @Param("3")Integer limit);

    @Query("MATCH (parameter:BimParameter) <-[r3]-(entity:BimType) -[r2]-> (project:Project)" +
            " WHERE (entity.bimId = {2} OR {2} IS null)" + " AND (project.projectID = {0} OR {0} IS null)" +
            " OPTIONAL MATCH (scheduledEntity:ScheduledEntity) <-[r1]- (entity:BimType)" +
            " WHERE (scheduledEntity.entityID = {1} OR {1} IS null)" +
            " RETURN DISTINCT entity, r1, scheduledEntity, r3, parameter LIMIT {3}")
    List<BimType> findBimType(@Param("0")String projectId, @Param("1")String scheduledEntityId,
                              @Param("2")Long bimId, @Param("3")Integer limit);

    @Query("MATCH (parameter:BimParameter) <-[r3]-(entity:BimRoom) -[r2]-> (project:Project)" +
            " WHERE (entity.bimId = {2} OR {2} IS null)" + " AND (project.projectID = {0} OR {0} IS null)" +
            " OPTIONAL MATCH (scheduledEntity:ScheduledEntity) <-[r1]- (entity:BimRoom)" +
            " WHERE (scheduledEntity.entityID = {1} OR {1} IS null)" +
            " RETURN DISTINCT entity, r1, scheduledEntity, r3, parameter LIMIT {3}")
    List<BimRoom> findBimRoom(@Param("0")String projectId, @Param("1")String scheduledEntityId,
                              @Param("2")Long bimId, @Param("3")Integer limit);

    @Query("MATCH (entity:BimFamily) -[r1]-> (project:Project)" +
            " WHERE (entity.bimId = {1} OR {1} IS null)" + " AND (project.projectID = {0} OR {0} IS null)" +
            " RETURN DISTINCT entity LIMIT {2}")
    List<BimFamily> findBimFamily(@Param("0")String projectId, @Param("1")Long bimId, @Param("2")Integer limit);

    @Query("MATCH ((entity:BimFamily) -[r2]-> (project:Project)" +
            " WHERE (entity.bimId = {1} OR {1} IS null)" + " AND (project.projectID = {0} OR {0} IS null)" +
            " RETURN DISTINCT entity LIMIT {2}")
    List<BimCategory> findBimCategory(@Param("0")String projectId, @Param("1")Long bimId, @Param("2")Integer limit);

    //    @Query("MATCH (entity:BimEntity) --> (project:Project)" + " WHERE (entity.bimId = $bimId OR $bimId IS NULL)" + " AND (project.projectID = $projectId OR $projectId IS NULL)" + " AND ($bimType IN labels(entity) OR $bimType IS NULL)" + " RETURN entity")
    @Query("MATCH (entity:BimEntity) --> (project:Project)" + " WHERE (entity.bimId = {1} OR {1} IS NULL) " +
                "AND (project.projectID = {0} OR {0} IS NULL)" + " AND ({2} IN labels(entity) OR {2} IS NULL)" + " RETURN entity")
    List<BimEntity> findAll(@Param("0")String projectId, @Param("1")Long bimId, @Param("2")String bimType);

//    @Query("MATCH (a:BimEntity {bimId: {bimId}})-[r]-(b) RETURN a,r,b")
    @Query("MATCH (a:BimEntity {bimId: {0}})-[r]-(b) RETURN a,r,b")
    List<BimEntity> findByBimIdWithRelated(@Param("0")long bimId);

//    @Query("MATCH (n:BimEntity), (s:ScheduledEntity)<-[rs*0..4]-(p:Project) where ID(n) = $bimNeoId AND s.wbsCode = $wbsCode AND p.projectID = $projectID CREATE (n)-[r:SCHEDULEDENTITY] -> (s) ")
    @Query("MATCH (n:BimEntity), (s:ScheduledEntity)<-[rs*0..4]-(p:Project) where ID(n) = {0} AND s.wbsCode = {1} " +
            "AND p.projectID = {2} CREATE (n)-[r:SCHEDULEDENTITY] -> (s) ")
    void buildTaskRelationbyWbs(@Param("0")Long bimNeoId, @Param("1")String wbsCode, @Param("2")String projectID);

//    @Query("MATCH (n:BimEntity), (s:ScheduledEntity)<-[rs*0..4]-(p:Project)  where ID(n) = $bimNeoId AND s.lbsCode = $lbsCode AND p.projectID = $projectID CREATE (n)-[r:SCHEDULEDENTITY] -> (s) ")
    @Query("MATCH (n:BimEntity), (s:ScheduledEntity)<-[rs*0..4]-(p:Project)  where ID(n) = {0} AND s.lbsCode = {1} " +
            "AND p.projectID = {2} CREATE (n)-[r:SCHEDULEDENTITY] -> (s) ")
    void buildTaskRelationbyLbs(@Param("0")Long bimNeoId, @Param("1")String lbsCode, @Param("2")String projectID);

//    @Query("MATCH (n:BimEntity), (s:ScheduledEntity)<-[rs*0..4]-(p:Project) where ID(n) = $bimNeoId AND s.wbsCode = $wbsCode AND s.lbsCode = $lbsCode AND p.projectID = $projectID CREATE (n)-[r:SCHEDULEDENTITY] -> (s) ")
    @Query("MATCH (n:BimEntity), (s:ScheduledEntity)<-[rs*0..4]-(p:Project) where ID(n) = {0} AND s.wbsCode = {1} " +
            "AND s.lbsCode = {2} AND p.projectID = {3} CREATE (n)-[r:SCHEDULEDENTITY] -> (s) ")
    void buildTaskRelationbyWbsAndLbs(@Param("0")Long bimNeoId, @Param("1")String wbsCode, @Param("2")String lbsCode,
                                      @Param("3")String projectID);

//    @Query("MATCH (b:BimElement) - [rb] - (s:ScheduledEntity) <- [r*0..3] - (p:Project) where p.projectID = $projectId and (s.completionStatus = $completionStatus OR $completionStatus IS NULL) AND (s.scheduleStatus = $scheduleStatus OR $scheduleStatus IS NULL) return b.bimId ")
    @Query("MATCH (b:BimElement) - [rb] - (s:ScheduledEntity) <- [r*0..3] - (p:Project) where p.projectID = {0} " +
            "AND (s.completionStatus = {1} OR {1} IS NULL) AND (s.scheduleStatus = {2} OR {2} IS NULL) return b.bimId ")
    List<Long> getBimEntitiesByTaskStatus(@Param("0")String projectId, @Param("1")String completionStatus,
                                          @Param("2")String scheduleStatus);

}
