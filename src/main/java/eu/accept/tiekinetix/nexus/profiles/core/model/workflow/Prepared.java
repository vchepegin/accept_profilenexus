package eu.accept.tiekinetix.nexus.profiles.core.model.workflow;

/**
 * This interface contains only one method to be called as a LAST step in the initialisation of an object.
 * The use case for it is, e.g. when Task is created and iff it has start AND end date THEN duration can be
 * calculated as well.
 *
 * So this method contains certain business logic for the validation followed by instantiation of related fields.
 *
 * Created by vchepegin on 19/07/2017.
 */
public interface Prepared {
    void wrapUp();
}
