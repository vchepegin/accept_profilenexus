package eu.accept.tiekinetix.nexus.profiles.repositories.core;

import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.MinMaxStartEndDates;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Project;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Set;

/**
 * Created by vchepegin on 01/06/2017.
 */
@RepositoryRestResource
public interface ProjectRepository extends GraphRepository<Project> {

    Project findByProjectID (String projectID);

    @Query("MATCH (p:Project {projectID: {0}}) RETURN p")
    List<Project> findAllByProjectID(@Param("0")String projectID);

    @Query("match (a:Project) return a")
    List<Project> findProjects ();

    @Query("match (a:Project {projectID: {0} }) DETACH delete a")
    void delete(@Param("0")String projectID);

    //TODO tune for performance reasons
    @Query("match (a:Project)-[r*]->(b:ScheduledEntity {entityID : {0} }) return a.projectID limit 1")
    String findProjectIdByChild(@Param("0")String entityID);

//    @Query("match (a:Project)-[r*]->(b:ScheduledEntity {entityID : {0} }) return a limit 1")
    @Query("match (a:Project)-[r*]->(b:ScheduledEntity {entityID : {0} }) return a limit 1")
    Project findProjectByChild(@Param("0")String entityID);

    @Query("MATCH (a:Task)<-[r:hasTask]-(p:Project { projectID : {0} })  " +
            "MATCH (a:Task)-[r2:hasChild*0..3]-(e) " +
            "MATCH (e) WHERE NOT ()-[:hasSuccessor]->(e) " +
            "MATCH (parent)-[:hasChild]-(e) " +
            "Match (parent) WHERE NOT ()-[:hasSuccessor]->(parent) " +
            "SET e.completionStatus = 'IN_PROGRESS', " +
            "a.completionStatus = 'IN_PROGRESS' " +
            "return collect(distinct p.projectID)")
    String initialiseStatusForProject (@Param("0")String projectID);

    @Query("match (p:Project {projectID: {0}})-[r*0..4]->(s:ScheduledEntity) return MIN(s.startDate), MAX(s.endDate)")
    MinMaxStartEndDates getTimeFrameOfProject(@Param("0")String projectID);

    @Query("match (p:Project {projectID: {0}})-[r*0..4]->(s:ScheduledEntity) return MIN(s.startDate) AS startDate")
    String getFirstStartDateOfProject(@Param("0")String projectID);

    @Query("match (p:Project {projectID: {0}})-[r*0..4]->(s:ScheduledEntity) return MAX(s.endDate) AS endDate")
    String getLastEndDateOfProject(@Param("0")String projectID);

//    @Query("match (p:Project {projectID: {0}}) -[r1]->(s:ScheduledEntity)-[r2*0..4]->(sChildren:ScheduledEntity) -[r3]-(q:Quantity)" +
//            "return s, sChildren, r3, q")
    @Query("match (p:Project {projectID: {0}}) -[r1]->(s:ScheduledEntity)-[r2*0..4]->(sChildren:ScheduledEntity) " +
            "OPTIONAL MATCH (sChildren:ScheduledEntity) -[r3]->(q:Quantity) " +
            "return s, sChildren, r3, q")
    Set<ScheduledEntity> getAllScheduledEntities(@Param("0")String projectID);
}
