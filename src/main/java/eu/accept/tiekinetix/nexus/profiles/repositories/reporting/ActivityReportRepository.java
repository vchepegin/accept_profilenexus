package eu.accept.tiekinetix.nexus.profiles.repositories.reporting;

import eu.accept.tiekinetix.nexus.profiles.reporting.vo.ActivityReport;
import org.springframework.data.neo4j.repository.GraphRepository;

/**
 * Created by vchepegin on 13/06/2017.
 */
public interface ActivityReportRepository extends GraphRepository<ActivityReport> {

    ActivityReport[] findByEntityID (String entityID);

}
