package eu.accept.tiekinetix.nexus.profiles.bim.controller;

import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

import eu.accept.tiekinetix.nexus.profiles.bim.model.BimEntity;
import eu.accept.tiekinetix.nexus.profiles.bim.model.BimParameter;
import eu.accept.tiekinetix.nexus.profiles.bim.model.HasCategories;
import eu.accept.tiekinetix.nexus.profiles.bim.model.HasEmbeddedParameters;
import eu.accept.tiekinetix.nexus.profiles.bim.repository.*;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Project;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.ProjectRepository;
import eu.accept.tiekinetix.nexus.profiles.util.BadRequestException;
import org.neo4j.ogm.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;

/**
 * Created by arian.kuschki on 10/07/17.
 */

public abstract class AbstractBimController {
    @Autowired
    protected BimEntityRepository bimEntityRepository;
    @Autowired
    protected CategoryRepository categoryRepository;
    @Autowired
    protected BimElementRepository bimElementRepository;
    @Autowired
    protected FamilyRepository familyRepository;
    @Autowired
    protected MaterialRepository materialRepository;
    @Autowired
    protected ParameterRepository parameterRepository;
    @Autowired
    protected TypeRepository typeRepository;
    @Autowired
    protected RoomRepository roomRepository;
    @Autowired
    protected Session session;
    @Autowired
    protected BimProjectRepository projectRepository;
    @Autowired
    protected BimScheduledEntityRepository scheduledEntityRepository;

//    private void setScheduledEntity(BimEntity entity, Optional<String> scheduledEntity) {
    private void setScheduledEntity(BimEntity entity, String scheduledEntity) {
        if (scheduledEntity == null || scheduledEntity.isEmpty()) { return;}
/*        scheduledEntity.ifPresent(id -> entity.getScheduledEntities()
                .add(scheduledEntityRepository.findByEntityID(id)
                        .stream()
                        .findFirst()
                        .orElseThrow(() -> new ResourceNotFoundException("ScheduledEntity " + id + " does not exist"))));
*/
        ScheduledEntity se = scheduledEntityRepository.getScheduledEntityByID(scheduledEntity);
        if (se != null) {
            entity.addScheduledEntity(se);
        }
    }

    protected void validatePayload(Long bimId, BimEntity entity) {
        if (entity.getNeoId() != null) {
            throw new BadRequestException("Client must not set neoId");
        }
    }

//    protected BimEntity processAndSave(String projectID, Long bimId, BimEntity entity, Optional<String> scheduledEntity) {
    protected BimEntity processAndSave(String projectID, Long bimId, BimEntity entity, String scheduledEntity) {
        setProject(projectID, entity);
        setScheduledEntity(entity, scheduledEntity);
        setNeoId(projectID, entity);
        if (!Objects.equals(bimId, entity.getBimId())) {
            throw new BadRequestException("Inconsistent value for bimId in request path and body! Path: " + bimId + ", body: " + entity.getBimId());
        }
        return bimEntityRepository.save(entity);
    }

    protected void setNeoId(String projectID, BimEntity entity) {
        Integer limit = 10000;
        bimEntityRepository.find(projectID, null, entity.getBimId(), entity.getClass()
                .getSimpleName(), limit)
                .stream() // there should only be one, but uniqueness of type + bimId is currently not enforced
                .findFirst()
                .map(BimEntity::getNeoId)
                .ifPresent(entity::setNeoId);
    }

    protected void setProject(String projectID, BimEntity entity) {
        final Project project = projectRepository.findByProjectID(projectID)
                .stream()
                .findFirst()
                .orElseThrow(() -> new ResourceNotFoundException("Project " + projectID + " does not exist"));
        entity.setProject(project);
        if (entity instanceof HasEmbeddedParameters) {
            ((HasEmbeddedParameters) entity).getParameters()
                    .forEach(p -> p.setProject(project));
        }
    }

    protected void deduplicateEmbeddedParameters(HasEmbeddedParameters entity, String projectID) {
        entity.setParameters(entity.getParameters()
                .stream()
                .map(p -> parameterRepository.findByProjectProjectIDAndGuid(projectID, p.getGuid())
                        .stream()
                        .findFirst()
                        .orElse(p))
                .collect(toSet()));
    }

    protected void setCategoryLink(HasCategories entity, String projectID) {
        entity.setCategories(entity.getCategoryIds()
                .stream()
                .map(id -> categoryRepository.findByProjectProjectIDAndBimId(projectID, id)
                        .stream()
                        .findFirst()
                        .orElse(null))
                .filter(Objects::nonNull)
                .collect(toSet()));

    }

    protected void buildRelationsToTasks(HasEmbeddedParameters entity, String projectId) {
        // added by Peter for connecting BIM-Entities with Tasks
        Set<BimParameter> params = entity.getParameters();
        final String wbsString = "WBS";
//        final String wbsCodeString = "WBS Code";
        final String lbsString = "LBS Code";
        Set<String> wbsValues = new HashSet<String>();
        String lbsValue = null;
        Long bimNeoId = entity.getNeoId();
        if (!params.isEmpty()) {
            for (BimParameter param : params) {
                String paramName = param.getName();
//       			if (paramName.equalsIgnoreCase(wbsString) || paramName.equalsIgnoreCase(wbsCodeString)) {
                if (paramName.startsWith(wbsString)) {
                    wbsValues.add(param.getValue());
                } else if (paramName.equalsIgnoreCase(lbsString)) {
                    lbsValue = param.getValue();
                }
            }
        }

        if (!wbsValues.isEmpty()) {
            for (String actualWbsCode : wbsValues) {
                if (lbsValue != null) {
                    System.out.println("BIM-Entity " + entity.getBimId()  + " connect to task with WBS: " + actualWbsCode + " LBS " + lbsValue);
                    bimEntityRepository.buildTaskRelationbyWbsAndLbs(bimNeoId, actualWbsCode, lbsValue, projectId);
                } else {
                    System.out.println("BIM-Entity " + entity.getBimId()  + " connect to task with WBS: " + actualWbsCode);
                    bimEntityRepository.buildTaskRelationbyWbs(bimNeoId, actualWbsCode, projectId);
                }
            }
        }
        else if (lbsValue != null) {
            System.out.println("BIM-Entity " + entity.getBimId()  + " connect to task with LBS: " + lbsValue);
            bimEntityRepository.buildTaskRelationbyLbs(bimNeoId, lbsValue, projectId);
        }
    }

}
