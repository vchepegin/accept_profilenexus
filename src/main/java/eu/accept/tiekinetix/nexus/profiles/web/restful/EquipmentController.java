package eu.accept.tiekinetix.nexus.profiles.web.restful;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.accept.tiekinetix.nexus.profiles.core.model.user.Crew;
import eu.accept.tiekinetix.nexus.profiles.core.model.user.Equipment;
import eu.accept.tiekinetix.nexus.profiles.core.model.user.Person;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Task;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.CrewRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.EquipmentRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.PersonRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;
import eu.accept.tiekinetix.nexus.profiles.util.CoreUtilities;
import org.neo4j.ogm.session.Session;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

/**
 * Created by vchepegin on 24/02/2017.
 */
@CrossOrigin
@RestController
public class EquipmentController {

    @Autowired
    private EquipmentRepository equipmentRepository;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private CrewRepository crewRepository;
    @Autowired
    private CoreUtilities coreUtilities;
    @Autowired
    Session session;

    private final ObjectMapper mapper = new ObjectMapper();

    @CrossOrigin
    @RequestMapping(value="/getEquipmentByID/{equipmentID}", method = RequestMethod.GET)
    public Equipment getTaskByID (@PathVariable(value="equipmentID") String equipmentID)
            throws Exception {
        Equipment equipment = equipmentRepository.findByEquipmentID(equipmentID);
        return equipment;
    }

    @CrossOrigin
    @RequestMapping(value="/createEquipment", method = RequestMethod.POST)
    public String createTaskByID (@RequestBody String payload) throws Exception {
        Equipment newEquipment= null;
        newEquipment = mapper.readValue(payload, Equipment.class);
        String guid = newEquipment.getEquipmentID();
        if (guid == null || guid.isEmpty()) {
            guid = java.util.UUID.randomUUID().toString();
            newEquipment.setEquipmentID(guid);
        }
        equipmentRepository.save(newEquipment);

        return guid;
    }
/*
    @RequestMapping(value="/createEquipment/{taskID}", method = RequestMethod.POST)
    public String createTaskByID (@RequestBody String payload,
                                @PathVariable(value="taskID") String taskID) throws Exception {
        Equipment newEquipment= null;
        newEquipment = mapper.readValue(payload, Equipment.class);

        Task task = taskRepository.findByEntityID(taskID);
        String guid = java.util.UUID.randomUUID().toString();
        newEquipment.setEquipmentID(guid);
        task.addEquipment(newEquipment);

        taskRepository.save(task);

        return guid;
    }

 */
    @CacheEvict(value = "task", allEntries = true)
    @CrossOrigin
    @RequestMapping(value="/linkEquipmentToTask/{equipmentID}/{taskID}", method = RequestMethod.POST)
    public void linkEquipmentToTask (@PathVariable(value="equipmentID") String equipmentID,
                                  @PathVariable(value="taskID") String taskID) throws Exception {

        Equipment equipment = equipmentRepository.findByEquipmentID(equipmentID);
        Task task = taskRepository.findByEntityID(taskID);
        task.addEquipment(equipment);
        taskRepository.save(task);
    }


    @CacheEvict(value = "person", allEntries = true)
    @CrossOrigin
    @RequestMapping(value="/linkEquipmentToPerson/{equipmentID}/{personID:.+}", method = RequestMethod.POST)
    public void linkEquipmentToPerson (@PathVariable(value="equipmentID") String equipmentID,
                                     @PathVariable(value="personID") String personID) throws Exception {

        Equipment equipment = equipmentRepository.findByEquipmentID(equipmentID);
        Person person = personRepository.findByPersonID(personID);
        person.addEquipment(equipment);
        personRepository.save(person);
    }

    @CrossOrigin
    @RequestMapping(value="/updateEquipment", method = RequestMethod.PUT)
    public void updateEquipment (@RequestBody Map<String, Object> payload) {

        Equipment equipment = equipmentRepository.
                findByEquipmentID(payload.get("equipmentID").toString());

        for (Map.Entry<String, Object> entry : payload.entrySet()) {

            try {
                Class<?> c = equipment.getClass();
                Field t = c.getDeclaredField(entry.getKey());
                t.setAccessible(true);
                if (t.getType().isEnum()) {
                    t.set(equipment, Enum.valueOf((Class<Enum>)t.getType(),
                            entry.getValue().toString()));
                } else {
                    t.set(equipment, entry.getValue());
                }
            } catch (NoSuchFieldException | IllegalAccessException ex) {
                ex.printStackTrace();
            }
        }

        equipmentRepository.save(equipment);
    }

    @CrossOrigin
    @RequestMapping(value="/linkEquipmentToCrew/{equipmentID}/{crewID}", method = RequestMethod.POST)
    public void linkEquipmentToCrew (@PathVariable(value="equipmentID") String equipmentID,
                                     @PathVariable(value="crewID") String crewID) throws Exception {

        Equipment equipment = equipmentRepository.findByEquipmentID(equipmentID);
        Crew crew = crewRepository.findByCrewID(crewID);
        crew.addEquipment(equipment);
        crewRepository.save(crew);
    }

    @CrossOrigin
    @RequestMapping(value="/deleteEquipment/{equipmentID}", method = RequestMethod.DELETE)
    public void deleteComponent (@PathVariable(value="equipmentID") String equipmentID)
            throws Exception {
        //TODO: whar are the consequences of this step in terms of links to other entities?!
        equipmentRepository.delete(equipmentID);
    }

    @CrossOrigin
    @RequestMapping(value="/findEquipment/{limit}", method = RequestMethod.POST)
    @Cacheable("equipment")
    public List<Equipment> findEquipment (@RequestBody Map<String, Object> payload,
                                           @PathVariable(value="limit") String limit) throws Exception {

        final String template = "match (a:Equipment) %1 return a limit " + limit;

        String cypher = coreUtilities.prepareCypher(template, payload);

        return coreUtilities.getObjectsByCriteria(session, cypher, payload, Equipment.class);

    }

}
