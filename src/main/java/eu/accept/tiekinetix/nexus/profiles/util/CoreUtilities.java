package eu.accept.tiekinetix.nexus.profiles.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import eu.accept.tiekinetix.nexus.messaging.Message;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Activity;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.QualityGate;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Task;
import org.neo4j.ogm.model.Result;
import org.neo4j.ogm.session.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by vchepegin on 29/05/2017.
 */
@Service
public final class CoreUtilities {

    private static final Logger LOGGER = LoggerFactory.getLogger(CoreUtilities.class);

    @Autowired
    private StringRedisTemplate template;

    private static final ObjectMapper mapper = new ObjectMapper();

    private static final Pattern tierPattern = Pattern.compile("\"TIER\":\"(?<tier>\\S*?)\"");
    private static final Pattern ttPattern = Pattern.compile("\"TASKTYPE\":\"(?<tt>\\S*?)\"");

    /**
     * Logic to check if the dates are in the right order if they are present in different combinations.
     * @param payload
     * @return
     * @throws Exception
     */
    public boolean datesAreInOrder (Map<String, Object> payload) throws Exception {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

        Date startDate = null;
        if (!Strings.isNullOrEmpty((String)payload.get("startDate"))) {
            startDate = formatter.parse(payload.get("startDate").toString());
        } else {
            return true;
        }

        Date endDate = null;
        if (!Strings.isNullOrEmpty((String)payload.get("endDate"))) {
            endDate = formatter.parse(payload.get("endDate").toString());
        }

        if (endDate == null && startDate != null) return false;

        if (endDate == null || startDate == null) {
            throw new IllegalArgumentException("It is necessary to provide the end or start date or both " +
                    "for performing this operation.");
        }

        return endDate.after(startDate);
    }

    /**
     * Preparing a Cypher query as a string based on the input parameters
     * @param template
     * @param payload
     * @return
     */
    public String prepareCypher (String template, Map<String, Object> payload) {
        StringBuilder criteria = new StringBuilder("");
        boolean isFirst = true;
        for (Map.Entry<String, Object> entry : payload.entrySet()) {

            String param = entry.getKey();

            if (!param.toUpperCase().equals("STARTDATE") && !param.toUpperCase().equals("ENDDATE")) {
                if (!isFirst) {
                    criteria.append(" AND ");
                }
                criteria.append("a." + param + " = {" + param + "}");
                isFirst = false;
            }

            switch (param.toUpperCase()) {
                case "STARTDATE":
                    if (!isFirst) {
                        criteria.append(" AND ");
                    }
                    criteria.append(" NOT a.endDate < {" + param + "}");
                    isFirst = false;
                    break;
                case "ENDDATE":
                    if (!isFirst) {
                        criteria.append(" AND ");
                    }
                    criteria.append(" NOT a.startDate > {" + param + "}");
                    isFirst = false;
                    break;
                default:
            }
        }

        String query = "";
        if (!"".equals(criteria.toString())) {
            criteria.insert(0, " WHERE ");
            query = template.replace("%1", criteria.toString());
            //query.insert(71, criteria.toString());
        } else {
            query = template.replace("%1", "");
        }

        return query;
    }

    public String prepareCypher4Reports (String template, Map<String, Object> payload) {
        StringBuilder criteria = new StringBuilder("");
        boolean isFirst = true;
        for (Map.Entry<String, Object> entry : payload.entrySet()) {

            String param = entry.getKey();
            if (!param.toUpperCase().equals("STARTDATE") && !param.toUpperCase().equals("ENDDATE") &&
                    !param.toUpperCase().equals("WBSCODE") && !param.toUpperCase().equals("LBSCODE") &&
                    !param.toUpperCase().equals("ALL_LOCATIONS") && !param.toUpperCase().equals("SINGLE_LOCATION")) {
                if (!isFirst) {
                    criteria.append(" AND ");
                }
                criteria.append("a." + param + " = {" + param + "}");
                isFirst = false;
            }

            switch (param.toUpperCase()) {
                case "WBSCODE":
                    if (!isFirst) {
                        criteria.append(" AND ");
                    }
                    criteria.append(" a.wbsCode STARTS WITH {" + param + "}");
                    isFirst = false;
                    break;
                case "LBSCODE":
                    if (!isFirst) {
                        criteria.append(" AND ");
                    }
                    criteria.append(" a.lbsCode STARTS WITH {" + param + "}");
                    isFirst = false;
                    break;
                case "STARTDATE":
                    if (!isFirst) {
                        criteria.append(" AND ");
                    }
                    criteria.append(" a.dateMonitored > {" + param + "}");
                    isFirst = false;
                    break;
                case "ENDDATE":
                    if (!isFirst) {
                        criteria.append(" AND ");
                    }
                    criteria.append(" a.dateMonitored < {" + param + "}");
                    String str = payload.get(param).toString();
                    payload.put(param, str + "T23:59:59");
                    isFirst = false;
                    break;
                case "ALL_LOCATIONS":
                    if (!isFirst) {
                        criteria.append(" AND ");
                    }
                    criteria.append(" a.lbsCode = {" + param + "}");
                    isFirst = false;
                    break;
                case "SINGLE_LOCATION":
                    if (!isFirst) {
                        criteria.append(" AND ");
                    }
                    criteria.append(" a.lbsCode <> {" + param + "}");
                    isFirst = false;
                    break;
                default:
            }
        }

        String query = "";
        if (!"".equals(criteria.toString())) {
            criteria.insert(0, " WHERE ");
            query = template.replace("%1", criteria.toString());

            //query.insert(71, criteria.toString());
        } else {
            query = template.replace("%1", "");
        }

        return query;
    }

    public Map<String, Object> appendAllLocationsFilter(Map<String, Object> payload) {
        Map<String, Object> newPayload = payload;
        boolean allLocationsUsed = false;
        boolean lbsUsed = false;
        String allLocs = new String("all locations");
        for (Map.Entry<String, Object> entry : payload.entrySet()) {
            String param = entry.getKey();
            if (param.toUpperCase().equals("ALL_LOCATIONS")) {
                allLocationsUsed = true;
            } else if (param.toUpperCase().equals("LBSCODE")) {
                lbsUsed = true;
            }
        }
        if (lbsUsed == false && allLocationsUsed == false) {
            newPayload.put("SINGLE_LOCATION", allLocs);
            newPayload.remove("all_locations");
        } else if (lbsUsed == false && allLocationsUsed == true) {
            newPayload.put("ALL_LOCATIONS", allLocs);
//            newPayload.remove("all_locations");
        }
        return newPayload;
    }
    /**
     * Does the actual retrieval of the data from the DB and creates a List of appropriate objects of a given
     * type (@klazz) and return the List of <T> that has to be equal or superclass of klazz. <T> is deducted from
     * the left-side of the statement.
     * @param session
     * @param query
     * @param payload
     * @param klazz
     * @param <T>
     * @return
     */
    public <T> List<T> getObjectsByCriteria (Session session, String query,
                                            Map<String, Object> payload,
                                            Class<T> klazz) {

//        Iterable<Map<String, Object>> iterableResult = session.query(query, payload).queryResults();
        Result a = session.query(query, payload);
        Iterable<Map<String, Object>> iterableResult = a.queryResults();
//        Iterable<Map<String, Object>> iterableResult = session.query(query, payload).queryResults();

        ArrayList<T> entities = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        for (Map<String, Object> map : iterableResult) {
            T entity = mapper.convertValue(map.get("a"), klazz);
            entities.add(klazz.cast(entity));
        }

        return entities;
    }

    /**
     * Factory method for ScheduledEntity children classes
     * @param payload
     * @param type
     * @return ScheduledEntity
     */
    public ScheduledEntity createScheduledEntity (String payload, String type) {
        ScheduledEntity newEntity = null;
        String reasonForException = "";

        if (type.equalsIgnoreCase("task")) {
            try {
                newEntity = new ObjectMapper().readValue(payload, Task.class);
                newEntity.setTaskType("TASK");
            } catch (IOException ex) {
                reasonForException = ex.getLocalizedMessage();
            }
        }

        if (type.equalsIgnoreCase("qualityGate")) {
            try {
                newEntity = new ObjectMapper().readValue(payload, QualityGate.class);
            } catch (IOException ex) {
                reasonForException = ex.getLocalizedMessage();
            }
        }

        if (type.equalsIgnoreCase("activity")) {
            try {
                newEntity = new ObjectMapper().readValue(payload, Activity.class);
                newEntity.setTier("TIER_3");
                newEntity.setTaskType("ACTIVITY");
                ((Activity) newEntity).calcScheduledWork();
            } catch (IOException ex) {
                reasonForException = ex.getLocalizedMessage();
            }
        }

        if (newEntity == null) {
            throw new IllegalArgumentException("Unknown type of a ScheduledEntity is being tried to be created. " +
            reasonForException);
        }

        return newEntity;
    }

    /**
     *
     * @param payload
     * @return
     */
    public String getTypeFromPayload (String payload) {

        String taskType = "";

        Matcher m1 = ttPattern.matcher(payload.replaceAll("\\s", "").toUpperCase());
        m1.find();
        try {
            taskType = m1.group("tt");
        } catch (IllegalStateException e) {
            //do nothing
        }

        if (taskType.isEmpty()) {
            Matcher m2 = tierPattern.matcher(payload.replaceAll("\\s", "").toUpperCase());
            m2.find();

            try {
                String str2 = m2.group("tier");
                if (str2.equals("TIER_0") || str2.equals("TIER_1") || str2.equals("TIER_2")) {
                    taskType = "TASK";
                }
            } catch (IllegalStateException e) {
                //do nothing
            }
        }
        return taskType;
    }

    /**
     * Send a command via Redis
     * @param msgType
     * @param id
     * @param msg
     */
    public void sendCommand (String msgType, String id, String msg) {
        try {
            template.convertAndSend("COMMANDS",
                    mapper.writeValueAsString(new Message(msgType, id, msg)));
        } catch (JsonProcessingException e) {
            LOGGER.info(e.getLocalizedMessage());
        }
    }

}
