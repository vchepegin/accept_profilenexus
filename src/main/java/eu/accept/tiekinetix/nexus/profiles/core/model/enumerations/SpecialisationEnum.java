package eu.accept.tiekinetix.nexus.profiles.core.model.enumerations;

/**
 * Created by vchepegin on 30/01/2017.
 */
public enum SpecialisationEnum {

    PLUMBER (1),
    CARPENTER (2),
    CONCRETE (3),
    ELECTRICAL (4),
    PAINTER (5),
    ROOFER (6),
    BRICKLAYER (7);

    private final int specialisationCode;

    SpecialisationEnum(int code) {
        this.specialisationCode = Integer.valueOf(code);
    }

    private int getSpecialisationCode() {
        return specialisationCode;
    }

    @Override
    public String toString () {
        String tmp = "Not defined";
        switch (getSpecialisationCode()) {
            case 1:
                tmp = "PLUMBER";
                break;

            case 2:
                tmp = "CARPENTER";
                break;

            case 3:
                tmp = "CONCRETE";
                break;
            case 4:
                tmp = "ELECTRICAL";
                break;
            case 5:
                tmp = "PAINTER";
                break;
            case 6:
                tmp = "ROOFER";
                break;
            case 7:
                tmp = "BRICKLAYER";
                break;
        }

        return tmp;
    }
}
