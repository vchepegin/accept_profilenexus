package eu.accept.tiekinetix.nexus.profiles.reporting.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.QualityGate;
import eu.accept.tiekinetix.nexus.profiles.reporting.KpiReporting;
import eu.accept.tiekinetix.nexus.profiles.util.CopyUtils;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

/**
 * VO for reporting and historisation of QGs
 * Created by vchepegin on 12/06/2017.
 */
@NodeEntity
@JsonIgnoreProperties(ignoreUnknown = true)
public class QualityGateReport extends BaseReport implements KpiReporting {

    @GraphId
    private Long id;

    //this is a separate node in the core model and can be found attached to the GQ itself with entityID
    private double quantityAmount;
    private String quantityUnit;

    public QualityGateReport () {}

    public static QualityGateReport getInstance (QualityGate qg) {
        QualityGateReport qgr = new QualityGateReport();

        return CopyUtils.copy(qg, qgr);
    }


    @JsonProperty("quantityAmount")
    public double getQuantityAmount() {
        return quantityAmount;
    }

    @JsonProperty("quantityAmount")
    public void setQuantityAmount(double quantityAmount) {
        this.quantityAmount = quantityAmount;
    }

    @JsonProperty("quantityUnit")
    public String getQuantityUnit() {
        return quantityUnit;
    }

    @JsonProperty("quantityUnit")
    public void setQuantityUnit(String quantityUnit) {
        this.quantityUnit = quantityUnit;
    }
}
