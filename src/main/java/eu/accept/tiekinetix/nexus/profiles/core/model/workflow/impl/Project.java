package eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by vchepegin on 01/06/2017.
 */
@NodeEntity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Project implements Serializable {

    private static final long serialVersionUID = -7651361534809990531L;

    @GraphId
    private Long id;

    private String projectID;
    private String projectName;

    private String clientName;
    private String country;
    private String address;

    private int timeZoneDifferenceToUTC;

    public Project () {}

    @Relationship(type="hasTask", direction = Relationship.OUTGOING)
    public Set<Task> tasks;
    /**
     * Add new task to the Project
     * @param task
     * @return Task for the fluent interface
     */
    public Project addTask (Task task) {
        if (tasks == null) {
            tasks = new HashSet<>();
        }
        tasks.add(task);

        return this;
    }

    @Relationship(type="hasWorkflow", direction = Relationship.OUTGOING)
    public Set<Workflow> workflows;
    /**
     * Add concrete, project specific workflow to the Project
     * @param workflow
     * @return Task for the fluent interface
     */
    public Project addWorkflow (Workflow workflow) {
        if (workflows == null) {
            workflows = new HashSet<>();
        }
        workflows.add(workflow);

        return this;
    }

    @JsonProperty("projectID")
    public String getProjectID() {
        return projectID;
    }

    @JsonProperty("projectID")
    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    @JsonProperty("projectName")
    public String getProjectName() {
        return projectName;
    }

    @JsonProperty("projectName")
    public void setProjectName(String name) {
        this.projectName = name;
    }

    @JsonProperty("clientName")
    public String getClientName() {
        return clientName;
    }

    @JsonProperty("clientName")
    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    @JsonProperty("country")
    public String getCountry() {
        return country;
    }

    @JsonProperty("country")
    public void setCountry(String country) {
        this.country = country;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("timeZoneDifferenceToUTC")
    public int getTimeZoneDifferenceToUTC() {
        return timeZoneDifferenceToUTC;
    }

    @JsonProperty("timeZoneDifferenceToUTC")
    public void setTimeZoneDifferenceToUTC(int timeZoneDifferenceToUTC) {
        this.timeZoneDifferenceToUTC = timeZoneDifferenceToUTC;
    }


}
