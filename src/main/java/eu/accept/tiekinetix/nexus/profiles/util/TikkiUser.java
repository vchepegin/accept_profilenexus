package eu.accept.tiekinetix.nexus.profiles.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TikkiUser {
    private String email;
    private String id;
    private String _id;
    private String password;
    private String firstName;
    private String lastName;

    @JsonProperty("email")
    public String getEmail() { return this.email;}

    @JsonProperty("email")
    public void setEmail(String email) { this.email = email;}

    @JsonProperty("id")
    public String getId() { return this.id;}

    @JsonProperty("id")
    public void setId(String id) { this.id = id;}

    @JsonProperty("_id")
    public String get_Id() { return this._id;}

    @JsonProperty("_id")
    public void set_Id(String _id) { this._id = _id;}

    @JsonProperty("password")
    public String getPassword() { return this.password;}

    @JsonProperty("password")
    public void setPassword(String password) { this.password = password;}

    @JsonProperty("firstName")
    public String getFirstName() { return this.firstName;}

    @JsonProperty("firstName")
    public void setFirstName(String firstName) { this.firstName = firstName;}

    @JsonProperty("lastName")
    public String getLastName() { return this.lastName;}

    @JsonProperty("lastName")
    public void setLastName(String lastName) { this.lastName = lastName;}
}
