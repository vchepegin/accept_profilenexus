package eu.accept.tiekinetix.nexus.profiles.bim.model;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import static java.util.stream.Collectors.toSet;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

/**
 * Created by peter.merz on 07/09/17.
 */
@Getter
@Setter
@NodeEntity
@XmlRootElement(name = "Room")
@ToString(callSuper = true)
public class BimRoom extends BimEntity implements HasEmbeddedParameters {

    @JacksonXmlProperty(localName = "Class")
    @JsonProperty("class")
    private String clazz;
    @JacksonXmlProperty(localName = "Guid")
    private String guid;
    @JacksonXmlProperty(localName = "Parameter")
    @JacksonXmlElementWrapper(localName = "Parameters")
    private Set<BimParameter> parameters = new HashSet<>();
    @JacksonXmlProperty(localName = "Name")
    private String name;
    @JacksonXmlProperty(localName = "IdLevel")
    private String idLevel;
    @JacksonXmlProperty(localName = "Height")
    private Double height;
    @JacksonXmlProperty(localName = "Perimeter")
    private Double perimeter;
    @JacksonXmlProperty(localName = "Area")
    private Double area;


    @Override
    public Set<Long> getParamIds(final String name) {
        return getParamsByName(name).stream()
                .map(BimParameter::getValue)
                .map(Long::parseLong)
                .collect(toSet());
    }

    @Override
    public Set<BimParameter> getParamsByName(String name) {
        return parameters.stream()
                .filter(p -> name.equals(p.getName()))
                .distinct()
                .collect(toSet());
    }

    @Override
    public Set<BimParameter> getParameters() {
        return parameters;
    }

    @Override
    public  void setParameters(Set<BimParameter> parameters) {
        this.parameters = parameters;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("IdLevel")
    public String getIdLevel() {
        return idLevel;
    }

    @JsonProperty("IdLevel")
    public void setIdLevel(String idLevel) {
        this.idLevel = idLevel;
    }

    @JsonProperty("Height")
    public Double getHeight() {
        return height;
    }

    @JsonProperty("Height")
    public void setHeight(Double height) {
        this.height = height;
    }

    @JsonProperty("Perimeter")
    public Double getPerimeter() {
        return perimeter;
    }

    @JsonProperty("Perimeter")
    public void setPerimeter(Double perimeter) {
        this.perimeter = perimeter;
    }

    @JsonProperty("Area")
    public Double getArea() {
        return area;
    }

    @JsonProperty("Area")
    public void setArea(Double area) {
        this.area = area;
    }
}
