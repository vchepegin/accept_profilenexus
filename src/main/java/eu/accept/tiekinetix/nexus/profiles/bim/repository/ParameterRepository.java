package eu.accept.tiekinetix.nexus.profiles.bim.repository;

import java.util.List;
import java.util.Set;

import eu.accept.tiekinetix.nexus.profiles.bim.model.BimParameter;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface ParameterRepository extends GraphRepository<BimParameter> {
    List<BimParameter> findByProjectProjectIDAndBimId(String projectID, Long bimId);
    List<BimParameter> findAllByProjectProjectIDAndBimId(String projectID, Set<Long> collect);


    List<BimParameter> findByProjectProjectIDAndGuid(String projectID, String guid);
}
