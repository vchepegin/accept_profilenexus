package eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.CompletionStatus;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.ScheduleStatus;
import eu.accept.tiekinetix.nexus.profiles.core.model.user.Crew;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.Constructable;
import eu.accept.tiekinetix.nexus.profiles.reporting.Processable;
import eu.accept.tiekinetix.nexus.profiles.reporting.processing.KPIProcessorService;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.springframework.beans.factory.annotation.Configurable;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static eu.accept.tiekinetix.nexus.profiles.util.DateUtils.dateAfterDays;
import static eu.accept.tiekinetix.nexus.profiles.util.DateUtils.dateAfterWorkingDays;
import static eu.accept.tiekinetix.nexus.profiles.util.DateUtils.getPureWbs;

/**
 * Created by vchepegin on 15/03/2017.
 */
@Configurable
@NodeEntity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Activity extends Task implements Constructable, Processable {

    private static final long serialVersionUID = 5587521343667454050L;

    @GraphId
    private long activityID;

    private double dailyGoal;
    private double dynamicDailyGoal;
    private double workingHoursPerDay;
    private int plannedNumberOfCrews;
    // one property of type Quantity is inherited from Task
    // private Quantity consumptionRate;
    private String reasonForNonCompletion;

    private String comment;
    private double contributionToActivityCompletion;
//    private double contributionToTaskCompletion;
    private double cumulativeDailyGoals;
    private double cumulativeDailyPAR;
    private boolean dailyCheckCritical;
    private double didAbsolute;
    private double didAbsoluteWorker;
    private double didPercent;
    private double didPercentWorker;
    private boolean goalAchieved;
    private int numberOfWorkers;
    private double PAR_average;
    private double PAR_monitored;
    private Quantity pricePerUnit;
    private double dynamicDailyGoalNextDay;
    private double delayIndicator;

    private Date dateValidated;

    @Override
    public void acceptKPIRequest(KPIProcessorService processor) {
        processor.processKPI(this);
    }

//    @Override
//    public void wrapUp() {
//
//    }

    /**
     * "will be recalculated as soon as a new PAR value will be added"
     *
     * So their should be methods to calculate al those values and they
     * should not be stored in this entity
     */
//    private double percentageOfTotalPitches;
//    private int goalPerDayAchivedPercent;
//    private double averagePARvalue;
//    private double degreeOfActivityCompletion;
//    private double contributionToOverallProgress;
//    private double numberOfWorker;
//    private int numberOfCrews;


    /**
     * Copy constructor. Note, that the activityID has to be set separately AFTER the creation of a new instance!
     *
     * @param activity
     * @return
     */
    public Activity(Activity activity) {

        super(activity);

        this.dailyGoal = activity.getDailyGoal();
        this.workingHoursPerDay = activity.getWorkingHoursPerDay();
        //RDI Issue #94
        this.plannedNumberOfCrews = (activity.plannedNumberOfCrews == 0) ? 1 : activity.plannedNumberOfCrews;
        this.reasonForNonCompletion = activity.reasonForNonCompletion;
        if (activity.consumptionRate != null) {
            this.consumptionRate = new Quantity(activity.consumptionRate);
        }

        if (activity.quantity != null) {
            this.quantity = new Quantity(activity.quantity);
        }

        wrapUp();
        calcScheduledWork();

        //TODO: should it be AFTER calling calcScheduledWork();?
        this.setDynamicDailyGoal(this.getDailyGoal());

//        if (activity.getPAR_Value() != null) {
//            this.PAR_Value = Arrays.copyOf(activity.getPAR_Value(), activity.getPAR_Value().length);
//        }
    }

    public Activity(String entityId, String name, String description, Double timeout, ScheduleStatus scheduleStatus,
                    CompletionStatus completionStatus, String wbsCode, String lbsCode,
                    int priority) {

        super(entityId, name, description, timeout, "TIER_3", scheduleStatus, completionStatus,
                wbsCode, lbsCode, priority);

        if (this.getScheduleStatus() == null) {
            this.setScheduleStatus(ScheduleStatus.ON_SCHEDULE);
        }

        if (this.getCompletionStatus() == null) {
            this.setCompletionStatus(CompletionStatus.TO_DO);
        }

        if (this.getPriority() == 0) {
            this.setPriority(2);
        }

        this.plannedNumberOfCrews = 1;
        wrapUp();
        calcScheduledWork();

        //super.setTier("TIER_3");
    }


    public Activity() {
    }

    @Override
    public Map<String, Object> construct(Map parameters) {

        if (!this.initialized) {
            setLbsCode(parameters.get("lbsCode").toString());

            setWbsCode(parameters.get("wbsCode").toString() + "." + getPureWbs(getWbsCode()));

            setCompletionStatus(CompletionStatus.TO_DO);
            //TODO: if I reset the number of crews here then if in the template it is different the result will be wrong
            if (this.plannedNumberOfCrews == 0) {
                this.plannedNumberOfCrews = 1;
            }
            this.initialized = true;
        }

        Quantity consumptionRate = getConsumptionRate();
        this.dailyGoal = workingHoursPerDay / consumptionRate.getAmount();
        this.dynamicDailyGoal = dailyGoal;

        //double quantity = (double) parameters.get("quantity");
        //all those transformations to int cause loose of precision. It is better to store and calculate in double
        calcScheduledWork();

        //#127
        this.setRemainingWorkingDays(this.getScheduledWorkingDays());
        Quantity quant = getQuantity();
        if (quant != null) {
            this.setRemainingToDosAbsolute(quant.getAmount());
        }

//        long interval = 2;
        long interval = Math.round(getTimeout());
        /*
        if (this.getTimeout() == 1) {
            interval = 1;
        }
        if (getTimeout() >= 2) {
            interval = Math.round(getTimeout() + 1);
        }
        setStartDate(dateAfterDays(getStartDate(), interval - 1));
        */
        setStartDate(dateAfterDays(getStartDate(), interval));
        setEndDate(dateAfterWorkingDays(this.getStartDate(), getScheduledWorkingDays()-1));

        LocalDate startDate = getStartDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate endDate = getEndDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        double duration = ChronoUnit.DAYS.between(startDate, endDate);
        setDuration(duration + 1d);

//        logger.debug(getWbsCode() + " : " + getNumberOfWorkingDays() +
//                " : start date " + getStartDate() + " till " + getEndDate());

        return parameters;
    }


    /**
     * Calculations of scheduled working days on the same instance of Activity
     */
    public void calcScheduledWork() {

        //RDI Issue #94
        if (plannedNumberOfCrews == 0) {
            this.plannedNumberOfCrews = 1;
        }

        double oldSWD = this.getScheduledWorkingDays();

        if (this.getCumulativeDaysMonitored() < 0) {
            throw new IllegalArgumentException("cumulativeDaysScheduled could not be negative.");
        }

        int effectiveNCrews = this.getPlannedNumberOfCrews();
        if (this.crews != null) {
            effectiveNCrews = this.crews.size() > this.getPlannedNumberOfCrews() ?
                    this.crews.size() : this.getPlannedNumberOfCrews();
        }

        Double performance = this.getWorkingHoursPerDay() * effectiveNCrews;
        if (performance == 0 || this.consumptionRate == null || this.quantity == null) {
            this.setScheduledWorkingDays(0);
        } else {
            Double tmp = (this.consumptionRate.getAmount() * this.quantity.getAmount()) / performance + 0.375;
            this.setScheduledWorkingDays((int) Math.round(tmp));
        }

        if (this.getCumulativeDaysMonitored() > 0 && this.getScheduledWorkingDays() > 0) {
            this.setScheduledWorkingDays(Math.round(this.getScheduledWorkingDays() * (this.getScheduledWorkingDays() -
                    this.getCumulativeDaysMonitored()) /
                    (this.getScheduledWorkingDays() + this.getCumulativeDaysMonitored() + 0.375)));
        }

        if (oldSWD != this.getScheduledWorkingDays()) {
            /*
             * if scheduledWorkingDays has changed, then we also have to adjust remainingWorkingDays, due to
             * PKI calculation later we have to adapt endDate; but this has to be clarified, whether it will
             * be done automatically or per notification
             */
            this.setRemainingWorkingDays(Math.max(this.getScheduledWorkingDays() -
                    this.getCumulativeDaysMonitored(), 1));

            this.setRemainingToDosAbsolute(Math.max(this.getQuantity().getAmount() -
                    this.getCumulativeDidGoals(), 0));
        }

        if (this.getScheduledWorkingDays() <= 1.0) {
            this.setDailyGoal(this.quantity.getAmount());
            this.setDynamicDailyGoal(this.quantity.getAmount());
        } else {
            int pnc = this.getPlannedNumberOfCrews() > 0 ? this.getPlannedNumberOfCrews() : 1;
            this.setDailyGoal(this.quantity.getAmount() / this.getScheduledWorkingDays());
            //issue #110
            if (this.getRemainingWorkingDays() == 0) {
                this.setDynamicDailyGoal(this.getDailyGoal());
            } else {
                this.setDynamicDailyGoal(this.getRemainingToDosAbsolute() / this.getRemainingWorkingDays());
            }
        }
    }

    /**
     * Reset of the cumulative parameters aka rewind to the previous state that it was before the last update
     */
    public void resetKPIrelevantParameters() {
        //Issue #113
        this.setCumulativeDaysMonitored(Math.max(0,
                this.getCumulativeDaysMonitored() - 1));
        this.setCumulativeDidGoals(this.getCumulativeDidGoals() - this.getDidAbsolute());
        this.setCumulativeDailyGoals(this.getCumulativeDailyGoals() - this.getDailyGoal());
        this.setCumulativeDailyPAR(this.getCumulativeDailyPAR() - this.getPAR_monitored());
        this.setProgress(this.getProgress() - this.getContributionToActivityCompletion());
        this.setCumulativeGoalAchieved(this.getCumulativeGoalAchieved() - (int) (this.getpPCofDay() / 100));
        this.setVerified(false);
        if (this.getQuantity() == null) {
            throw new IllegalArgumentException("Activity MUST have quantity in order to perform KPI calculations");
        }
        this.setRemainingToDosAbsolute(Math.max(this.getQuantity().getAmount() -
                this.getCumulativeDidGoals(), 0));

        this.setRemainingWorkingDays(Math.max(this.getScheduledWorkingDays() -
                this.getCumulativeDaysMonitored(), 1));
    }

    @Relationship(type = "hasConsumptionRate", direction = Relationship.OUTGOING)
    public Quantity consumptionRate;

    /**
     * Add an quantity to the ScheduledEntity
     *
     * @param consumptionRate
     * @return ScheduledEntity for the fluent interface
     */
    public ScheduledEntity addConsumptionRate(Quantity consumptionRate) {
        this.consumptionRate = consumptionRate;

        return this;
    }

    @JsonProperty("consumptionRate")
    public Quantity getConsumptionRate() {
        return this.consumptionRate;
    }

    @Relationship(type = "hasCrew", direction = Relationship.OUTGOING)
    public Set<Crew> crews;

    /**
     * Add a crew to the Task
     *
     * @param crew
     * @return Task for the fluent interface
     */
    public Activity addCrew(Crew crew) {
        if (crews == null) {
            crews = new HashSet<>();
        }

        if (!crews.contains(crew)) {

            crews.add(crew);

            if (crews.size() > this.plannedNumberOfCrews) {
                calcScheduledWork();
            }
        }

        return this;
    }

    /**
     * Remove a crew from this activity
     *
     * @param crew
     */
    public void removeCrew(Crew crew) {
        if (crews != null) {
            crews.remove(crew);
        }

        if (crews != null && crews.size() >= this.plannedNumberOfCrews) {
            calcScheduledWork();
        }

        //TODO: if size becomes less than plannedNumberOfCrews NOTIFY
    }

    @JsonProperty("crews")
    public Set<Crew> getCrews() {
        return crews;
    }

    @JsonProperty("crews")
    public void setCrews(Set<Crew> crews) {
        this.crews = crews;
    }

    @JsonProperty("dailyGoal")
    public double getDailyGoal() {
        return dailyGoal;
    }

    @JsonProperty("dailyGoal")
    public void setDailyGoal(double dailyGoal) {
        this.dailyGoal = dailyGoal;
    }

    @JsonProperty("dynamicDailyGoal")
    public double getDynamicDailyGoal() {
        return dynamicDailyGoal;
    }

    @JsonIgnore
    public void setDynamicDailyGoal(double dynamicDailyGoal) {
        this.dynamicDailyGoal = dynamicDailyGoal;
    }

    @JsonProperty("reasonForNonCompletion")
    public String getReasonForNonCompletion() {
        return reasonForNonCompletion;
    }

    @JsonProperty("reasonForNonCompletion")
    public void setReasonForNonCompletion(String reasonForNonCompletion) {
        this.reasonForNonCompletion = reasonForNonCompletion;
    }

    @JsonProperty("workingHoursPerDay")
    public double getWorkingHoursPerDay() {
        return workingHoursPerDay;
    }

    @JsonProperty("workingHoursPerDay")
    public void setWorkingHoursPerDay(double workingHoursPerDay) {
        this.workingHoursPerDay = workingHoursPerDay;
    }

    @JsonProperty("assignedNumberOfCrews")
    public int getNumberOfAssignedCrews() {
        if (crews == null) return 0;
        return crews.size();
    }

    @JsonProperty("plannedNumberOfCrews")
    public int getPlannedNumberOfCrews() {
        return plannedNumberOfCrews;
    }

    public void setPlannedNumberOfCrews(int plannedNumberOfCrews) {
        this.plannedNumberOfCrews = plannedNumberOfCrews;
    }

    @JsonProperty("comment")
    public String getComment() {
        return comment;
    }

    @JsonProperty("comment")
    public void setComment(String comment) {
        this.comment = comment;
    }

    @JsonIgnore
    public double getContributionToActivityCompletion() {
        return contributionToActivityCompletion;
    }

    @JsonProperty("contributionToActivityCompletion")
    public void setContributionToActivityCompletion(double contributionToActivityCompletion) {
        this.contributionToActivityCompletion = contributionToActivityCompletion;
    }
/*
    @JsonIgnore
    public double getContributionToTaskCompletion() {
        return contributionToTaskCompletion;
    }

    @JsonProperty("contributionToTaskCompletion")
    public void setContributionToTaskCompletion(double contributionToTaskCompletion) {
        this.contributionToTaskCompletion = contributionToTaskCompletion;
    }
*/
    @JsonProperty("cumulativeDailyGoals")
    public double getCumulativeDailyGoals() {
        return cumulativeDailyGoals;
    }

    @JsonProperty("cumulativeDailyGoals")
    public void setCumulativeDailyGoals(double cumulativeDailyGoals) {
        this.cumulativeDailyGoals = cumulativeDailyGoals;
    }

    @JsonProperty("cumulativeDailyPAR")
    public double getCumulativeDailyPAR() {
        return cumulativeDailyPAR;
    }

    @JsonProperty("cumulativeDailyPAR")
    public void setCumulativeDailyPAR(double cumulativeDailyPAR) {
        this.cumulativeDailyPAR = cumulativeDailyPAR;
    }

    @JsonProperty("dailyCheckCritical")
    public boolean isDailyCheckCritical() {
        return dailyCheckCritical;
    }

    @JsonProperty("dailyCheckCritical")
    public void setDailyCheckCritical(boolean dailyCheckCritical) {
        this.dailyCheckCritical = dailyCheckCritical;
    }

    @JsonProperty("didAbsolute")
    public double getDidAbsolute() {
        return didAbsolute;
    }

    @JsonProperty("didAbsolute")
    public void setDidAbsolute(double didAbsolute) {
        this.didAbsolute = didAbsolute;
    }

    @JsonProperty("didAbsoluteWorker")
    public double getDidAbsoluteWorker() {
        return didAbsoluteWorker;
    }

    @JsonProperty("didAbsoluteWorker")
    public void setDidAbsoluteWorker(double didAbsoluteWorker) {
        this.didAbsoluteWorker = didAbsoluteWorker;
    }

    @JsonProperty("didPercent")
    public double getDidPercent() {
        return didPercent;
    }

    @JsonProperty("didPercent")
    public void setDidPercent(double didPercent) {
        this.didPercent = didPercent;
    }

    @JsonProperty("didPercentWorker")
    public double getDidPercentWorker() {
        return didPercentWorker;
    }

    @JsonProperty("didPercentWorker")
    public void setDidPercentWorker(double didPercentWorker) {
        this.didPercentWorker = didPercentWorker;
    }

    @JsonProperty("goalAchieved")
    public boolean isGoalAchieved() {
        return goalAchieved;
    }

    @JsonProperty("goalAchieved")
    public void setGoalAchieved(boolean goalAchieved) {
        this.goalAchieved = goalAchieved;
    }

    @JsonIgnore
    public int getNumberOfWorkers() {
        return numberOfWorkers;
    }

    @JsonProperty("numberOfWorkers")
    public void setNumberOfWorkers(int numberOfWorkers) {
        this.numberOfWorkers = numberOfWorkers;
    }

    @JsonProperty("PAR_average")
    public double getPAR_average() {
        return PAR_average;
    }

    @JsonProperty("PAR_average")
    public void setPAR_average(double PAR_average) {
        this.PAR_average = PAR_average;
    }

    @JsonProperty("PAR_monitored")
    public double getPAR_monitored() {
        return PAR_monitored;
    }

    @JsonProperty("PAR_monitored")
    public void setPAR_monitored(double PAR_monitored) {
        this.PAR_monitored = PAR_monitored;
    }

    @JsonProperty("pricePerUnit")
    public Quantity getPricePerUnit() {
        return pricePerUnit;
    }

    @JsonProperty("pricePerUnit")
    public void setPricePerUnit(Quantity pricePerUnit) {
        this.pricePerUnit = pricePerUnit;
    }

    @JsonIgnore
    public double getDynamicDailyGoalNextDay() {
        return dynamicDailyGoalNextDay;
    }

    @JsonProperty("dynamicDailyGoalNextDay")
    public void setDynamicDailyGoalNextDay(double dynamicDailyGoalNextDay) {
        this.dynamicDailyGoalNextDay = dynamicDailyGoalNextDay;
    }

    public Date getDateValidated() {
        if (dateValidated == null) return null;
        return new Date(dateValidated.getTime());
    }

    public void setDateValidated(Date dateValidated) {
        if (dateValidated != null)
            this.dateValidated = new Date(dateValidated.getTime());
    }

    @JsonProperty("delayIndicator")
    public double getDelayIndicator() {
        return delayIndicator;
    }

    @JsonProperty("delayIndicator")
    public void setDelayIndicator(double delayIndicator) {
        this.delayIndicator = delayIndicator;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Activity)) return false;
        if (!super.equals(o)) return false;

        Activity activity = (Activity) o;

        if (getPlannedNumberOfCrews() != activity.getPlannedNumberOfCrews()) return false;
        if (isGoalAchieved() != activity.isGoalAchieved()) return false;
        if (getComment() != null ? !getComment().equals(activity.getComment()) : activity.getComment() != null)
            return false;
        return getConsumptionRate() != null ? getConsumptionRate().equals(activity.getConsumptionRate()) : activity.getConsumptionRate() == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getPlannedNumberOfCrews();
        result = 31 * result + (getComment() != null ? getComment().hashCode() : 0);
        result = 31 * result + (isGoalAchieved() ? 1 : 0);
        result = 31 * result + (getConsumptionRate() != null ? getConsumptionRate().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Activity{ ID=" + getEntityID() +
                ", dailyGoal=" + dailyGoal +
                ", workingHoursPerDay=" + workingHoursPerDay +
                ", plannedNumberOfCrews=" + plannedNumberOfCrews +
                ", goalAchieved=" + goalAchieved +
                ", owner=" + owner +
                ", location=" + location +
                '}';
    }

    @Override
    public void reset() {
        super.reset();
        dynamicDailyGoal = dailyGoal;
        dynamicDailyGoalNextDay = dailyGoal;
        contributionToActivityCompletion = 0;
        cumulativeDailyGoals = 0;
        cumulativeDailyPAR = 0;
        dailyCheckCritical = false;
        didAbsolute = 0;
        didAbsoluteWorker = 0;
        didPercent = 0;
        didPercentWorker = 0;
        PAR_average = 0;
        PAR_monitored = 0;
        delayIndicator = 0;
        dateValidated = null;
        comment = "";
        reasonForNonCompletion = "";
    }
}