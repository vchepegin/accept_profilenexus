package eu.accept.tiekinetix.nexus.profiles.bim.repository;

import java.util.List;

import eu.accept.tiekinetix.nexus.profiles.bim.model.BimType;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface TypeRepository extends GraphRepository<BimType> {
    List<BimType> findByProjectProjectIDAndBimId(String projectID, Long bimId);

//    @Query("MATCH (s:ScheduledEntity)<-[r*1..4]- (p:Project) where s.wbsCode = $wbsCode AND p.projectID = $projectID MATCH (b:BimType) WHERE b.bimId = $bimId CREATE (b)-[rb:SCHEDULEDENTITY] -> (s)")
    @Query("MATCH (s:ScheduledEntity)<-[r*1..4]- (p:Project) where s.wbsCode = {1} AND p.projectID = {2} " +
            "MATCH (b:BimType) WHERE b.bimId = {0} CREATE (b)-[rb:SCHEDULEDENTITY] -> (s)")
    void attachBimTypeToTaskByWbs(@Param("0")Long bimId, @Param("1")String wbsCode, @Param("2")String projectID);

//    @Query("MATCH (s:ScheduledEntity)<-[r*1..4]- (p:Project) where s.lbsCode = $lbsCode AND p.projectID = $projectID MATCH (b:BimType) WHERE b.bimId = $bimId CREATE (b)-[rb:SCHEDULEDENTITY] -> (s)")
    @Query("MATCH (s:ScheduledEntity)<-[r*1..4]- (p:Project) where s.lbsCode = {1} AND p.projectID = {2} " +
            "MATCH (b:BimType) WHERE b.bimId = {0} CREATE (b)-[rb:SCHEDULEDENTITY] -> (s)")
    void attachBimTypeToTaskByLbs(@Param("0")Long bimId, @Param("1")String lbsCode, @Param("2")String projectID);

//    @Query("MATCH (s:ScheduledEntity)<-[r*1..4]- (p:Project) where s.wbsCode = $wbsCode AND s.lbsCode = $lbsCode AND p.projectID = $projectID MATCH (b:BimType) WHERE b.bimId = $bimId CREATE (b)-[rb:SCHEDULEDENTITY] -> (s)")
    @Query("MATCH (s:ScheduledEntity)<-[r*1..4]- (p:Project) where s.wbsCode = {1} AND s.lbsCode = {2} " +
            "AND p.projectID = {3} MATCH (b:BimType) WHERE b.bimId = {0} CREATE (b)-[rb:SCHEDULEDENTITY] -> (s)")
    void attachBimTypeToTaskByWbsAndLbs(@Param("0")Long bimId, @Param("1")String wbsCode, @Param("2")String lbsCode,
                                        @Param("3")String projectID);

//    @Query("MATCH (s:ScheduledEntity)<-[r*1..4]- (p:Project) where p.projectID = $projectID MATCH (b:BimType)- [rs:SCHEDULEDENTITY] -(s) return s " )
    @Query("MATCH (s:ScheduledEntity)<-[r*1..4]- (p:Project) where p.projectID = {0} " +
            "MATCH (b:BimType)- [rs:SCHEDULEDENTITY] -(s) return s " )
    List<ScheduledEntity> getScheduledEntitiesOfBimTypes(@Param("0")String projectID);
}
