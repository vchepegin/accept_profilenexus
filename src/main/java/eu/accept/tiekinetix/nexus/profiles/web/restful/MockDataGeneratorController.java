//package eu.accept.tiekinetix.nexus.profiles.controllers;
//
//import TimeFrame;
//import eu.accept.tiekinetix.nexus.profiles.model.enumerations.*;
//import eu.accept.tiekinetix.nexus.profiles.model.user.*;
//import Location;
//import Quantity;
//import Task;
//import ValueRange;
//import eu.accept.tiekinetix.nexus.profiles.repositories.CrewRepository;
//import eu.accept.tiekinetix.nexus.profiles.repositories.TaskRepository;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.Calendar;
//import java.util.Date;
//import java.util.TimeZone;
//import java.util.concurrent.atomic.AtomicInteger;
//
//import static eu.accept.tiekinetix.nexus.profiles.mocking.RandomPersonsFactory.getNewRandomWorker;
//import static eu.accept.tiekinetix.nexus.profiles.mocking.RandomPersonsFactory.getRandomSpecialisation;
//
///**
// * A collection of methods that has to be used ONLY for the debugging and development phase. They cannot be executed
// * agains the production or demo servers and they should have limited use for the testing environment as well becasue
// * they create side effects and thus might lead to unexpected results in tests.
// *
// * Created by vchepegin on 31/01/2017.
// */
//@CrossOrigin
//@RestController
//public class MockDataGeneratorController {
//
//    @Autowired //don't forget the setter
//    private CrewRepository crewRepository;
//    @Autowired
//    private TaskRepository taskRepository;
//
//    private static AtomicInteger crewsCounter = new AtomicInteger();
//
//    @CrossOrigin
//    @RequestMapping(value="/generateMockCrews/{numberOfCrews}/{numberOfMembers}", method= RequestMethod.POST)
//    public void generateMockCrews(@PathVariable(value="numberOfCrews") int numberOfCrews,
//                                  @PathVariable(value="numberOfMembers") int numberOfMembers) {
//
//        Person newPerson;
//        Crew crew2 = null;
//        for (int i = 0; i < numberOfCrews; i++) {
//            crew2 = new Crew("crew_" + crewsCounter,  "Crew" + crewsCounter, CrewType.INTERNAL,
//                    "new random crew #" + crewsCounter);
//            crewsCounter.getAndIncrement();
//            for (int j = 0; j < numberOfMembers; j++) {
//                newPerson = getNewRandomWorker();
//                newPerson.addSpecialisation(getRandomSpecialisation());
//                crew2.addCrewMember(newPerson);
//            }
//            crewRepository.save(crew2);
//        }
//    }
//
//    @CrossOrigin
//    @RequestMapping(value="/wipeWholeGraph", method= RequestMethod.POST)
//    public void wipeWholeGraph() {
//        crewRepository.deleteAll();
//    }
//
//    @CrossOrigin
//    @RequestMapping(value="/generateVHMockGraph", method= RequestMethod.POST)
//    public void generateVHMockGraph() {
//
//        Person craig = new Person("101", "Site Manager Martin", PersonStatus.ACTIVE,
//                "Phillips","Martin", PersonRole.SITE_MANAGER, "en-GB");
//        craig.addSpecialisation(new Specialisation(SpecialisationEnum.ELECTRICAL));
//        craig.addSpecialisation(new Specialisation(SpecialisationEnum.CARPENTER));
//        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
//        calendar.set(2017, Calendar.FEBRUARY, 27);
//        Date startDate = new Date (calendar.getTimeInMillis());
//        calendar.set(2017, Calendar.MARCH, 12);
//        Date endDate = new Date (calendar.getTimeInMillis());
//        Location siteLocation = new Location("Main location", "2nd floor",
//                "sector_56", "main unit", "south-south-west");
//        Quantity quantity = new Quantity("q1", "rolls", 12.2);
//        ValueRange temperatureRange = new ValueRange("temperature", "Celsius degrees",
//                -35.0, 42.99);
//
//        Task task1 = new Task("task_1", "task with ID = #1", "description",
//                ScheduleStatus.ON_SCHEDULE, CompletionStatus.INCOMPLETE,
//                 10, 2, 1.23, 1.0);
//
//        task1.addQuantity(quantity); //.addTemperatureRange(temperatureRange);
//
//        Task task2 = new Task("task_2", "task with ID = #2", "description",
//                 ScheduleStatus.ON_SCHEDULE, CompletionStatus.INCOMPLETE,
//                10, 1, 1.23, 1.2);
//
//        Task task3 = new Task("task_3", "task with ID = #3", "description",
//                ScheduleStatus.ON_SCHEDULE, CompletionStatus.INCOMPLETE,
//                1, 3,1.23, 0.75);
//
//        task1.addSuccessor(task2).addSuccessor(task3);
//
//        Task task4 = new Task("task_4", "task with ID = #4", "description",
//                ScheduleStatus.ON_SCHEDULE, CompletionStatus.INCOMPLETE,
//                12, 3, 1.1, 0.75);
//
//        task3.addSuccessor(task4);
//        task2.addSuccessor(task4);
//
//        Task workPackage = new Task("Work Package : foundations", "concrete foundations",
//                "description",
//                ScheduleStatus.BEHIND_SCHEDULE, CompletionStatus.INCOMPLETE,
//                12, 1, 0.6, 0.53);
//
//        workPackage.addChild(task1);
//
//        Task digGround = new Task("Workflow_123", "how to dig a whole in the ground",
//                "description",
//                ScheduleStatus.BEHIND_SCHEDULE, CompletionStatus.INCOMPLETE,
//                14, 1, 0.77, 0.88);
//
//        task2.addChild(digGround);
//
//        taskRepository.save(workPackage);
//    }
//
//    @CrossOrigin
//    @RequestMapping(value="/generatePreparedGraph", method= RequestMethod.POST)
//    public void generatePreparedGraph() {
//
//        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
//        calendar.set(2016, Calendar.JULY, 21);
//        Date issuedAt = new Date (calendar.getTimeInMillis());
//        calendar.set(2020, Calendar.JULY, 20);
//        Date validUntil = new Date (calendar.getTimeInMillis());
//        Certificate certificate = new Certificate("#12345", "How to paint the wall " +
//                "right in blue with yeallow colour", "A very necessary certificate",
//                "WeEducatePainters",
//                issuedAt, validUntil);
//
//
//        calendar.set(2011, Calendar.JULY, 12);
//        Date activeFrom1 = new Date (calendar.getTimeInMillis());
//        Skill skill1 = new Skill("skill_0_1","Painting blue", "He can paint in blue",
//                "Quality master that can work with blue color",
//                activeFrom1);
//
//        calendar.set(2014, Calendar.MARCH, 9);
//        Date activeFrom2 = new Date (calendar.getTimeInMillis());
//        Skill skill2 = new Skill("skill_0_2","The Great Carpenter", "carpet everywhere",
//                "Floors are ready in no time with the high quality",
//                activeFrom2);
//        skill1.addCertificate(certificate);
//
//        JobData jobData1 = new JobData("jobData_$12",0.5, "high",
//                "Just a great professional");
//
//        Person paul  = new Person("1", "Worker Paul", PersonStatus.ACTIVE,"Charlston",
//                "Paul", PersonRole.WORKER, "en-GB");
//
//        paul.addSpecialisation(new Specialisation(SpecialisationEnum.PAINTER)).
//                addSpecialisation(new Specialisation(SpecialisationEnum.ROOFER));
//        paul.addPerformanceIndicator(jobData1);
//        paul.addSkill(skill1);
//        paul.addSkill(skill2);
//
//        Person roy   = new Person("2", "Worker Menson", PersonStatus.ACTIVE,"Menson",
//                "Roy", PersonRole.WORKER, "NL-be");
//        roy.addSpecialisation(new Specialisation(SpecialisationEnum.CONCRETE));
//        ContactDetails royContacts = new ContactDetails("contactDetails_#123",
//                "056", "Belgium",
//                "Brussels", "Rue du Canard", "+32 555 55 55",
//                "example@accept.eu");
//        roy.setContactDetails(royContacts);
//
//
//        Person craig = new Person("3", "Worker Craig", PersonStatus.ACTIVE,"McArthur",
//                "Craig", PersonRole.WORKER, "en-GB");
//        craig.addSpecialisation(new Specialisation(SpecialisationEnum.ROOFER)).
//                addSpecialisation(new Specialisation(SpecialisationEnum.PLUMBER));
//        calendar.set(2017, Calendar.JANUARY, 27);
//        Date startDate = new Date (calendar.getTimeInMillis());
//        calendar.set(2017, Calendar.FEBRUARY, 26);
//        Date endDate = new Date (calendar.getTimeInMillis());
//        Task task1 = new Task("task_987", "task with ID = #987",
//                "Put the wires in block #16",
//                 ScheduleStatus.ON_SCHEDULE, CompletionStatus.INCOMPLETE,
//                10, 1,1.23, 1.0);
//
//        calendar.set(2014, Calendar.OCTOBER, 1);
//        Date availableAt = new Date (calendar.getTimeInMillis());
//        Equipment equipment = new Equipment("#eq_678", "Screw driver",
//                EquipmentStatus.AVAILABLE, "basic tools", availableAt);
//
//        task1.addEquipment(equipment);
//
//        calendar.set(2017, Calendar.MAY, 1);
//        Date leaveStartDate = new Date (calendar.getTimeInMillis());
//        calendar.set(2017, Calendar.MAY, 15);
//        Date leaveEndDate = new Date (calendar.getTimeInMillis());
//        TimeFrame craigLeave = new TimeFrame("xyz_onLeave_456", leaveStartDate,
//                leaveEndDate, "children's holidays");
//        craig.addTask(task1).addOnLeaveEvent(craigLeave);
//
//        //List<Person> crewMembers = Arrays.asList(paul, roy, craig);
//
//        Crew crew = new Crew("Test crew",  "Super Crew", CrewType.INTERNAL,
//                "workers rule");
//
//        calendar.set(2017, Calendar.FEBRUARY, 11);
//        Date startDate2 = new Date (calendar.getTimeInMillis());
//        calendar.set(2017, Calendar.APRIL, 29);
//        Date endDate2 = new Date (calendar.getTimeInMillis());
//        Task task = new Task("task111", "task with ID = #111",
//                "Task in block #17",
//                ScheduleStatus.ON_SCHEDULE, CompletionStatus.INCOMPLETE,
//                12, 2,1.1, 1.45);
//
////                ("Activity123",
////                "Activity with wires", "Set up all electrical network for building #12",
////                ScheduleStatus.ON_SCHEDULE, CompletionStatus.INCOMPLETE,
////                10.0, Priority.CRITICAL, 3456.67, 1.23, 1.0,
////                1.2, "activity status 987", 4);
//
//
//        crew.addCrewMember(paul).addCrewMember(roy).addCrewMember(craig).addTask(task);
//
//        crewRepository.save(crew);
//    }
//
//
//
//}
