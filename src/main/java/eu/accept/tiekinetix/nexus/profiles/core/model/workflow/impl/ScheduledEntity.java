package eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Strings;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.CompletionStatus;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.ScheduleStatus;
import eu.accept.tiekinetix.nexus.profiles.core.model.user.Person;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.KPIRecord;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.Prepared;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.behaviour.AopMonitor;
import eu.accept.tiekinetix.nexus.profiles.reporting.Processable;
import eu.accept.tiekinetix.nexus.profiles.reporting.processing.KPIProcessorService;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.Index;
import org.neo4j.ogm.annotation.Relationship;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.util.StringUtils;

import java.io.Serializable;
import java.time.ZoneId;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import static eu.accept.tiekinetix.nexus.profiles.util.DateUtils.simpleDaysBetween;

/**
 * Created by vchepegin on 21/02/2017
 *
 * This class cannot be abstract. I would create this class as a concrete class
 * since we need to instantiate it when using all the methods from SchedulingController API.
 * But due to the {@code GraphId} property managed by SDN it is nto possible.
 */
@AopMonitor
@Configurable
@JsonIgnoreProperties(ignoreUnknown = true)
public class ScheduledEntity implements Comparable<ScheduledEntity>, Processable, Prepared, Serializable {


    private static final long serialVersionUID = 7649713844877576237L;

    @GraphId
    private Long scheduledEntityID;

    @Index(unique=true, primary=true)
    private String entityID;
    private String name;
    private String description;
    private double scheduledWorkingDays;
    private double remainingWorkingDays;
    private double pPCofDay;
    //days after preceding task is DONE - the waiting period to perform this quality check
    private double timeout;

    /** So instead of using language and compiler support
     *  we use a property of a type String that has to be consistently
     *  maintaned in all system components. This is also create less
     *  efficient queries for this model.
     */
    private String tier; //TIER_0, TIER_1, TIER_2; TIER_3_Workflow, TIER_3_Activity
    private String taskType;
    private ScheduleStatus scheduleStatus;
    private CompletionStatus completionStatus;
    private double duration;
    private int priority = 2;
    protected String tikkiQueueID;


    /** The pattern as specified by {@link java.text.SimpleDateFormat} */
//    public static final String ISO_LOCAL_DATE_PATTERN = "yyyy-MM-dd";
//    @JsonDeserialize(using = LocalDateDeserializer.class)
//    @JsonSerialize(using = LocalDateSerializer.class)
    private Date startDate;
//    @JsonDeserialize(using = LocalDateDeserializer.class)
//    @JsonSerialize(using = LocalDateSerializer.class)
    private Date endDate;

    @JsonFormat (shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date dateMonitored;


    private String wbsCode;
    private String lbsCode;

    private boolean verified;

    protected boolean initialized = false;

    /**
     * Copy constructor
     * @param scheduledEntity
     */
    public ScheduledEntity  (ScheduledEntity scheduledEntity) {
        this.name = scheduledEntity.getName();
        this.description = scheduledEntity.getDescription();
        this.tier = scheduledEntity.getTier();
        this.taskType = scheduledEntity.getTaskType();
        this.scheduleStatus = scheduledEntity.getScheduleStatus();
        this.completionStatus = scheduledEntity.getCompletionStatus();
        this.duration = scheduledEntity.getDuration();
        this.priority = scheduledEntity.getPriority();
        this.scheduledWorkingDays = scheduledEntity.getScheduledWorkingDays();
        this.wbsCode = scheduledEntity.getWbsCode();
        this.lbsCode = scheduledEntity.getLbsCode();
        this.timeout = scheduledEntity.getTimeout();

        //TODO: does it really necessary in this cloning constructor?
        if (scheduledEntity.getStartDate() != null) {
            setStartDate(scheduledEntity.getStartDate());
        }

        if (scheduledEntity.getEndDate() != null) {
            setEndDate(scheduledEntity.getEndDate());
        }

        if (scheduledEntity.getDateMonitored() != null) {
            setDateMonitored(scheduledEntity.getDateMonitored());
        }

        checkLists = new HashSet<Checklist>();
        if (scheduledEntity.checkLists != null) {
            for (Checklist checklist : scheduledEntity.checkLists) {
                this.checkLists.add(new Checklist(checklist));
            }
        }

        this.wrapUp();
    }

    @Override
    public void wrapUp() {
        //RDI-story-xyz & Issue#100
        if (this.startDate != null && this.endDate != null) {
            this.duration = simpleDaysBetween(startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(),
                    endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()) + 1;
        }

        //JIRA: RDI-3107
        if (this.scheduleStatus == null) {
            this.scheduleStatus = ScheduleStatus.ON_SCHEDULE;
        }

        //JIRA: RDI-3106
        this.tier = tierType();
    }

    /**
     * Return the taskType based on the logic defined by JIRA RDI-3106
     * @return
     */
    private String tierType () {

        if (Strings.isNullOrEmpty(this.wbsCode)) {
            throw new IllegalArgumentException("WBS code has to be defined in order " +
                    "to deduce the tier of a ScheduledEntity");
        }

        if (this instanceof Activity || this instanceof QualityGate) {
            return "TIER_3";
        }

        int occurance = StringUtils.countOccurrencesOf(this.wbsCode, ".");
        switch (occurance) {
            case 0: return "TIER_0";
            case 1: return "TIER_1";
            default: return "TIER_2";
        }

    }

    /**
     *
     * @return Set<ScheduledEntity> - a set of children
     */
    public Set<ScheduledEntity> getChildren () {
        return children;
    }


    //super(entityId, name, description, tierType, scheduleStatus, completionStatus, wbsCode,
    // lbsCode, priority);
    public ScheduledEntity(String entityID, String name, String description, double timeout,
                           String tier, ScheduleStatus scheduleStatus,
                           CompletionStatus completionStatus,
                           String wbsCode, String lbsCode, int priority) {
        this.entityID = entityID;
        this.name = name;
        this.description = description;
        this.tier = tier;
        this.scheduleStatus = scheduleStatus;
        this.completionStatus = completionStatus;
        this.lbsCode = lbsCode;
        this.wbsCode = wbsCode;
        this.priority = priority;
        this.timeout = timeout;
    }

    public ScheduledEntity () {
        this.scheduleStatus = ScheduleStatus.ON_SCHEDULE;
        this.completionStatus = CompletionStatus.TO_DO;
    }


    @Relationship(type="hasInitialState", direction = Relationship.OUTGOING)
    private InitialState initialState;
    public ScheduledEntity addInitialState (InitialState initialState) {
        if (this.initialState == null) {
            this.initialState =  InitialState.getInstance(initialState);
        }
        return this;
    }

    @JsonProperty("initialState")
    public InitialState getInitialState() {
        return initialState;
    }

    @Relationship(type="hasOwner", direction = Relationship.OUTGOING)
    public Person owner;
    /**
     * Add an owner to the ScheduledEntity
     * @param owner
     * @return ScheduledEntity for the fluent interface
     */
    public ScheduledEntity addOwner (Person owner) {
        this.owner = owner;

        return this;
    }

    /**
     *
     * @return
     */
    public Person getOwner () {
        return owner;
    }

    @Relationship(type="hasLocation", direction = Relationship.OUTGOING)
    public Location location;
    /**
     * Add an location to the ScheduledEntity
     * @param location
     * @return ScheduledEntity for the fluent interface
     */
    public ScheduledEntity addLocation (Location location) {
        this.location = location;

        return this;
    }

//    @Relationship(type="hasQuantity", direction = Relationship.OUTGOING)
//    public Quantity quantity;
//    /**
//     * Add an quantity to the ScheduledEntity
//     * @param quantity
//     * @return ScheduledEntity for the fluent interface
//     */
//    public ScheduledEntity addQuantity (Quantity quantity) {
//        this.quantity = quantity;
//
//        return this;
//    }

    /**
     * Horizontal navigation aka Workflow
     *
     * NOTE: IT IS STRONGLY RECOMMENDED TO USE ONLY one link hasPredassassor or
     *  hasSuccessor in one model but NOT BOTH
     */
//    @Relationship(type="hasPredecessor", direction = Relationship.OUTGOING)
//    public Set<ScheduledEntity> predecessors;
//    /**
//     * Add new predecessor to the ScheduledEntity
//     * @param predecessor
//     * @return ScheduledEntity for the fluent interface
//     */
//    public ScheduledEntity addPredecessor (ScheduledEntity predecessor) {
//        if (predecessors == null) {
//            predecessors = new HashSet<>();
//        }
//        predecessors.add(predecessor);
//
//        return this;
//    }

    @Relationship(type="hasSpecialScheduling", direction = Relationship.OUTGOING)
    @JsonIgnore
    public Set<ScheduledEntity> specialSchedulingRelations;
    /**
     * Add new special temporal/scheduling relationship
     * @param entity
     * @return ScheduledEntity for the fluent interface
     */
    public ScheduledEntity addSpecialSchedulingRelation (ScheduledEntity entity) {
        if (specialSchedulingRelations == null) {
            specialSchedulingRelations = new HashSet<>();
        }
        specialSchedulingRelations.add(entity);

        return this;
    }

    public ScheduledEntity removespecialSchedulingRelation (ScheduledEntity entity) {
        if (specialSchedulingRelations != null) {
            specialSchedulingRelations.remove(entity);
        }

        return this;
    }

    @JsonProperty("specialScheduling")
    public Set<ScheduledEntity> getspecialSchedulingRelations () {
        return specialSchedulingRelations;
    }


    @Relationship(type="hasKpiRecord", direction = Relationship.OUTGOING)
    public Set<KPIRecord> kpiRecords;
    /**
     * Add new KPI record to the ScheduledEntity
     * @param kpiRecord
     * @return ScheduledEntity for the fluent interface
     */
    public ScheduledEntity addKpiRecord (KPIRecord kpiRecord) {
        if (kpiRecords == null) {
            kpiRecords = new HashSet<>();
        }
        kpiRecords.add(kpiRecord);

        return this;
    }

    public ScheduledEntity removeKpiRecord (KPIRecord kpiRecord) {
        if (kpiRecords != null) {
            kpiRecords.remove(kpiRecord);
        }

        return this;
    }

    @JsonIgnore
    public Set<KPIRecord> getKpiRecords () {
        return kpiRecords;
    }


    @Relationship(type="hasSuccessor", direction = Relationship.OUTGOING)
    public Set<ScheduledEntity> successors;
    /**
     * Add new successor to the ScheduledEntity
     * @param successor
     * @return ScheduledEntity for the fluent interface
     */
    public ScheduledEntity addSuccessor (ScheduledEntity successor) {
        if (successors == null) {
            successors = new HashSet<>();
        }
        successors.add(successor);

        return this;
    }

    public ScheduledEntity removeSuccessor (ScheduledEntity successor) {
        if (successors != null) {
            successors.remove(successor);
        }

        return this;
    }

    @JsonProperty("successors")
    public Set<ScheduledEntity> getSuccessors () {
        return successors;
    }

    /**
     * Vertical navigation from building to work packages to tasks to activities and back
     *
     * NOTE: IT IS STRONGLY RECOMMENDED TO USE ONLY one link hasParent or
     *  hadChild in one model but NOT BOTH
     */
//    @Relationship(type="hasParent", direction = Relationship.OUTGOING)
//    public ScheduledEntity parent;
//    /**
//     * Add new parent to the ScheduledEntity
//     * @param parent
//     * @return ScheduledEntity for the fluent interface
//     */
//    public ScheduledEntity addParent (ScheduledEntity parent) {
//        this.parent = parent;
//
//        return this;
//    }

    @Relationship(type="hasChild", direction = Relationship.OUTGOING)
    public Set<ScheduledEntity> children;
    /**
     * Add new child to the ScheduledEntity
     * @param child
     * @return ScheduledEntity for the fluent interface
     */
    public ScheduledEntity addChild (ScheduledEntity child) {
        if (children == null) {
            children = new HashSet<>();
        }
        children.add(child);

        return this;
    }

    @Relationship(type="hasCheckList", direction = Relationship.OUTGOING)
    public Set<Checklist> checkLists;

    /**
     * Add new checkList to the QualityGate
     * @param checkList
     * @return QualityGate for the fluent interface
     */
    public ScheduledEntity addCheckList (Checklist checkList) {
        if (checkLists == null) {
            checkLists = new HashSet<>();
        }
        checkLists.add(checkList);

        return this;
    }

    @JsonProperty("entityID")
    public String getEntityID() {
        return entityID;
    }

    @JsonProperty("entityID")
    public void setEntityID(String entityID) {
        this.entityID = entityID;
    }

    @JsonIgnore
    public double getTimeout() {
        return timeout;
    }

    @JsonProperty("timeout")
    public void setTimeout(double timeout) {
        this.timeout = timeout;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("tier")
    public String getTier() {
        return tier;
    }

    @JsonProperty("tier")
    public void setTier(String tier) {
        this.tier = tier;
    }

    @JsonProperty("scheduleStatus")
    public ScheduleStatus getScheduleStatus() {
        return scheduleStatus;
    }

    @JsonProperty("scheduleStatus")
    public void setScheduleStatus(ScheduleStatus scheduleStatus) {
        this.scheduleStatus = scheduleStatus;
    }

    @JsonProperty("completionStatus")
    public CompletionStatus getCompletionStatus() {
        return completionStatus;
    }

    @JsonProperty("completionStatus")
    public void setCompletionStatus(CompletionStatus completionStatus) {
        this.completionStatus = completionStatus;
    }

    @JsonProperty("duration")
    public double getDuration() {
        return duration;
    }

    @JsonProperty("duration")
    public void setDuration(double duration) {
        this.duration = duration;
    }

    @JsonProperty("priority")
    public int getPriority() {
        return priority;
    }

    @JsonProperty("priority")
    public void setPriority(int priority) {
        this.priority = priority;
    }

//    @JsonFormat(pattern="yyyy-MM-dd")
    @JsonProperty("startDate")
    public Date getStartDate() {
        if (startDate == null) return null;
        return new Date (startDate.getTime());
    }

    @JsonProperty("startDate")
    public void setStartDate(Date startDate) {
        if (startDate != null)
            this.startDate = new Date (startDate.getTime());
    }

//    @JsonFormat(pattern="yyyy-MM-dd")
    @JsonProperty("endDate")
    public Date getEndDate() {
        if (endDate == null) return null;
        return new Date (endDate.getTime());
    }

    @JsonProperty("endDate")
    public void setEndDate(Date endDate) {
        this.endDate = new Date(endDate.getTime());
    }

    @JsonProperty("wbsCode")
    public String getWbsCode() {
        return wbsCode;
    }

    @JsonProperty("wbsCode")
    public void setWbsCode(String wbsCode) {
        this.wbsCode = wbsCode;
    }

    @JsonProperty("lbsCode")
    public String getLbsCode() {
        return lbsCode;
    }

    @JsonProperty("lbsCode")
    public void setLbsCode(String lbsCode) {
        this.lbsCode = lbsCode;
    }

    @JsonProperty("code")
    public String getCode() {
        return getWbsCode() + "_" + getLbsCode();
    }

    @JsonProperty("taskType")
    public String getTaskType() {
        return taskType;
    }

    @JsonProperty("taskType")
    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    @JsonProperty("scheduledWorkingDays")
    public double getScheduledWorkingDays() {
        return scheduledWorkingDays;
    }

    @JsonProperty("scheduledWorkingDays")
    public void setScheduledWorkingDays(double scheduledWorkingDays) {
        this.scheduledWorkingDays = scheduledWorkingDays;
    }

    @JsonProperty("dateMonitored")
    public Date getDateMonitored() {
        if (dateMonitored == null) return null;
        return new Date (dateMonitored.getTime());
    }

    @JsonProperty("dateMonitored")
    public void setDateMonitored(Date dateMonitored) {
        this.dateMonitored = new Date (dateMonitored.getTime());
    }

    @JsonProperty("verified")
    public boolean isVerified() {
        return verified;
    }

    @JsonProperty("verified")
    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    @JsonProperty("remainingWorkingDays")
    public double getRemainingWorkingDays() {
        return remainingWorkingDays;
    }

    @JsonProperty("remainingWorkingDays")
    public void setRemainingWorkingDays(double remainingWorkingDays) {
        this.remainingWorkingDays = remainingWorkingDays;
    }

    @JsonProperty("PPCofDay")
    public double getpPCofDay() {
        return pPCofDay;
    }

    @JsonProperty("PPCofDay")
    public void setpPCofDay(double pPCofDay) {
        this.pPCofDay = pPCofDay;
    }

    @JsonProperty("tikkiQueueID")
    public String getTikkiQueueID() {
        return tikkiQueueID;
    }

    @JsonProperty("tikkiQueueID")
    public void setTikkiQueueID(String tikkiQueueID) { this.tikkiQueueID = tikkiQueueID; }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ScheduledEntity)) return false;

        ScheduledEntity entity = (ScheduledEntity) o;

        if (!getEntityID().equals(entity.getEntityID())) return false;
        if (!getName().equals(entity.getName())) return false;
        if (getDescription() != null ? !getDescription().equals(entity.getDescription()) : entity.getDescription() != null)
            return false;
//        if (!getTier().equals(entity.getTier())) return false;
        if (!getTaskType().equals(entity.getTaskType())) return false;
        if (!getWbsCode().equals(entity.getWbsCode())) return false;
        if (getLbsCode() != null ? !getLbsCode().equals(entity.getLbsCode()) : entity.getLbsCode() != null)
            return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = getEntityID().hashCode();
        result = 31 * result + getName().hashCode();
        result = 31 * result + (getDescription() != null ? getDescription().hashCode() : 0);
        result = 31 * result + getTaskType().hashCode();
        result = 31 * result + getWbsCode().hashCode();
        result = 31 * result + (getLbsCode() != null ? getLbsCode().hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(ScheduledEntity that) {
        if (this.wbsCode.compareTo(that.wbsCode) < 0) {
            return -1;
        } else if (this.wbsCode.compareTo(that.wbsCode) > 0) {
            return 1;
        }
        return 0;
    }

    @Override
    public void acceptKPIRequest(KPIProcessorService processor) {
       throw new IllegalStateException("This method cannot be called in ScheduledEntity, " +
               "only on its concrete children.");
    }

    public void reset() {
        remainingWorkingDays = scheduledWorkingDays;
        pPCofDay = 0;
        scheduleStatus = ScheduleStatus.ON_SCHEDULE;
        completionStatus = CompletionStatus.TO_DO;
        dateMonitored = null;
    }

}
