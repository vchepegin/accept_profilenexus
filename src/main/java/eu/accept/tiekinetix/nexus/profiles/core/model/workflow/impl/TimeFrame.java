package eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by vchepegin on 30/01/2017.
 */
@NodeEntity
public class TimeFrame implements Serializable {

    private static final long serialVersionUID = 7220605029129759376L;
    @GraphId
    private Long id;

    private String timeFrameID;
    private Date startDate;
    private Date endDate;
    /* reason explains the retionale of this time frame existance in the model, e.g. Sick, onHolidays, etc.
       It is better to have a fixed set of reasons for automated reasoning
     */
    private String reason;

    public TimeFrame(String timeFrameID, Date startDate, Date endDate, String reason) {
        this.timeFrameID = timeFrameID;
        this.startDate = new Date (startDate.getTime());
        this.endDate = new Date( endDate.getTime());
        this.reason = reason;
    }

    private TimeFrame () {}

    public String getTimeFrameID() {
        return timeFrameID;
    }

    public void setTimeFrameID(String timeFrameID) {
        this.timeFrameID = timeFrameID;
    }

    public Date getStartDate() {
        return (Date) startDate.clone();
    }

    public void setStartDate(Date startDate) {
        this.startDate = new Date (startDate.getTime());
    }

    public Date getEndDate() {
        return (Date )endDate.clone();
    }

    public void setEndDate(Date endDate) {
        this.endDate = new Date (endDate.getTime());
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
