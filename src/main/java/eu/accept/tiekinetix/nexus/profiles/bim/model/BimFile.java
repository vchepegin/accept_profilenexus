package eu.accept.tiekinetix.nexus.profiles.bim.model;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotEmpty;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

/**
 * Created by akuschki on 12/06/2017.
 */

@Getter
@Setter
@XmlRootElement(name = "ModelInfo")
@JacksonXmlRootElement(localName = "ModelInfo")
@JsonIgnoreProperties({"UNITS", "PHASES", "GROUP_TYPES", "GROUPS", "MATERIAL_QUANTITIES", "HOLES", "ROOMS", "DIMENSIONS", "LEVELS", "PARAMETERS"})
@ToString(callSuper = true)
@NodeEntity
public class BimFile extends BimEntity {

    @JacksonXmlProperty(localName = "Name")
    public String name;
    @NotEmpty
    @JacksonXmlProperty(localName = "Model")
    private String model;

    @Relationship
    @JacksonXmlElementWrapper(localName = "Project")
    @JacksonXmlProperty(localName = "Project")
    private BimProject bimProject;

    @Relationship
    @JacksonXmlProperty(localName = "FAMILY")
    @JacksonXmlElementWrapper(localName = "FAMILIES")
    private Set<BimFamily> families = new HashSet<>();

    @Relationship
    @JacksonXmlElementWrapper(localName = "MATERIALS")
    @JacksonXmlProperty(localName = "MATERIAL")
    private Set<BimMaterial> materials = new HashSet<>();

    @Relationship
    @JacksonXmlElementWrapper(localName = "TYPES")
    @JacksonXmlProperty(localName = "TYPE")
    private Set<BimType> types = new HashSet<>();

    @Relationship
    @JacksonXmlElementWrapper(localName = "ROOMS")
    @JacksonXmlProperty(localName = "ROOM")
    private Set<BimRoom> rooms = new HashSet<>();

    @Relationship
    @JacksonXmlElementWrapper(localName = "ELEMENTS")
    @JacksonXmlProperty(localName = "ELEMENT")
    private Set<BimElement> elements = new HashSet<>();

    @Relationship
    @JacksonXmlElementWrapper(localName = "CATEGORIES")
    @JacksonXmlProperty(localName = "CATEGORY")
    private Set<BimCategory> categories = new HashSet<>();

    @Relationship
    @JacksonXmlElementWrapper(localName = "Tasks")
    @JacksonXmlProperty(localName = "Task")
    private Set<BimTask> bimTasks = new HashSet<>();

    public BimProject getBimProject() { return bimProject;}
    public Set<BimCategory> getCategories() { return categories;}
    public Set<BimFamily> getFamilies() { return families;}
    public Set<BimMaterial> getMaterials() { return materials;}
    public Set<BimType> getTypes() { return types;}
    public Set<BimRoom> getRooms() { return rooms;}
    public Set<BimElement> getElements() { return elements;}
    public Set<BimTask> getBimTasks() { return bimTasks;}

}

