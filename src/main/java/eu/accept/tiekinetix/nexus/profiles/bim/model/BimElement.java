package eu.accept.tiekinetix.nexus.profiles.bim.model;

import java.util.HashSet;
//import java.util.List;
//import java.util.Map;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;

import static java.util.stream.Collectors.toSet;
import javax.xml.bind.annotation.XmlRootElement;

import eu.accept.tiekinetix.nexus.profiles.util.StringifiedArrayDeserializer;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

/**
 * Created by arian.kuschki on 12/06/17.
 */
@Getter
@Setter
@NodeEntity
@XmlRootElement(name = "Element")
@ToString(callSuper = true)
@JsonIgnoreProperties({"familyId", "categoryId"})
public class BimElement extends BimEntity implements HasEmbeddedParameters, HasCategories {

    @JsonDeserialize(using = StringifiedArrayDeserializer.class)
    @JacksonXmlProperty(localName = "GuestElementIds")
    private Set<Long> guestElementIds = new HashSet<>();
    @Relationship
    private Set<BimType> types;
    @Relationship
    private Set<BimCategory> categories = new HashSet<>();
    @Relationship
    private Set<BimFamily> families = new HashSet<>();
    @Relationship
    private Set<BimMaterial> materials = new HashSet<>();
    @JacksonXmlProperty(localName = "TypeId")
    private Long typeId;
    @JsonProperty("HostId")
    @JacksonXmlProperty(localName = "HostId")
    private Long hostId;
    @JacksonXmlProperty(localName = "LevelId")
    private Long levelId;
    @JacksonXmlProperty(localName = "CreationPhaseId")
    private Long creationPhaseId;
    @JacksonXmlProperty(localName = "DemolitionPhaseId")
    private Long demolitionPhaseId;
    @JacksonXmlProperty(localName = "GroupId")
    private Long groupId;
    @JsonDeserialize(using = StringifiedArrayDeserializer.class)
    @JacksonXmlProperty(localName = "RoomsIds")
    private Set<Long> roomsIds = new HashSet<>();


    @Relationship
    @JacksonXmlProperty(localName = "Parameter")
    @JacksonXmlElementWrapper(localName = "Parameters")
    private Set<BimParameter> parameters = new HashSet<>();

    @Override
    @JsonIgnoreProperties
    public Set<Long> getCategoryIds() {
        return this.getParamIds("Category");
    }

    public Set<Long> getFamilyIds() {
        return getParamIds("Family");
    }

    @JsonIgnoreProperties
    @Override
    public Set<Long> getParamIds(final String name) {
        return getParamsByName(name).stream()
                .map(BimParameter::getValue)
                .map(Long::parseLong)
                .collect(toSet());
    }

    @JsonIgnoreProperties
    @Override
    public Set<BimParameter> getParamsByName(String name) {
        return parameters.stream()
                .filter(p -> name.equals(p.getName()))
                .distinct()
                .collect(toSet());
    }

    public Set<Long> getTypeIds() {
        final Set<Long> typeIds = parameters.stream()
                .filter(p -> "Type Id".equals(p.getName()))
                .map(BimParameter::getValue)
                .map(Long::parseLong)
                .collect(toSet());
        if (typeId != null) {
            typeIds.add(typeId);
        }
        return typeIds;
    }

    @Override
    public Set<BimParameter> getParameters() {
        return parameters;
    }

    @Override
    public  void setParameters(Set<BimParameter> parameters) {
        this.parameters = parameters;
    }

    @Override
    public Set<BimCategory> getCategories() {
        return categories;
    }

    @Override
    public void setCategories(Set<BimCategory> categories) {
        this.categories = categories;
    }

    public void setTypes(Set<BimType> types) {
        this.types = types;
    }

    public Set<BimMaterial> getMaterials() {
        return materials;
    }

    public Set<BimType> getTypes() {
        return types;
    }

    public Set<BimFamily> getFamilies() {
        return families;
    }

    public void setFamilies(Set<BimFamily> families) {
        this.families = families;
    }

    @JsonProperty("TypeId")
    public Long getTypeId() {
        return typeId;
    }

    @JsonProperty("TypeId")
    public void setTypeId(Long typeId) {
        this.typeId = typeId;
    }

    @JsonProperty("HostId")
    public Long getHostId() {
        return hostId;
    }

    @JsonProperty("HostId")
    public void setHostId(Long hostId) {
        this.hostId = hostId;
    }

    @JsonProperty("LevelId")
    public Long getLevelId() {
        return levelId;
    }

    @JsonProperty("LevelId")
    public void setLevelId(Long levelId) {
        this.levelId = levelId;
    }

    @JsonProperty("CreationPhaseId")
    public Long getCreationPhaseId() {
        return creationPhaseId;
    }

    @JsonProperty("CreationPhaseId")
    public void setCreationPhaseId(Long creationPhaseId) {
        this.creationPhaseId = creationPhaseId;
    }

    @JsonProperty("DemolitionPhaseId")
    public Long getDemolitionPhaseId() {
        return demolitionPhaseId;
    }

    @JsonProperty("DemolitionPhaseId")
    public void setDemolitionPhaseId(Long demolitionPhaseId) {
        this.demolitionPhaseId = demolitionPhaseId;
    }

    @JsonProperty("GroupId")
    public Long getGroupId() {
        return groupId;
    }

    @JsonProperty("GroupId")
    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

}
