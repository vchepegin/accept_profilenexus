package eu.accept.tiekinetix.nexus.profiles.repositories.reporting;

import eu.accept.tiekinetix.nexus.profiles.reporting.vo.DailyProgressReport;
import org.springframework.data.neo4j.repository.GraphRepository;

/**
 * Created by vchepegin on 06/06/2017.
 */
public interface ProgressReportRepository extends GraphRepository<DailyProgressReport> {

    DailyProgressReport[] findByEntityID (String id);
}
