package eu.accept.tiekinetix.nexus.profiles.core.model.enumerations;

/**
 * Created by vchepegin on 11/08/2017.
 */
public enum Intention {
    KPI_CALCULATION (1),
    SCHEDULED_WORKING_DAYS_CALCULATION (2);

    private final int intentionCode;

    Intention (int code) {
        this.intentionCode = Integer.valueOf(code);
    }

    private int getIntentionCode () {
        return intentionCode;
    }

    @Override
    public String toString () {
        String tmp;
        switch (intentionCode) {
            case 1:
                tmp = "KPI_CALCULATION";
                break;

            case 2:
                tmp = "SCHEDULED_WORKING_DAYS_CALCULATION";
                break;
            default:
                tmp = "Not defined";
        }

        return tmp;
    }
}
