package eu.accept.tiekinetix.nexus.profiles.web.restful;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vchepegin on 25/01/2017.
 */

@RestController
public class DefaultController {

    @RequestMapping("/")
    public String index() {
        return "Welcome to the ACCEPT User Profile, part of the Profiel Nexus.";
    }
}
