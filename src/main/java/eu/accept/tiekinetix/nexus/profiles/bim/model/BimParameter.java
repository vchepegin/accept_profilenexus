package eu.accept.tiekinetix.nexus.profiles.bim.model;

import java.util.Objects;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import javax.xml.bind.annotation.XmlRootElement;

import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Project;
import lombok.ToString;
import org.neo4j.ogm.annotation.NodeEntity;


/**
 * Created by arian.kuschki on 12/06/17.
 * Embedded Parameter that does not occur as a top-level bim element but only embedded in other top-level Elements (e.g. {@link BimMaterial}).
 */
@NodeEntity
@XmlRootElement(name = "Parameter")
@ToString(callSuper = true)
public class BimParameter extends BimEntity {

    @JacksonXmlProperty(localName = "Name", isAttribute = true)
    private String name;

    @JacksonXmlProperty(localName = "Id", isAttribute = true)
    private Long id;

    @JacksonXmlProperty(localName = "Value", isAttribute = true)
    private String value;

    /**
     * This method only guarantees uniqueness when all BimParameters belong to the same project!
     */
    @Override
    public boolean equals(Object obj) {

        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        BimParameter other = (BimParameter) obj;
        return Objects.equals(this.id, other.id) && Objects.equals(this.name, other.name) && Objects.equals(this.value, other.value);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
        this.bimId = id;
        initGuid();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        initGuid();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
        initGuid();
    }

    /**
     * This method only guarantees uniqueness when all BimParameters belong to the same project!
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, name, value);
    }

    @Override
    public void setProject(Project project) {
        super.setProject(project);
        initGuid();
    }

    /**
     * The guid is needed for the equals and hashcode methods which are used before the project is set (and hence before the unique entityId is available
     */
    private void initGuid() {
        if (name != null && id != null && value != null) {
            this.guid = id + ":" + name + ":" + value;
        }
        if (project != null) {
            initEntityId();
        }
    }

    @Override
    protected void initEntityId() {
        if (project != null && project.getProjectID() != null && guid != null) {
            entityId = project.getProjectID() + ":" + guid;
        }
    }


}

