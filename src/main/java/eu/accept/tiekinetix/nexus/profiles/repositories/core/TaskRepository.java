package eu.accept.tiekinetix.nexus.profiles.repositories.core;

import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Task;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Set;

/**
 * Created by vchepegin on 08/02/2017.
 */
public interface TaskRepository extends GraphRepository<Task> {

 //   Task findByTaskID (String id);

    Task findByEntityID (String id);

    @Query("match (a:Task {entityID: {0} }) DETACH delete a")
    void delete(@Param("0")String taskID);

    @Query("match (a:ScheduledEntity)-[r:hasOwner]-(b:Person {personID: {0}} ) return a")
    List<ScheduledEntity> findTaskByOwner (@Param("0")String personID);
//
//    @Query("MATCH (n:ScheduledEntity {entityID : {0} })-[:hasSuccessor]->(a:Task {completionStatus : 'TO_DO'}) " +
//            "MATCH  (a)<-[:hasSuccessor*0..10]-(b {completionStatus : 'DONE'}) " +
//            "WITH a SET a.completionStatus = 'READY_TO_START'  return collect(distinct a.entityID)")
//    Set<String> setReadyIfPreviousDone (String entityID);

    @Query("MATCH (n:ScheduledEntity {entityID : {0} })<-[:hasChild]-(a:ScheduledEntity) " +
            "MATCH (a:Task)-[:hasChild]->(b:ScheduledEntity)  WITH a,  " +
            "SUM(CASE WHEN b.completionStatus =  'DONE' THEN 1 ELSE 0 END) as s, count(b) as k  " +
            "return CASE WHEN s = k THEN  a.entityID ELSE '' END")
    String ifAllChildrenComplete (@Param("0")String entityID);

    @Query("match (n:ScheduledEntity {entityID : {0} })-[:hasSuccessor]->(a:ScheduledEntity {completionStatus : 'TO_DO'}) " +
            "MATCH (a)<-[:hasSuccessor]-(b) " +
            "WITH a,  SUM(CASE WHEN b.completionStatus =  'DONE' THEN 1 ELSE 0 END) as s," +
            " count(b) as k  return CASE WHEN s = k THEN  a.entityID ELSE '' END")
    Set<String> find2bReady (@Param("0")String id);

    @Query("MATCH (a:ScheduledEntity {entityID: {0} })-[r:hasChild]->(b) " +
            "MATCH (b) WHERE NOT ()-[:hasSuccessor]->(b) " +
            "SET b.completionStatus = 'IN_PROGRESS' return collect(distinct b.entityID)")
    Set<String> setInitialSubTasks2Ready (@Param("0")String entityID);

    @Query("match (a:ScheduledEntity {entityID : {0} }) SET a.completionStatus = {1}")
    void setStatus(@Param("0")String id, @Param("1")String status);

    @Query ("MATCH (n:Project {projectID:{0} }) -[r*0..4] -> (e:Task {wbsCode: {1} }) RETURN e")
    Set<Task> findTaskWithSameWbsCode (@Param("0")String projectID, @Param("1")String wbsCode);

    @Query("match (b:Task {entityID: {0}} )-[r:hasChild]->(child:ScheduledEntity) return child ORDER BY child.wbsCode ASC")
    Set<Task> findSubTasks (@Param("0")String id);

}
