package eu.accept.tiekinetix.nexus.profiles.core.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by vchepegin on 01/02/2017.
 */
@ResponseStatus(value= HttpStatus.CONFLICT, reason="The entity with the same ID does already exist.") //409
public class EntityAlreadyExistsException extends RuntimeException {
    /**
     * Unique ID for Serialized object
     */
    private static final long serialVersionUID = -8790211652911971729L;

    public EntityAlreadyExistsException(String entityID) {
        super(entityID + " already exist");
    }
}
