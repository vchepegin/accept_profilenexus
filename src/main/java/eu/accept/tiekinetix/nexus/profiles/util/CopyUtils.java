package eu.accept.tiekinetix.nexus.profiles.util;

import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Activity;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.QualityGate;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Task;
import eu.accept.tiekinetix.nexus.profiles.reporting.vo.ActivityReport;
import eu.accept.tiekinetix.nexus.profiles.reporting.vo.BaseReport;
import eu.accept.tiekinetix.nexus.profiles.reporting.vo.QualityGateReport;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by vchepegin on 12/06/2017.
 */
public abstract class CopyUtils {

    public static QualityGateReport copy (final QualityGate from, final QualityGateReport to) {
        return (QualityGateReport) copyObjects(from, to);
    }

    public static ActivityReport copy (final Activity from, final ActivityReport to) {
        ActivityReport activityReport = (ActivityReport) copyObjects(from, to);

        //JIRA Issue-#103
        activityReport.setQuantityAmount(from.getQuantity().getAmount());
        activityReport.setQuantityUnit(from.getQuantity().getUnit());

        activityReport.setConsumptionRateAmount(from.getConsumptionRate().getAmount());
        activityReport.setConsumptionRateUnit(from.getConsumptionRate().getUnit());


        return activityReport;
    }

    public static BaseReport copy (final Task from, final BaseReport to) {
        return (BaseReport) copyObjects(from, to);
    }


    /**
     * Copy fields with the same names from one object to another
     * @param from
     * @param to
     * @return
     */
    private static Object copyObjects (final Object from, final Object to) {
        Map<String, Field> fromFields = analyze(from);
        Map<String, Field> toFields = analyze(to);
        fromFields.keySet().retainAll(toFields.keySet());
        for (Map.Entry<String, Field> fromFieldEntry : fromFields.entrySet()) {
            final String name = fromFieldEntry.getKey();
            final Field sourceField = fromFieldEntry.getValue();
            final Field targetField = toFields.get(name);
            if (targetField.getType().isAssignableFrom(sourceField.getType())) {
                sourceField.setAccessible(true);
                if (Modifier.isFinal(targetField.getModifiers())) continue;
                targetField.setAccessible(true);
                try {
                    targetField.set(to, sourceField.get(from));
                } catch (IllegalAccessException e) {
                    throw new IllegalStateException(name + " field does not exist in this class hierarchy");
                }
            }
        }
        return to;
    }

    /**
     * Create a map of all fields in the hierarchy
     * @param object
     * @return
     */
    private static Map<String, Field> analyze (Object object) {
        if (object == null) throw new NullPointerException();

        Map<String, Field> map = new TreeMap<String, Field>();

        Class<?> current = object.getClass();
        while (current != Object.class) {
            for (Field field : current.getDeclaredFields()) {
                if (!Modifier.isStatic(field.getModifiers())) {
                    if (!map.containsKey(field.getName())) {
                        map.put(field.getName(), field);
                    }
                }
            }
            current = current.getSuperclass();
        }
        return map;
    }
}
