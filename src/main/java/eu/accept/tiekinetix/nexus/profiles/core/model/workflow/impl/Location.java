package eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

import java.io.Serializable;

/**
 * Created by vchepegin on 08/02/2017.
 */
@NodeEntity
public class Location implements Serializable {

    private static final long serialVersionUID = 8484819773948914698L;
    @GraphId
    private Long id;

    private String locationID;
    private String level;
    private String sector;
    private String unit;
    private String orientation;

    public Location () {}

    public Location(String locationID, String level, String sector, String unit, String orientation) {
        this.locationID = locationID;
        this.level = level;
        this.sector = sector;
        this.unit = unit;
        this.orientation = orientation;
    }

    @JsonProperty("locationID")
    public String getLocationID() {
        return locationID;
    }

    @JsonProperty("locationID")
    public void setLocationID(String locationID) {
        this.locationID = locationID;
    }

    @JsonProperty("level")
    public String getLevel() {
        return level;
    }

    @JsonProperty("level")
    public void setLevel(String level) {
        this.level = level;
    }

    @JsonProperty("sector")
    public String getSector() {
        return sector;
    }

    @JsonProperty("sector")
    public void setSector(String sector) {
        this.sector = sector;
    }

    @JsonProperty("unit")
    public String getUnit() {
        return unit;
    }

    @JsonProperty("unit")
    public void setUnit(String unit) {
        this.unit = unit;
    }

    @JsonProperty("orientation")
    public String getOrientation() {
        return orientation;
    }

    @JsonProperty("orientation")
    public void setOrientation(String orientation) {
        this.orientation = orientation;
    }
}
