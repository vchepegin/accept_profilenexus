package eu.accept.tiekinetix.nexus.profiles.repositories.messaging;

import eu.accept.tiekinetix.nexus.profiles.reporting.vo.ChangedObjects;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;

/**
 * Use a messaging functionality provided by {@code MessageDispatcher} and related classes
 * Created by vchepegin on 04/07/2017.
 */
@Deprecated
public interface ChangedObjectsRepository  extends GraphRepository<ChangedObjects> {

    @Query("match (a:ChangedObjects) WHERE NOT EXISTS(a.validUntil) return a")
    ChangedObjects findCurrent ();
}
