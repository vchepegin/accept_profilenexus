package eu.accept.tiekinetix.nexus.profiles.core.model.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by vchepegin on 30/01/2017.
 */
@NodeEntity
public class Skill implements Serializable {

    private static final long serialVersionUID = 587979743757651397L;
    @GraphId
    private Long id;

    private Set<String> skills;

    public Skill(String skill) {
        if (this.skills == null) {
            this.skills = new HashSet<>();
        }
        this.skills.add(skill);
    }

    private Skill() {}

    @JsonProperty("skill")
    public Set<String> getSkills() {
        return skills;
    }

    public void add(String skill) {
        if (this.skills == null) {
            this.skills = new HashSet<>();
        }
        this.skills.add(skill);
    }

    public void remove(String skill) {
        this.skills.remove(skill);
    }

/*
    private String skillID;
    private String skillName;
    private String skillDescription;
    private String skillExplained; //recommended to use a URL to the human readable explanation
    private Date activeFrom;

    @Relationship(type="confirmedBy", direction = Relationship.OUTGOING)
    public Set<Certificate> certificates;
*/
    /**
     * Add a new certificate to confirm the skill
     * @param certificate
     * @return Skill for the fluent interface
     */
/*
    public Skill addCertificate (Certificate certificate) {
        if (certificates == null) {
            certificates = new HashSet<>();
        }
        certificates.add(certificate);

        return this;
    }

    public Skill(String skillID, String skillName, String skillDescription,
                 String skillExplained, Date activeFrom) {
        this.skillID = skillID;
        this.skillName = skillName;
        this.skillDescription = skillDescription;
        this.skillExplained = skillExplained;
        this.activeFrom = new Date(activeFrom.getTime());
    }

    private Skill () {}

    @JsonProperty("skillID")
    public String getSkillID() {
        return skillID;
    }

    @JsonProperty("skillID")
    public void setSkillID(String skillID) {
        this.skillID = skillID;
    }

    @JsonProperty("skillName")
    public String getSkillName() {
        return skillName;
    }

    @JsonProperty("skillName")
    public void setSkillName(String skillName) {
        this.skillName = skillName;
    }

    @JsonProperty("skillDescription")
    public String getSkillDescription() {
        return skillDescription;
    }

    @JsonProperty("skillDescription")
    public void setSkillDescription(String skillDescription) {
        this.skillDescription = skillDescription;
    }

    @JsonProperty("skillExplained")
    public String getSkillExplained() {
        return skillExplained;
    }

    @JsonProperty("skillExplained")
    public void setSkillExplained(String skillExplained) {
        this.skillExplained = skillExplained;
    }

    @JsonProperty("activeFrom")
    public Date getActiveFrom() {
        return (Date) activeFrom.clone();
    }

    @JsonProperty("activeFrom")
    public void setActiveFrom(Date activeFrom) {
        this.activeFrom = new Date(activeFrom.getTime());
    }
*/
}
