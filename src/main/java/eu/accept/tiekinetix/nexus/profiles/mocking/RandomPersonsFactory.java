package eu.accept.tiekinetix.nexus.profiles.mocking;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.PersonRole;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.PersonStatus;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.SpecialisationEnum;
import eu.accept.tiekinetix.nexus.profiles.core.model.user.Person;
import eu.accept.tiekinetix.nexus.profiles.core.model.user.Specialisation;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

/**
 * Created by vchepegin on 31/01/2017.
 */
public class RandomPersonsFactory {

    private static HttpHeaders headers = new HttpHeaders();

    private static final List<SpecialisationEnum> VALUES =
            Collections.unmodifiableList(Arrays.asList(SpecialisationEnum.values()));
    private static final int SIZE = VALUES.size();
    private static final Random random = new Random();

    public static Person getNewRandomWorker () {

        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>("", headers);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> jsonString = restTemplate.exchange("http://api.namefake.com/en_GB/both",
                HttpMethod.GET,
                entity,
                String.class);

        MockPersonVO mock = new MockPersonVO();
        try {
            mock = new ObjectMapper().readValue(jsonString.getBody().toString(), MockPersonVO.class);
        } catch (IOException  ex) {
            ex.printStackTrace();
        }

        IntStream intStream = random.ints (10000, 99999);

        int i = mock.getName().indexOf(" ");
        String firstName = mock.getName().substring(0, i);
        String lastName = mock.getName().substring(i+1);
        Person randomPerson = new Person(intStream.distinct().findAny().toString(), mock.getUsername(),
                PersonStatus.ACTIVE, lastName, firstName, PersonRole.WORKER, "en-GB");

        return randomPerson;

    }
/*
    public static Specialisation getRandomSpecialisation () {
        return new Specialisation(VALUES.get(random.nextInt(SIZE)));
    }
*/
    private RandomPersonsFactory () {}
}
