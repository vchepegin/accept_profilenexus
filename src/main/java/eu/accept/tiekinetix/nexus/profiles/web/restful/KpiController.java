package eu.accept.tiekinetix.nexus.profiles.web.restful;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.accept.tiekinetix.nexus.messaging.ChangedEntitiesLoggingService;
import eu.accept.tiekinetix.nexus.messaging.Message;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.Intention;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Activity;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.QualityGate;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity;
import eu.accept.tiekinetix.nexus.profiles.reporting.processing.KPIProcessorService;
import eu.accept.tiekinetix.nexus.profiles.reporting.vo.*;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.ActivityRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.ProjectRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.ScheduledEntityRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.messaging.ChangedObjectsRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.reporting.ActivityReportRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.reporting.BaseReportRepository;
//import eu.accept.tiekinetix.nexus.profiles.repositories.reporting.RNCRepository;
//import eu.accept.tiekinetix.nexus.profiles.repositories.reporting.ReasonsCatalogueReporsitory;
import eu.accept.tiekinetix.nexus.profiles.util.CoreUtilities;
import org.neo4j.ogm.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * Created by vchepegin on 16/05/2017.
 */
@CrossOrigin
@RestController
public class KpiController {

    @Autowired
    private ActivityRepository activityRepository;
    @Autowired
    ScheduledEntityRepository scheduledEntityRepository;
    @Autowired
    private BaseReportRepository baseReportRepository;
    @Autowired
    private ActivityReportRepository activityReportRepository;
    @Autowired
    private ChangedObjectsRepository changedObjectsRepository;
/*
    @Autowired
    private RNCRepository rncRepository;
    @Autowired
    private ReasonsCatalogueReporsitory reasonsCatalogueReporsitory;
    */
    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    ChangedEntitiesLoggingService loggingService;
    @Autowired
    KPIProcessorService kpiProcessor;
    @Autowired
    private CoreUtilities coreUtilities;
    @Autowired
    private StringRedisTemplate template;
/*    @Autowired
    MongoTemplate mongoTemplate;
*/
    @Autowired
    Session session;

    private final ReasonsCatalogue reasonsCatalogue = new ReasonsCatalogue("EN");

    private final ObjectMapper mapper = new ObjectMapper();

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/dailyMonitoringWorker", method = RequestMethod.POST)
    public void sendProgressReport (@RequestBody String payload)
            throws Exception {

        DailyProgressReport[] reports = mapper.readValue(payload, DailyProgressReport[].class);

        for (DailyProgressReport dpr : reports) {
            Activity activity = activityRepository.findByEntityID(dpr.getEntityID());

            //commented out as requested by Issue #113
//            if (dpr.getDidPercent() == 0 && dpr.getDidAbsolute() == 0) {
//                throw new IllegalArgumentException("Absolute value and % cannot be zero at the same time.");
//            }

            if (dpr.getDidPercent() != 0 && dpr.getDidAbsolute() == 0) {
                dpr.setDidAbsolute(activity.getDynamicDailyGoal() * dpr.getDidPercent()  / 100);
            }
            else if (dpr.getDidPercent() == 0 && dpr.getDidAbsolute() != 0) {
                dpr.setDidPercent((int) (dpr.getDidAbsolute() * 100 / activity.getDynamicDailyGoal()));
            }


            activity.setDateMonitored(dpr.getDateMonitored());
            activity.setDidAbsoluteWorker(dpr.getDidAbsolute());
            activity.setDidPercentWorker(dpr.getDidPercent());
//            ReasonsCatalogue reasons = reasonsCatalogueReporsitory.getReasons4Locale("EN");
//            activity.setReasonForNonCompletion(reasons.toString(dpr.getReasonForNonCompletion()));
            activity.setReasonForNonCompletion(reasonsCatalogue.getRNC(dpr.getReasonForNonCompletion()));

            activityRepository.save(activity);
/*
            //save reason for non completion report into MongoDB
            Reason4NonCompletion rnc = getRNCInstance(activity,
                    projectRepository.findProjectIdByChild(activity.getEntityID()),
                    dpr.getReasonForNonCompletion());
            rncRepository.save(rnc);
*/
            //TODO: one unified mechanism
            loggingService.logChangedEntity(activity.getEntityID());
            template.convertAndSend("CHANGED_ACTIVITY",
                    mapper.writeValueAsString(new Message("ChangedActivity",
                            activity.getEntityID(), "")));

        }
    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/dailyMonitoringValidation", method = RequestMethod.POST)
    public void dailyMonitoringValidation (@RequestBody String payload)
            throws Exception {

        DailyProgressReport[] reports = mapper.readValue(payload, DailyProgressReport[].class);



        for (DailyProgressReport dpr : reports) {

            //InitialState (e.g. palnned workign days) has to be initialised and exist! Otherwise either NPE or /0
            ScheduledEntity entity = scheduledEntityRepository.findByEntityID2(dpr.getEntityID());

            //At the moment processing/updates and reporting happening only for Activities
            if (entity instanceof Activity) {
                Activity activity = (Activity) entity;

                if (!dpr.isVerified() && dpr.getDidPercent() == 0 && dpr.getDidAbsolute() == 0) {
                    return;
                }
                if (dpr.getDateMonitored().equals(activity.getDateValidated())) {
                    activity.resetKPIrelevantParameters ();
                } else {
                    activity.setDateValidated(dpr.getDateMonitored());
                }

                //commented out as requested by Issue #113
//                if (dpr.getDidPercent() == 0 && dpr.getDidAbsolute() == 0) {
//                    throw new IllegalArgumentException("Absolute value and % cannot be zero at the same time.");
//                }

                //#127
                if (dpr.isVerified() && dpr.getDidPercent() == 0 && dpr.getDidAbsolute() == 0) {
                    dpr.setDidAbsolute(activity.getDidAbsoluteWorker());
                    dpr.setDidPercent((int)activity.getDidPercentWorker());
                } else {
                    if (dpr.getDidPercent() == 0 && dpr.getDidAbsolute() != 0) {
                        dpr.setDidPercent((int) (dpr.getDidAbsolute() * 100 / activity.getDynamicDailyGoal()));
                    } else  if (dpr.getDidPercent() != 0 && dpr.getDidAbsolute() == 0) {
                        dpr.setDidAbsolute(activity.getDynamicDailyGoal() * dpr.getDidPercent()  / 100);
                    }
                }

                activity.setDateMonitored(dpr.getDateMonitored());
                activity.setDidAbsolute(dpr.getDidAbsolute());
                activity.setDidPercent(dpr.getDidPercent());
                activity.setComment(dpr.getComment());
                activity.setVerified(dpr.isVerified());

                activity.acceptKPIRequest(kpiProcessor);

                //TODO: one unified mechanism
                loggingService.logChangedEntity(activity.getEntityID());

                //TODO  "VALIDATED_ACTIVITIES_" + projectID
                template.boundSetOps("VALIDATED_ACTIVITIES").add(activity.getEntityID());
            }

            if (entity instanceof QualityGate) {
                QualityGate qg= (QualityGate) entity;
                qg.setVerified(dpr.isVerified());
            }

            scheduledEntityRepository.save(entity);


    /*
    * TODO: save all entityIDs of ACTIVITIES only for further processing -> used in the batch calcs for parent tasks and PPC
    * the end point/service will be used as a scheduled job off the office hours
    * preprocessing: get all unique parent/effected Task IDs
    *   (?) I stop now at tier_2 task
    * processing: calc all parameters, generate reports, dump reports to... BigQuery
    */
        }
        kpiProcessor.propagateUpwards(Intention.KPI_CALCULATION);

    }

    @CrossOrigin
    @RequestMapping(value = "/getKPIHistory/{projectID}", method = RequestMethod.POST)
    public List<BaseReport> getKPIHistory (@RequestBody Map<String, Object> payload,
                                           @PathVariable(value="projectID") String projectID)
            throws Exception {

        //Issue #111
        payload.put("projectID", projectID);

        final String template = "match (a:BaseReport) %1 return a";

        String cypher = coreUtilities.prepareCypher4Reports(template, payload);

        List<BaseReport> reports = coreUtilities.getObjectsByCriteria(session, cypher, payload, BaseReport.class);

        return reports;
    }

    @CrossOrigin
    @RequestMapping(value = "/getReasonsCatalogue", method = RequestMethod.GET)
    public String getReasonsCatalogue () throws Exception {
/*        ReasonsCatalogue reasons = reasonsCatalogueReporsitory.getReasons4Locale("EN");
        return reasons.toJson();
*/
        return reasonsCatalogue.toJson();
    }
/*
    @CrossOrigin
    @RequestMapping(value = "/getReasonsCatalogue/{languageCode}", method = RequestMethod.GET)
    public String getReasonsCatalogue (@PathVariable(value="languageCode") String languageCode) throws Exception {
        ReasonsCatalogue reasons = reasonsCatalogueReporsitory.getReasons4Locale(languageCode);
        if (reasons == null) {
            reasons = reasonsCatalogueReporsitory.getReasons4Locale("EN");
        }
        return reasons.toJson();
    }
*/
    @CrossOrigin
    @RequestMapping(value = "/getFirstAndLastDateMonitored/{projectID}", method = RequestMethod.GET)
    public MinMaxReportingDates getFirstAndLastDateMonitored (@PathVariable(value="projectID") String projectID)
            throws Exception {
        return baseReportRepository.findMinMaxMonitoringDates(projectID);
    }


    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/startKPIProcessing/{projectID}", method = RequestMethod.POST)
    public void startKPIProcessing (@PathVariable(value="projectID") String projectID) throws Exception {
        kpiProcessor.propagateUpwards(Intention.KPI_CALCULATION);
    }


    @CrossOrigin
    @RequestMapping(value = "/getKpiRncRecords/{projectID}", method = RequestMethod.POST)
    public Map<String, String> getKpiRncRecords (@RequestBody Map<String, Object> payload,
                                    @PathVariable(value="projectID") String projectID) throws Exception {
//        ReasonsCatalogue reasons = reasonsCatalogueReporsitory.getReasons4Locale("EN");
//
//        MatchOperation matchStage = Aggregation.match(
//                new Criteria("wbsCode").regex("1.1.*").and("projectID").is(projectID));
//        GroupOperation groupStage = Aggregation.group("rncCode").count().as("count");
//
//        Aggregation aggregation
//                = Aggregation.newAggregation(matchStage, groupStage);
//
//        AggregationResults<RawKpiRnc> results
//                = mongoTemplate.aggregate(aggregation, "noncompletion", RawKpiRnc.class);
//
//        return results.getMappedResults();

        final String template = "MATCH (a:ActivityReport) %1 RETURN a.reasonForNonCompletion, count(a.reasonForNonCompletion)";

        payload.put("projectID", projectID);
        String cypher = coreUtilities.prepareCypher4Reports (template, payload);

        Iterable<Map<String, Object>> iterableResult = session.query(cypher, payload).queryResults();

        Map<String, String> results = new HashMap<>();
        for (Map<String, Object> map : iterableResult) {
            if (Integer.valueOf(map.get("count(a.reasonForNonCompletion)").toString()) == 0 ) {
                results.put("No records found", "Check projectID or start KPI processing first.");
            } else {
                results.put(map.get("a.reasonForNonCompletion").toString(),
                        map.get("count(a.reasonForNonCompletion)").toString());
            }
        }

        return results;

    }



    @CrossOrigin
    @RequestMapping(value = "/secret", method = RequestMethod.GET)
    public void secret () throws Exception {
        //        org.springframework.data.mongodb.core.query.Query q = new BasicQuery("{'type' : 'EN'}");
//        List<ReasonsCatalogue> rc = mongoTemplate.find(q, ReasonsCatalogue.class);
//
//        ReasonsCatalogue reasons = reasonsCatalogueReporsitory.getReasons4Locale("EN");
//        return reasons.toJson();

//        Reason4NonCompletion rnc1 = new Reason4NonCompletion();
//        rnc1.setProjectID("test1");
//        rnc1.setLbsCode("floor#1");
//        rnc1.setWbsCode("1.2.1");
//        Map<Integer, Integer> reasons = new HashMap<>();
//        reasons.put(6, 1);
//        reasons.put(7, 1);
//        rnc1.setRnc(reasons);
//        rncRepository.save(rnc1);

        //mock data
//        template.boundSetOps("VALIDATED_ACTIVITIES").add("Activity4Project1");
//        template.boundSetOps("VALIDATED_ACTIVITIES").add("Activity4Project2");
//
//        template.convertAndSend("COMMANDS",
//                mapper.writeValueAsString(new Message("PpcGeneration",
//                        "secret")));
    }

    @CacheEvict( value = "reasons", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/clearReasonsCache", method = RequestMethod.GET)
    public void clearReasonsCache () throws Exception {

    }

}