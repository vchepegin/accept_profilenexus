package eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

import java.io.Serializable;

/**
 * Created by vchepegin on 23/02/2017.
 */
@NodeEntity
public class Quantity implements Serializable {

    private static final long serialVersionUID = -7258416551851033236L;

    @GraphId
    private Long id;

    private String quantityID;
    private String unit;
    private double amount;

    private static final ObjectMapper mapper = new ObjectMapper();

    /**
     * Copy constructor. Note, that quantityID has to be set separately.
     * @param quantity
     * @return a new instance of Quantity with the same values.
     */
    public Quantity (Quantity quantity) {
        //new entity = new ID
        this.quantityID = java.util.UUID.randomUUID().toString();
        this.amount = quantity.getAmount();
        this.unit = quantity.getUnit();
    }

    public Quantity () {}

    public Quantity(String quantityID, String unit, double amount) {
        if (quantityID ==  null || quantityID.isEmpty()) {
            this.quantityID = java.util.UUID.randomUUID().toString();
        } else {
            this.quantityID = quantityID;
        }
        this.unit = unit;
        this.amount = amount;
    }

    /**
     * Static constructor from a JSON string and gives it an ID if not provided by the client
     * @param obj
     * @return
     */
    public static Quantity fromObject (Object obj) {
        Quantity quantity = mapper.convertValue(obj, Quantity.class);
        if ( Strings.isNullOrEmpty(quantity.getQuantityID()) ) {
            quantity.setQuantityID(java.util.UUID.randomUUID().toString());
        }
        return quantity;

    }

    @JsonProperty("quantityID")
    public String getQuantityID() {
        return quantityID;
    }

    @JsonProperty("quantityID")
    public void setQuantityID(String quantityID) {
        this.quantityID = quantityID;
    }

    @JsonProperty("unit")
    public String getUnit() {
        return unit;
    }

    @JsonProperty("unit")
    public void setUnit(String unit) {
        this.unit = unit;
    }

    @JsonProperty("amount")
    public double getAmount() {
        return amount;
    }

    @JsonProperty("amount")
    public void setAmount(double amount) {
        this.amount = amount;
    }

    @JsonIgnore
    public Long getId() {
        return id;
    }
}
