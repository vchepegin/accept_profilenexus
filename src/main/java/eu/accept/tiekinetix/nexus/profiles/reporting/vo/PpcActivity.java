package eu.accept.tiekinetix.nexus.profiles.reporting.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.accept.tiekinetix.nexus.profiles.reporting.KpiReporting;
import org.neo4j.ogm.annotation.GraphId;

import java.io.Serializable;

/**
 *
 * Created by vchepegin on 26/07/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PpcActivity implements KpiReporting, Serializable {


    private static final long serialVersionUID = -3827536505209439024L;

    @GraphId
    private Long id;

    private String wbsCode;
    private double ppc;

    public PpcActivity () {};

    @JsonProperty("wbsCode")
    public String getWbsCode() {
        return wbsCode;
    }

    @JsonProperty("wbsCode")
    public void setWbsCode(String wbsCode) {
        this.wbsCode = wbsCode;
    }

    @JsonProperty("ppc")
    public double getPpc() {
        return ppc;
    }

    @JsonProperty("ppc")
    public void setPpc(double ppc) {
        this.ppc = ppc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PpcActivity)) return false;

        PpcActivity that = (PpcActivity) o;

        if (Double.compare(that.getPpc(), getPpc()) != 0) return false;
        return getWbsCode().equals(that.getWbsCode());
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = getWbsCode().hashCode();
        temp = Double.doubleToLongBits(getPpc());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
