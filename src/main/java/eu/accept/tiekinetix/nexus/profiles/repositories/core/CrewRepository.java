package eu.accept.tiekinetix.nexus.profiles.repositories.core;

import eu.accept.tiekinetix.nexus.profiles.core.model.user.Crew;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

import java.util.Set;

/**
 * Created by vchepegin on 30/01/2017.
 */
public interface CrewRepository extends GraphRepository<Crew> {

    @Query("match (a:Crew {crewID: {0}} ) " +
            "OPTIONAL MATCH (a:Crew)-[r:hasOwner*0..1]-(b:Person) " +
            "OPTIONAL MATCH (a:Crew {crewID: {0}} )-[r2:hasWorker*]-(c:Person) " +
            "OPTIONAL MATCH (a:Crew {crewID: {0}} )-[r3:hasEquipment*]-(d:Equipment) " +
            "OPTIONAL MATCH (a:Crew {crewID: {0}} )-[r4:hasTasksAssigned*]-(e:ScheduledEntity) " +
            " return a, r, b, r2, c, r3, d, r4, e")
    Crew findByCrewID (@Param("0")String crewID);

    @Query("match (a:Crew {crewID: {0} }) DETACH delete a")
    void delete(@Param("0")String crewID);

    @Query("MATCH (n:Crew {crewID: {0} })<-[r:hasCrew]-(b:Activity) return distinct b.entityID as activityID")
    Set<String> findActivitiesByCrewId (@Param("0")String crewId);

    @Query("MATCH (n:Crew ) return n")
    Set<Crew> getCrews ();

    @Query("MATCH (p:Person {personID: {0} } )<-[r:hasWorker]-(c:Crew) return c")
    Set<Crew> getCrewsOfWorker(@Param("0")String personID);
}
