package eu.accept.tiekinetix.nexus.profiles.bim.model;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import static java.util.stream.Collectors.toSet;
import javax.xml.bind.annotation.XmlRootElement;

import eu.accept.tiekinetix.nexus.profiles.util.StringifiedArrayDeserializer;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

/**
 * Created by arian.kuschki on 12/06/17.
 */
@NodeEntity
@XmlRootElement(name = "Type")
@Getter
@Setter
@ToString(callSuper = true)
public class BimType extends BimEntity implements HasCategories, HasEmbeddedParameters {

    /*	// PM included
            @JsonDeserialize(using = StringifiedArrayDeserializer.class)
            @JacksonXmlProperty(localName = "GuestElementIds")
            private Set<Long> guestElementIds = new HashSet<>();
        // PM included up to here */
    @Relationship
    private Set<BimCategory> categories;

    @Relationship
    private Set<BimFamily> families;

    @Relationship
    private Set<BimMaterial> materials = new HashSet<>();

    @JacksonXmlProperty(localName = "IdCategory")
    private Long idCategory;

    @JsonDeserialize(using = StringifiedArrayDeserializer.class)
    @JacksonXmlProperty(localName = "MaterialsIds")
    private Set<Long> materialsIds = new HashSet<>();

    @JacksonXmlProperty(localName = "Name")
    private String name;

    @Relationship
    @JacksonXmlProperty(localName = "Parameter")
    @JacksonXmlElementWrapper(localName = "Parameters")
    private Set<BimParameter> parameters = new HashSet<>();

    @JsonIgnoreProperties
    @Override
    public Set<Long> getCategoryIds() {
        final Set<Long> categoryIds = parameters.stream()
                .filter(p -> "Category".equals(p.getName()))
                .map(BimParameter::getValue)
                .map(Long::parseLong)
                .collect(toSet());
        if (idCategory != null) {
            categoryIds.add(idCategory);
        }
        return categoryIds;
    }

    @Override
    public Set<Long> getParamIds(final String name) {
        return getParamsByName(name).stream()
                .map(BimParameter::getValue)
                .map(Long::parseLong)
                .collect(toSet());
    }

    @Override
    public void setParameters(final Set<BimParameter> parameters) {
        this.parameters = parameters;
    }

    @Override
    public Set<BimParameter> getParamsByName(String name) {
        return parameters.stream()
                .filter(p -> name.equals(p.getName()))
                .collect(toSet());
    }

    @Override
    public Set<BimParameter> getParameters() {
        return parameters;
    }

    @Override
    public Set<BimCategory> getCategories() {
        return categories;
    }

    @Override
    public void setCategories(Set<BimCategory> categories) {
        this.categories = categories;
    }

    public Set<BimMaterial> getMaterialsIds() {
        return materials;
    }

    public Set<BimMaterial> getMaterials() {
        return materials;
    }
    public void setMaterials(Set<BimMaterial> materials) {
        this.materials = materials;
    }

    public void setFamilies(Set<BimFamily> families) {
        this.families = families;
    }

    @JsonProperty("IdCategory")
    public Long getIdCategory() {
        return idCategory;
    }

    @JsonProperty("IdCategory")
    public void setIdCategory(Long idCategory) {
        this.idCategory = idCategory;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }

}
