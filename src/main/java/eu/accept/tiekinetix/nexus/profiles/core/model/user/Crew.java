package eu.accept.tiekinetix.nexus.profiles.core.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.CrewType;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Task;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by vchepegin on 27/01/2017.
 */
@NodeEntity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Crew implements Serializable {

    private static final long serialVersionUID = -3741212585487386074L;
    @GraphId
    private Long id;

    private String crewID;
    private String crewName;
    private CrewType crewType;
    private String acronym;
    private double numberOfPlannedWorker;
    private double numberOfActualWorker;


    @Relationship (type="hasOwner", direction = Relationship.OUTGOING)
    public Person owner;

    /**
     * Add a new Owner or replace existing one to the crew
     * @param person
     * @return Crew for the fluent interface
     */
    public Crew setOwner (Person person) {
        this.owner = person;

        return this;
    }

    public Person getOwner () {
        return owner;
    }

    @Relationship (type="hasWorker", direction = Relationship.OUTGOING)
    public Set<Person> crewMembers;

    /**
     * Add a new Member to the crew
     * @param person
     * @return Crew for the fluent interface
     */
    public Crew addCrewMember (Person person) {
        if (crewMembers == null) {
            crewMembers = new HashSet<>();
        }
        crewMembers.add(person);

        return this;
    }

    public Set<Person> getCrewMembers () {
        return crewMembers;
    }

    public boolean removeCrewMember (Person person) {
        boolean status = false;
        if (crewMembers != null) {
            status = crewMembers.remove(person);
        }
        return status;
    }

    @Relationship(type="hasEquipment", direction = Relationship.OUTGOING)
    public Set<Equipment> equipment;

    /**
     * Add new equipment to the crew
     * @param newEquipment
     * @return Task for the fluent interface
     */
    public Crew addEquipment (Equipment newEquipment) {
        if (equipment == null) {
            equipment = new HashSet<>();
        }
        equipment.add(newEquipment);

        return this;
    }

    /**
     * Remove an equipment from the crew
     * @param oldEquipment
     * @return
     */
    public Crew removeEquipment (Equipment oldEquipment) {
        if (oldEquipment != null) {
            equipment.remove(oldEquipment);
        }

        return this;
    }

    @Relationship(type="hasTasksAssigned", direction = Relationship.OUTGOING)
    public Set<Task> assignedTasks;

    /**
     * Add new task event to the crew
     * @param task
     * @return Crew for the fluent interface
     */
    public Crew addTask (Task task) {
        if (assignedTasks == null) {
            assignedTasks = new HashSet<>();
        }
        assignedTasks.add(task);

        return this;
    }

    /**
     * Remove an equipment from the crew
     * @param oldTask
     * @return
     */
    public Crew removeTask (Task oldTask) {
        if (oldTask != null) {
            assignedTasks.remove(oldTask);
        }

        return this;
    }


    /**
     * @return A printable string with the memebrs of the crew
     */
    public String toString() {
        return this.crewName + "'s crew mebmbers => "
                + Optional.ofNullable(this.crewMembers).orElse(
                Collections.emptySet()).stream().map(
                person -> person.getLastName ()).collect(Collectors.toList());
    }

    private Crew() {}

    public Crew(String crewID, String crewName, CrewType crewType, String acronym) {
        this.crewID = crewID;
        this.crewName = crewName;
        this.crewType = crewType;
        this.acronym = acronym;
    }

    @JsonProperty("crewID")
    public String getCrewID() {
        return crewID;
    }

    @JsonProperty("crewID")
    public void setCrewID(String crewID) {
        this.crewID = crewID;
    }

    @JsonProperty("crewName")
    public String getCrewName() {
        return crewName;
    }

    @JsonProperty("crewName")
    public void setCrewName(String crewName) {
        this.crewName = crewName;
    }

    @JsonProperty("crewType")
    public CrewType getCrewType() {
        return crewType;
    }

    @JsonProperty("crewType")
    public void setCrewType(CrewType crewType) {
        this.crewType = crewType;
    }

    @JsonProperty("acronym")
    public String getAcronym() {
        return acronym;
    }

    @JsonProperty("acronym")
    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    @JsonProperty("numberOfPlannedWorker")
    public double getNumberOfPlannedWorker() {
        return numberOfPlannedWorker;
    }

    @JsonProperty("numberOfPlannedWorker")
    public void setNumberOfPlannedWorker(double numberOfPlannedWorker) {
        this.numberOfPlannedWorker = numberOfPlannedWorker;
    }

    @JsonProperty("numberOfActualWorker")
    public double getNumberOfActualWorker() {
        return numberOfActualWorker;
    }

    @JsonProperty("numberOfActualWorker")
    public void setNumberOfActualWorker(double numberOfActualWorker) {
        this.numberOfActualWorker = numberOfActualWorker;
    }
}
