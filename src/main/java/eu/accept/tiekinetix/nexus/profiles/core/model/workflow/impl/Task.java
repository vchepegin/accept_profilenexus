package eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Strings;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.CompletionStatus;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.ScheduleStatus;
import eu.accept.tiekinetix.nexus.profiles.core.model.user.Equipment;
import eu.accept.tiekinetix.nexus.profiles.reporting.Processable;
import eu.accept.tiekinetix.nexus.profiles.reporting.processing.KPIProcessorService;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;
import org.springframework.beans.factory.annotation.Configurable;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by vchepegin on 30/01/2017.
 */
@Configurable
@NodeEntity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Task extends ScheduledEntity implements Processable {

    private static final long serialVersionUID = -7587748983327513763L;

    @GraphId
    private Long taskID;

    private double progress;

    private double ppc;
    private double par;
    private Quantity priceTotal;
    private double cumulativeInitialDailyGoals;
    private double cumulativeDidGoals;
    private long cumulativeDaysMonitored;
    private int cumulativeGoalAchieved;
    private double plannedProgress;
    private double extraEffort;
    private double remainingToDosAbsolute;

    private Date expectedEndDate;

    @Override
    public void acceptKPIRequest(KPIProcessorService processor) {
        processor.processKPI(this);
    }

    /**
     * Static Task factory from an existing task that also instantiate fields by runnign necessary
     * logic such as wrapUp() method. This can be changed or extended. This method helps to avoid errors in
     * a process of creation new Task objects by hiding details of its initialisation.
     *
     * @param task
     * @param id
     * @return
     */
    public static Task getInstanceFromTemplate (Task task, String id) {
        Task result = new Task(task);
        result.setEntityID(id);
        result.wrapUp();
        return result;
    }

    /**
     * Copy constructor. Note, that the entityID has to be set separately AFTER the
     * creation of a new instance!
     * @param task
     */
    public Task (Task task) {
        super (task);

        this.progress = task.getProgress();
        this.ppc = task.getPPC();
        this.par = task.getPAR();
        if (task.quantity != null) {
            this.quantity = new Quantity(task.quantity);
        }

    }

    public Task(String entityId, String name, String description, Double timeout, String tierType,
                ScheduleStatus scheduleStatus, CompletionStatus completionStatus,
                String wbsCode, String lbsCode, int priority) {

        super(entityId, name, description, timeout, tierType, scheduleStatus, completionStatus,
                wbsCode, lbsCode, priority);

        if (Strings.isNullOrEmpty(wbsCode)) {
            throw new IllegalArgumentException("wbsCode has to be initialised!");
        }
    }

    @Relationship(type="hasQuantity", direction = Relationship.OUTGOING)
    public Quantity quantity;
    /**
     * Add an quantity to the ScheduledEntity
     * @param quantity
     * @return ScheduledEntity for the fluent interface
     */
    public ScheduledEntity addQuantity (Quantity quantity) {
        this.quantity = quantity;

        return this;
    }

    public Quantity getQuantity () {
        return this.quantity;
    }

    @Relationship(type="hasTemperatureRange", direction = Relationship.OUTGOING)
    public ValueRange temperatureRange;

    /**
     * Add a temperatureRange to the Task
     * @param temperatureRange
     * @return Task for the fluent interface
     */
    public Task addTemperatureRange (ValueRange temperatureRange) {
        this.temperatureRange = temperatureRange;

        return this;
    }

    @Relationship(type="hasEquipment", direction = Relationship.OUTGOING)
    public Set<Equipment> equipment;

    /**
     * Add new equipment to the task
     * @param e
     * @return Task for the fluent interface
     */
    public Task addEquipment (Equipment e) {
        if (equipment == null) {
            equipment = new HashSet<>();
        }
        equipment.add(e);

        return this;
    }

    public Task () {}

    @JsonProperty("progress")
    public double getProgress() {
        return progress;
    }

    @JsonProperty("progress")
    public void setProgress(double progress) {
        this.progress = progress;
    }

    @JsonProperty("PPC")
    public double getPPC() {
        return ppc;
    }

    @JsonProperty("PPC")
    public void setPPC(double performanceIndicatorPPC) {
        this.ppc = performanceIndicatorPPC;
    }

    @JsonProperty("PAR")
    public double getPAR() {
        return par;
    }

    @JsonProperty("PAR")
    public void setPAR(double performanceIndicatorPAR) {
        this.par = performanceIndicatorPAR;
    }

    @JsonProperty("temperatureRange")
    public ValueRange getTemperatureRange() {
        return temperatureRange;
    }

    @JsonProperty("temperatureRange")
    public void setTemperatureRange(ValueRange temperatureRange) {
        this.temperatureRange = temperatureRange;
    }

    @JsonProperty("equipment")
    public Set<Equipment> getEquipment() {
        return equipment;
    }

    @JsonProperty("priceTotal")
    public Quantity getPriceTotal() {
        return priceTotal;
    }

    @JsonProperty("priceTotal")
    public void setPriceTotal(Quantity priceTotal) {
        this.priceTotal = priceTotal;
    }

    @JsonIgnore
    public double getCumulativeInitialDailyGoals() {
        return cumulativeInitialDailyGoals;
    }

    @JsonProperty("cumulativeInitialDailyGoals")
    public void setCumulativeInitialDailyGoals(double cumulativeInitialDailyGoals) {
        this.cumulativeInitialDailyGoals = cumulativeInitialDailyGoals;
    }

    @JsonProperty("cumulativeDidGoals")
    public double getCumulativeDidGoals() {
        return cumulativeDidGoals;
    }

    @JsonProperty("cumulativeDidGoals")
    public void setCumulativeDidGoals(double cumulativeDidGoals) {
        this.cumulativeDidGoals = cumulativeDidGoals;
    }

    @JsonProperty("cumulativeGoalAchieved")
    public int getCumulativeGoalAchieved() {
        return cumulativeGoalAchieved;
    }

    @JsonProperty("cumulativeGoalAchieved")
    public void setCumulativeGoalAchieved(int cumulativeGoalAchieved) {
        this.cumulativeGoalAchieved = cumulativeGoalAchieved;
    }

    @JsonProperty("plannedProgress")
    public double getPlannedProgress() {
        return plannedProgress;
    }

    @JsonProperty("plannedProgress")
    public void setPlannedProgress(double plannedProgress) {
        this.plannedProgress = plannedProgress;
    }

    @JsonProperty("cumulativeDaysMonitored")
    public long getCumulativeDaysMonitored() {
        return cumulativeDaysMonitored;
    }

    @JsonProperty("cumulativeDaysMonitored")
    public void setCumulativeDaysMonitored(long cumulativeDaysMonitored) {
        this.cumulativeDaysMonitored = cumulativeDaysMonitored;
    }

    @JsonProperty("extraEffort")
    public double getExtraEffort() {
        return extraEffort;
    }

    @JsonProperty("extraEffort")
    public void setExtraEffort(double extraEffort) {
        this.extraEffort = extraEffort;
    }

    @JsonProperty("remainingToDosAbsolute")
    public double getRemainingToDosAbsolute() {
        return remainingToDosAbsolute;
    }

    @JsonProperty("remainingToDosAbsolute")
    public void setRemainingToDosAbsolute(double remainingToDosAbsolute) {
        this.remainingToDosAbsolute = remainingToDosAbsolute;
    }

    @JsonProperty("expectedEndDate")
    public Date getExpectedEndDate() {
        if (expectedEndDate == null) return null;
        return new Date (expectedEndDate.getTime());
    }

    @JsonProperty("expectedEndDate")
    public void setExpectedEndDate(Date expectedEndDate) {
        this.expectedEndDate = new Date (expectedEndDate.getTime());
    }


    @JsonIgnore
    public Long getTaskID() {
        return taskID;
    }

    @Override
    public String toString() {
        return "Task{ ID=" + getEntityID() +
                ", progress=" + progress +
                ", priceTotal=" + priceTotal +
                ", quantity=" + quantity +
                ", owner=" + owner +
                ", location=" + location +
                ", children=" + children +
                '}';
    }

    @Override
    public void reset() {
        super.reset();
        progress = 0;
        ppc = 0;
        par = 0;
        cumulativeInitialDailyGoals = 0;
        cumulativeDidGoals = 0;
        cumulativeDaysMonitored = 0;
        cumulativeGoalAchieved = 0;
        plannedProgress = 0;
        extraEffort = 0;
        if (quantity != null) {
            remainingToDosAbsolute = quantity.getAmount();
        } else {
            remainingToDosAbsolute = 0;
        }
        expectedEndDate = null;
    }


}
