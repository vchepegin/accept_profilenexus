package eu.accept.tiekinetix.nexus.profiles.bim.controller;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import com.google.common.collect.Iterables;
import eu.accept.tiekinetix.nexus.profiles.bim.model.*;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Project;

import static java.util.stream.Collectors.toSet;
import javax.validation.constraints.NotNull;

/**
 * Created by arian.kuschki on 21/06/17.
 * Helper Class
 */
class BimRelationshipService {

    @SafeVarargs
    static Map<String, List<BimParameter>> getUniqueParameters(Set<? extends HasEmbeddedParameters>... elements) {
        return Arrays.stream(elements)
                .flatMap(Collection::stream)
                .flatMap(e -> e.getParameters()
                        .stream())
                .distinct()
                .collect(Collectors.groupingBy(BimParameter::getGuid));
    }

    static <T extends BimEntity> Map<Long, List<T>> groupById(Set<T> entities) {
        return entities.stream()
                .collect(Collectors.groupingBy(BimEntity::getBimId));
    }

    private static void setCategoryLink(final HasCategories entity, Map<Long, List<BimCategory>> categoriesById) {
        entity.setCategories(entity.getCategoryIds()
                .stream()
                .filter(categoriesById::containsKey)
                .map(categoriesById::get)
                .map(Iterables::getOnlyElement)
                .collect(toSet()));
    }

    private static void setElementMaterialLink(final BimElement element, Map<Long, List<BimMaterial>> materialsById) {
        final long bimId = element.getBimId();
        // sometimes elementId == materialId, in this case we need to
        // create the relationship as if the material were an embedded param
        if (materialsById.containsKey(bimId)) {
            element.getMaterials().add(materialsById.get(bimId)
                    .get(0));
        }
    }

    private static void setFamilyLinkForElement(BimElement element, Map<Long, List<BimFamily>> familiesById) {
        element.setFamilies(element.getFamilyIds()
                .stream()
                .filter(familiesById::containsKey)
                .map(familiesById::get)
                .map(Iterables::getOnlyElement)
                .collect(toSet()));
        if (element.getFamilies()
                .isEmpty() && !element.getTypes()
                .isEmpty()) {
            setFamilyLinkViaTypeLinkForElement(element, familiesById);
        }
    }

    private static void setFamilyLinkForType(BimType type, Map<Long, List<BimFamily>> familiesById) {
        type.setFamilies(type.getParamsByName("Family Name")
                .stream()
                .map(BimParameter::getValue)
                .map(value -> familiesById.values()
                        .stream()
                        .map(Iterables::getOnlyElement)
                        .filter(family -> value.equals(family.getName()))
                        .findFirst()
                        .orElse(null))
                .filter(Objects::nonNull)
                .collect(toSet()));
    }

    private static void setFamilyLinkViaTypeLinkForElement(BimElement element, Map<Long, List<BimFamily>> familiesById) {
        element.getFamilies()
                .addAll(element.getTypes()
                        .stream()
                        .flatMap(type -> type.getParamsByName("Family Name")
                                .stream()
                                .map(BimParameter::getValue)
                                .map(value -> familiesById.values()
                                        .stream()
                                        .map(Iterables::getOnlyElement)
                                        .filter(family -> value.equals(family.getName()))
                                        .findFirst()
                                        .orElse(null))
                                .filter(Objects::nonNull))
                        .collect(toSet()));
    }

    //    static void setLinksForElement(BimElement element, Project project, ScheduledEntity scheduledEntity, Map<Long, List<BimType>> typesById, Map<Long, List<BimMaterial>> materialsById, Map<String, List<BimParameter>> parameters, Map<Long, List<BimFamily>> familiesById, Map<Long, List<BimCategory>> categoriesById) {
    static void setLinksForElement(BimElement element, Project project, Map<Long, List<BimType>> typesById, Map<Long, List<BimMaterial>> materialsById, Map<String, List<BimParameter>> parameters, Map<Long, List<BimFamily>> familiesById, Map<Long, List<BimCategory>> categoriesById) {
//        genericInit(element, project, scheduledEntity);
        genericInit(element, project);
        setCategoryLink(element, categoriesById);
        setTypeLink(element, typesById);
        setElementMaterialLink(element, materialsById);
        setFamilyLinkForElement(element, familiesById);
        uniquifyEmbeddedParameters(parameters, element);
//        genericInitForEmbeddedParameters(element, project, scheduledEntity);
        genericInitForEmbeddedParameters(element, project);
    }

    //    static void setLinksForFamily(BimFamily family, Project project, ScheduledEntity scheduledEntity, Map<Long, List<BimCategory>> categoriesById) {
    static void setLinksForFamily(BimFamily family, Project project, Map<Long, List<BimCategory>> categoriesById) {
//        genericInit(family, project, scheduledEntity);
        genericInit(family, project);
        setCategoryLink(family, categoriesById);
    }

    //    static void setLinksForMaterial(BimMaterial material, Project project, ScheduledEntity scheduledEntity, Map<Long, List<BimCategory>> categoriesById, Map<String, List<BimParameter>> parameters) {
    static void setLinksForMaterial(BimMaterial material, Project project, Map<Long, List<BimCategory>> categoriesById, Map<String, List<BimParameter>> parameters) {
//        genericInit(material, project, scheduledEntity);
        genericInit(material, project);
        setCategoryLink(material, categoriesById);
        uniquifyEmbeddedParameters(parameters, material);
//        genericInitForEmbeddedParameters(material, project, scheduledEntity);
        genericInitForEmbeddedParameters(material, project);
    }

    static void setLinksForRoom(BimRoom room, Project project, Map<String, List<BimParameter>> parameters) {
//      genericInit(material, project, scheduledEntity);
        genericInit(room, project);
        uniquifyEmbeddedParameters(parameters, room);
//      genericInitForEmbeddedParameters(material, project, scheduledEntity);
        genericInitForEmbeddedParameters(room, project);
    }

    //    static void setLinksForType(BimType type, Project project, ScheduledEntity scheduledEntity, Map<Long, List<BimFamily>> familiesById, Map<Long, List<BimMaterial>> materialsById, Map<String, List<BimParameter>> parameters, Map<Long, List<BimCategory>> categoriesById) {
    static void setLinksForType(BimType type, Project project, Map<Long, List<BimFamily>> familiesById, Map<Long, List<BimMaterial>> materialsById, Map<String, List<BimParameter>> parameters, Map<Long, List<BimCategory>> categoriesById) {
//        genericInit(type, project, scheduledEntity);
        genericInit(type, project);
        setCategoryLink(type, categoriesById);
        setFamilyLinkForType(type, familiesById);
        setMaterialsLinks(type, materialsById);
        uniquifyEmbeddedParameters(parameters, type);
//        genericInitForEmbeddedParameters(type, project, scheduledEntity);
        genericInitForEmbeddedParameters(type, project);
    }

    private static void setMaterialsLinks(final BimType type, Map<Long, List<BimMaterial>> materialsById) {
        type.setMaterials(type.getMaterialsIds()
                .stream()
                .filter(materialsById::containsKey)
                .map(materialsById::get)
                .map(ids -> ids.get(0))
                .collect(toSet()));
    }

    //    static <T extends BimEntity> T genericInit(T bimEntity, @NotNull Project project, ScheduledEntity scheduledEntity) {
    static <T extends BimEntity> T genericInit(T bimEntity, @NotNull Project project) {
        bimEntity.setProject(project);
/* PM
        bimEntity.getScheduledEntities()
                 .add(scheduledEntity);
*/
        return bimEntity;
    }

    //    private static void genericInitForEmbeddedParameters(HasEmbeddedParameters entity, Project project, ScheduledEntity scheduledEntity) {
    private static void genericInitForEmbeddedParameters(HasEmbeddedParameters entity, Project project) {
        Set<BimParameter> params = entity.getParameters();
//       entity.setParameters(entity.getParameters()
        entity.setParameters(params.stream()
                .map(p -> {
//                                       genericInit(p, project, scheduledEntity);
                    genericInit(p, project);
                    return p;
                })
                .collect(toSet()));
    }

    private static void setTypeLink(final BimElement element, Map<Long, List<BimType>> typesById) {
        element.setTypes(element.getTypeIds()
                .stream()
                .filter(typesById::containsKey)
                .map(typesById::get)
                .map(Iterables::getOnlyElement)
                .collect(toSet()));
    }

    private static void uniquifyEmbeddedParameters(Map<String, List<BimParameter>> parameters, HasEmbeddedParameters entity) {
        entity.setParameters(entity.getParameters()
                .stream()
                .map(BimParameter::getGuid)
                .map(parameters::get)
                .map(Iterables::getOnlyElement)
                .collect(toSet()));
    }


}
