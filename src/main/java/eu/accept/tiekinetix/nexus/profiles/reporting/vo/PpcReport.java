package eu.accept.tiekinetix.nexus.profiles.reporting.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.accept.tiekinetix.nexus.profiles.reporting.KpiReporting;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.Relationship;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * Created by vchepegin on 26/07/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PpcReport implements KpiReporting, Serializable {

    private static final long serialVersionUID = -2746485566281101915L;

    @GraphId
    private Long id;

    private  String projectID;

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date dateMonitored;

    public PpcReport () {};

    public PpcReport(String projectID, Date dateMonitored) {
        this.projectID = projectID;
        this.dateMonitored = dateMonitored;
    }

    @Relationship(type="hasActivityPPC", direction = Relationship.OUTGOING)
    public Set<PpcActivity> ppc4Activity;
    /**
     * Add ppc to the report
     * @param ppc
     * @return PpcReport for the fluent interface
     */
    public PpcReport addPpc (PpcActivity ppc) {
        if (ppc4Activity == null) {
            ppc4Activity = new HashSet<>();
        }
        ppc4Activity.add(ppc);

        return this;
    }

    public Set<PpcActivity> getPPC () {
        return this.ppc4Activity;
    }

    @JsonProperty("projectID")
    public String getProjectID() {
        return projectID;
    }

    @JsonProperty("projectID")
    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    @JsonProperty("dateMonitored")
    public Date getDateMonitored() {
        return (this.dateMonitored == null? null : new Date(this.dateMonitored.getTime()));
    }

    @JsonProperty("dateMonitored")
    public void setDateMonitored(Date dateMonitored) {
        //if not set make it the current system time
        this.dateMonitored = (dateMonitored == null) ?
                new Date (System.currentTimeMillis()) : new Date(dateMonitored.getTime());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PpcReport)) return false;

        PpcReport ppcReport = (PpcReport) o;

        if (id != null ? !id.equals(ppcReport.id) : ppcReport.id != null) return false;
        if (!projectID.equals(ppcReport.projectID)) return false;
        return dateMonitored.equals(ppcReport.dateMonitored);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + projectID.hashCode();
        result = 31 * result + dateMonitored.hashCode();
        return result;
    }
}
