package eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl;

import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

import java.io.Serializable;

/**
 * Created by vchepegin on 02/02/2017.
 */
@NodeEntity
public class Supplier implements Serializable {
    private static final long serialVersionUID = -4520446076078638574L;
    @GraphId
    private Long id;

}
