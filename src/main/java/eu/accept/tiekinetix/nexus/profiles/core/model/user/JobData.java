package eu.accept.tiekinetix.nexus.profiles.core.model.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

import java.io.Serializable;

/**
 * Created by vchepegin on 30/01/2017.
 */
@NodeEntity
public class JobData implements Serializable {

    private static final long serialVersionUID = -8824859170820387322L;
    @GraphId
    private Long id;

    private String jobDataID;
    private double fte;
    private String rating;
    private String otherData;

    public JobData(String jobDataID, double fte, String rating, String otherData) {
        this.jobDataID = jobDataID;
        this.fte = fte;
        this.rating = rating;
        this.otherData = otherData;
    }

    private JobData() {}

    @JsonProperty("jobDataID")
    public String getJobDataID() {
        return jobDataID;
    }

    @JsonProperty("jobDataID")
    public void setJobDataID(String jobDataID) {
        this.jobDataID = jobDataID;
    }

    @JsonProperty("fte")
    public double getFte() {
        return fte;
    }

    @JsonProperty("fte")
    public void setFte(double fte) {
        this.fte = fte;
    }

    @JsonProperty("rating")
    public String getRating() {
        return rating;
    }

    @JsonProperty("rating")
    public void setRating(String rating) {
        this.rating = rating;
    }

    @JsonProperty("otherData")
    public String getOtherData() {
        return otherData;
    }

    @JsonProperty("otherData")
    public void setOtherData(String otherData) {
        this.otherData = otherData;
    }
}
