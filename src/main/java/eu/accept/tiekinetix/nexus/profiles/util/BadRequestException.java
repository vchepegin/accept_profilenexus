package eu.accept.tiekinetix.nexus.profiles.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {
    private String reason;

    public BadRequestException(String message) {
        super(message);
    }
}
