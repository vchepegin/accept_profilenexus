package eu.accept.tiekinetix.nexus.profiles.core.model.enumerations;

/**
 * Created by vchepegin on 21/02/2017.
 */
public enum Priority {
    SHOW_STOPPER (1),
    CRITICAL (2),
    HIGH (3),
    NORMAL (4),
    LOW (5);

    private final int priorityCode;

    Priority(int code) {
        this.priorityCode = Integer.valueOf(code);
    }

    private int getPriorityCode () {
        return priorityCode;
    }

    @Override
    public String toString () {
        String tmp = "Not defined";
        switch (priorityCode) {
            case 1:
                tmp = "SHOW_STOPPER";
                break;

            case 2:
                tmp = "CRITICAL";
                break;

            case 3:
                tmp = "HIGH";
                break;
            case 4:
                tmp = "NORMAL";
                break;
            case 5:
                tmp = "LOW";
                break;
        }

        return tmp;
    }

}