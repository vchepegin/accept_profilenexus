package eu.accept.tiekinetix.nexus.profiles.bim.repository;

import java.util.List;

import eu.accept.tiekinetix.nexus.profiles.bim.model.BimCategory;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface CategoryRepository extends GraphRepository<BimCategory> {
    List<BimCategory> findByProjectProjectIDAndBimId(String projectID, Long bimId);

}
