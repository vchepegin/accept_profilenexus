package eu.accept.tiekinetix.nexus.profiles.bim.repository;

import java.util.List;

import eu.accept.tiekinetix.nexus.profiles.bim.model.BimFile;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

//import eu.accept.tiekinetix.bimimport.model.BimFile;

@RepositoryRestResource(exported = false)
public interface BimFileRepository extends GraphRepository<BimFile> {
    List<BimFile> findByModel(@Param("model") String model);

    @Query("match (a:Bim)-->(:Project {name: {0}} ) return a;")
    List<BimFile> findByProject(@Param("0")String name);

}
