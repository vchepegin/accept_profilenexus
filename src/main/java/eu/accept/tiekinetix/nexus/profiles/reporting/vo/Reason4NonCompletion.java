package eu.accept.tiekinetix.nexus.profiles.reporting.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Activity;
import org.springframework.data.annotation.Id;
//import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 * Created by vchepegin on 08/08/2017.
 */
//@Document(collection = "noncompletion")
public final class Reason4NonCompletion {

    @Id
    private String id;

    private String projectID;
    private String entityID;
    private String lbsCode;
    private String wbsCode;
    private Date dateMonitored;

    private int rncCode;

    public Reason4NonCompletion(String projectID, String entityID, String lbsCode,
                                String wbsCode, Date dateMonitored, int rncCode) {
        this.projectID = projectID;
        this.entityID = entityID;
        this.lbsCode = lbsCode;
        this.wbsCode = wbsCode;
        this.dateMonitored = dateMonitored;
        this.rncCode = rncCode;
    }

    public static Reason4NonCompletion getRNCInstance (Activity activity, String projectID, int rncCode) {
        Reason4NonCompletion reason = new Reason4NonCompletion(projectID, activity.getEntityID(),
                activity.getLbsCode(), activity.getWbsCode(), activity.getDateMonitored(), rncCode);
        return reason;
    }

    public Reason4NonCompletion() {};

    @JsonProperty("projectID")
    public String getProjectID() {
        return projectID;
    }

    @JsonProperty("projectID")
    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    @JsonProperty("entityID")
    public String getEntityID() {
        return entityID;
    }

    @JsonProperty("entityID")
    public void setEntityID(String entityID) {
        this.entityID = entityID;
    }

    @JsonProperty("lbsCode")
    public String getLbsCode() {
        return lbsCode;
    }

    @JsonProperty("lbsCode")
    public void setLbsCode(String lbsCode) {
        this.lbsCode = lbsCode;
    }

    @JsonProperty("wbsCode")
    public String getWbsCode() {
        return wbsCode;
    }

    @JsonProperty("wbsCode")
    public void setWbsCode(String wbsCode) {
        this.wbsCode = wbsCode;
    }

    @JsonProperty("rncCode")
    public int getRncCode() {
        return rncCode;
    }

    @JsonProperty("rncCode")
    public void setRncCode(int rncCode) {
        this.rncCode = rncCode;
    }

    @JsonProperty("rnc")
    public Date getDateMonitored() {
        return dateMonitored;
    }

    @JsonProperty("rnc")
    public void setDateMonitored(Date dateMonitored) {
        this.dateMonitored = dateMonitored;
    }

    @Override
    public String toString() {
        return String.format("R4NC [id=%s, projectID='%s', lbsCode='%s', wbsCode='%s', rnc code='%d']",
                id, projectID, lbsCode, wbsCode, rncCode);
    }
}
