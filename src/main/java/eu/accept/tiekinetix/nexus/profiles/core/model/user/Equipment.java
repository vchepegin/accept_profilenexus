package eu.accept.tiekinetix.nexus.profiles.core.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.EquipmentStatus;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by vchepegin on 27/01/2017.
 */
@NodeEntity
@JsonIgnoreProperties(ignoreUnknown = true)

public class Equipment implements Serializable {

    private static final long serialVersionUID = -8843101772347056570L;
    @GraphId
    private Long id;

    private String equipmentID;
    private String equipmentName;
    private EquipmentStatus equipmentStatus;
    private String acronym;
    private Date availableAt;

    public Equipment(String equipmentID, String equipmentName, EquipmentStatus equipmentStatus,
                     String acronym, Date availableAt) {
        this.equipmentID = equipmentID;
        this.equipmentName = equipmentName;
        this.equipmentStatus = equipmentStatus;
        this.acronym = acronym;
        this.availableAt = new Date (availableAt.getTime());
    }

    private Equipment() {}

    @JsonProperty("equipmentID")
    public String getEquipmentID() {
        return equipmentID;
    }

    @JsonProperty("equipmentID")
    public void setEquipmentID(String equipmentID) {
        this.equipmentID = equipmentID;
    }

    @JsonProperty("equipmentName")
    public String getEquipmentName() {
        return equipmentName;
    }

    @JsonProperty("equipmentName")
    public void setEquipmentName(String equipmentName) {
        this.equipmentName = equipmentName;
    }

    @JsonProperty("equipmentStatus")
    public EquipmentStatus getEquipmentStatus() {
        return equipmentStatus;
    }

    @JsonProperty("equipmentStatus")
    public void setEquipmentStatus(EquipmentStatus equipmentStatus) {
        this.equipmentStatus = equipmentStatus;
    }

    @JsonProperty("acronym")
    public String getAcronym() {
        return acronym;
    }

    @JsonProperty("acronym")
    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    @JsonProperty("availableAt")
    public Date getAvailableAt() {
        return (Date) availableAt.clone();
    }

    @JsonProperty("availableAt")
    public void setAvailableAt(Date availableAt) {
        this.availableAt = new Date (availableAt.getTime());
    }
}
