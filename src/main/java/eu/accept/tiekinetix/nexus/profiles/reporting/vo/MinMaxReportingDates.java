package eu.accept.tiekinetix.nexus.profiles.reporting.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.neo4j.ogm.annotation.Property;
import org.springframework.data.neo4j.annotation.QueryResult;

import java.util.Date;

/**
 * Created by vchepegin on 07/09/2017.
 */

@QueryResult
public class MinMaxReportingDates {

    @Property(name = "MIN(n.dateMonitored)")
    private Date min;
    @Property(name = "MAX(n.dateMonitored)")
    private Date max;

    public MinMaxReportingDates() {};

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonProperty("first")
    public Date getMin() {
        return min;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonProperty("last")
    public Date getMax() {
        return max;
    }
}
