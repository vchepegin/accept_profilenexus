/**
 * Created by pmerz on 20/10/2017.
 */
package eu.accept.tiekinetix.nexus.profiles.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import eu.accept.tiekinetix.nexus.profiles.core.model.user.Crew;
import eu.accept.tiekinetix.nexus.profiles.core.model.user.Person;
import org.apache.http.HttpResponse;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.neo4j.ogm.json.JSONArray;
import org.neo4j.ogm.json.JSONException;
import org.neo4j.ogm.json.JSONObject;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TikkiQueue {
    private String name;
    private String icon;
    private Boolean notifyUserOnNewTickets;
    private String _id;
    private Set<String> subQueues;
    private Set<String> representativeUserId;
    private Set<String> assignedUserId;
    private Set<TikkiQueueAdditionalProperties> additionalProperties;

    private static final ObjectMapper mapper = new ObjectMapper();

    @JsonProperty("additionalProperties")
    public Set<TikkiQueueAdditionalProperties> getAdditionalProperties() { return this.additionalProperties;}

    @JsonProperty("additionalProperties")
    public void setAdditionalProperties(Set<TikkiQueueAdditionalProperties> newValue) { this.additionalProperties = newValue;}

    public void addAdditionalProperties(TikkiQueueAdditionalProperties newValue) {
        this.additionalProperties.add(newValue);
    }

    @JsonProperty("name")
    public String getName() { return this.name;}

    @JsonProperty("name")
    public void setName(String name) { this.name = name;}

    @JsonProperty("icon")
    public String getIcon() { return this.icon;}

    @JsonProperty("icon")
    public void setIcon(String icon) { this.icon = icon;}

    @JsonProperty("notifyUserOnNewTickets")
    public Boolean getNotifyUserOnNewTickets() { return this.notifyUserOnNewTickets;}

    @JsonProperty("notifyUserOnNewTickets")
    public void setNotifyUserOnNewTickets(Boolean notifyUserOnNewTickets) { this.notifyUserOnNewTickets = notifyUserOnNewTickets;}

//    @JsonProperty("_id")
    public String get_id() { return this._id;}

    @JsonProperty("_id")
    public void set_id(String _id) { this._id = _id;}

    @JsonProperty("subQueues")
    public Set<String> getSubQueues() { return this.subQueues;}

    @JsonProperty("subQueues")
    public void setSubQueues(Set<String> subQueues) { this.subQueues = subQueues;}

    @JsonProperty("representativeUserId")
    public Set<String> getRepresentativeUserId() { return this.representativeUserId;}

    @JsonProperty("representativeUserId")
    public void setRepresentativeUserId(Set<String> representativeUserId) { this.representativeUserId = representativeUserId;}

    @JsonProperty("assignedUserId")
    public Set<String> getAssignedUserId() { return this.assignedUserId;}

    @JsonProperty("assignedUserId")
    public void setAssignedUserId(Set<String> assignedUserId) { this.assignedUserId = assignedUserId;}

    static public String createTikkiQueueForOwner(String ownerTikkiID, String entityCode, String projectName) throws IOException {
        final String url = "https://tikki-api.accept-project.com/api/queuesKey";
        String queueName = entityCode + "_" + projectName;
        Date now = new Date();
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);

        JSONObject json = new JSONObject();
        try {
            json.put("name", queueName);
            json.put("icon", "icon-email-open");
            json.put("mainQueue", false);
            json.put("notifyUserOnNewTickets", true);
            JSONArray jsonArray = new JSONArray();
            JSONObject managerRecord = new JSONObject();
            managerRecord.put("key", "assignedManagers");
            managerRecord.put("expireDate", now.toString());
            managerRecord.append("value",ownerTikkiID );
            json.put("additionalProperties",managerRecord );
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        String jsonPayload = json.toString();
        StringEntity entity = new StringEntity(jsonPayload);
        httpPost.setEntity(entity);
        httpPost.setHeader("Content-type", "application/json");
        httpPost.setHeader("APIKEY", "549657169");

        ResponseHandler<String> handler = new BasicResponseHandler();
        HttpResponse response = client.execute(httpPost);
        String body = handler.handleResponse(response);
        System.out.println("New TIKKI Queue created: " + response.toString());
        // Check if the response 200. Otherwise BAD_REQUEST
        client.close();
        TikkiQueue theNewTikkiQueue = mapper.readValue(body, TikkiQueue.class);
        String tikkiQueueID = theNewTikkiQueue.get_id();
        return tikkiQueueID;
    }

    static public String createTikkiQueueForCrew(Set<Crew> theCrews, String entityCode, String projectName) throws IOException {
        final String url = "https://tikki-api.accept-project.com/api/queuesKey";
        String queueName = entityCode + "_" + projectName;
        Set<String> tikkiUserIDs = new HashSet<String>();
        for (Crew theCrew : theCrews) {
            Set<Person> persons = theCrew.getCrewMembers();
            for (Person person : persons) {
                if (person.getTikkiUserID() != null) {
                    tikkiUserIDs.add(person.getTikkiUserID());
                }
            }
        }
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(url);

        JSONObject json = new JSONObject();
        try {
            json.put("name", queueName);
            json.put("icon", "icon-email-open");
            json.put("mainQueue", false);
            json.put("notifyUserOnNewTickets", true);
            json.put("assignedUserId",tikkiUserIDs );
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        String jsonPayload = json.toString();
        StringEntity entity = new StringEntity(jsonPayload);
        httpPost.setEntity(entity);
        httpPost.setHeader("Content-type", "application/json");
        httpPost.setHeader("APIKEY", "549657169");

        ResponseHandler<String> handler = new BasicResponseHandler();
        HttpResponse response = client.execute(httpPost);
        String body = handler.handleResponse(response);
        System.out.println("New TIKKI Queue created: " + response.toString());
        // Check if the response 200. Otherwise BAD_REQUEST
        client.close();
        TikkiQueue theNewTikkiQueue = mapper.readValue(body, TikkiQueue.class);
        String tikkiQueueID = theNewTikkiQueue.get_id();
        return tikkiQueueID;
    }
    /*
        public static String getResponseBody(HttpEntity entity) throws ParseException, IOException {
            return EntityUtils.toString(entity, "UTF-8");
        }
    */
    static public void updateTikkiQueue(String ownerTikkiID, String tikkiQueueID)  throws IOException {
        final String url = "https://tikki-api.accept-project.com/api/queuesKey/" + tikkiQueueID;
        CloseableHttpClient client = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        httpGet.setHeader("Content-type", "application/json");
        httpGet.setHeader("APIKEY", "549657169");
        ResponseHandler<String> handler = new BasicResponseHandler();
        HttpResponse response = client.execute(httpGet);
        String body = handler.handleResponse(response);
        // Check if the response 200. Otherwise BAD_REQUEST
        client.close();
        TikkiQueue theNewTikkiQueue = mapper.readValue(body, TikkiQueue.class);
        Set<TikkiQueueAdditionalProperties> managers = theNewTikkiQueue.getAdditionalProperties();
        for (TikkiQueueAdditionalProperties firstElement : managers) {
            firstElement.addManager(ownerTikkiID);
        }
        theNewTikkiQueue.setAdditionalProperties(managers);
        CloseableHttpClient postClient = HttpClients.createDefault();
        HttpPut httpPut = new HttpPut(url);
        String jsonPayload = mapper.writeValueAsString(theNewTikkiQueue);
        StringEntity entity = new StringEntity(jsonPayload);
        httpPut.setEntity(entity);
        httpPut.setHeader("Content-type", "application/json");
        httpPut.setHeader("APIKEY", "549657169");

        response = postClient.execute(httpPut);
        postClient.close();
    }

    static public void updateTikkiQueueForCrew(Set<Crew> theCrews, String tikkiQueueID)  throws IOException {
        if (tikkiQueueID == null || tikkiQueueID.isEmpty()) {
            return;
        }
        final String url = "https://tikki-api.accept-project.com/api/queuesKey/" + tikkiQueueID;
        CloseableHttpClient client = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        httpGet.setHeader("Content-type", "application/json");
        httpGet.setHeader("APIKEY", "549657169");
        ResponseHandler<String> handler = new BasicResponseHandler();
        HttpResponse response = client.execute(httpGet);
        String body = handler.handleResponse(response);
        // Check if the response 200. Otherwise BAD_REQUEST
        client.close();
        TikkiQueue theNewTikkiQueue = mapper.readValue(body, TikkiQueue.class);
        Set<String> tikkiUserIDs = new HashSet<String>();
        for (Crew theCrew : theCrews) {
            Set<Person> persons = theCrew.getCrewMembers();
            for (Person person : persons) {
                if (person.getTikkiUserID() != null) {
                    tikkiUserIDs.add(person.getTikkiUserID());
                }
            }
        }
        theNewTikkiQueue.setAssignedUserId(tikkiUserIDs);
        CloseableHttpClient postClient = HttpClients.createDefault();
        HttpPut httpPut = new HttpPut(url);
        String jsonPayload = mapper.writeValueAsString(theNewTikkiQueue);
        StringEntity entity = new StringEntity(jsonPayload);
        httpPut.setEntity(entity);
        httpPut.setHeader("Content-type", "application/json");
        httpPut.setHeader("APIKEY", "549657169");

        response = postClient.execute(httpPut);
        postClient.close();
    }

    static public String createTikkiUser(Person aPerson) throws IOException{
        String tikkiUserID = new String();
        if (aPerson.contactDetails.getEmail() != null && !aPerson.contactDetails.getEmail().isEmpty()) {
            final String url = "https://tikki-api.accept-project.com/api/usersKey";
            CloseableHttpClient client = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost(url);
            JSONObject json = new JSONObject();
            try {
                json.put("email", aPerson.contactDetails.getEmail());
                json.put("firstName", aPerson.getFirstName());
                json.put("lastName", aPerson.getLastName());
                json.put("password", "Accept12");
                json.put("roles", "[\"user\"]");
                json.put("useSPC", false);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String jsonPayload = json.toString();
            StringEntity entity = new StringEntity(jsonPayload);
            httpPost.setEntity(entity);
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("APIKEY", "549657169");

            ResponseHandler<String> handler = new BasicResponseHandler();
            HttpResponse response = client.execute(httpPost);
            String body = handler.handleResponse(response);
            System.out.println("New Person in TIKKI created: " + response.toString());
            // Check if the response 200. Otherwise BAD_REQUEST
            client.close();
            TikkiUser theNewTikkiUser = mapper.readValue(body, TikkiUser.class);
            tikkiUserID = theNewTikkiUser.getId();
        }
        return tikkiUserID;
    }

    static public void sendTikkiMessage(Set<String> toReceiver, String tikkiQueueID, String subject, String textBody) throws IOException{
        if (toReceiver != null && !toReceiver.isEmpty() && tikkiQueueID != null && !tikkiQueueID.isEmpty()) {
            final String url = "https://tikki-api.accept-project.com/api/ticketsKey/new/messages";
            CloseableHttpClient client = HttpClients.createDefault();
            HttpPost httpPost = new HttpPost(url);
            JSONArray toList = new JSONArray();
            String jsonDataString = "{\"type\": \"message\", \"from\": {\"name\": \"ACCEPT Tikki\", " +
                "\"email\": \"tikki-accept@ascora.de\"}, \"cc\": [], \"bcc\": [], \"attachments\": [] }";
            String jsonPayload = new String();
            try {
                JSONObject json = new JSONObject(jsonDataString);
                json.put("subject", subject);
                json.put("textBody", textBody);
                json.put("isReply", false);
                json.put("closeTicket", false);
                json.put("sendOutMail", true);
                json.put("queueId", tikkiQueueID);
                for (String to : toReceiver) {
                    JSONObject jsonEmail = new JSONObject();
                    jsonEmail.put("email", to);
                    json.append("to", jsonEmail);
                }
                jsonPayload = json.toString();
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            StringEntity entity = new StringEntity(jsonPayload);
            httpPost.setEntity(entity);
            httpPost.setHeader("Content-type", "application/json");
            httpPost.setHeader("APIKEY", "549657169");

            ResponseHandler<String> handler = new BasicResponseHandler();
            HttpResponse response = client.execute(httpPost);
            String body = handler.handleResponse(response);
            System.out.println("New TIKKI message sent: " + response.toString());
            // Check if the response 200. Otherwise BAD_REQUEST
            client.close();
        }
    }

}
