package eu.accept.tiekinetix.nexus.profiles.bim.controller;

import java.io.IOException;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static java.util.stream.Collectors.toSet;
import javax.validation.Valid;
import javax.websocket.server.PathParam;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.accept.tiekinetix.nexus.profiles.bim.model.BimElement;
import eu.accept.tiekinetix.nexus.profiles.bim.model.BimParameter;
//import org.neo4j.ogm.json.JSONObject;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.client.RestTemplate;
import org.springframework.web.bind.annotation.*;

/**
 * Created by akuschki on 12/06/2017.
 */

@CrossOrigin
@RestController
//@RequestMapping(value = "/projects/{projectID}/bimElements/{bimId}", produces = {"application/json"}, consumes = {"application/json", "*/*"})
@RequestMapping(value = "/projects/{projectID}/bimElements/{bimId}", produces = {"application/json"}, consumes = {"application/json", "*/*"})
public class BimElementController extends AbstractBimController {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Transactional
    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId) throws IOException {
        bimElementRepository.delete(findOne(projectID, bimId));
    }

    @RequestMapping(method = RequestMethod.GET)
    public BimElement get(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId) throws IOException {
        return findOne(projectID, bimId);
    }

    @Transactional
    @RequestMapping(method = RequestMethod.PUT)
//    public void put(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId, @Valid @RequestBody BimElement element, @PathParam("scheduledEntity") Optional<String> scheduledEntity) throws IOException {
    public void put(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId, @Valid @RequestBody String payload) throws IOException {
        BimElement element = mapper.readValue(payload, BimElement.class);
        validatePayload(bimId, element);
        setTypeLink(element, projectID);
        setCategoryLink(element, projectID);
        setMaterialsLink(element, projectID);
        setFamilyLink(element, projectID);
        setFamilyLinkViaType(element, projectID);
        deduplicateEmbeddedParameters(element, projectID);
        processAndSave(projectID, bimId, element, null);
        buildRelationsToTasks(element, projectID);
    }
    private BimElement findOne(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId) {
        return bimElementRepository.findByProjectProjectIDAndBimId(projectID, bimId)
                .stream()
                .findFirst()
                .orElseThrow(ResourceNotFoundException::new);
    }

    private void setFamilyLink(BimElement element, String projectID) {
        element.setFamilies(element.getFamilyIds()
                .stream()
                .map(id -> familyRepository.findByProjectProjectIDAndBimId(projectID, id)
                        .stream()
                        .findFirst()
                        .orElse(null))
                .filter(Objects::nonNull)
                .collect(toSet()));

    }


    private void setFamilyLinkViaType(BimElement element, String projectID) {
        if (element.getFamilies()
                .isEmpty() && !element.getTypes()
                .isEmpty()) {
            element.getFamilies()
                    .addAll(element.getTypes()
                            .stream()
                            .flatMap(type -> type.getParamsByName("Family Name")
                                    .stream()
                                    .map(BimParameter::getValue)
                                    .map(name -> familyRepository.findByProjectProjectIDAndName(projectID, name)
                                            .stream()
                                            .findFirst()
                                            .orElse(null))
                                    .filter(Objects::nonNull))
                            .collect(toSet()));
        }
    }

    private void setMaterialsLink(BimElement element, String projectID) {
        materialRepository.findByProjectProjectIDAndBimId(projectID, element.getBimId())
                .stream()
                .findFirst()
                .ifPresent(material -> element.getMaterials()
                        .add(material));
    }

    private void setTypeLink(BimElement element, String projectID) {
        element.setTypes(element.getTypeIds()
                .stream()
                .map(id -> typeRepository.findByProjectProjectIDAndBimId(projectID, id)
                        .stream()
                        .findFirst()
                        .orElse(null))
                .filter(Objects::nonNull)
                .collect(toSet()));
    }

}
