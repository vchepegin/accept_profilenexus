package eu.accept.tiekinetix.nexus.profiles.bim.model;

import java.util.Set;

/**
 * Created by arian.kuschki on 19/06/17.
 */
public interface HasCategories {
    public void setCategories(Set<BimCategory> categories);
    public Set<BimCategory> getCategories();
    public Set<Long> getCategoryIds();
}

