package eu.accept.tiekinetix.nexus.profiles.util;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
public class TruthyIntToBooleanDeserializer extends JsonDeserializer<Boolean> {

    @Override public Boolean deserialize(
            final JsonParser p, final DeserializationContext ctxt) throws IOException, JsonProcessingException {
        final int value = p.getIntValue();
        if (value == 0) {
            return false;
        } else if (value == 1) {
            return true;
        } else {
            throw new IllegalArgumentException(value + " + is not a truthy integer");
        }
    }
}
