package eu.accept.tiekinetix.nexus.profiles.core.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by vchepegin on 19/05/2017.
 */
@ResponseStatus(value= HttpStatus.EXPECTATION_FAILED, reason="Person not found.") //409
public class PersonNotFoundException extends RuntimeException {

    public PersonNotFoundException (String personID) {
        super(personID + " could not be found.");
    }
}
