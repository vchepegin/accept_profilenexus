package eu.accept.tiekinetix.nexus.profiles.core.model.user;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Project;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by vchepegin on 01/06/2017.
 */
@NodeEntity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Company implements Serializable {


    private static final long serialVersionUID = -7336096174530079516L;
    @GraphId
    private Long id;

    private String companyID;
    private String companyName;
    private String companyAddress;

    public Company () {}

    @Relationship(type="hasProject", direction = Relationship.OUTGOING)
    public Set<Project> projects;
    /**
     * Add new project to the Company
     * @param project
     * @return Task for the fluent interface
     */
    public Company addProject (Project project) {
        if (projects == null) {
            projects = new HashSet<>();
        }
        projects.add(project);

        return this;
    }

    @JsonProperty("companyID")
    public String getCompanyID() {
        return companyID;
    }

    @JsonProperty("companyID")
    public void setCompanyID(String companyID) {
        this.companyID = companyID;
    }

    @JsonProperty("companyName")
    public String getCompanyName() {
        return companyName;
    }

    @JsonProperty("companyName")
    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @JsonProperty("companyAddress")
    public String getCompanyAddress() {
        return companyAddress;
    }

    @JsonProperty("companyAddress")
    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }
}
