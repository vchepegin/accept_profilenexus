package eu.accept.tiekinetix.nexus.profiles.repositories.core;

import eu.accept.tiekinetix.nexus.profiles.core.model.user.ContactDetails;
import eu.accept.tiekinetix.nexus.profiles.core.model.user.Person;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by vchepegin on 13/09/2017.
 */
public interface ContactDetailsRepository extends GraphRepository<ContactDetails> {

    ContactDetails findByContactDetailsID (String cdID);

    @Query("MATCH (a:Person {personID: {0} }) " +
            "OPTIONAL MATCH (a:Person {personID: {0} })-[r:hasContactInformation*0..1]->(b:ContactDetails) " +
            "RETURN a, r, b")
    Person findByPersonID (@Param("0")String personID);
}
