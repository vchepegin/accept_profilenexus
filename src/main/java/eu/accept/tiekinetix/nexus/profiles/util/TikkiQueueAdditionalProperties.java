package eu.accept.tiekinetix.nexus.profiles.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TikkiQueueAdditionalProperties {
    private String expireDate;
    private String key;
    private Set<String> value;

    public void addManager(String managerTikkiUserID) {
        value.add(managerTikkiUserID);
    }
    @JsonProperty("value")
    public Set<String> getValue() { return this.value;}

    @JsonProperty("value")
    public void setValue(Set<String> newValue) { this.value = newValue;}

    @JsonProperty("key")
    public String getKey() { return this.key;}

    @JsonProperty("key")
    public void setKey(String key) { this.key = key;}

    @JsonProperty("expireDate")
    public String getExpireDate() { return this.expireDate;}

    @JsonProperty("expireDate")
    public void setExpireDate(String expireDate) { this.expireDate = expireDate;}
}
