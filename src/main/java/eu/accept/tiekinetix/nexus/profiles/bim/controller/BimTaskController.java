package eu.accept.tiekinetix.nexus.profiles.bim.controller;

import java.io.IOException;
import java.util.List;


import javax.validation.Valid;
import javax.websocket.server.PathParam;

import eu.accept.tiekinetix.nexus.profiles.bim.repository.*;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by pmerz on 28/08/2017.
 */

@CrossOrigin
@RestController
public class BimTaskController  {

    @Autowired
    protected BimElementRepository bimElementRepository;
    @Autowired
    private MaterialRepository materialRepository;
    @Autowired
    private TypeRepository typeRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private BimEntityRepository bimEntityRepository;


    @CrossOrigin
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value="getBimEntitiesByTaskStatus/{projectId}", method = RequestMethod.GET, produces = {"application/json"})
    public List<Long> get(@Valid @PathVariable("projectId") String projectId, @PathParam("completionStatus") String completionStatus, @PathParam("scheduleStatus") String scheduleStatus) throws IOException {
        List<Long> bimIds = bimEntityRepository.getBimEntitiesByTaskStatus(projectId, completionStatus, scheduleStatus);
        return bimIds;
        //    	return bimEntityRepository.getBimEntitiesByTaskStatus(projectId, completionStatus);
//        return bimEntityRepository.getBimEntitiesByTaskStatus(projectId, completionStatus, scheduledStatus);
    }

    @CrossOrigin
    @RequestMapping(value="/attachBimElementToTask/{projectId}/{bimId}", method = RequestMethod.POST)
    public void attachBimElementToTaskByWbs(@PathVariable(value="projectId") String projectId, @PathVariable(value="bimId") Long bimId,
                                            @PathParam(value="wbsCode") String wbsCode, @PathParam(value="lbsCode") String lbsCode) throws IOException {
        if (wbsCode != null)
        {
            if (lbsCode != null)
            {
                bimElementRepository.attachBimElementToTaskByWbsAndLbs(bimId, wbsCode, lbsCode, projectId);

            }
            else {
                bimElementRepository.attachBimElementToTaskByWbs(bimId, wbsCode, projectId);

            }
        } else if (lbsCode != null) {
            bimElementRepository.attachBimElementToTaskByLbs(bimId, lbsCode, projectId);

        }
    }

    @CrossOrigin
    @RequestMapping(value="/attachBimTypeToTask/{projectId}/{bimId}", method = RequestMethod.POST)
    public void attachBimTypeToTaskByWbs(@PathVariable(value="projectId") String projectId, @PathVariable(value="bimId") Long bimId,
                                         @PathParam(value="wbsCode") String wbsCode, @PathParam(value="lbsCode") String lbsCode) throws IOException {
        if (wbsCode != null)
        {
            if (lbsCode != null)
            {
                typeRepository.attachBimTypeToTaskByWbsAndLbs(bimId, wbsCode, lbsCode, projectId);

            }
            else {
                typeRepository.attachBimTypeToTaskByWbs(bimId, wbsCode, projectId);

            }
        } else if (lbsCode != null) {
            typeRepository.attachBimTypeToTaskByLbs(bimId, lbsCode, projectId);

        }
    }

    @CrossOrigin
    @RequestMapping(value="/attachBimMaterialToTask/{projectId}/{bimId}", method = RequestMethod.POST)
    public void attachBimMaterialToTaskByWbs(@PathVariable(value="projectId") String projectId, @PathVariable(value="bimId") Long bimId,
                                             @PathParam(value="wbsCode") String wbsCode, @PathParam(value="lbsCode") String lbsCode) throws IOException {
        if (wbsCode != null)
        {
            if (lbsCode != null)
            {
                materialRepository.attachBimMaterialToTaskByWbsAndLbs(bimId, wbsCode, lbsCode, projectId);

            }
            else {
                materialRepository.attachBimMaterialToTaskByWbs(bimId, wbsCode, projectId);

            }
        } else if (lbsCode != null) {
            materialRepository.attachBimMaterialToTaskByLbs(bimId, lbsCode, projectId);

        }
    }

    @CrossOrigin
    @RequestMapping(value="/attachBimRoomToTask/{projectId}/{bimId}", method = RequestMethod.POST)
    public void attachBimRoomToTask(@PathVariable(value="projectId") String projectId, @PathVariable(value="bimId") Long bimId,
                                    @PathParam(value="wbsCode") String wbsCode, @PathParam(value="lbsCode") String lbsCode) throws IOException {
        if (wbsCode != null)
        {
            if (lbsCode != null)
            {
                roomRepository.attachBimRoomToTaskByWbsAndLbs(bimId, wbsCode, lbsCode, projectId);

            }
            else {
                roomRepository.attachBimRoomToTaskByWbs(bimId, wbsCode, projectId);

            }
        } else if (lbsCode != null) {
            roomRepository.attachBimRoomToTaskByLbs(bimId, lbsCode, projectId);

        }
    }

    @CrossOrigin
    @RequestMapping(value="/getScheduledEntitiesOfBimRooms/{projectId}", method = RequestMethod.GET)
    public List<ScheduledEntity> getScheduledEntitiesOfBimRooms(@Valid @PathVariable String projectId) throws IOException {
        return roomRepository.getScheduledEntitiesOfBimRooms(projectId);
    }

    @CrossOrigin
    @RequestMapping(value="/getScheduledEntitiesOfBimMaterials/{projectId}", method = RequestMethod.GET)
    public List<ScheduledEntity> getScheduledEntitiesOfBimMaterials(@Valid @PathVariable String projectId) throws IOException {
        return materialRepository.getScheduledEntitiesOfBimMaterials(projectId);
    }

    @CrossOrigin
    @RequestMapping(value="/getScheduledEntitiesOfBimTypes/{projectId}", method = RequestMethod.GET)
    public List<ScheduledEntity> getScheduledEntitiesOfBimTypes(@Valid @PathVariable String projectId) throws IOException {
        return typeRepository.getScheduledEntitiesOfBimTypes(projectId);
    }


    @CrossOrigin
    @RequestMapping(value="/getScheduledEntitiesOfBimElements/{projectId}", method = RequestMethod.GET)
    public List<ScheduledEntity> getScheduledEntitiesOfBimElements(@Valid @PathVariable String projectId) throws IOException {
        return bimElementRepository.getScheduledEntitiesOfBimElements(projectId);
    }
}
