package eu.accept.tiekinetix.nexus.profiles.mocking;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by vchepegin on 31/01/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MockPersonVO {

    private String name;
    private String address;
    private double latitude;
    private double longitude;
    private String maiden_name;
    private String birth_data;
    private String phone_h;
    private String phone_w;
    private String email_u;
    private String email_d;
    private String username;
    private String password;
    private String domain;
    private String useragent;
    private String ipv4;
    private String macaddress;
    private String plasticcard;
    private String cardexpir;
    private double bonus;
    private String company;
    private String color;
    private String uuid;
    private double height;
    private double weight;
    private String blood;
    private String eye;
    private String hair;
    private String pict;
    private String url;
    private String sport;
    private String ipv4_url;
    private String email_url;
    private String domain_url;

    public MockPersonVO() {}

    public MockPersonVO(String name, String address, double latitude, double longitude,
                        String maiden_name, String birth_data, String phone_h, String phone_w,
                        String email_u, String email_d, String username, String password,
                        String domain, String useragent, String ipv4, String macaddress,
                        String plasticcard, String cardexpir, double bonus, String company,
                        String color, String uuid, double height, double weight, String blood,
                        String eye, String hair, String pict, String url, String sport,
                        String ipv4_url, String email_url, String domain_url) {
        this.name = name;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.maiden_name = maiden_name;
        this.birth_data = birth_data;
        this.phone_h = phone_h;
        this.phone_w = phone_w;
        this.email_u = email_u;
        this.email_d = email_d;
        this.username = username;
        this.password = password;
        this.domain = domain;
        this.useragent = useragent;
        this.ipv4 = ipv4;
        this.macaddress = macaddress;
        this.plasticcard = plasticcard;
        this.cardexpir = cardexpir;
        this.bonus = bonus;
        this.company = company;
        this.color = color;
        this.uuid = uuid;
        this.height = height;
        this.weight = weight;
        this.blood = blood;
        this.eye = eye;
        this.hair = hair;
        this.pict = pict;
        this.url = url;
        this.sport = sport;
        this.ipv4_url = ipv4_url;
        this.email_url = email_url;
        this.domain_url = domain_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getMaiden_name() {
        return maiden_name;
    }

    public void setMaiden_name(String maiden_name) {
        this.maiden_name = maiden_name;
    }

    public String getBirth_data() {
        return birth_data;
    }

    public void setBirth_data(String birth_data) {
        this.birth_data = birth_data;
    }

    public String getPhone_h() {
        return phone_h;
    }

    public void setPhone_h(String phone_h) {
        this.phone_h = phone_h;
    }

    public String getPhone_w() {
        return phone_w;
    }

    public void setPhone_w(String phone_w) {
        this.phone_w = phone_w;
    }

    public String getEmail_u() {
        return email_u;
    }

    public void setEmail_u(String email_u) {
        this.email_u = email_u;
    }

    public String getEmail_d() {
        return email_d;
    }

    public void setEmail_d(String email_d) {
        this.email_d = email_d;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getUseragent() {
        return useragent;
    }

    public void setUseragent(String useragent) {
        this.useragent = useragent;
    }

    public String getIpv4() {
        return ipv4;
    }

    public void setIpv4(String ipv4) {
        this.ipv4 = ipv4;
    }

    public String getMacaddress() {
        return macaddress;
    }

    public void setMacaddress(String macaddress) {
        this.macaddress = macaddress;
    }

    public String getPlasticcard() {
        return plasticcard;
    }

    public void setPlasticcard(String plasticcard) {
        this.plasticcard = plasticcard;
    }

    public String getCardexpir() {
        return cardexpir;
    }

    public void setCardexpir(String cardexpir) {
        this.cardexpir = cardexpir;
    }

    public double getBonus() {
        return bonus;
    }

    public void setBonus(double bonus) {
        this.bonus = bonus;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getBlood() {
        return blood;
    }

    public void setBlood(String blood) {
        this.blood = blood;
    }

    public String getEye() {
        return eye;
    }

    public void setEye(String eye) {
        this.eye = eye;
    }

    public String getHair() {
        return hair;
    }

    public void setHair(String hair) {
        this.hair = hair;
    }

    public String getPict() {
        return pict;
    }

    public void setPict(String pict) {
        this.pict = pict;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSport() {
        return sport;
    }

    public void setSport(String sport) {
        this.sport = sport;
    }

    public String getIpv4_url() {
        return ipv4_url;
    }

    public void setIpv4_url(String ipv4_url) {
        this.ipv4_url = ipv4_url;
    }

    public String getEmail_url() {
        return email_url;
    }

    public void setEmail_url(String email_url) {
        this.email_url = email_url;
    }

    public String getDomain_url() {
        return domain_url;
    }

    public void setDomain_url(String domain_url) {
        this.domain_url = domain_url;
    }
}
