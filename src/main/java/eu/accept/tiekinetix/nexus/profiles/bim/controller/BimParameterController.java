package eu.accept.tiekinetix.nexus.profiles.bim.controller;

import java.io.IOException;

import javax.validation.Valid;

import eu.accept.tiekinetix.nexus.profiles.bim.model.BimParameter;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by akuschki on 12/06/2017.
 */

@CrossOrigin
@RestController
@RequestMapping(value = "/projects/{projectID}/bimParameters/{bimId}", produces = {"application/json"}, consumes = {"*/*"})
public class BimParameterController extends AbstractBimController {

    @Transactional
    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId) throws IOException {
        parameterRepository.delete(findOne(projectID, bimId));
    }

    @RequestMapping(method = RequestMethod.GET)
    public BimParameter get(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId) throws IOException {
        return findOne(projectID, bimId);
    }

    private BimParameter findOne(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId) {
        return parameterRepository.findByProjectProjectIDAndBimId(projectID, bimId)
                .stream()
                .findFirst()
                .orElseThrow(ResourceNotFoundException::new);
    }
}
