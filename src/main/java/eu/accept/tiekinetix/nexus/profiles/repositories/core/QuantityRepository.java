package eu.accept.tiekinetix.nexus.profiles.repositories.core;

import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Quantity;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.scheduling.annotation.Async;

/**
 * Created by vchepegin on 22/03/2017.
 */
public interface QuantityRepository extends GraphRepository<Quantity> {

    @Async
    Quantity findByQuantityID (String id);
}
