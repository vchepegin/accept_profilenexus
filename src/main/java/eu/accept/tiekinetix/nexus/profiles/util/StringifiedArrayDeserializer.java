package eu.accept.tiekinetix.nexus.profiles.util;

import java.io.IOException;
import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

public class StringifiedArrayDeserializer extends JsonDeserializer<Set<Long>> {

    @Override public Set<Long> deserialize(
            final JsonParser p, final DeserializationContext ctxt) throws IOException, JsonProcessingException {
        return Arrays.stream(p.getValueAsString().split("[;\\]\\[]")).map(String::trim).filter(id -> !id.isEmpty())
                .map(Long::parseLong).collect(Collectors.toSet());
    }
}
