package eu.accept.tiekinetix.nexus.profiles.reporting.bigQuery;

import eu.accept.tiekinetix.nexus.profiles.reporting.KpiReporting;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 * This is an asynchronous logger for the historisation of the KPI records that writes to the BigQuery
 * Created by vchepegin on 30/06/2017.
 */
@Service
public class KpiRepositoryService {

    @Async
    public void saveKPIRecord (KpiReporting br) {
        //dump record to BigQuery
    }
}
