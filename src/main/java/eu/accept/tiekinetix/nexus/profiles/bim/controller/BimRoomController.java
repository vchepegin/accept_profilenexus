package eu.accept.tiekinetix.nexus.profiles.bim.controller;

import java.io.IOException;
import java.util.Optional;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.accept.tiekinetix.nexus.profiles.bim.model.BimRoom;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by pMerz on 07/09/2017.
 */

@CrossOrigin
@RestController
@RequestMapping(value = "/projects/{projectID}/bimRooms/{bimId}",produces = {"application/json"}, consumes = {"application/json", "*/*"})
public class BimRoomController extends AbstractBimController {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Transactional
    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId) throws IOException {
        roomRepository.delete(findOne(projectID, bimId));
    }

    @RequestMapping(method = RequestMethod.GET)
    public BimRoom get(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId) throws IOException {
        return findOne(projectID, bimId);
    }

    @Transactional
    @RequestMapping(method = RequestMethod.PUT)
//    public void put(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId, @Valid @RequestBody BimRoom room, @PathParam("setScheduledEntity") Optional<String> scheduledEntity) throws IOException {
    public void put(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId, @Valid @RequestBody String payload) throws IOException {
        BimRoom room = mapper.readValue(payload, BimRoom.class);
        validatePayload(bimId, room);
        deduplicateEmbeddedParameters(room, projectID);
        processAndSave(projectID, bimId, room, null);
        buildRelationsToTasks(room, projectID);
    }

    private BimRoom findOne(String projectID, Long bimId) {
        return roomRepository.findByProjectProjectIDAndBimId(projectID, bimId)
                .stream()
                .findFirst()
                .orElseThrow(ResourceNotFoundException::new);
    }
}
