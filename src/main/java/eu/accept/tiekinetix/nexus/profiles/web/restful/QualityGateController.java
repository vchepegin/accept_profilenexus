package eu.accept.tiekinetix.nexus.profiles.web.restful;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.QualityGate;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Task;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.QualityGateRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by vchepegin on 24/02/2017.
 */
@CrossOrigin
@RestController
public class QualityGateController {

    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private QualityGateRepository qualityGateRepository;

    private final ObjectMapper mapper = new ObjectMapper();

    @CrossOrigin
    @RequestMapping(value="/getQualityGateByID/{qualityGateID}", method = RequestMethod.GET)
    public QualityGate getQualityGateByID (@PathVariable(value="qualityGateID") String qualityGateID)
            throws Exception {
        QualityGate qualityGate = qualityGateRepository.findByEntityID(qualityGateID);
        return qualityGate;
    }

    @CrossOrigin
    @RequestMapping(value="/createQualityGate/{parentTaskID}", method = RequestMethod.POST)
    public String createQualityGate (@RequestBody String payload,
                              @PathVariable(value="parentTaskID") String parentTaskID)
            throws Exception {
        QualityGate newQualityGate = null;
        newQualityGate = mapper.readValue(payload, QualityGate.class);

        Task task = taskRepository.findByEntityID(parentTaskID);
        task.addChild(newQualityGate);
        String guid = java.util.UUID.randomUUID().toString();
        newQualityGate.setEntityID(guid);

        taskRepository.save(task);

        return guid;
    }

}
