package eu.accept.tiekinetix.nexus.profiles.web.restful;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.accept.tiekinetix.nexus.messaging.ChangedEntitiesLoggingService;
import eu.accept.tiekinetix.nexus.profiles.core.exceptions.EntityAlreadyExistsException;
import eu.accept.tiekinetix.nexus.profiles.core.exceptions.EntityDoesNotExist;
import eu.accept.tiekinetix.nexus.profiles.core.model.user.Crew;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Activity;
import eu.accept.tiekinetix.nexus.profiles.reporting.processing.KPIProcessorService;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.ActivityRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.CrewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.web.bind.annotation.*;

import static eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.Intention.SCHEDULED_WORKING_DAYS_CALCULATION;

/**
 * Created by vchepegin on 16/03/2017.
 */
@CrossOrigin
@RestController
public class ActivityController {

    @Autowired
    private ActivityRepository activityRepository;
    @Autowired
    private CrewRepository crewRepository;
    @Autowired
    KPIProcessorService kpiProcessor;
    @Autowired
    ChangedEntitiesLoggingService loggingService;

    private final ObjectMapper mapper = new ObjectMapper();

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value="/createActivity", method = RequestMethod.POST)
    public String createActivity (@RequestBody String payload) throws Exception {

        Activity newActivity = null;
        newActivity = mapper.readValue(payload, Activity.class);


        String guid = newActivity.getEntityID();

        Activity activity = activityRepository.findByEntityID(guid);
        if (activity != null) {
            throw new EntityAlreadyExistsException("The activity with ID = " + guid + "already exists.");
        }

        if (guid == null ||  guid.isEmpty()) {
            guid = java.util.UUID.randomUUID().toString();
            newActivity.setEntityID(guid);
        }
        activityRepository.save(newActivity);

        return guid;
    }
/*
    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value="/assignCrewToActivity/{crewID}/{activityID}", method = RequestMethod.POST)
    public void assignCrewToActivity (@PathVariable(value="crewID") String crewID,
                                      @PathVariable(value="activityID") String activityID) throws Exception {

        Activity activity = activityRepository.findByEntityID(activityID);
        if (activity == null) {
            throw new EntityDoesNotExist("Activity with ID " + activityID + " does not exist.");
        }

        Crew crew = crewRepository.findByCrewID(crewID);
        if (crew == null) {
            throw new EntityDoesNotExist("Crew with ID " + crewID + " does not exist.");
        }

        activity.addCrew(crew);
        activityRepository.save(activity);

        loggingService.logChangedEntity(activity.getEntityID());
        kpiProcessor.propagateUpwards(SCHEDULED_WORKING_DAYS_CALCULATION);


    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value="/removeCrewFromActivity/{crewID}/{activityID}", method = RequestMethod.POST)
    public void removeCrewFromActivity ( @PathVariable(value="crewID") String crewID,
                                         @PathVariable(value="activityID") String activityID)
            throws Exception {


        Activity activity = activityRepository.findByEntityID(activityID);
        if (activity == null) {
            throw new EntityDoesNotExist("Activity with ID " + activityID + " does not exist.");
        }

        for (Crew crew : activity.getCrews()) {
            if (crew.getCrewID().equals(crewID)) {
                activity.removeCrew(crew);
                break;
            }
        }

        activityRepository.save(activity);

        loggingService.logChangedEntity(activity.getEntityID());
        kpiProcessor.propagateUpwards(SCHEDULED_WORKING_DAYS_CALCULATION);
    }
*/
}
