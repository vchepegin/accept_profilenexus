package eu.accept.tiekinetix.nexus.profiles.web.restful;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.accept.tiekinetix.nexus.profiles.core.exceptions.EntityDoesNotExist;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Quantity;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Task;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.QuantityRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * Created by vchepegin on 22/03/2017.
 */
@CrossOrigin
@RestController
public class QuantityController {

    @Autowired
    private QuantityRepository quantityRepository;
    @Autowired
    private TaskRepository taskRepository;

    private final ObjectMapper mapper = new ObjectMapper();

    @CrossOrigin
    @RequestMapping(value="/createQuantity/{taskID}", method = RequestMethod.POST)
    public String createQuantityByTaskId (@RequestBody String payload,
                                     @PathVariable(value="taskID") String taskID)
            throws Exception {
        Quantity quantity = null;
        quantity = mapper.readValue(payload, Quantity.class);


        Task task = taskRepository.findByEntityID(taskID);

        String guid = quantity.getQuantityID();
        if (guid == null ||  guid.isEmpty()) {
            guid = java.util.UUID.randomUUID().toString();
            quantity.setQuantityID(guid);
        }

        if (task == null) {
            throw new EntityDoesNotExist("The Task wit ID = " + taskID + " does nto exist");
        }
        task.addQuantity(quantity);
        taskRepository.save(task);

        return guid;
    }

    @CrossOrigin
    @RequestMapping(value="/deleteQuantity/{quantityID}", method = RequestMethod.DELETE)
    public void createComponent (@PathVariable(value="quantityID") String quantityID)
            throws Exception {
        Quantity quantity = quantityRepository.findByQuantityID(quantityID);
        quantityRepository.delete(quantity);
    }
}
