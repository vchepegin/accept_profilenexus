package eu.accept.tiekinetix.nexus.profiles.reporting;

import eu.accept.tiekinetix.nexus.profiles.reporting.processing.KPIProcessorService;

/**
 * Created by vchepegin on 06/06/2017.
 */
public interface Processable {

    void acceptKPIRequest(KPIProcessorService processor);
}
