package eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.CompletionStatus;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.ScheduleStatus;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.Constructable;
import eu.accept.tiekinetix.nexus.profiles.reporting.Processable;
import eu.accept.tiekinetix.nexus.profiles.reporting.processing.KPIProcessorService;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

import javax.validation.constraints.NotNull;
import java.util.Map;

import static eu.accept.tiekinetix.nexus.profiles.util.DateUtils.dateAfterDays;
import static eu.accept.tiekinetix.nexus.profiles.util.DateUtils.getPureWbs;

/**
 * Created by vchepegin on 23/02/2017.
 */
@NodeEntity
@JsonIgnoreProperties(ignoreUnknown = true)
public class QualityGate extends ScheduledEntity implements Constructable, Processable {

    private static final long serialVersionUID = 2668890318160650906L;

    @GraphId
    private Long qualityGateID;

    @NotNull
    private String typeOfQualityGate;
    private final int numberOfWorkingDays = 1;
    private String checklistTemplateID;

    @Override
    public void acceptKPIRequest(KPIProcessorService processor) {
        processor.processKPI(this);
    }

    /**
     * Copy constructor. Note, that the entityID has to be set separately AFTER the creation of a new instance!
     *
     * @param qualityGate
     * @return
     */
    public QualityGate(QualityGate qualityGate) {

        super(qualityGate);

        this.typeOfQualityGate = qualityGate.getTypeOfQualityGate();
        this.checklistTemplateID = qualityGate.checklistTemplateID;
    }

    //Default constructor
    public QualityGate() {
        this.setTier("TIER_3");
        this.setTaskType("QUALITYGATE");
    }

    /**
     * @param entityId
     * @param name
     * @param description
     * @param tier
     * @param scheduleStatus
     * @param completionStatus
     * @param wbsCode
     * @param lbsCode
     * @param priority
     * @param typeOfQualityGate
     * @param timeout
     */
    public QualityGate(String entityId, String name, String description,
                       String tier, ScheduleStatus scheduleStatus, CompletionStatus completionStatus,
                       String wbsCode, String lbsCode, int priority,
                       String typeOfQualityGate, double timeout) {
        super(entityId, name, description, timeout, "TIER_3", scheduleStatus, completionStatus,
                wbsCode, lbsCode, priority);
        this.typeOfQualityGate = typeOfQualityGate;
//        this.setTier("TIER_3");
    }

    @Override
    public Map<String, Object> construct(Map<String, Object> parameters) {

        if (!initialized) {
            setLbsCode(parameters.get("lbsCode").toString());
            setWbsCode(parameters.get("wbsCode").toString() + "." + getPureWbs(getWbsCode()));
            setDuration(1);
            initialized = true;
        }

//        System.out.println(getWbsCode() + " : " + getStartDate());
//        long interval = (1 > getTimeout()) ? 1 : Math.round(getTimeout());
        long interval = Math.round(getTimeout());
        setStartDate(dateAfterDays(getStartDate(), interval));

        setEndDate(getStartDate());

        return parameters;
    }

    @JsonProperty("typeOfQualityGate")
    public String getTypeOfQualityGate() {
        return typeOfQualityGate;
    }

    @JsonProperty("typeOfQualityGate")
    public void setTypeOfQualityGate(String typeOfQualityGate) {
        this.typeOfQualityGate = typeOfQualityGate;
    }

    @JsonIgnore
    public Long getQualityGateID() {
        return qualityGateID;
    }

    @JsonProperty("numberOfWorkingDays")
    public int getNumberOfWorkingDays() {
        return numberOfWorkingDays;
    }

    @JsonProperty("checklistTemplateID")
    public String getChecklistTemplateID() {
        return checklistTemplateID;
    }

    @JsonProperty("checklistTemplateID")
    public void setChecklistTemplateID(String checklistTemplateID) {
        this.checklistTemplateID = checklistTemplateID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof QualityGate)) return false;
        if (!super.equals(o)) return false;

        QualityGate that = (QualityGate) o;

        return getTypeOfQualityGate().equals(that.getTypeOfQualityGate());
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getTypeOfQualityGate().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "QualityGate{ ID=" + getEntityID() +
                ", typeOfQualityGate='" + typeOfQualityGate + '\'' +
                ", owner=" + owner +
                ", location=" + location +
                '}';
    }
/*
    @Override
    public void reset() {
        super.reset();
    }
*/
}