package eu.accept.tiekinetix.nexus.profiles.bim.controller;

import java.io.IOException;
import java.util.Optional;

import javax.validation.Valid;
import javax.websocket.server.PathParam;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.accept.tiekinetix.nexus.profiles.bim.model.BimMaterial;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by akuschki on 12/06/2017.
 */

@CrossOrigin
@RestController
@RequestMapping(value = "/projects/{projectID}/bimMaterials/{bimId}",produces = {"application/json"}, consumes = {"application/json", "*/*"})
public class BimMaterialController extends AbstractBimController {

    private static final ObjectMapper mapper = new ObjectMapper();

    @Transactional
    @RequestMapping(method = RequestMethod.DELETE)
    public void delete(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId) throws IOException {
        materialRepository.delete(findOne(projectID, bimId));
    }

    @RequestMapping(method = RequestMethod.GET)
    public BimMaterial get(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId) throws IOException {
        return findOne(projectID, bimId);
    }

    @Transactional
    @RequestMapping(method = RequestMethod.PUT)
//    public void put(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId, @Valid @RequestBody BimMaterial material, @PathParam("setScheduledEntity") Optional<String> scheduledEntity) throws IOException {
    public void put(@Valid @PathVariable String projectID, @Valid @PathVariable Long bimId, @Valid @RequestBody String payload) throws IOException {
        BimMaterial material = mapper.readValue(payload, BimMaterial.class);
        validatePayload(bimId, material);
        deduplicateEmbeddedParameters(material, projectID);
        setCategoryLink(material, projectID);
        processAndSave(projectID, bimId, material, null);
        buildRelationsToTasks(material, projectID);
    }

    private BimMaterial findOne(String projectID, Long bimId) {
        return materialRepository.findByProjectProjectIDAndBimId(projectID, bimId)
                .stream()
                .findFirst()
                .orElseThrow(ResourceNotFoundException::new);
    }
}
