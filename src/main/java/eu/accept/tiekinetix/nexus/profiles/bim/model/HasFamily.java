package eu.accept.tiekinetix.nexus.profiles.bim.model;

import java.util.Optional;

/**
 * Created by arian.kuschki on 12/07/17.
 */
public interface HasFamily {
    public Optional<BimFamily> getFamily();
}
