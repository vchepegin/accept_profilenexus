package eu.accept.tiekinetix.nexus.profiles.repositories.reporting;

import eu.accept.tiekinetix.nexus.notifications.Notification;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface NotificationRepository extends GraphRepository<Notification> {

    @Query("match (a:Notification) WHERE a.projectID = {0} AND a.receiverID = {1}" +
            " AND a.timestamp > {2} return a")
    List<Notification> findNotificationsByFilter (@Param("0")String projectID, @Param("1")String receiverID,
                                                  @Param("2")String timestamp );

    @Query("match (n:Notification ) where n.timestamp < {0} delete n")
    void deleteNotificationsOlderThanDate(@Param("0")String timestamp);
}
