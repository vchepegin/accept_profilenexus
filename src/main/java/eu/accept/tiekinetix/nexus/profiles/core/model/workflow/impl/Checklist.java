package eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

import java.io.Serializable;

/**
 * Created by vchepegin on 23/02/2017.
 */
@NodeEntity
public class Checklist implements Serializable {
    private static final long serialVersionUID = -4114121469552507335L;
    @GraphId
    private Long id;

    private String checklistID;

    /**
     * Copy constructor. Note, that checklistID is NOT set and has to be managed separately!
     * @param checklist
     */
    public Checklist (Checklist checklist) {
        this.checklistID = checklist.checklistID;
    }

    public Checklist () {}

    public Checklist(String checklistID, String URI) {
        this.checklistID = checklistID;
    }

    @JsonProperty("checklistNameID")
    public String getChecklistID() {
        return checklistID;
    }

    @JsonProperty("checklistNameID")
    public void setChecklistID(String checklistID) {
        this.checklistID = checklistID;
    }

}
