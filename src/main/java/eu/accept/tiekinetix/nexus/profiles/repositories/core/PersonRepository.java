package eu.accept.tiekinetix.nexus.profiles.repositories.core;

import eu.accept.tiekinetix.nexus.profiles.core.model.user.Person;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Project;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

import java.util.Set;

/**
 * Created by vchepegin on 27/01/2017.
 */
public interface PersonRepository extends GraphRepository<Person> {
/*
    @Query("match (a:Person {personID: {0} })," +
            "(a:Person {personID: {0}} )-[r:hasContactInformation*0..1]->(b)  return a, r, b")
*/
/*
    @Query("match (a:Person {personID: {0} })," +
            "(a:Person {personID: {0}} )-[r*0..1]-(b)  return a, r, b")
*/
    @Query("match (a:Person {personID: {0}} )-[r*0..1]-(b)  return a, r, b")
    Person findByPersonID (@Param("0")String personID);
    Person findByLastName (String lastName);

    @Query("match (a:Person {personID: {0} }) DETACH delete a")
    void delete(@Param("0")String personID);

    @Query("match (a:Person {personID: {0} })-[r:onLeave]-(b) DETACH delete b")
    void deleteLeave(@Param("0")String personID);

    @Query ("match (p:Person) return DISTINCT p.specialisations")
    Set<String> getSpecialisations ();

    @Query ("match (p:Person) return DISTINCT p.skills AS skills")
    Set<String> getSkills ();

    @Query ("MATCH (s:ScheduledEntity {entityID: {0}})-[o:hasOwner]->(p:Person) return p.personID as personID " +
            "UNION MATCH (s:ScheduledEntity {entityID: {0}})-[c:hasChild*0..3]->(a:ScheduledEntity)-[o:hasOwner]->(p) " +
            "return p.personID as personID " +
            "UNION MATCH (s:ScheduledEntity {entityID: {0}})-[c:hasChild*0..3]-> (a:ScheduledEntity)-[r1:hasCrew]- " +
            "(b:Crew)-[r2:hasWorker]-(p:Person) return DISTINCT p.personID as personID")
    Set<String> getAllPersonsInvolvedInTask (@Param("0")String taskID);

    @Query ("MATCH (n:Project {projectID:{0} })-[r*]->(s:ScheduledEntity) MATCH (s)-[o:hasOwner]->(p:Person) " +
            "return p.personID as personID " +
            "UNION MATCH (n:Project {projectID: {0} })-[r*]->(s:ScheduledEntity) " +
            "MATCH (s) -[r1:hasCrew]-(b:Crew)-[r2:hasWorker]-(p:Person) return DISTINCT p.personID as personID")
    Set<String> getAllPersonsInvolvedInProject(@Param("0")String projectID);

    @Query("match (a:Person {personID: {0} })-[r:hasContactInformation*0..1]->(b) return b.email")
    Set<String> getEmailAddress(@Param("0")String personID);

    @Query("MATCH (n:Project)-[r*]->(s:ScheduledEntity) MATCH (s)-[o:hasOwner]->(p:Person {personID:{0}}) " +
            "return n as project " +
            "UNION MATCH (n:Project)-[r*]->(s:ScheduledEntity) " +
            "MATCH (s) -[r1:hasCrew]-(b:Crew)-[r2:hasWorker]-(p:Person {personID:{0}}) return DISTINCT n as project")
    Set<Project> getProjectsOfPerson(@Param("0")String personID);

    @Query("MATCH (n:Project)-[r*]->(s:ScheduledEntity) MATCH (s)-[o:hasOwner]->(p:Person {personID:{0}}) " +
            "return s as scheduledEntity " +
            "UNION MATCH (n:Project)-[r*]->(s:ScheduledEntity) " +
            "MATCH (s) -[r1:hasCrew]-(b:Crew)-[r2:hasWorker]-(p:Person {personID:{0}}) return DISTINCT s as scheduledEntity")
    Set<ScheduledEntity> getTasksPersonIsInvolved(@Param("0")String personID);
}
