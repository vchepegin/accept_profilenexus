package eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.neo4j.driver.v1.util.Immutable;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by vchepegin on 30/08/2017
 *
 * This immutable class that holds the initial state of the related object. It can be written only once and then read
 * multiple times but not changed any more.
 */
@Immutable
@NodeEntity
@JsonIgnoreProperties(ignoreUnknown = true)
public final class InitialState implements Serializable {

    private static final long serialVersionUID = -5528072449392618594L;

    @GraphId
    private Long id;

    private double plannedScheduledWorkingDays;
    private Date plannedStartDate;
    private Date plannedEndDate;

    public static InitialState getInstance (InitialState initialState) {
        return new InitialState(initialState.getPlannedScheduledWorkingDays(),
                initialState.getPlannedStartDate(), initialState.getPlannedEndDate());
    }

    public InitialState () {};

    @JsonCreator
    public InitialState(@JsonProperty("plannedScheduledWorkingDays") double plannedScheduledWorkingDays,
                        @JsonProperty("plannedStartDate") Date plannedStartDate,
                        @JsonProperty("plannedEndDate") Date plannedEndDate) {
        this.plannedScheduledWorkingDays = plannedScheduledWorkingDays;
        this.plannedStartDate = plannedStartDate;
        this.plannedEndDate = plannedEndDate;
    }

    @JsonProperty("plannedScheduledWorkingDays")
    public double getPlannedScheduledWorkingDays() {
        return plannedScheduledWorkingDays;
    }

    @JsonProperty("plannedStartDate")
    public Date getPlannedStartDate() {
        return plannedStartDate;
    }

    @JsonProperty("plannedEndDate")
    public Date getPlannedEndDate() {
        return plannedEndDate;
    }

    @Override
    public String toString() {
        return "InitialState{" +
                "plannedScheduledWorkingDays=" + plannedScheduledWorkingDays +
                ", plannedStartDate=" + plannedStartDate +
                ", plannedEndDate=" + plannedEndDate +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof InitialState)) return false;

        InitialState that = (InitialState) o;

        if (Double.compare(that.getPlannedScheduledWorkingDays(), getPlannedScheduledWorkingDays()) != 0) return false;
        if (getPlannedStartDate() != null ? !getPlannedStartDate().equals(that.getPlannedStartDate()) : that.getPlannedStartDate() != null)
            return false;
        return getPlannedEndDate() != null ? getPlannedEndDate().equals(that.getPlannedEndDate()) : that.getPlannedEndDate() == null;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getPlannedScheduledWorkingDays());
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + (getPlannedStartDate() != null ? getPlannedStartDate().hashCode() : 0);
        result = 31 * result + (getPlannedEndDate() != null ? getPlannedEndDate().hashCode() : 0);
        return result;
    }
}
