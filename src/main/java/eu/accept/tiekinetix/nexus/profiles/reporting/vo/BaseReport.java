package eu.accept.tiekinetix.nexus.profiles.reporting.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.CompletionStatus;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.ScheduleStatus;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Task;
import eu.accept.tiekinetix.nexus.profiles.reporting.KpiReporting;
import eu.accept.tiekinetix.nexus.profiles.util.CopyUtils;
import org.neo4j.ogm.annotation.GraphId;

import java.util.Date;

/**
 * Base class for the KPI reports. The main reason for its creation not the intention to save some
 * space for the shared fields (which is also a good bonus) but because of the repository pattern, i.e.
 * it is possible to CRUD all children using one repository for this base class.
 *
 * Created by vchepegin on 15/06/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class BaseReport implements KpiReporting {

    @GraphId
    private Long id;

    //Only the following 5 fields are necessary, the rest is the same as of the QualityGate instance
    // itself and can be found using entityID
//    @Index(unique=true, primary=true)
    private String entityID;
    private Date dateMonitored;
    private String comment;
    private boolean verified; //has to be enum or boolean
    private String personID; //the one who filed this report

    private String projectID;

    private Date startDate;
    private Date endDate;

    private String lbsCode;
    private String wbsCode;

    private String tier;
    private String taskType;
    private String name;

    private int cumulativeGoalAchieved;
    private double plannedProgress;
    private double progress;
    private double ppc;
    private double par;
    private double extraEffort;
    private ScheduleStatus scheduleStatus;
    private CompletionStatus completionStatus;

    private double scheduledWorkingDays;
    private double remainingWorkingDays;
    private double pPCofDay;

    private Date expectedEndDate;

    public BaseReport () {}

    public static BaseReport getInstance (Task task) {
        BaseReport baseReport = new BaseReport();

        return CopyUtils.copy(task, baseReport);
    }
    public BaseReport(String name, String projectID, String wbsCode, String lbsCode, Date dateMonitored,
                      double ppc, double par, double progress, double extraEffort, String tier, String taskType,
                      double scheduledWorkingDays, double remainingWorkingDays) {
        this.name = name;
        this.projectID = projectID;
        this.wbsCode = wbsCode;
        this.lbsCode = lbsCode;
        this.entityID = wbsCode + "_" + lbsCode;
        this.dateMonitored = dateMonitored;
        this.ppc = ppc;
        this.par = par;
        this.progress = progress;
        this.extraEffort = extraEffort;
        this.tier = tier;
        this.taskType = taskType;
        this.scheduledWorkingDays = scheduledWorkingDays;
        this.remainingWorkingDays = remainingWorkingDays;
    }

    @JsonProperty("entityID")
    public String getEntityID() {
        return entityID;
    }

    @JsonProperty("entityID")
    public void setEntityID(String entityID) {
        this.entityID = entityID;
    }

    @JsonProperty("dateMonitored")
    public Date getDateMonitored() {
        if (dateMonitored == null) return null;
        return new Date (dateMonitored.getTime());
    }

    @JsonProperty("dateMonitored")
    public void setDateMonitored(Date dateMonitored) {
        this.dateMonitored = new Date(dateMonitored.getTime());
    }

    @JsonProperty("expectedEndDate")
    public Date getExpectedEndDate() {
        if (expectedEndDate == null) return null;
        return new Date (expectedEndDate.getTime());
    }

    @JsonProperty("expectedEndDate")
    public void setExpectedEndDate(Date expectedEndDate) {
        this.expectedEndDate = new Date(expectedEndDate.getTime());;
    }

    @JsonProperty("comment")
    public String getComment() {
        return comment;
    }

    @JsonProperty("comment")
    public void setComment(String comment) {
        this.comment = comment;
    }

    @JsonProperty("verified")
    public boolean isVerified() {
        return verified;
    }

    @JsonProperty("verified")
    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    @JsonProperty("personID")
    public String getPersonID() {
        return personID;
    }

    @JsonProperty("personID")
    public void setPersonID(String personID) {
        this.personID = personID;
    }

    @JsonProperty("projectID")
    public String getProjectID() {
        return projectID;
    }

    @JsonProperty("projectID")
    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    @JsonProperty("startDate")
    public Date getStartDate() {
        if (startDate == null) return null;
        return new Date (startDate.getTime());
    }

    @JsonProperty("startDate")
    public void setStartDate(Date startDate) {
        this.startDate = new Date (startDate.getTime());
    }

    @JsonProperty("endDate")
    public Date getEndDate() {
        if (endDate == null) return null;
        return new Date (endDate.getTime());
    }

    @JsonProperty("endDate")
    public void setEndDate(Date endDate) {
        this.endDate = new Date(endDate.getTime());
    }
    @JsonProperty("lbsCode")
    public String getLbsCode() {
        return lbsCode;
    }

    @JsonProperty("lbsCode")
    public void setLbsCode(String lbsCode) {
        this.lbsCode = lbsCode;
    }

    @JsonProperty("wbsCode")
    public String getWbsCode() {
        return wbsCode;
    }

    @JsonProperty("wbsCode")
    public void setWbsCode(String wbsCode) {
        this.wbsCode = wbsCode;
    }

    @JsonProperty("tier")
    public String getTier() {
        return tier;
    }

    @JsonProperty("tier")
    public void setTier(String tier) {
        this.tier = tier;
    }

    @JsonProperty("taskType")
    public String getTaskType() {
        return taskType;
    }

    @JsonProperty("taskType")
    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("cumulativeGoalAchieved")
    public int getCumulativeGoalAchieved() {
        return cumulativeGoalAchieved;
    }

    @JsonProperty("cumulativeGoalAchieved")
    public void setCumulativeGoalAchieved(int cumulativeGoalAchieved) {
        this.cumulativeGoalAchieved = cumulativeGoalAchieved;
    }

    @JsonProperty("progress")
    public double getProgress() {
        return progress;
    }

    @JsonProperty("progress")
    public void setProgress(double progress) {
        this.progress = progress;
    }

    @JsonProperty("plannedProgress")
    public double getPlannedProgress() {
        return plannedProgress;
    }

    @JsonProperty("plannedProgress")
    public void setPlannedProgress(double plannedProgress) {
        this.plannedProgress = plannedProgress;
    }

    @JsonProperty("PPC")
    public double getPpc() {
        return ppc;
    }

    @JsonProperty("PPC")
    public void setPpc(double ppc) {
        this.ppc = ppc;
    }

    @JsonProperty("PAR")
    public double getPAR() {
        return par;
    }

    @JsonProperty("PAR")
    public void setPAR(double par) {
        this.par = par;
    }

    @JsonProperty("extraEffort")
    public double getExtraEffort() {
        return extraEffort;
    }

    @JsonProperty("extraEffort")
    public void setExtraEffort(double extraEffort) {
        this.extraEffort = extraEffort;
    }

    @JsonProperty("scheduleStatus")
    public ScheduleStatus getScheduleStatus() {
        return scheduleStatus;
    }

    @JsonProperty("scheduleStatus")
    public void setScheduleStatus(ScheduleStatus scheduleStatus) {
        this.scheduleStatus = scheduleStatus;
    }

    @JsonProperty("completionStatus")
    public CompletionStatus getCompletionStatus() {
        return completionStatus;
    }

    @JsonProperty("completionStatus")
    public void setCompletionStatus(CompletionStatus completionStatus) {
        this.completionStatus = completionStatus;
    }

    @JsonProperty("scheduledWorkingDays")
    public double getScheduledWorkingDays() {
        return scheduledWorkingDays;
    }

    @JsonProperty("scheduledWorkingDays")
    public void setScheduledWorkingDays(double scheduledWorkingDays) {
        this.scheduledWorkingDays = scheduledWorkingDays;
    }

    @JsonProperty("remainingWorkingDays")
    public double getRemainingWorkingDays() {
        return remainingWorkingDays;
    }

    @JsonProperty("remainingWorkingDays")
    public void setRemainingWorkingDays(double remainingWorkingDays) {
        this.remainingWorkingDays = remainingWorkingDays;
    }

    @JsonProperty("PPCofDay")
    public double getpPCofDay() {
        return pPCofDay;
    }

    @JsonProperty("PPCofDay")
    public void setpPCofDay(double pPCofDay) {
        this.pPCofDay = pPCofDay;
    }
}
