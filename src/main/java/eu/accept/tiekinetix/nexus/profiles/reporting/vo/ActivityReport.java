package eu.accept.tiekinetix.nexus.profiles.reporting.vo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Activity;
import eu.accept.tiekinetix.nexus.profiles.reporting.KpiReporting;
import eu.accept.tiekinetix.nexus.profiles.util.CopyUtils;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

/**
 * Created by vchepegin on 13/06/2017.
 */
@NodeEntity
@JsonIgnoreProperties(ignoreUnknown = true)
public final class ActivityReport extends BaseReport implements KpiReporting {

    @GraphId
    private Long id;

    //this is a separate node in the core model and can be found attached to the GQ itself with entityID
    private double quantityAmount;
    private String quantityUnit;

    //KPI related - no need for them to be in the main Activity object
    private double consumptionRateAmount;
    private String consumptionRateUnit;

    private int numberOfAssignedCrews;
    private double PAR_monitored;
    private double PAR_average;
    private double dailyGoal;
    private double dynamicDailyGoal;
    private double didAbsolute;
    private double didAbsoluteWorker;
    private double didPercent;
    private double didPercentWorker;
    private double remainingToDosAbsolute;
    private double cumulativeDailyGoals;
    private double cumulativeDailyPAR;
    private String reasonForNonCompletion;
    private boolean dailyCheckCritical;

    public ActivityReport () {}

    public static ActivityReport getInstance (Activity activity) {
        ActivityReport activityReport = new ActivityReport();

        return CopyUtils.copy(activity, activityReport);
    }

    @JsonProperty("remainingToDosAbsolute")
    public double getRemainingToDosAbsolute() {
        return remainingToDosAbsolute;
    }

    @JsonProperty("remainingToDosAbsolute")
    public void setRemainingToDosAbsolute(double remainingToDosAbsolute) {
        this.remainingToDosAbsolute = remainingToDosAbsolute;
    }

    @JsonProperty("quantityAmount")
    public double getQuantityAmount() {
        return quantityAmount;
    }

    @JsonProperty("quantityAmount")
    public void setQuantityAmount(double quantityAmount) {
        this.quantityAmount = quantityAmount;
    }

    @JsonProperty("quantityUnit")
    public String getQuantityUnit() {
        return quantityUnit;
    }

    @JsonProperty("quantityUnit")
    public void setQuantityUnit(String quantityUnit) {
        this.quantityUnit = quantityUnit;
    }

    @JsonProperty("consumptionRateAmount")
    public double getConsumptionRateAmount() {
        return consumptionRateAmount;
    }

    @JsonProperty("consumptionRateAmount")
    public void setConsumptionRateAmount(double consumptionRateAmount) {
        this.consumptionRateAmount = consumptionRateAmount;
    }

    @JsonProperty("consumptionRateUnit")
    public String getConsumptionRateUnit() {
        return consumptionRateUnit;
    }

    @JsonProperty("consumptionRateUnit")
    public void setConsumptionRateUnit(String consumptionRateUnit) {
        this.consumptionRateUnit = consumptionRateUnit;
    }

    @JsonProperty("numberOfAssignedCrews")
    public int getNumberOfAssignedCrews() {
        return numberOfAssignedCrews;
    }

    @JsonProperty("numberOfAssignedCrews")
    public void setNumberOfAssignedCrews(int numberOfAssignedCrews) {
        this.numberOfAssignedCrews = numberOfAssignedCrews;
    }

    @JsonProperty("PAR_monitored")
    public double getPAR_monitored() {
        return PAR_monitored;
    }

    @JsonProperty("PAR_monitored")
    public void setPAR_monitored(double PAR_monitored) {
        this.PAR_monitored = PAR_monitored;
    }

    @JsonProperty("PAR_average")
    public double getPAR_average() {
        return PAR_average;
    }

    @JsonProperty("PAR_average")
    public void setPAR_average(double PAR_average) {
        this.PAR_average = PAR_average;
    }

    @JsonProperty("dailyGoal")
    public double getDailyGoal() {
        return dailyGoal;
    }

    @JsonProperty("dailyGoal")
    public void setDailyGoal(double dailyGoal) {
        this.dailyGoal = dailyGoal;
    }

    @JsonProperty("dynamicDailyGoal")
    public double getDynamicDailyGoal() {
        return dynamicDailyGoal;
    }

    @JsonProperty("dynamicDailyGoal")
    public void setDynamicDailyGoal (double dynamicDailyGoal) {
        this.dynamicDailyGoal = dynamicDailyGoal;
    }

    @JsonProperty("didAbsolute")
    public double getDidAbsolute() {
        return didAbsolute;
    }

    @JsonProperty("didAbsolute")
    public void setDidAbsulute(double didAbsolute) {
        this.didAbsolute = didAbsolute;
    }

    @JsonProperty("didAbsoluteWorker")
    public double getDidAbsoluteWorker() {
        return didAbsoluteWorker;
    }

    @JsonProperty("didAbsoluteWorker")
    public void setDidAbsoluteWorker(double didAbsoluteWorker) {
        this.didAbsoluteWorker = didAbsoluteWorker;
    }

    @JsonProperty("didPercent")
    public double getDidPercent() {
        return didPercent;
    }

    @JsonProperty("didPercent")
    public void setDidPercent(double didPercent) {
        this.didPercent = didPercent;
    }

    @JsonProperty("didPercentWorker")
    public double getDidPercentWorker() {
        return didPercentWorker;
    }

    @JsonProperty("didPercentWorker")
    public void setDidPercentWorker(double didPercentWorker) {
        this.didPercentWorker = didPercentWorker;
    }

    @JsonProperty("cumulativeDailyGoals")
    public double getCumulativeDailyGoals() {
        return cumulativeDailyGoals;
    }

    @JsonProperty("cumulativeDailyGoals")
    public void setCumulativeDailyGoals(double cumulativeDailyGoals) {
        this.cumulativeDailyGoals = cumulativeDailyGoals;
    }

    @JsonProperty("cumulativeDailyPAR")
    public double getCumulativeDailyPAR() {
        return cumulativeDailyPAR;
    }

    @JsonProperty("cumulativeDailyPAR")
    public void setCumulativeDailyPAR(double cumulativeDailyPAR) {
        this.cumulativeDailyPAR = cumulativeDailyPAR;
    }

    @JsonProperty("reasonForNonCompletion")
    public String getReasonForNonCompletion() {
        return reasonForNonCompletion;
    }

    @JsonProperty("reasonForNonCompletion")
    public void setReasonForNonCompletion(String reasonForNonCompletion) {
        this.reasonForNonCompletion = reasonForNonCompletion;
    }

    @JsonProperty("dailyCheckCritical")
    public boolean isDailyCheckCritical() {
        return dailyCheckCritical;
    }

    @JsonProperty("dailyCheckCritical")
    public void setDailyCheckCritical(boolean dailyCheckCritical) {
        this.dailyCheckCritical = dailyCheckCritical;
    }

}
