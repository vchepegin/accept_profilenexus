package eu.accept.tiekinetix.nexus.profiles.bim.repository;

import java.util.List;

import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Project;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface BimProjectRepository extends GraphRepository<Project> {
    @Query("MATCH (p:Project {projectID: {0}}) RETURN p")
    //@Query("MATCH (p:Project) where p.projectID = $projectID RETURN p")
    List<Project> findByProjectID(@Param("0")String projectID);
    List<Project> getByProjectID(@Param("0")String projectID);
}
