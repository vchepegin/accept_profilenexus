package eu.accept.tiekinetix.nexus.profiles.core.model.enumerations;

/**
 * Created by vchepegin on 27/01/2017.
 */
public enum PersonStatus {
    ACTIVE (1),
    SICK (2),
    SPECIAL_LEAVE(3),
    LEAVE (4),
    HOLIDAYS (5),
    INACTIVE (6);

    private final int userStatusCode;

    PersonStatus(int code) {
        this.userStatusCode = Integer.valueOf(code);
    }

    private int getUserStatusCode() {
        return userStatusCode;
    }

    @Override
    public String toString () {
        String tmp = "Not defined";
        switch (getUserStatusCode()) {
            case 1:
                tmp = "ACTIVE";
                break;

            case 2:
                tmp = "SICK";
                break;

            case 3:
                tmp = "SPECIAL LEAVE";
                break;

            case 4:
                tmp = "LEAVE";
                break;

            case 5:
                tmp = "HOLIDAYS";
                break;

            case 6:
                tmp = "INACTIVE";
                break;
        }

        return tmp;
    }
}
