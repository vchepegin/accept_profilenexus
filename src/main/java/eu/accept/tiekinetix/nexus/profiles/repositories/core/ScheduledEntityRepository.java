package eu.accept.tiekinetix.nexus.profiles.repositories.core;

import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Task;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.Set;

/**
 * Created by vchepegin on 14/03/2017.
 */
public interface ScheduledEntityRepository extends GraphRepository<ScheduledEntity> {

    @Query("match (a {entityID: {0} }) return a")
    ScheduledEntity findByEntityID (@Param("0")String id);

    @Query("match (a {entityID: {0}})-[r:hasSuccessor]->(b) return distinct b")
    Set<ScheduledEntity> findSuccessors (@Param("0")String id);

    @Query("match (a {entityID: {0}})-[r:hasSuccessor*..25]->(b) " +
            "WHERE NOT b.completionStatus IN ['IN_PROGRESS', 'DONE']  return distinct b")
    Set<ScheduledEntity> findAllSuccessors (@Param("0")String id);

    @Query("match (a:ScheduledEntity {entityID: {0}} )-[r:hasQuantity*0..1]->(b), " +
            "(a:ScheduledEntity {entityID: {0}} )-[r2:hasConsumptionRate*0..1]->(c), " +
            "(a:ScheduledEntity {entityID: {0}} )-[r3:hasOwner*0..1]->(d), " +
            "(a:ScheduledEntity {entityID: {0}} )-[r4:hasCrew*0..1]->(e), " +
            "(a:ScheduledEntity {entityID: {0}} )-[r5:hasLocation*0..1]->(f), " +
//            "(a:ScheduledEntity {entityID: {0}} )-[r6:hasChild*0..1]->(g)," +
//            "(a:ScheduledEntity {entityID: {0}} )-[r8:hasSuccessor*0..1]->(j)," +
            "(a:ScheduledEntity {entityID: {0}} )-[r7:hasInitialState*0..1]->(h)" +
            " return a, r, b, r2, c, r3, d, r4, e, r5, f, r7, h") //r6, g, r8, j
    ScheduledEntity findByEntityID2 (@Param("0")String id);

    @Query("match (b:ScheduledEntity {entityID: {0}} )-[r:hasChild]->(child:ScheduledEntity) return child ORDER BY child.wbsCode ASC")
    Set<ScheduledEntity> findChildren (@Param("0")String id);

    @Query("match (b:ScheduledEntity {entityID: {0}} )-[r:hasChild]->(child:ScheduledEntity {completionStatus: {1}}) " +
            " return child ORDER BY child.wbsCode ASC")
    Set<ScheduledEntity> findChildrenWithCompletionStatus (@Param("0")String id, @Param("1")String status);

    @Query("match (b:Task {entityID: {0}} )-[r:hasChild]->(child:Task) return child ORDER BY child.wbsCode ASC")
    Set<Task> findTasksBelow (@Param("0")String id);

    @Query("match (a:ScheduledEntity {entityID: {0}} )<-[r:hasChild]-(b), " +
            "(b)-[r2:hasChild]->(c) return a, c")
    Set<ScheduledEntity> findParentsChildren (@Param("0")String id);

    ScheduledEntity findByName (String name);

//    @Query("match (a {entityID: {0} }) return a")
//    ScheduledEntity getByID (String ID);

    @Query("match (a {entityID: {0} })-[r]-(b {entityID: {1} }) DETACH delete b")
    void delete(@Param("0")String entityID, @Param("1")String childID);

    @Query("match (a:ScheduledEntity {entityID : {0} } ) " +
            "OPTIONAL MATCH (a) --> (b:Quantity) detach delete a, b")
    void delete(@Param("0")String entityID);

    @Query ("MATCH (n:Task {entityID: {0} })-[r]->(s:ScheduledEntity) OPTIONAL MATCH (s:ScheduledEntity) -[rq]->(q:Quantity) detach delete r,s, rq, q")
    void deleteSubTasks(@Param("0")String entityID);

    @Query("match (a {entityID: {0} })<-[r:hasChild]-(b:Task), " +
            "(b)-[r2:hasInitialState*0..1]-(c)" +
            "return b, r2, c")
    Task getParentTask(@Param("0")String entityID);

    //check if the entity exists - speed up validation
    @Query("MATCH (a:ScheduledEntity {entityID : {0}}) return case when count(a) > 0 then true else false end")
    boolean isEntityPresent (@Param("0")String entityID);

    @Query ("match (a:Task {entityID: {0}})-[:hasChild]->(b:Task) return MAX(b.dateMonitored)")
    String findLastMonitoredTaskDate (@Param("0")String entityID);

    @Query ("match (a:Task {entityID: {0}})<-[:hasSuccessor]-(b:ScheduledEntity) return MAX(b.endDate)")
    Date findLatestEndDate (@Param("0")String entityID);

    //if local - take it, else fetch from a direct parent task
    @Query("MATCH (n:ScheduledEntity {entityID : {0} } )-[:hasOwner]->(a:Person)-[:hasContactInformation]->(b:ContactDetails)  " +
            "return distinct b.email as email " +
            "UNION MATCH (n:ScheduledEntity {entityID : {0}  } )<-[:hasChild]-(c:Task)-[:hasOwner]->(d:Person)-[:hasContactInformation]->(e:ContactDetails) " +
            "RETURN distinct e.email as email")
    Set<String> findOwnersEmails (@Param("0")String entityID);

    //if local - take it, else fetch from a direct parent task
    @Query("MATCH (n:ScheduledEntity {entityID : {0} } )-[:hasOwner]->(a:Person) return distinct a.personID as personID " +
            "UNION MATCH (n:ScheduledEntity {entityID : {0}  } )<-[:hasChild]-(c:Task)-[:hasOwner]->(d:Person) " +
            "RETURN distinct d.personID as personID")
    Set<String> findOwnersIDs (@Param("0")String entityID);

    @Query("match (a:ScheduledEntity {entityID: {0}} )-[r:hasChild | hasSuccessor*0..1]->(b) return a, b")
    Set<ScheduledEntity> findTaskWithSubAndSuccessors (@Param("0")String id);
//    Set<ScheduledEntity> findTaskWithSubandSuccesors (String id);

    @Query("MATCH (n:Project {projectID: {0} })-[r*]->(s:ScheduledEntity)-[o:hasOwner]->(p:Person {personID: {1}}) return s as Task " +
            "UNION MATCH (n:Project {projectID: {0} })-[r*]->(a:ScheduledEntity)-[o:hasOwner]->(p:Person {personID: {1}}) " +
            "MATCH (a)-[c:hasChild*0..3] -> (s) return s AS Task " +
            "UNION MATCH (n:Project {projectID: {0} })-[r*]->(s:ScheduledEntity)-[r1:hasCrew]-(b:Crew)-[r2:hasWorker]->" +
            "(p:Person {personID: {1}}) return DISTINCT s as Task ORDER BY s.wbsCode ASC")
    Set<ScheduledEntity> findAllScheduledEntitiesPersonIsInvolved(@Param("0")String projectID, @Param("1")String personID);

}
