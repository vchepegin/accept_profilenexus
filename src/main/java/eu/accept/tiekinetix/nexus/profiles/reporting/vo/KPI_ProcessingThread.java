package eu.accept.tiekinetix.nexus.profiles.reporting.vo;

import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.Intention;
import eu.accept.tiekinetix.nexus.profiles.reporting.processing.KPIProcessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class KPI_ProcessingThread implements  Runnable{
    private KPIProcessorService kpiProcessor;
    public KPI_ProcessingThread(KPIProcessorService kpiProcessor) {
        this.kpiProcessor = kpiProcessor;
    }
    @Override
    public void run() {
//        @Autowired
        this.kpiProcessor.propagateUpwards(Intention.KPI_CALCULATION);
    }
}
