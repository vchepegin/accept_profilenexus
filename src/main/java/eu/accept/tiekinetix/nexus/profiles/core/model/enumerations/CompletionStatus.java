package eu.accept.tiekinetix.nexus.profiles.core.model.enumerations;

import java.io.Serializable;

/**
 * Created by vchepegin on 17/02/2017.
 */
public enum CompletionStatus {

    TO_DO (1),
    IN_PROGRESS(2),
    DONE (3),
    FIX_NEEDED (4);

    private final int complitionStatusCode;

    CompletionStatus(int code) {
        this.complitionStatusCode = Integer.valueOf(code);
    }

    private int completionStatus () {
        return complitionStatusCode;
    }

    @Override
    public String toString () {
        String tmp = "Not defined";
        switch (complitionStatusCode) {
            case 1:
                tmp = "TO_DO";
                break;
            case 2:
                tmp = "IN_PROGRESS";
                break;
            case 3:
                tmp = "DONE";
                break;
            case 4:
                tmp = "FIX_NEEDED";
                break;
        }

        return tmp;
    }
}
