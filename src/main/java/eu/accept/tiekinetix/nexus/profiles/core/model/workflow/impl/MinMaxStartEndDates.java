/**
 * Created by pMerz on 16/11/2017.
 */
package eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.neo4j.ogm.annotation.Property;
import org.springframework.data.neo4j.annotation.QueryResult;

import java.util.Date;

@QueryResult
public class MinMaxStartEndDates {

    @Property(name = "MIN(s.startDate)")
//    private Date min;
    private String min;
    @Property(name = "MAX(s.endDate)")
    private String max;
//    private Date max;

    public MinMaxStartEndDates() {};

//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonProperty("startDate")
    public String getMin() { return min; }
//    public Date getMin() { return min; }

//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @JsonProperty("endDate")
    public String getMax() {
        return max;
    }
//    public Date getMax() { return max; }
}
