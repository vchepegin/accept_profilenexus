package eu.accept.tiekinetix.nexus.profiles.web.restful;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import eu.accept.tiekinetix.nexus.profiles.core.exceptions.EntityAlreadyExistsException;
import eu.accept.tiekinetix.nexus.profiles.core.exceptions.EntityDoesNotExist;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Activity;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.QualityGate;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Task;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.*;
import org.neo4j.ogm.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Map;

/**
 * Created by vchepegin on 14/03/2017.
 */

@CrossOrigin
@RestController
public class SchedulingController {

    @Autowired
    private ScheduledEntityRepository scheduledEntityRepository;
    @Autowired
    private PersonRepository personRepository;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private QualityGateRepository qualityGateRepository;
    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    Session session;

    private final ObjectMapper mapper = new ObjectMapper();

    @CrossOrigin
    @RequestMapping(value="/addSuccessorByType/{parentID}/{type}", method = RequestMethod.POST)
    public String addSuccessorByType(@RequestBody String payload,
                             @PathVariable(value="parentID") String parentID,
                             @PathVariable(value="type") String type) throws Exception {

        ScheduledEntity newEntity = createScheduledEntity(payload, type);

        String guid = newEntity.getEntityID();

        if (Strings.isNullOrEmpty(guid) && scheduledEntityRepository.isEntityPresent(guid)) {
            throw new EntityAlreadyExistsException("The entity with ID = " + guid +
                    "already exists.");
        }

        ScheduledEntity parentEntity = scheduledEntityRepository.findByEntityID(parentID);
        if (parentEntity != null) {
            parentEntity.addSuccessor(newEntity);
        } else {
            throw new EntityDoesNotExist("The parent entity with ID = " + guid +
                    "does not exist.");
        }

        if (guid == null ||  guid.isEmpty()) {
            guid = java.util.UUID.randomUUID().toString();
            newEntity.setEntityID(guid);
        }

        saveScheduledEntity(parentEntity);
//        saveScheduledEntity(newEntity);

        return guid;
    }


    //TODO: USE METHODS BELOW FOR SAVING THE REAL OBJECT TYPE!!!
    @CrossOrigin
    @RequestMapping(value="/updateScheduledEntity/{entityID}", method = RequestMethod.PUT)
    public void updateScheduledEntity (@RequestBody Map<String, Object> payload,
                                       @PathVariable(value="entityID") String entityID) {

        ScheduledEntity scheduledEntity = scheduledEntityRepository.findByEntityID
                (payload.get("entityID").toString());

        for (Map.Entry<String, Object> entry : payload.entrySet()) {

            try {
                Class<?> c = scheduledEntity.getClass();
                Field t = c.getDeclaredField(entry.getKey());
                t.setAccessible(true);
                if (t.getType().isEnum()) {
                    t.set(scheduledEntity, Enum.valueOf((Class<Enum>)t.getType(),
                            entry.getValue().toString()));
                } else {
                    t.set(scheduledEntity, entry.getValue());
                }
            } catch (NoSuchFieldException | IllegalAccessException ex) {
                ex.printStackTrace();
            }
        }

        scheduledEntityRepository.save(scheduledEntity);
    }

    /**
     * Save specific entity which is-a ScheduledEntity
     * @param entity
     */
    private void saveScheduledEntity (ScheduledEntity entity) {
        if (entity instanceof Task) {
            taskRepository.save((Task)entity);
        }
        if (entity instanceof QualityGate) {
            qualityGateRepository.save((QualityGate)entity);
        }
        if (entity instanceof Activity) {
            activityRepository.save((Activity)entity);
        }
//        if (entity instanceof Workflow) {
//            workflowRepository.save((Workflow) entity);
//        }
    }

    /**
     * Factory method for ScheduledEntity children classes
     * @param payload
     * @param type
     * @return ScheduledEntity
     */
    private ScheduledEntity createScheduledEntity (String payload, String type) {
        ScheduledEntity newEntity = null;

        if (type.equalsIgnoreCase("task")) {
            try {
                newEntity = new ObjectMapper().readValue(payload, Task.class);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        if (type.equalsIgnoreCase("qualityGate")) {
            try {
                newEntity = new ObjectMapper().readValue(payload, QualityGate.class);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

//        if (type.equalsIgnoreCase("workflow")) {
//            try {
//                newEntity = new ObjectMapper().readValue(payload, Workflow.class);
//            } catch (IOException ex) {
//                ex.printStackTrace();
//            }
//        }

        if (type.equalsIgnoreCase("activity")) {
            try {
                newEntity = new ObjectMapper().readValue(payload, Activity.class);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        return newEntity;
    }

    @CrossOrigin
    @RequestMapping(value="/removeSuccessor/{predecessorID}/{successorID}", method = RequestMethod.DELETE)
    public void removeSuccessor (@PathVariable(value="predecessorID") String predecessorID,
                              @PathVariable(value="successorID") String successorID) throws Exception {

        if (predecessorID != null &&  !predecessorID.isEmpty() &&
                successorID != null &&  !successorID.isEmpty()) {
            ScheduledEntity predecessor = scheduledEntityRepository.findByEntityID(predecessorID);
            if (predecessor == null) {
                throw new EntityDoesNotExist("Entity with ID = " + predecessorID + " does nto exist.");
            }

            ScheduledEntity successor = scheduledEntityRepository.findByEntityID(successorID);
            if (successor == null) {
                throw new EntityDoesNotExist("Entity with ID = " + successorID + " does nto exist.");
            }


            predecessor.removeSuccessor(successor);


            scheduledEntityRepository.save(predecessor);
        }
    }

    @CrossOrigin
    @RequestMapping(value="/linkSuccesssorToEntity/{parentID}/{childID}", method = RequestMethod.POST)
    public void linkEquipmentToCrew (@PathVariable(value="parentID") String parentID,
                                     @PathVariable(value="childID") String childID) throws Exception {

        ScheduledEntity parent = scheduledEntityRepository.findByEntityID(parentID);
        ScheduledEntity child = scheduledEntityRepository.findByEntityID(childID);

        parent.addSuccessor(child);
        scheduledEntityRepository.save(parent);
    }

    @CrossOrigin
    @RequestMapping(value="/getScheduledEntityByID/{entityID}", method = RequestMethod.GET)
    public ScheduledEntity getScheduledEntityByID (
            @PathVariable(value="entityID") String entityID) throws Exception {

        return scheduledEntityRepository.findByEntityID2(entityID);
    }


    @CrossOrigin
    @RequestMapping(value="/deleteScheduledEntity/{entityID}", method = RequestMethod.PUT)
    public void deleteScheduledEntity (@PathVariable(value="entityID") String entityID) {

//        if (!scheduledEntityRepository.isEntityPresent(entityID)) {
//            throw new EntityDoesNotExist("Entity with ID = " + entityID + " does nto exist.");
//        }

        scheduledEntityRepository.delete(entityID);
    }

}
