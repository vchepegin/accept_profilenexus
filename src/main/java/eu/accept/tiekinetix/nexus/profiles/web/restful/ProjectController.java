package eu.accept.tiekinetix.nexus.profiles.web.restful;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import eu.accept.tiekinetix.nexus.profiles.core.exceptions.EntityAlreadyExistsException;
import eu.accept.tiekinetix.nexus.profiles.core.exceptions.EntityDoesNotExist;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.*;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.ProjectRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.ScheduledEntityRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.TaskRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.WorkflowRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.reporting.BaseReportRepository;
import eu.accept.tiekinetix.nexus.profiles.util.Asserts;
import org.neo4j.ogm.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Field;
import java.util.*;


/**
 * Created by vchepegin on 01/06/2017.
 */
@CrossOrigin
@RestController
public class ProjectController {

    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private TaskRepository taskRepository;
    @Autowired
    private ScheduledEntityRepository scheduledEntityRepository;
    @Autowired
    private WorkflowRepository workflowRepository;
    @Autowired
    private BaseReportRepository baseReportRepository;



    @Autowired
    Session session;

    private final ObjectMapper mapper = new ObjectMapper();

    @CrossOrigin
    @RequestMapping(value="/getProjectByID/{projectID}", method= RequestMethod.GET,
            produces = {"application/json"})
    @Cacheable("project")
    public Project getProjectByID (@PathVariable(value="projectID") String projectID)
            throws Exception  {

        return projectRepository.findByProjectID(projectID);
    }

    @CacheEvict(value = "project", allEntries = true)
    @CrossOrigin
    @RequestMapping(value="/createProject", method = RequestMethod.POST)
    public String createProject (@RequestBody String payload) throws Exception {
        return createProject(payload, projectRepository);
        /*
        //introduce Optionals
        Project newProject = null;
        newProject = mapper.readValue(payload, Project.class);


        String guid = newProject.getProjectID();

        Project project = projectRepository.findByProjectID(guid);
        if (project != null) {
            throw new EntityAlreadyExistsException("The project with ID = " + guid +
                    " already exists but ID has to be unique.");
        }

        if (guid == null ||  guid.isEmpty()) {
            guid = java.util.UUID.randomUUID().toString();
            newProject.setProjectID(guid);
        }
        projectRepository.save(newProject);

        return guid;
        */
    }

    @CacheEvict(value = "project", allEntries = true)
    @CrossOrigin
    @RequestMapping(value="/updateProject/{projectID}", method = RequestMethod.PUT)
    public void updateProject (@RequestBody Map<String, Object> payload,
                            @PathVariable(value="projectID") String projectID) {
        updateTheProject(payload, projectID, projectRepository);
/*
        Project project = projectRepository.findByProjectID(projectID);

        if (project == null) {
            throw new EntityDoesNotExist("The project with ID " + projectID + " does not exist");
        }

        Project deltaProject = mapper.convertValue(payload, Project.class);

        for (Map.Entry<String, Object> entry : payload.entrySet()) {

            try {
                Class<?> c = project.getClass();
                Field t = null;
                //search in the whole hierarchy upwards
                while (t == null && c.getSuperclass() != null) {
                    try {
                        t = c.getDeclaredField(entry.getKey());
                    } catch (NoSuchFieldException nsEx) {
                        nsEx.printStackTrace();
                    }
                    c = c.getSuperclass();
                }

                if (t != null) {
                    t.setAccessible(true);

                        if (t.getType().isEnum()) {
                            t.set(project, Enum.valueOf((Class<Enum>) t.getType(),
                                    entry.getValue().toString()));
                        } else {
                            Object value = t.get(deltaProject);
                            t.set(project, value);
    //                            t.set(task, entry.getValue());
                        }
                } else {
                    throw new IllegalArgumentException(entry.getKey() + " field does not exist in this class hierarchy");
                }

            } catch (IllegalAccessException ex) {
                ex.printStackTrace();
            }

            projectRepository.save(project);
        }
*/
    }

    @CacheEvict(value = "project", allEntries = true)
    @CrossOrigin
    @RequestMapping(value="/addTaskToProject/{projectID}/{taskID}", method = RequestMethod.POST)
    public void addTaskToProject (@PathVariable(value="projectID") String projectID,
                                  @PathVariable(value="taskID") String taskID) throws Exception {

        if (projectID != null &&  !projectID.isEmpty() &&
                taskID != null &&  !taskID.isEmpty()) {
            Project project = projectRepository.findByProjectID(projectID);
            if (project == null) {
                throw new EntityDoesNotExist("Project with ID = " + projectID + " does not exist.");
            }
            Task task = taskRepository.findByEntityID(taskID);
            if (task == null) {
                throw new EntityDoesNotExist("Task with ID = " + taskID + " does not exist.");
            }

            project.addTask(task);

            projectRepository.save(project);
        } else {
            throw new IllegalArgumentException("Both ProjectID and TaskID have to be provided to perform this operation.");
        }
    }

    @CacheEvict(value = {"project", "workflow", "scheduledentity"}, allEntries = true)
    @CrossOrigin
    @RequestMapping(value="/addWorkflow/{projectID}/{workflowID}", method = RequestMethod.POST)
    public void addWorkflow (@PathVariable(value="projectID") String projectID,
                                  @PathVariable(value="workflowID") String workflowID) throws Exception {

        if (Strings.isNullOrEmpty(projectID)  && Strings.isNullOrEmpty(workflowID)) {
            Project project = projectRepository.findByProjectID(projectID);
            Asserts.notNull(project, "Project with ID = " + projectID);

            Workflow workflow = workflowRepository.findByWorkflowID(workflowID);
            Asserts.notNull(workflow, "Workflow with ID = " + workflowID);

            project.addWorkflow(workflow);

            projectRepository.save(project);
        } else {
            throw new IllegalArgumentException("Both ProjectID and WorkflowID have to be provided to perform this operation.");
        }
    }

    @CacheEvict(value = "project", allEntries = true)
    @CrossOrigin
    @RequestMapping(value="/deleteProject/{projectID}", method = RequestMethod.DELETE)
    public void deleteProject (@PathVariable(value="projectID") String projectID)
            throws Exception {
        //TODO: what are the consequences of this step in terms of links to other entities?!
        projectRepository.delete(projectID);
    }

    @CrossOrigin
    @RequestMapping(value="/getProjects", method = RequestMethod.GET)
    @Cacheable("project")
    public List<Project> getProjects ()
            throws Exception {

        return projectRepository.findProjects();
    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value="/startProject/{projectID}", method = RequestMethod.POST)
    public void startProject (@PathVariable(value="projectID") String projectID)
            throws Exception {
        projectRepository.initialiseStatusForProject(projectID);
    }

    @CrossOrigin
    @RequestMapping(value="/getTimeFrameOfProject/{projectID}", method= RequestMethod.GET)
    public MinMaxStartEndDates getTimeFrameOfProject (@PathVariable(value="projectID") String projectID)
            throws Exception  {
        return projectRepository.getTimeFrameOfProject(projectID);
    }

    @CacheEvict(value = "scheduledentity", allEntries = true)
    @CrossOrigin
    @RequestMapping(value="/resetProjectToInitialState/{projectID}", method = RequestMethod.POST)
    public void resetProjectToInitialState (@PathVariable(value="projectID") String projectID)
            throws Exception {
        Set<ScheduledEntity> entities = new HashSet<>();
        entities = projectRepository.getAllScheduledEntities(projectID);
        for (ScheduledEntity se : entities) {
            if (se != null) {
                se.reset();
                scheduledEntityRepository.save(se);
            }
        }
        projectRepository.initialiseStatusForProject(projectID);
        baseReportRepository.deleteAllReportsOfProject(projectID);
    }

    public static void updateTheProject(Map<String, Object> payload, String projectID,
                                        ProjectRepository projectRepository) {

        final ObjectMapper mapper = new ObjectMapper();

        Project project = projectRepository.findByProjectID(projectID);

        if (project == null) {
            throw new EntityDoesNotExist("The project with ID " + projectID + " does not exist");
        }

        Project deltaProject = mapper.convertValue(payload, Project.class);

        for (Map.Entry<String, Object> entry : payload.entrySet()) {

            try {
                Class<?> c = project.getClass();
                Field t = null;
                //search in the whole hierarchy upwards
                while (t == null && c.getSuperclass() != null) {
                    try {
                        t = c.getDeclaredField(entry.getKey());
                    } catch (NoSuchFieldException nsEx) {
                        nsEx.printStackTrace();
                    }
                    c = c.getSuperclass();
                }

                if (t != null) {
                    t.setAccessible(true);

                    if (t.getType().isEnum()) {
                        t.set(project, Enum.valueOf((Class<Enum>) t.getType(),
                                entry.getValue().toString()));
                    } else {
                        Object value = t.get(deltaProject);
                        t.set(project, value);
                        //                            t.set(task, entry.getValue());
                    }
                } else {
                    throw new IllegalArgumentException(entry.getKey() + " field does not exist in this class hierarchy");
                }

            } catch (IllegalAccessException ex) {
                ex.printStackTrace();
            }
        }
        projectRepository.save(project);
    }

    public static String createProject(String payload , ProjectRepository projectRepository)  throws Exception {
        final ObjectMapper mapper = new ObjectMapper();
        Project newProject = null;
        newProject = mapper.readValue(payload, Project.class);

        String guid = newProject.getProjectID();

        Project project = projectRepository.findByProjectID(guid);
        if (project != null) {
            throw new EntityAlreadyExistsException("The project with ID = " + guid +
                    " already exists but ID has to be unique.");
        }

        if (guid == null || guid.isEmpty()) {
            guid = java.util.UUID.randomUUID().toString();
            newProject.setProjectID(guid);
        }
        projectRepository.save(newProject);

        return guid;
    }
}
