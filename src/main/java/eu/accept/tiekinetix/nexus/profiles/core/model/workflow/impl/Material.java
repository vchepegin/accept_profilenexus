package eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

import java.io.Serializable;

/**
 * Created by vchepegin on 02/02/2017.
 */
@NodeEntity
public class Material implements Serializable {
    private static final long serialVersionUID = 937779458712314948L;
    @GraphId
    private Long id;

    private String materialID;
    private String BIM_ID;
    private String name;
    private double quantity;
    private String description;

    public Material () {}

    public Material(String materialID, String BIM_ID, String name, double quantity, String description) {
        this.materialID = materialID;
        this.BIM_ID = BIM_ID;
        this.name = name;
        this.quantity = quantity;
        this.description = description;
    }

    @JsonProperty("materialID")
    public String getMaterialID() {
        return materialID;
    }

    @JsonProperty("materialID")
    public void setMaterialID(String materialID) {
        this.materialID = materialID;
    }

    @JsonProperty("BIM_ID")
    public String getBIM_ID() {
        return BIM_ID;
    }

    @JsonProperty("BIM_ID")
    public void setBIM_ID(String BIM_ID) {
        this.BIM_ID = BIM_ID;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("quantity")
    public double getQuantity() {
        return quantity;
    }

    @JsonProperty("quantity")
    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }
}
