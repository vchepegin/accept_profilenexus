package eu.accept.tiekinetix.nexus.profiles.bim.controller;

import java.io.IOException;
import java.util.Collections;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by akuschki on 12/06/2017.
 */

@CrossOrigin
@RestController
@RequestMapping(value = "/indexes")
public class NeoIndexController extends AbstractBimController {

    @RequestMapping(method = RequestMethod.POST)
    public void createIndexes() throws IOException {
        session.query("CREATE CONSTRAINT ON (e:BimEntity) ASSERT e.entityId IS UNIQUE", Collections.emptyMap());
        session.query("CREATE CONSTRAINT ON (e:Project) ASSERT e.projectID IS UNIQUE", Collections.emptyMap());
        session.query("CREATE INDEX ON :ScheduledEntity(entityID)", Collections.emptyMap());
        session.query("CREATE INDEX ON :BimEntity(bimId)", Collections.emptyMap());
        session.query("CREATE INDEX ON :BimCategory(bimId)", Collections.emptyMap());
        session.query("CREATE INDEX ON :BimFamily(bimId)", Collections.emptyMap());
        session.query("CREATE INDEX ON :BimMaterial(bimId)", Collections.emptyMap());
        session.query("CREATE INDEX ON :BimType(bimId)", Collections.emptyMap());
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public void dropIndexes() throws IOException {
        swallowExceptions(() -> session.query("DROP CONSTRAINT ON (e:BimEntity) ASSERT e.entityId IS UNIQUE", Collections.emptyMap()));
        swallowExceptions(() -> session.query("DROP INDEX ON :BimElement(bimId)", Collections.emptyMap()));
        swallowExceptions(() -> session.query("DROP INDEX ON :BimCategory(bimId)", Collections.emptyMap()));
        swallowExceptions(() -> session.query("DROP INDEX ON :BimFamily(bimId)", Collections.emptyMap()));
        swallowExceptions(() -> session.query("DROP INDEX ON :BimMaterial(bimId)", Collections.emptyMap()));
        swallowExceptions(() -> session.query("DROP INDEX ON :BimParameter(guid)", Collections.emptyMap()));
        swallowExceptions(() -> session.query("DROP INDEX ON :BimType(bimId)", Collections.emptyMap()));
        swallowExceptions(() -> session.query("DROP INDEX ON :Project(projectID)", Collections.emptyMap()));
        swallowExceptions(() -> session.query("DROP INDEX ON :ScheduledEntity(entityID)", Collections.emptyMap()));
    }

    private void swallowExceptions(Runnable r) {
        try {
            r.run();
        } catch (Exception ex) {
        }
    }

}
