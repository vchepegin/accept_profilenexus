package eu.accept.tiekinetix.nexus.profiles.core.model.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by vchepegin on 30/01/2017.
 */
@NodeEntity
public class Certificate implements Serializable {

    private static final long serialVersionUID = 9042007212526603176L;
    @GraphId
    private Long id;

    private String certificateID; //recommend to use a URL to the certificate
    private String title;
    private String description;
    private String issuedBy; //recommnd to use a URL to the organisation
    private Date issuedAt;
    private Date validUntil; //null means it does nto expire

    public Certificate(String certificateID, String title, String description,
                       String issuedBy, Date issuedAt, Date validUntil) {
        this.certificateID = certificateID;
        this.title = title;
        this.description = description;
        this.issuedBy = issuedBy;
        this.issuedAt = new Date (issuedAt.getTime());
        this.validUntil = new Date (validUntil.getTime());
    }

    private Certificate () {}

    @JsonProperty("certificateID")
    public String getCertificateID() {
        return certificateID;
    }

    @JsonProperty("certificateID")
    public void setCertificateID(String certificateID) {
        this.certificateID = certificateID;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("issuedBy")
    public String getIssuedBy() {
        return issuedBy;
    }

    @JsonProperty("issuedBy")
    public void setIssuedBy(String issuedBy) {
        this.issuedBy = issuedBy;
    }

    @JsonProperty("issuedAt")
    public Date getIssuedAt() {
        return (Date) issuedAt.clone();
    }

    @JsonProperty("issuedAt")
    public void setIssuedAt(Date issuedAt) {
        this.issuedAt = new Date (validUntil.getTime());
    }

    @JsonProperty("validUntil")
    public Date getValidUntil() {
        return (Date)validUntil.clone();
    }

    @JsonProperty("validUntil")
    public void setValidUntil(Date validUntil) {
        this.validUntil = new Date (validUntil.getTime());
    }
}
