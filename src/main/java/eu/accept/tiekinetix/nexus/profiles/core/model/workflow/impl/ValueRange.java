package eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

import java.io.Serializable;

/**
 * Created by vchepegin on 24/02/2017.
 */
@NodeEntity
public class ValueRange implements Serializable {
    private static final long serialVersionUID = 3297710964768983642L;
    @GraphId
    private Long id;

    private String rangeID;
    private String unit;
    private double min;
    private double max;

    public ValueRange () {}

    public ValueRange(String rangeID, String unit, double min, double max) {
        this.rangeID = rangeID;
        this.unit = unit;
        this.min = min;
        this.max = max;
    }

    @JsonProperty("rangeID")
    public String getRangeID() {
        return rangeID;
    }

    @JsonProperty("rangeID")
    public void setRangeID(String rangeID) {
        this.rangeID = rangeID;
    }

    @JsonProperty("unit")
    public String getUnit() {
        return unit;
    }

    @JsonProperty("unit")
    public void setUnit(String unit) {
        this.unit = unit;
    }

    @JsonProperty("min")
    public double getMin() {
        return min;
    }

    @JsonProperty("min")
    public void setMin(double min) {
        this.min = min;
    }

    @JsonProperty("max")
    public double getMax() {
        return max;
    }

    @JsonProperty("max")
    public void setMax(double max) {
        this.max = max;
    }
}
