package eu.accept.tiekinetix.nexus.profiles.bim.model;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.google.common.collect.Sets;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.ToString;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

/**
 * Created by arian.kuschki on 12/06/17.
 */
@NodeEntity
@XmlRootElement(name = "Family")
@ToString(callSuper = true)
public class BimFamily extends BimEntity implements HasCategories {

    @Relationship
    private Set<BimCategory> categories;

    @JacksonXmlProperty(localName = "IdCategory")
    private Long idCategory;

    @JacksonXmlProperty(localName = "Name")
    private String name;

    @Override
    public Set<BimCategory> getCategories() {
        return categories;
    }

    @Override
    public void setCategories(Set<BimCategory> categories) {
        this.categories = categories;
    }

    @JsonIgnoreProperties
    @Override
    public Set<Long> getCategoryIds() {
        final HashSet<Long> categoryIds = Sets.newHashSet();
        if (idCategory != null) {
            categoryIds.add(idCategory);
        }
        return categoryIds;
    }

    @JsonProperty("IdCategory")
    public Long getIdCategory() {
        return idCategory;
    }

    @JsonProperty("IdCategory")
    public void setIdCategory(final Long idCategory) {
        this.idCategory = idCategory;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(final String name) {
        this.name = name;
    }
}
