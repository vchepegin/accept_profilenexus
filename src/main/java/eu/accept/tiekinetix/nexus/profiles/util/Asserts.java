package eu.accept.tiekinetix.nexus.profiles.util;

import com.google.common.base.Strings;

/**
 * Ripped from Apache http utils but adjusted to ACCEPT
 * Created by vchepegin on 15/07/2017.
 */
public abstract class Asserts {

    public static void notNull(final Object object, final String name) {
        if (object == null) {
            throw new IllegalStateException(name + " is null");
        }
    }

    public static void notEmpty(final String s, final String name) {
        if (Strings.isNullOrEmpty(s)) {
            throw new IllegalStateException(name + " is empty");
        }
    }

    public static void notBlank(final CharSequence s, final String name) {
        if (isBlank(s)) {
            throw new IllegalStateException(name + " is blank");
        }
    }

    public static boolean isBlank(final CharSequence s) {
        if (s == null) {
            return true;
        }
        for (int i = 0; i < s.length(); i++) {
            if (!Character.isWhitespace(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }
}
