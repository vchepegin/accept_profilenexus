package eu.accept.tiekinetix.nexus.profiles.reporting.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

import java.util.Date;

/**
 * Created by vchepegin on 19/05/2017.
 */
@NodeEntity(label = "HistoryRecord")
@JsonIgnoreProperties(ignoreUnknown = true)
public class DailyProgressReport  {

    @GraphId
    private Long id;

    private String dailyProgressReportID;

    private String entityID;
    private Date dateMonitored;
    private double didAbsolute = 0;
    private int didPercent = 0;
    private int reasonForNonCompletion;

    private boolean isVerified;
    private String comment;

    public DailyProgressReport () {}

    @JsonProperty("dateMonitored")
    public Date getDateMonitored() {
        if (dateMonitored == null) return null;
        return new Date (dateMonitored.getTime());
    }

    @JsonProperty("dateMonitored")
    public void setDateMonitored(Date dateMonitored) {
        this.dateMonitored = new Date (dateMonitored.getTime());
    }

    @JsonProperty("didAbsolute")
    public double getDidAbsolute() {
        return didAbsolute;
    }

    @JsonProperty("didAbsolute")
    public void setDidAbsolute(double didAbsolute) {
        this.didAbsolute = didAbsolute;
    }

    @JsonProperty("didPercent")
    public int getDidPercent() {
        return didPercent;
    }

    @JsonProperty("didPercent")
    public void setDidPercent(int didPercent) {
        this.didPercent = didPercent;
    }

    @JsonProperty("reasonForNonCompletion")
    public int getReasonForNonCompletion() {
        return reasonForNonCompletion;
    }

    @JsonProperty("reasonForNonCompletion")
    public void setReasonForNonCompletion(int reasonForNonCompletion) {
        this.reasonForNonCompletion = reasonForNonCompletion;
    }

    @JsonProperty("entityID")
    public String getEntityID() {
        return entityID;
    }

    @JsonProperty("entityID")
    public void setEntityID(String entityID) {
        this.entityID = entityID;
    }

    @JsonProperty("verified")
    public boolean isVerified() {
        return isVerified;
    }

    @JsonProperty("verified")
    public void setVerified(boolean verified) {
        isVerified = verified;
    }

    @JsonProperty("comment")
    public String getComment() {
        return comment;
    }

    @JsonProperty("comment")
    public void setComment(String comment) {
        this.comment = comment;
    }

    @JsonIgnore
    public String getDailyProgressReportID() {
        return dailyProgressReportID;
    }

    @JsonIgnore
    public void setDailyProgressReportID(String dailyProgressReportID) {
        this.dailyProgressReportID = dailyProgressReportID;
    }
}
