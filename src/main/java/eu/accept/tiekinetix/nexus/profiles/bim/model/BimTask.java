package eu.accept.tiekinetix.nexus.profiles.bim.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.neo4j.ogm.annotation.NodeEntity;

/**
 * Created by arian.kuschki on 20/07/17.
 */
@Getter
@Setter
@NodeEntity
@XmlRootElement(name = "Task")
@ToString(callSuper = true)
public class BimTask {
    @JacksonXmlProperty(localName = "ID")
    private int id;
    @JacksonXmlProperty(localName = "Name")
    private String name;
    //    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss")
    //    @JacksonXmlProperty(localName = "Start")
    @JacksonXmlProperty(localName = "Start_Date")
    private Date start;
    //    @JacksonXmlProperty(localName = "Finish")
//    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd'T'HH:mm:ss")
    @JacksonXmlProperty(localName = "Finish_Date")
    private Date finish;
    @JacksonXmlProperty(localName = "Duration")
    private String duration;
    //    @JacksonXmlProperty(localName = "WBS")
//    private String wbs;
    @JacksonXmlProperty(localName = "WBS_Code")
    private String wbsCode;
    //    @JacksonXmlProperty(localName = "LBS")
//    private String lbs;
    @JacksonXmlProperty(localName = "LBS_Code")
    private String lbsCode;
    @JacksonXmlProperty(localName = "Predecessor")
    private String predecessor;
    @JacksonXmlProperty(localName = "LBS_Name")
    private String description;
    @JacksonXmlProperty(localName = "Quantity")
    private double quantity;
    @JacksonXmlProperty(localName = "Unit")
    private String quantityUnit;

    public int getId() { return id;}
    public String getWbsCode() {return wbsCode;}
    public String getLbsCode() {return lbsCode;}
    public String getName() {return name;}
    public Date getStart() {return start;}
    public Date getFinish() {return finish;}
    public String getDescription() {return duration;}
    public String getQuantityUnit() {return quantityUnit;}
    public double getQuantity() {return quantity;}
    public String getPredecessor() {return predecessor;}
}
