package eu.accept.tiekinetix.nexus.profiles.repositories.reporting;

import eu.accept.tiekinetix.nexus.profiles.reporting.vo.PpcReport;
import org.springframework.data.neo4j.repository.GraphRepository;

public interface PpcReportRepository extends GraphRepository<PpcReport> {


}
