package eu.accept.tiekinetix.nexus.profiles.reporting.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 *
 * Created by vchepegin on 26/07/2017.
 */
@Deprecated
@JsonIgnoreProperties(ignoreUnknown = true)
public class KpiFilter {

    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date intervalStartDate;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date intervalEndDate;
    private String tier;
    private String taskType;
    private String lbsCode;
    private String wbsCode;
    private String entityID;

    public KpiFilter(Date intervalStartDate, Date intervalEndDate, String tier, String taskType,
                     String lbsCode, String wbsCode, String entityID) {
        this.intervalStartDate = intervalStartDate;
        this.intervalEndDate = intervalEndDate;
        this.tier = tier;
        this.taskType = taskType;
        this.lbsCode = lbsCode;
        this.wbsCode = wbsCode;
        this.entityID = entityID;
    }

    public KpiFilter () {};

    @JsonProperty("intervalStartDate")
    public Date getIntervalStartDate() {
        return intervalStartDate;
    }

    @JsonProperty("intervalStartDate")
    public void setIntervalStartDate(Date intervalStartDate) {
        this.intervalStartDate = intervalStartDate;
    }

    @JsonProperty("intervalEndDate")
    public Date getIntervalEndDate() {
        return intervalEndDate;
    }

    @JsonProperty("intervalEndDate")
    public void setIntervalEndDate(Date intervalEndDate) {
        this.intervalEndDate = intervalEndDate;
    }

    @JsonProperty("tier")
    public String getTier() {
        return tier;
    }

    @JsonProperty("tier")
    public void setTier(String tier) {
        this.tier = tier;
    }

    @JsonProperty("taskType")
    public String getTaskType() {
        return taskType;
    }

    @JsonProperty("taskType")
    public void setTaskType(String taskType) {
        this.taskType = taskType;
    }

    @JsonProperty("lbsCode")
    public String getLbsCode() {
        return lbsCode;
    }

    @JsonProperty("lbsCode")
    public void setLbsCode(String lbsCode) {
        this.lbsCode = lbsCode;
    }

    @JsonProperty("wbsCode")
    public String getWbsCode() {
        return wbsCode;
    }

    @JsonProperty("wbsCode")
    public void setWbsCode(String wbsCode) {
        this.wbsCode = wbsCode;
    }

    @JsonProperty("entityID")
    public String getEntityID() {
        return entityID;
    }

    @JsonProperty("entityID")
    public void setEntityID(String entityID) {
        this.entityID = entityID;
    }

}
