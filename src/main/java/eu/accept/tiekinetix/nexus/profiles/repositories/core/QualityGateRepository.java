package eu.accept.tiekinetix.nexus.profiles.repositories.core;

import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.QualityGate;
import org.springframework.data.neo4j.repository.GraphRepository;

/**
 * Created by vchepegin on 24/02/2017.
 */
public interface QualityGateRepository extends GraphRepository<QualityGate> {

    QualityGate findByEntityID (String entityID);

}
