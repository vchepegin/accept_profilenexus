package eu.accept.tiekinetix.nexus.profiles.core.model.enumerations;

/**
 * Created by vchepegin on 17/02/2017.
 */
public enum ScheduleStatus {
    AHEAD_OF_SCHEDULE (1),
    ON_SCHEDULE (2),
    BEHIND_SCHEDULE (3);


    private final int scheduleStatusCode;

    ScheduleStatus(int code) {
        this.scheduleStatusCode = Integer.valueOf(code);
    }

    private int getScheduleStatusCode() {
        return scheduleStatusCode;
    }

    @Override
    public String toString () {
        switch (this) {
            case AHEAD_OF_SCHEDULE: return "AHEAD_OF_SCHEDULE";
            case ON_SCHEDULE: return "ON_SCHEDULE";
            case BEHIND_SCHEDULE: return "BEHIND_SCHEDULE";
            default: throw new IllegalArgumentException("No such status within ScheduleStatus enum.");
        }
    }
}
