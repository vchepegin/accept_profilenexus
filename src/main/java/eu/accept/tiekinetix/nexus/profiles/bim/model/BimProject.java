package eu.accept.tiekinetix.nexus.profiles.bim.model;

import java.util.Date;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.neo4j.ogm.annotation.NodeEntity;

/**
 * Created by peter.merz on 12/09/17.
 */
@Getter
@Setter
@NodeEntity
@XmlRootElement(name = "Project")
@ToString(callSuper = true)
public class BimProject {
    @JacksonXmlProperty(localName = "ProjectName")
    private String projectName;
    @JacksonXmlProperty(localName = "ProjectID")
    private String projectID;
    @JacksonXmlProperty(localName = "ClientName")
    private String clientName;
    @JacksonXmlProperty(localName = "Country")
    private String country;
    @JacksonXmlProperty(localName = "Address")
    private String address;

    public String getProjectID() {
        return projectID;
    }
    public String getProjectName() {
        return projectName;
    }
    public String getClientName() {
        return clientName;
    }
    public String getCountry() {
        return country;
    }
    public String getAddress() {
        return address;
    }
}