package eu.accept.tiekinetix.nexus.profiles.bim.repository;

import java.util.List;

import eu.accept.tiekinetix.nexus.profiles.bim.model.BimFamily;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource
public interface FamilyRepository extends GraphRepository<BimFamily> {
    List<BimFamily> findByProjectProjectIDAndBimId(String projectID, Long bimId);
    List<BimFamily> findByProjectProjectIDAndName(String projectID, String name);


}
