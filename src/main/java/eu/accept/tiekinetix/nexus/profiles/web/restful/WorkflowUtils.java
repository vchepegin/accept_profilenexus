package eu.accept.tiekinetix.nexus.profiles.web.restful;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.Constructable;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Activity;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.QualityGate;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Quantity;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity;

import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by vchepegin on 23/06/2017.
 */
public final class WorkflowUtils {

    /**
     *
     * @param currentParent
     * @param params
     * @return
     */
    public static Set<ScheduledEntity> doConstruction (ScheduledEntity currentParent, Map<String, Object> params) {

        Set<ScheduledEntity> newStartNodes = new HashSet<>(25);
        if (currentParent.getSuccessors() != null && !currentParent.getSuccessors().isEmpty()) {
            //update dates for all its successors
            for (ScheduledEntity entity : currentParent.getSuccessors()) {

                ScheduledEntity specialPredassessor = (ScheduledEntity) params.get(entity.getEntityID());

                //calculate current ONLY IF the date is changed
                if (specialPredassessor != null) {
//                    ScheduledEntity startNode = (ScheduledEntity) params.get(entity.getEntityID());
                    entity.setStartDate(specialPredassessor.getEndDate());

                    ((Constructable) entity).construct(params);

                } else {
                    Date oldStartDate = entity.getStartDate();
                    Date newStartDate = currentParent.getEndDate();
//                    entity.setStartDate(currentParent.getEndDate());
                    if (oldStartDate == null || oldStartDate.before(newStartDate)) {
                        entity.setStartDate(newStartDate);

                        ((Constructable) entity).construct(params);
                    }
                }

//                Constructable asConstructable = (Constructable) entity;
//                asConstructable.construct(params);

                newStartNodes.add(entity);
            }
        }

        return newStartNodes;
    }

    /**
     * Update the new Workflow step with the parameters specified by the client
     * @param se
     * @param params
     */
    public static void updateWithNewParameters (ScheduledEntity se,  Map<String, Object> params) {

        //se.setWbsCode(params.get("wbsCode").toString());
        final ObjectMapper mapper = new ObjectMapper();
        if (se instanceof Activity) {
            Activity activity = ((Activity) se);
            activity.setWorkingHoursPerDay(Double.parseDouble(params.get("workingHoursPerDay").toString()));

            int plannedNumberOfCrews;
            if (params.get("plannedNumberOfCrews") != null ) {
                plannedNumberOfCrews = Integer.valueOf(params.get("plannedNumberOfCrews").toString());
                activity.setPlannedNumberOfCrews(plannedNumberOfCrews);
            }

            Object o = params.get("consumptionRate");
            if (o != null) {
                Quantity cr = mapper.convertValue(o, Quantity.class);
                activity.addConsumptionRate(cr);
            }
//            else {
//                Quantity cr = new Quantity(((Activity) se).getConsumptionRate());
//                ((Activity) se).addConsumptionRate(cr);
//            }

            o = params.get("quantity");
            if (o != null) {
                Quantity qt = mapper.convertValue(o, Quantity.class);
                activity.addQuantity(qt);
            }
//            else {
//                Quantity cr = new Quantity(((Activity) se).getQuantity());
//                ((Activity) se).addQuantity(cr);
//            }

            activity.calcScheduledWork();
        }

        if (se instanceof QualityGate || se instanceof Activity) {
            String t = params.get("timeout").toString();
            if (t != null) {
                ((ScheduledEntity) se).setTimeout(Double.valueOf(t));
            }
        }

    }

}
