package eu.accept.tiekinetix.nexus.profiles.core.model.workflow;

import java.util.Map;

/**
 * This interface declares one method construct() to be used for the initialisation of different dependent parameters
 * for dependent Tasks, Activities, QualityGates
 *
 * Created by vchepegin on 25/04/2017.
 */
public interface Constructable {

    /**
     * The method accepts a map with key, values and this is its responsibility to know how to process them.
     * @param parameters
     */
    Map<String, Object> construct (Map<String, Object> parameters);
}
