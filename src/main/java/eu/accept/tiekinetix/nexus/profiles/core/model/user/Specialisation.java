package eu.accept.tiekinetix.nexus.profiles.core.model.user;

import com.fasterxml.jackson.annotation.JsonProperty;
import eu.accept.tiekinetix.nexus.profiles.core.model.enumerations.SpecialisationEnum;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by vchepegin on 07/03/2017.
 */
@NodeEntity
public class Specialisation implements Serializable {

    private static final long serialVersionUID = 3877728888535958403L;
    @GraphId
    private Long id;

    private Set<String> specialisations;

    public Specialisation(String specialisation) {
        if (this.specialisations == null) {
            this.specialisations = new HashSet<>();
        }
        this.specialisations.add(specialisation);
    }

    private Specialisation() {}

    @JsonProperty("specialisation")
    public Set<String> getSpecialisations() {
        return specialisations;
    }

//    @JsonProperty("specialisation")
//    public void addSpecialisation(SpecialisationEnum specialisation) {
    public void add(String specialisation) {
//        this.specialisations.add(specialisation.toString());
        if (specialisations == null) {
            specialisations = new HashSet<>();
        }
        this.specialisations.add(specialisation);
    }

    public void remove(String specialisation) {
        this.specialisations.remove(specialisation);
    }

}
