package eu.accept.tiekinetix.nexus.profiles.web.restful;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Strings;
import eu.accept.tiekinetix.nexus.profiles.core.exceptions.EntityAlreadyExistsException;
import eu.accept.tiekinetix.nexus.profiles.core.exceptions.EntityDoesNotExist;
import eu.accept.tiekinetix.nexus.profiles.core.model.user.ContactDetails;
import eu.accept.tiekinetix.nexus.profiles.core.model.user.Crew;
import eu.accept.tiekinetix.nexus.profiles.core.model.user.Person;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Activity;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Project;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.TimeFrame;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.ActivityRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.ContactDetailsRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.CrewRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.PersonRepository;
import eu.accept.tiekinetix.nexus.profiles.util.TikkiQueue;
import org.neo4j.ogm.session.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Field;
import java.util.*;

/**
 *Created by vchepegin on 25/01/2017.
 */

@CrossOrigin
@RestController
public class PersonController {

    @Autowired //don't forget the setter
    private PersonRepository personRepository;
    @Autowired
    private CrewRepository crewRepository;
    @Autowired
    private ContactDetailsRepository contactDetailsRepository;
    @Autowired
    private ActivityRepository activityRepository;

    @Autowired
    Session session;

    private final ObjectMapper mapper = new ObjectMapper();

    //    @Cacheable(value = "person")
    @CrossOrigin
    @RequestMapping(value = "/findPersonByFilter/{limit}", method = RequestMethod.POST)
    public List<Person> findPersonByFilter(@RequestBody Map<String, Object> payload,
                                           @PathVariable(value = "limit") String limit) throws Exception {

        //issue #107 - added *0..1 for optional
        final String template = "match (a:Person)-[r*0..1]->(b) %1 return a, r, b limit " + limit;

        String cypher = prepareCypher4Person(template, payload);

        Iterable<Map<String, Object>> iterableResult = session.query(cypher, payload).queryResults();

        Set<Person> persons = new HashSet<Person>();
        ObjectMapper mapper = new ObjectMapper();
        for (Map map : iterableResult) {
            Person person = mapper.convertValue(map.get("a"), Person.class);
            persons.add(person);
        }
        ArrayList<Person> personList = new ArrayList<>();
        personList.addAll(persons);
        return personList;
    }

    /**
     * Create a string which is a specific query for /findPersonByFilter/{limit} endpoint
     *
     * @param template
     * @param payload
     * @return
     */
    private String prepareCypher4Person(String template, Map<String, Object> payload) {
        StringBuilder criteria = new StringBuilder("");
        boolean isFirst = true;
        for (Map.Entry<String, Object> entry : payload.entrySet()) {

            String param = entry.getKey();

            if (entry.getValue() instanceof Map) {
                Map<String, Object> map2 = (Map<String, Object>) entry.getValue();
                for (Map.Entry<String, Object> entry2 : map2.entrySet()) {
                    payload.put(entry2.getKey(), entry2.getValue());
                    String param2 = entry2.getKey();
                    if (!param2.toUpperCase().equals("EMAIL") && !param2.toUpperCase().equals("CITY")) {
                        if (!isFirst) {
                            criteria.append(" AND ");
                        }

                        criteria.append("b." + param2 + " = {" + param2 + "}");
                        payload.put(param2, entry2.getValue());
                        isFirst = false;
                    } else {
                        switch (param2.toUpperCase()) {
                            case "EMAIL":
                                if (!isFirst) {
                                    criteria.append(" AND ");
                                }
                                criteria.append(" LOWER(b.email) CONTAINS LOWER({" + param2 + "})");
                                isFirst = false;
                                break;
                            case "CITY":
                                if (!isFirst) {
                                    criteria.append(" AND ");
                                }
                                criteria.append(" LOWER(b.city) CONTAINS LOWER({" + param2 + "})");
                                isFirst = false;
                                break;
                            default:
                                break;
                        }
                    }
                }

                payload.remove(entry.getKey());

            } else {
                if (!param.toUpperCase().equals("FIRSTNAME") && !param.toUpperCase().equals("LASTNAME") &&
                        !param.toUpperCase().equals("ACCEPTUSERNAME") && !param.toUpperCase().equals("SPECIALISATIONS")
                        && !param.toUpperCase().equals("SKILLS")) {
                    if (!isFirst) {
                        criteria.append(" AND ");
                    }
                    criteria.append("a." + param + " = {" + param + "}");
                    isFirst = false;
                }
                switch (param.toUpperCase()) {
                    case "FIRSTNAME":
                        if (!isFirst) {
                            criteria.append(" AND ");
                        }
                        criteria.append(" LOWER(a.firstName) CONTAINS LOWER({" + param + "})");
                        isFirst = false;
                        break;
                    case "LASTNAME":
                        if (!isFirst) {
                            criteria.append(" AND ");
                        }
                        criteria.append(" LOWER(a.lastName) CONTAINS LOWER({" + param + "})");
                        isFirst = false;
                        break;
                    case "ACCEPTUSERNAME":
                        if (!isFirst) {
                            criteria.append(" AND ");
                        }
                        criteria.append(" LOWER(a.acceptUserName) CONTAINS LOWER({" + param + "})");
                        isFirst = false;
                        break;
                    case "SPECIALISATIONS":
                        if (!isFirst) {
                            criteria.append(" AND ");
                        }
                        if (entry.getValue().toString() == "") {
                            criteria.append(" a.specialisations <> [] ");
                        } else {
                            param = "\"(?i).*" + entry.getValue().toString().toLowerCase() + ".*\")";
                            criteria.append(" ANY(item IN a.specialisations WHERE item =~" + param);
//                            criteria.append(" LOWER({" + param + "})" + " IN a.specialisations");
                        }
                        payload.remove(entry.getKey());
                        isFirst = false;
                        break;
                    case "SKILLS":
                        if (!isFirst) {
                            criteria.append(" AND ");
                        }
                        if (entry.getValue().toString() == "") {
                            criteria.append(" a.skills <> [] ");
                        } else {
                            param = "\"(?i).*" + entry.getValue().toString().toLowerCase() + ".*\")";
                            criteria.append(" ANY(item IN a.skills WHERE item =~" + param);
//                            criteria.append(" LOWER({" + param + "})" + " IN a.skills");
                        }
                        payload.remove(entry.getKey());
                        isFirst = false;
                        break;
                    default:
                }

/*
                if (!isFirst) {
                    criteria.append(" AND ");
                }
                criteria.append("a." + param + " = {" + param + "}");
                isFirst = false;
*/
            }
        }

        String query = "";
        if (!"".equals(criteria.toString())) {
            criteria.insert(0, " WHERE ");
            query = template.replace("%1", criteria.toString());
            //query.insert(71, criteria.toString());
        } else {
            query = template.replace("%1", "");
        }

        return query;
    }

    @Cacheable(value = "person")
    @CrossOrigin
    @RequestMapping(value = "/getPersonByID/{personID:.+}", method = RequestMethod.GET)
    public Person getPersonById(@PathVariable(value = "personID") String personID)
            throws Exception {
        Person person = personRepository.findByPersonID(personID);
        if (person == null) {
            throw new EntityDoesNotExist("Person with ID = " + personID + " does not exist");
        }
        return person;
    }

    @CacheEvict(value = "person", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/createPerson", method = RequestMethod.POST)
    public String createPerson(@RequestBody String payload) throws Exception {

        Person newPerson = null;
        newPerson = mapper.readValue(payload, Person.class);

        String guid = newPerson.getPersonID();

        Person person = personRepository.findByPersonID(guid);
        if (person != null) {
            throw new EntityAlreadyExistsException("The person with ID = " + guid + "already exists.");
        }

        if (guid == null || guid.isEmpty()) {
            guid = java.util.UUID.randomUUID().toString();
            newPerson.setPersonID(guid);
        }
        if (newPerson.getTikkiUserID() == null || newPerson.getTikkiUserID().isEmpty()) {
            newPerson.setTikkiUserID(TikkiQueue.createTikkiUser(newPerson));
        }
        personRepository.save(newPerson);

        return guid;
    }

    @CacheEvict(value = "person", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/createPerson/{personID:.+}", method = RequestMethod.POST)
    public String createPerson(@RequestBody String payload,
                               @PathVariable(value = "personID") String personID) throws Exception {

        Person oldPerson = personRepository.findByPersonID(personID);
        if (oldPerson != null) {
            throw new EntityAlreadyExistsException("The person with ID = " + personID + "already exists.");
        }

        Person newPerson = null;
        newPerson = new ObjectMapper().readValue(payload, Person.class);

        newPerson.setPersonID(personID);
        if (newPerson.getTikkiUserID() == null || newPerson.getTikkiUserID().isEmpty()) {
            newPerson.setTikkiUserID(TikkiQueue.createTikkiUser(newPerson));
        }
        personRepository.save(newPerson);

        return personID;
    }

    @CacheEvict(value = "person", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/updatePerson", method = RequestMethod.PUT)
    public void updatePerson(@RequestBody Map<String, Object> payload) {

        Person person = personRepository.findByPersonID
                (payload.get("personID").toString());

        if (person == null) {
            throw new EntityDoesNotExist(payload.get("personID").toString());
        }

        person = buildObjectFromPayload(payload, person);

        personRepository.save(person);
    }

    @CacheEvict(value = "person", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/addOnLeaveToPerson/{personID:.+}", method = RequestMethod.POST)
    public String addOnLeaveToPerson(@RequestBody String payload,
                                     @PathVariable(value = "personID") String personID) throws Exception {

        Person person = personRepository.findByPersonID(personID);
        if (person == null) {
            throw new EntityDoesNotExist(personID);
        }

        TimeFrame onLeave = null;
        onLeave = new ObjectMapper().readValue(payload, TimeFrame.class);

        String guid = java.util.UUID.randomUUID().toString();

        onLeave.setTimeFrameID(guid);
        person.addOnLeaveEvent(onLeave);
        personRepository.save(person);

        return guid;
    }

    @CacheEvict(value = "person", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/linkPersonToCrew/{personID:.+}/{crewID}", method = RequestMethod.POST)
    public void linkPersonToCrew(@PathVariable(value = "personID") String personID,
                                 @PathVariable(value = "crewID") String crewID) throws Exception {

        Person person = personRepository.findByPersonID(personID);

        if (person == null) {
            throw new EntityDoesNotExist(personID);
        }

        Crew crew = crewRepository.findByCrewID(crewID);
        crew.addCrewMember(person);
        crewRepository.save(crew);
        Set<String> assignedToActivities = crewRepository.findActivitiesByCrewId(crewID);
        for (String activityID : assignedToActivities) {
            Activity activity = activityRepository.findByEntityID(activityID);
            TikkiQueue.updateTikkiQueueForCrew(activity.getCrews(), activity.getTikkiQueueID());
        }
    }

    @CacheEvict(value = "person", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/deletePerson/{personID:.+}", method = RequestMethod.DELETE)
    public void deletePerson(@PathVariable(value = "personID") String personID)
            throws Exception {
        //TODO: whar are the consequences of this step in terms of links to other entities?!
        personRepository.delete(personID);
    }

    @CacheEvict(value = "person", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/removeLeaveFromPerson/{personID:.+}", method = RequestMethod.DELETE)
    public void removeLeaveFromPerson(@PathVariable(value = "personID") String personID)
            throws Exception {
        //TODO: whar are the consequences of this step in terms of links to other entities?!
        personRepository.deleteLeave(personID);
    }

    @CacheEvict(value = "person", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/removePersonFromCrew/{crewID}/{personID:.+}", method = RequestMethod.DELETE)
    public String removePersonFromCrew(@PathVariable(value = "crewID") String crewID,
                                       @PathVariable(value = "personID") String personID) throws Exception {

        Crew crew = crewRepository.findByCrewID(crewID);
        if (crew == null) {
            throw new EntityDoesNotExist("The crew with ID = " + crewID + "does not exist.");
        }

        Person person = personRepository.findByPersonID(personID);
        if (person == null) {
            throw new EntityDoesNotExist("The person with ID = " + personID + "does not exist.");
        }


        String status;
        if (crew.removeCrewMember(person)) {
            status = "removed";
        } else {
            status = "was not in the crew";
        }

        crewRepository.save(crew);
        Set<String> assignedToActivities = crewRepository.findActivitiesByCrewId(crewID);
        for (String activityID : assignedToActivities) {
            Activity activity = activityRepository.findByEntityID(activityID);
            TikkiQueue.updateTikkiQueueForCrew(activity.getCrews(), activity.getTikkiQueueID());
        }

        return "{ \"status\":\"" + status + "\"}";
    }

    @CacheEvict(value = "person", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/createContactDetails/{personID:.+}", method = RequestMethod.POST)
    public void createContactDetails(@RequestBody String payload,
                                     @PathVariable(value = "personID") String personID)
            throws Exception {

        Person person = personRepository.findByPersonID(personID);
        if (person == null) {
            throw new EntityDoesNotExist("The person with ID = " + personID + "does not exist.");
        }

        ContactDetails contactDetails = mapper.readValue(payload, ContactDetails.class);
        person.setContactDetails(contactDetails);

        personRepository.save(person);
    }

    @CacheEvict(value = "person", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/updateContactDetails/{personID:.+}", method = RequestMethod.POST)
    public void updateContactDetails(@RequestBody Map<String, Object> payload,
                                     @PathVariable(value = "personID") String personID) {

        Person person = contactDetailsRepository.findByPersonID(personID);

        if (person == null) {
            throw new EntityDoesNotExist("Contact details for person with ID = " + personID);
        }

        ContactDetails cd = new ContactDetails();

        cd = buildObjectFromPayload(payload, cd);
        person.setContactDetails(cd);

        personRepository.save(person);
    }

    private <T> T buildObjectFromPayload(Map<String, Object> payload, T obj) {
        for (Map.Entry<String, Object> entry : payload.entrySet()) {

            try {
                Class<?> c = obj.getClass();
                Field t = c.getDeclaredField(entry.getKey());
                t.setAccessible(true);
                if (t.getType().isEnum()) {
                    t.set(obj, Enum.valueOf((Class<Enum>) t.getType(),
                            entry.getValue().toString()));
                } else {
                    t.set(obj, entry.getValue());
                }
            } catch (NoSuchFieldException | IllegalAccessException ex) {
                ex.printStackTrace();
            }
        }
        return obj;
    }

    @CacheEvict(value = "person", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/addSpecialization/{personID:.+}/{specialization}", method = RequestMethod.POST)
    public Person addSpecialization(@PathVariable(value = "personID") String personID,
                                    @PathVariable(value = "specialization") String specialization) {

        Person person = personRepository.findByPersonID(personID);
        if (person == null) {
            throw new EntityDoesNotExist("Person with ID = " + personID + " does not exist");
        } else {
            person.addSpecialisation(specialization);
        }
        personRepository.save(person);
        return person;
    }

    @CacheEvict(value = "person", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/removeSpecialization/{personID:.+}/{specialization}", method = RequestMethod.POST)
    public Person removeSpecialization(@PathVariable(value = "personID") String personID,
                                       @PathVariable(value = "specialization") String specialization) {

        Person person = personRepository.findByPersonID(personID);
        if (person == null) {
            throw new EntityDoesNotExist("Person with ID = " + personID + " does not exist");
        } else {
            person.removeSpecialisation(specialization);
        }
        personRepository.save(person);
        return person;
    }

    @CacheEvict(value = "person", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/addSkill/{personID:.+}/{skill}", method = RequestMethod.POST)
    public Person addSkill(@PathVariable(value = "personID") String personID,
                           @PathVariable(value = "skill") String skill) {

        Person person = personRepository.findByPersonID(personID);
        if (person == null) {
            throw new EntityDoesNotExist("Person with ID = " + personID + " does not exist");
        } else {
            person.addSkill(skill);
        }
        personRepository.save(person);
        return person;
    }

    @CacheEvict(value = "person", allEntries = true)
    @CrossOrigin
    @RequestMapping(value = "/removeSkill/{personID:.+}/{skill}", method = RequestMethod.POST)
    public Person removeSkill(@PathVariable(value = "personID") String personID,
                              @PathVariable(value = "skill") String skill) {

        Person person = personRepository.findByPersonID(personID);
        if (person == null) {
            throw new EntityDoesNotExist("Person with ID = " + personID + " does not exist");
        } else {
            person.removeSkill(skill);
        }
        personRepository.save(person);
        return person;
    }

    @CrossOrigin
    @RequestMapping(value = "/getSkills", method = RequestMethod.GET)
    public List<String> getSkills() throws Exception {
        return prepareStringSet(personRepository.getSkills());

    }

    @CrossOrigin
    @RequestMapping(value = "/getSpecialisations", method = RequestMethod.GET)
    public List<String> getSpecialisations() throws Exception {
        return prepareStringSet(personRepository.getSpecialisations());
    }

    private ArrayList<String> prepareStringSet(Set<String> input) {
        ArrayList<String> specList = new ArrayList<>();
        Iterator iter = input.iterator();
        if (iter.hasNext()) {
            iter.next();
            while (iter.hasNext()) {
                String result = iter.next().toString();
                if (result != null && result.length() >= 2) {
                    String reducedResult = result.substring(1, result.length() - 1);
                    specList.add(reducedResult);
                }
            }
        }
        return specList;

    }

    @CrossOrigin
    @RequestMapping(value = "/getProjectsOfPerson/{personID:.+}", method = RequestMethod.GET)
    public Set<Project> getProjectsOfPerson(@PathVariable(value = "personID") String personID) throws Exception {
        return personRepository.getProjectsOfPerson(personID);
    }

    @CrossOrigin
    @RequestMapping(value = "/getTasksPersonIsInvolved/{personID:.+}", method = RequestMethod.GET)
    public Set<ScheduledEntity> getTasksPersonIsInvolved(@PathVariable(value = "personID") String personID) throws Exception {
        return personRepository.getTasksPersonIsInvolved(personID);
    }
}