package eu.accept.tiekinetix.nexus.profiles.bim.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.ToString;
import org.neo4j.ogm.annotation.NodeEntity;

/**
 * Created by arian.kuschki on 12/06/17.
 */
@NodeEntity
@XmlRootElement(name = "Category")
@ToString(callSuper = true)
public class BimCategory extends BimEntity {

    @JacksonXmlProperty(localName = "InternalName")
    @JsonProperty("InternalName")
    public String internalName;

    @JacksonXmlProperty(localName = "IdMainCategory")
    @JsonProperty("IdMainCategory")
    public String idMainCategory;

    @JacksonXmlProperty(localName = "Name")
    @JsonProperty("Name")
    public String name;

    @JsonProperty("InternalName")
    public String getInternalName() {
        return internalName;
    }

    @JsonProperty("InternalName")
    public void setInternalName(String internalName) {
        this.internalName = internalName;
    }

    @JsonProperty("IdMainCategory")
    public String getIdMainCategory() {
        return idMainCategory;
    }

    @JsonProperty("IdMainCategory")
    public void setIdMainCategory(String idMainCategory) {
        this.idMainCategory = idMainCategory;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }
}

