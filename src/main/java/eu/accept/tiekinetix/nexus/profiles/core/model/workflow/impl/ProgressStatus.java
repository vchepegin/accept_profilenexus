package eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by vchepegin on 08/02/2017.
 */
@NodeEntity
public class ProgressStatus implements Serializable  {

    private static final long serialVersionUID = -2850362284591719775L;
    @GraphId
    private Long id;

    private String progressStatusID;
    private String statusValue; //APPROVED, ON_SCHEDULE, BEHIND_SCHEDULE, AHEAD_SCHEDULE
    private String statusReason;
    private Date statusUpdateTimeStamp;
    private String inspectorID;
    private float ppcValue;
    private float parValue;


    public ProgressStatus(String progressStatusID, String statusValue,
                          String statusReason, Date statusUpdateTimeStamp, String inspectorID) {
        this.progressStatusID = progressStatusID;
        this.statusValue = statusValue;
        this.statusReason = statusReason;
        this.statusUpdateTimeStamp = new Date (statusUpdateTimeStamp.getTime());
        this.inspectorID = inspectorID;
    }

    public ProgressStatus() {}

    @JsonProperty("progressStatusID")
    public String getProgressStatusID() {
        return progressStatusID;
    }

    @JsonProperty("progressStatusID")
    public void setProgressStatusID(String progressStatusID) {
        this.progressStatusID = progressStatusID;
    }

    @JsonProperty("statusValue")
    public String getStatusValue() {
        return statusValue;
    }

    @JsonProperty("statusValue")
    public void setStatusValue(String statusValue) {
        this.statusValue = statusValue;
    }

    @JsonProperty("statusReason")
    public String getStatusReason() {
        return statusReason;
    }

    @JsonProperty("statusReason")
    public void setStatusReason(String statusReason) {
        this.statusReason = statusReason;
    }

    @JsonProperty("statusUpdateTimeStamp")
    public Date getStatusUpdateTimeStamp() {
        if (statusUpdateTimeStamp == null) return null;
        return new Date (statusUpdateTimeStamp.getTime());
    }

    @JsonProperty("statusUpdateTimeStamp")
    public void setStatusUpdateTimeStamp(Date statusUpdateTimeStamp) {
        this.statusUpdateTimeStamp = new Date (statusUpdateTimeStamp.getTime());
    }

    @JsonProperty("inspectorID")
    public String getInspectorID() {
        return inspectorID;
    }

    @JsonProperty("inspectorID")
    public void setInspectorID(String inspectorID) {
        this.inspectorID = inspectorID;
    }

    @JsonProperty("ppcValue")
    public float getPpcValue() {
        return ppcValue;
    }

    @JsonProperty("ppcValue")
    public void setPpcValue(float ppcValue) {
        this.ppcValue = ppcValue;
    }

    @JsonProperty("parValue")
    public float getParValue() {
        return parValue;
    }

    @JsonProperty("parValue")
    public void setParValue(float parValue) {
        this.parValue = parValue;
    }
}
