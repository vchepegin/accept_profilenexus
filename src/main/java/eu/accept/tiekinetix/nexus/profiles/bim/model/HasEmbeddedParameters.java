package eu.accept.tiekinetix.nexus.profiles.bim.model;

import java.util.Set;

/**
 * Created by arian.kuschki on 19/06/17.
 */
public interface HasEmbeddedParameters {

    Set<BimParameter> getParamsByName(String name);

    Set<Long> getParamIds(final String name);
    Set<BimParameter> getParameters();
    void setParameters(Set<BimParameter> parameters);
    // added by Peter
    Long getNeoId();
    Long getBimId();
    String getBimEntityId();
}
