package eu.accept.tiekinetix.nexus.profiles.bim.repository;

import java.util.List;

import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


@RepositoryRestResource
public interface BimScheduledEntityRepository extends GraphRepository<ScheduledEntity> {
    // Task bean currently has no project link
//    List<Task> findByProjectProjectIDAndEntityID(String projectID, String entityID);
    List<ScheduledEntity> findByEntityID(String entityID);
    List<ScheduledEntity> getByEntityID(String entityID);

    @Query("match (a {entityID: {0} }) return a")
    ScheduledEntity getScheduledEntityByID (@Param("0")String id);

}
