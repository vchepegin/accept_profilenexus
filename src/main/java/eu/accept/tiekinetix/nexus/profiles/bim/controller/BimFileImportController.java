package eu.accept.tiekinetix.nexus.profiles.bim.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static eu.accept.tiekinetix.nexus.profiles.bim.controller.BimRelationshipService.*;
import static eu.accept.tiekinetix.nexus.profiles.web.restful.ProjectController.createProject;
import static eu.accept.tiekinetix.nexus.profiles.web.restful.ProjectController.updateTheProject;
import static eu.accept.tiekinetix.nexus.profiles.web.restful.TaskController.*;
import static java.util.stream.Collectors.toSet;

import eu.accept.tiekinetix.nexus.profiles.bim.model.*;
import eu.accept.tiekinetix.nexus.profiles.bim.repository.*;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Project;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.ProjectRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.ScheduledEntityRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.TaskRepository;
import lombok.Getter;

import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.neo4j.ogm.json.JSONException;
import org.neo4j.ogm.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by akuschki on 12/06/2017.
 * deniz.coskun
 */
@CrossOrigin
@RestController
public class BimFileImportController {

    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private CategoryRepository categoryRepository;
    @Autowired
    private BimElementRepository bimElementRepository;
    @Autowired
    private BimEntityRepository bimEntityRepository;
    @Autowired
    private FamilyRepository familyRepository;
    @Autowired
    private MaterialRepository materialRepository;
    @Autowired
    private TypeRepository typeRepository;
    @Autowired
    private RoomRepository roomRepository;
    @Autowired
    private ScheduledEntityRepository scheduledEntityRepository;
    @Autowired
    private TaskRepository taskRepository;

    //    private String URI = "http://35.185.0.167:7070/";	// Development Server
//    private String URI = "http://accept.tiekinetix.net:7070/";	// Production Server
//    private String URI = "localhost:7070/";


    @CacheEvict(value = "project", allEntries = true)
    @RequestMapping(value = "/importBIM/{projectID}/{whatToImport}", method = RequestMethod.POST, produces =
        {"application/json", "application/xml"}, consumes = {"application/json", "application/xml"})
    public ResponseEntity importBIM(@Validated @RequestBody BimFile bimFile,
                             @PathVariable(value = "projectID") String projectID,
                             @PathVariable(value = "whatToImport") String whatToImport)
        throws IOException

    {
        // TODO: create the project if it does not exist
        final List<Project> projects = projectRepository.findAllByProjectID(projectID);
        if (projects == null || projects.isEmpty()) {
            JSONObject json = new JSONObject();
            try {
                json.put("projectID", projectID);
                json.put("projectName", projectID);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String jsonPayload = json.toString();
            try {
                createProject(jsonPayload, projectRepository);
            } catch (Exception e) {
                e.printStackTrace();
            }
            // Check if the response 200. Otherwise BAD_REQUEST
            Project proj = new Project();
            proj.setProjectID(projectID);
            proj.setProjectName(projectID);
            projects.add(proj);
        }

        //remove any previously imported BimEntities from the same project
        bimEntityRepository.detachDeleteByProject(projectID);

        final BimFileData bt = new BimFileData(bimFile, projects.get(0));
        if (whatToImport == null || whatToImport.isEmpty()) {
            whatToImport = "both";
        }
        if (whatToImport.toLowerCase().equals("task") || whatToImport.toLowerCase().equals("both") ) {

            // map with wbs-code as key
            HashMap<String, String> entityIdWbsMap = new HashMap<String, String>();
            Set<BimPredecessorSuccessor> predSuccSet = new HashSet<BimPredecessorSuccessor>();

            BimProject bimProject = bt.getBimProject();

            if (bimProject.getProjectID().equalsIgnoreCase(projects.get(0).getProjectID())) {
                Map<String, Object> jsonParams = new HashMap<String, Object>();
//                CloseableHttpClient client = HttpClients.createDefault();
                JSONObject json = new JSONObject();
//                String requestUrl = URI + "updateProject/" + bimProject.getProjectID();
//                HttpPut httpPut = new HttpPut(requestUrl);
//                System.out.println(requestUrl);
                try {
                    if (bimProject.getProjectName() != null && !bimProject.getProjectName().isEmpty()) {
                        jsonParams.putIfAbsent("projectName", bimProject.getProjectName());
                    }
                    if (bimProject.getClientName() != null && !bimProject.getClientName().isEmpty()) {
                        jsonParams.putIfAbsent("clientName", bimProject.getClientName());
                    }
                    if (bimProject.getCountry() != null && !bimProject.getCountry().isEmpty()) {
                        jsonParams.putIfAbsent("country", bimProject.getCountry());
                    }
                    if (bimProject.getAddress() != null && !bimProject.getAddress().isEmpty()) {
                        jsonParams.putIfAbsent("address", bimProject.getAddress());
                    }
                    json.put("projectName", bimProject.getProjectName());
                    json.put("clientName", bimProject.getClientName());
                    if (bimProject.getCountry() != null) {
                        json.put("country", bimProject.getCountry());
                    }
                    json.put("address", bimProject.getAddress());
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                updateTheProject(jsonParams, bimProject.getProjectID(), projectRepository);
            }

            for(BimTask task : bt.getTasks()) {
                if (task.getId() != 0) {
                    String taskWbs = task.getWbsCode();
                    String taskLbs = task.getLbsCode();
                    if (taskWbs.isEmpty() || taskLbs.isEmpty()) {
                        break;
                    }
                    int taskLevel = getTaskLevel(taskWbs);
                    String taskID = Integer.toString(task.getId());

//                    CloseableHttpClient client = HttpClients.createDefault();
                    JSONObject json = new JSONObject();

                    if(taskLevel == 1) {
//                        String requestUrl = URI + "createTaskByID/" + task.getId() + "/" +projects.get(0).getProjectID();
//                        System.out.println(requestUrl);
                        try {
                            json.put("name", task.getName());
                            json.put("taskType", "Task");
                            json.put("startDate", getDate(task.getStart()));
                            json.put("endDate", getDate(task.getFinish()));
                            json.put("wbsCode", taskWbs);
                            json.put("lbsCode", taskLbs);
                            json.put("description", task.getDescription());
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        String jsonPayload = json.toString();
                        try {
                            String taskEntityID = createTaskByIdWithProjectID(jsonPayload, taskID, projectID,
                                    scheduledEntityRepository, projectRepository);
                            entityIdWbsMap.put(taskWbs, taskEntityID);
                        } catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                    else {
                        String parentWbsID = getParentKey(task.getWbsCode());
                        String parentID = entityIdWbsMap.get(parentWbsID);
 //                       String requestUrl = URI + "createTaskWithParent/" + parentID + "/" + task.getId();
 //                       System.out.println(requestUrl);
                        try {
                            json.put("name", task.getName());
                            json.put("taskType", "Task");
                            json.put("startDate", getDate(task.getStart()));
                            json.put("endDate", getDate(task.getFinish()));
                            json.put("wbsCode", taskWbs);
                            json.put("lbsCode", taskLbs);
                            json.put("description", task.getDescription());
                            if (task.getQuantityUnit() != null) {
                                JSONObject jsonQuantity = new JSONObject();
                                jsonQuantity.put("amount", task.getQuantity());
                                jsonQuantity.put("unit", task.getQuantityUnit());
                                json.put("quantity", jsonQuantity );
                            }
                        } catch (JSONException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                        String jsonPayload = json.toString();
                        String taskEntityID = createTaskByIdWithParentID(jsonPayload, taskID, parentID,
                                scheduledEntityRepository, taskRepository);
                        entityIdWbsMap.put(taskWbs, taskEntityID);
                        String predecessor = task.getPredecessor();
                        if (predecessor != null)
                        {
                            Pattern p = Pattern.compile("[a-zA-Z]");
                            java.util.regex.Matcher m = p.matcher(predecessor);
                            if (m.find()) {
                                predecessor = predecessor.substring(0, predecessor.length() - 2);
                            }
                            BimPredecessorSuccessor pair = new BimPredecessorSuccessor(
                                     predecessor + "@" + projectID, task.getId() + "@" + projectID);
                            predSuccSet.add(pair);
                        }
                        System.out.println("Tier " + taskLevel  + " task entity id: " + taskEntityID);
                    }
                }
            }
            // now generate the predecessor-successor relations
            if (!predSuccSet.isEmpty()) {
                for(BimPredecessorSuccessor pair : predSuccSet) {
                    addSuccessorTask(pair.getPredecessor(), pair.getSuccessor(), scheduledEntityRepository);
                }
            }
        }
        if (whatToImport.toLowerCase().equals("bim") || whatToImport.toLowerCase().equals("both")) {

            for (BimCategory category : bt.getCategories()) {
                genericInit(category, bt.getProject());
            }
            for (BimFamily family : bt.getFamilies()) {
                setLinksForFamily(family, bt.getProject(), bt.getCategoriesById());
            }

            for (BimMaterial material : bt.getMaterials()) {
                setLinksForMaterial(material, bt.getProject(), bt.getCategoriesById(), bt.getParametersById());
            }

            for (BimType type : bt.getTypes()) {
                setLinksForType(type, bt.getProject(), bt.getFamiliesById(), bt.getMaterialsById(),
                        bt.getParametersById(), bt.getCategoriesById());
            }


            for (BimElement element : bt.getElements()) {
                setLinksForElement(element, bt.getProject(), bt.getTypesById(), bt.getMaterialsById(),
                        bt.getParametersById(), bt.getFamiliesById(), bt.getCategoriesById());
            }

            for (BimRoom room : bt.getRooms()) {
                setLinksForRoom(room, bt.getProject(), bt.getParametersById());
            }


            categoryRepository.save(bt.getCategories());
            familyRepository.save(bt.getFamilies());
            materialRepository.save(bt.getMaterials());
            typeRepository.save(bt.getTypes());
            roomRepository.save(bt.getRooms());
            bimElementRepository.save(bt.getElements());

            for (BimMaterial material : bt.getMaterials()) {
                buildRelationsToTasks(material, projectID);
            }

            for (BimRoom room : bt.getRooms()) {
                buildRelationsToTasks(room, projectID);
            }

            for (BimType type : bt.getTypes()) {
                buildRelationsToTasks(type, projectID);
            }

            for (BimElement element : bt.getElements()) {
                buildRelationsToTasks(element, projectID);
            }
            //       bimElementRepository.save(bt.getElements());
        }
        return new ResponseEntity<>(bimFile, HttpStatus.CREATED);
    }


    public static String getDate(Date date) {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        String newDate = sdf.format(date);
        return newDate;
    }

    public static int getTaskLevel(String wbsCode) {
        return wbsCode.split("\\.").length;
    }

    public static String getParentKey(String wbsCode) {
        // Only Tier2 and less level of wbs code should be sent
        int length = wbsCode.length();
        return wbsCode.substring(0, length-2);
    }

    public static String getResponseBody(HttpEntity entity) throws ParseException, IOException {
        return EntityUtils.toString(entity, "UTF-8");
    }
    /*
        private static void setTypeLink(final BimElement element, Map<Long, List<BimType>> typesById) {
            element.setTypes(element.getTypeIds()
                                    .stream()
                                    .filter(typesById::containsKey)
                                    .map(typesById::get)
                                    .map(Iterables::getOnlyElement)
                                    .collect(toSet()));
        }
    */
    private void buildRelationsToTasks(HasEmbeddedParameters entity, String projectId) {
        // added by Peter for connecting BIM-Entities with Tasks
        Set<BimParameter> params = entity.getParameters();
        final String wbsString = "WBS";
//        final String wbsCodeString = "WBS Code";
        final String lbsString = "LBS Code";
        Set<String> wbsValues = new HashSet<String>();
        String lbsValue = null;
        Long bimNeoId = entity.getNeoId();
        if (!params.isEmpty()) {
            for (BimParameter param : params) {
                String paramName = param.getName();
//       			if (paramName.equalsIgnoreCase(wbsString) || paramName.equalsIgnoreCase(wbsCodeString)) {
                if (paramName.startsWith(wbsString)) {
                    wbsValues.add(param.getValue());
                } else if (paramName.equalsIgnoreCase(lbsString)) {
                    lbsValue = param.getValue();
                }
            }
        }

        if (!wbsValues.isEmpty()) {
            for (String actualWbsCode : wbsValues) {
                if (lbsValue != null) {
                    System.out.println("BIM-Entity " + entity.getBimId()  + " connect to task with WBS: " + actualWbsCode + " LBS " + lbsValue);
                    bimEntityRepository.buildTaskRelationbyWbsAndLbs(bimNeoId, actualWbsCode, lbsValue, projectId);
                } else {
                    System.out.println("BIM-Entity " + entity.getBimId()  + " connect to task with WBS: " + actualWbsCode);
                    bimEntityRepository.buildTaskRelationbyWbs(bimNeoId, actualWbsCode, projectId);
                }
            }
        }
        else if (lbsValue != null) {
            System.out.println("BIM-Entity " + entity.getBimId()  + " connect to task with LBS: " + lbsValue);
            bimEntityRepository.buildTaskRelationbyLbs(bimNeoId, lbsValue, projectId);
        }
    }

    @Getter
    public static class BimPredecessorSuccessor {

        final private String predecessor;
        final private String successor;
        BimPredecessorSuccessor(String pre, String succ) {
            predecessor = pre;
            successor = succ;
        }
        public String getPredecessor() {return predecessor;}
        public String getSuccessor() {return successor;}
    }

    @Getter
    public static class BimFileData {

        final private Project project;
        final private BimProject bimProject;
        final private Set<BimCategory> categories;
        final private Set<BimFamily> families;
        final private Set<BimMaterial> materials;
        final private Set<BimType> types;
        final private Set<BimElement> elements;
        final private List<BimTask> tasks;
        final private Set<BimRoom> rooms;

        private final Map<Long, List<BimCategory>> categoriesById;
        private final Map<Long, List<BimFamily>> familiesById;
        private final Map<Long, List<BimMaterial>> materialsById;
        private final Map<Long, List<BimType>> typesById;
        private final Map<Long, List<BimRoom>> roomsById;
        private final Map<String, List<BimParameter>> parametersById;

        BimFileData(BimFile bimFile, Project project) {
            this.project = project;
            bimProject = bimFile.getBimProject();
            categories = bimFile.getCategories();
            families = bimFile.getFamilies();
            materials = bimFile.getMaterials();
            types = bimFile.getTypes();
            rooms = bimFile.getRooms();
            elements = bimFile.getElements()
                    .stream()
                    //.peek(e -> genericInit(e, project))
                    .collect(toSet());
            Set<BimTask> unsortedTasks = bimFile.getBimTasks();

            tasks = unsortedTasks.stream()
                    //.sorted(Comparator.comparing(BimTask::getId))
                    .sorted((t1,t2) -> t1.getWbsCode().compareTo(t2.getWbsCode()))
                    .collect(Collectors.toList());

            categoriesById = groupById(categories);
            familiesById = groupById(families);
            materialsById = groupById(materials);
            typesById = groupById(types);
            roomsById = groupById(rooms);
            parametersById = getUniqueParameters(elements, types, materials, rooms);
        }

        public Map<Long, List<BimCategory>> getCategoriesById() { return categoriesById;}
        public Map<String, List<BimParameter>> getParametersById() { return parametersById;}
        public Map<Long, List<BimFamily>> getFamiliesById() { return familiesById;}
        public Map<Long, List<BimMaterial>> getMaterialsById() { return materialsById;}
        public Map<Long, List<BimType>> getTypesById() { return typesById;}
        BimProject getBimProject() { return bimProject;}
        List<BimTask> getTasks() {return tasks;}
        Set<BimCategory> getCategories() { return categories;}
        Project getProject() {return project;}
        Set<BimFamily> getFamilies() { return families;}
        Set<BimMaterial> getMaterials() { return materials;}
        Set<BimElement> getElements() { return elements;}
        Set<BimType> getTypes() { return types;}
        Set<BimRoom> getRooms() { return rooms;}
    }
/*
    private static void uniquifyEmbeddedParameters(Map<String, List<BimParameter>> parameters, HasEmbeddedParameters entity) {
        entity.setParameters(entity.getParameters()
                                   .stream()
                                   .map(BimParameter::getGuid)
                                   .map(parameters::get)
                                   .map(Iterables::getOnlyElement)
                                   .collect(toSet()));
    }
*/
}
