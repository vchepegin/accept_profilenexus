package eu.accept.tiekinetix.nexus.profiles.core.model.enumerations;

/**
 * Created by vchepegin on 30/01/2017.
 */
public enum CrewType {

    INTERNAL (1),
    FREELANCE (2),
    TEMPORAL (3);

    private final int crewTypeCode;

    CrewType(int code) {
        this.crewTypeCode = Integer.valueOf(code);
    }

    private int userStatusCode () {
        return crewTypeCode;
    }

    @Override
    public String toString () {
        String tmp = "Not defined";
        switch (crewTypeCode) {
            case 1:
                tmp = "INTERNAL";
                break;

            case 2:
                tmp = "FREELANCE";
                break;

            case 3:
                tmp = "TEMPORAL";
                break;
        }

        return tmp;
    }
}
