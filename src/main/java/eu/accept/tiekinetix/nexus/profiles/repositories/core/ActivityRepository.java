package eu.accept.tiekinetix.nexus.profiles.repositories.core;

import eu.accept.tiekinetix.nexus.profiles.core.model.user.Person;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Activity;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.repository.query.Param;

import java.util.Set;

/**
 * Created by vchepegin on 15/03/2017.
 */
public interface ActivityRepository extends GraphRepository<Activity> {

//    Activity findByActivityID (String id);

    Activity findByEntityID (String id);

    @Query("match (a:Activity {entityID: {0}} )-[:hasCrew*0..1]-(c:Crew)-[:hasWorker*]-(m) return m")
    Set<Person> findCrewMembers (@Param("0")String id);

    @Query("match (a:Activity) WHERE a.entityID IN [{0}] return a")
    Set<ScheduledEntity> findMultipleByIDs(@Param("0")String ids);
}
