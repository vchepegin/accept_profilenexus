package eu.accept.tiekinetix.nexus.profiles.core.model.enumerations;

/**
 * Created by vchepegin on 30/01/2017.
 */
public enum EquipmentStatus {
    AVAILABLE (1),
    UNAVAILABLE (2),
    BROKEN (3);

    private final int equipmentStatusCode;

    EquipmentStatus(int code) {
        this.equipmentStatusCode = Integer.valueOf(code);
    }

    private int getEquipmentStatusCode() {
        return equipmentStatusCode;
    }

    @Override
    public String toString () {
        String tmp = "Not defined";
        switch (equipmentStatusCode) {
            case 1:
                tmp = "AVAILABLE";
                break;

            case 2:
                tmp = "UNAVAILABLE";
                break;

            case 3:
                tmp = "BROKEN";
                break;
        }

        return tmp;
    }


}
