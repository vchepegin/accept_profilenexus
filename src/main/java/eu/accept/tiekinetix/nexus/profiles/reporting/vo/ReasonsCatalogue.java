package eu.accept.tiekinetix.nexus.profiles.reporting.vo;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.data.annotation.Id;
//import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

//@Document(collection = "noncompletion")
public class ReasonsCatalogue implements Serializable {

    private static final long serialVersionUID = -6894883359449381688L;

    private final static ObjectMapper mapper = new ObjectMapper();


    @Id
    private String id;

    private String languageCode;
    Map<String, String> rncCodes;

    public ReasonsCatalogue () {};

    public ReasonsCatalogue(String languageCode, Map<String, String> rncCodes) {
        this.languageCode = languageCode;
        this.rncCodes = rncCodes;
    }

    public ReasonsCatalogue(String languageCode) {
        this.languageCode = languageCode;
        rncCodes = new HashMap<String, String>();
        rncCodes.putIfAbsent("0", "");
        rncCodes.putIfAbsent("1", "Prerequisites");
        rncCodes.putIfAbsent("2", "Too many activities assigned");
        rncCodes.putIfAbsent("3", "Activity performance overestimation");
        rncCodes.putIfAbsent("4", "Rework");
        rncCodes.putIfAbsent("5", "Missing/inadequate planning");
        rncCodes.putIfAbsent("6", "Weather");
        rncCodes.putIfAbsent("7", "Delay in material delivery");
        rncCodes.putIfAbsent("8", "Unavailability of skilled workers");
        rncCodes.putIfAbsent("9", "Long waiting time for inspection & testing");
        rncCodes.putIfAbsent("10", "Equipment");
        rncCodes.putIfAbsent("11", "Safety");
        rncCodes.putIfAbsent("12", "Inappropriate construction methods");
        rncCodes.putIfAbsent("13", "Not accessible construction site");
        rncCodes.putIfAbsent("14", "No permission");
        rncCodes.putIfAbsent("15", "Technically not feasible");
        rncCodes.putIfAbsent("16", "Absence of person in charge");
        rncCodes.putIfAbsent("17", "Reduced number of workers");
    }

    public String getRNC(int index) {
        return rncCodes.get(index);
    }

    public String getLanguageCode () {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    public Map<String, String> getRncCodes() {
        return rncCodes;
    }

    public void setRncCodes(Map<String, String> rncCodes) {
        this.rncCodes = rncCodes;
    }

    public String toJson () throws JsonProcessingException {
        return mapper.writeValueAsString(rncCodes);
    }

    public String toString (int code) {
        return rncCodes.get(String.valueOf(code));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ReasonsCatalogue)) return false;

        ReasonsCatalogue that = (ReasonsCatalogue) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (!getLanguageCode().equals(that.getLanguageCode())) return false;
        return getRncCodes() != null ? getRncCodes().equals(that.getRncCodes()) : that.getRncCodes() == null;
    }

    @Override
    public int hashCode() {
        int result = getLanguageCode().hashCode();
        result = 31 * result + (getRncCodes() != null ? getRncCodes().hashCode() : 0);
        return result;
    }
}
