package eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;
import org.neo4j.ogm.annotation.Relationship;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * This class is a place-holder for task templates, so called Activities. It has a name and it indicates
 * the start of the un-initialised (templated) sequences of Activities and GateWays.
 *
 * Created by vchepegin on 20/04/2017.
 */
@NodeEntity
@JsonIgnoreProperties(ignoreUnknown = true)
public class Workflow implements Serializable {

    private static final long serialVersionUID = -4142281731616477161L;

    @GraphId
    private Long id;

    private String workflowID;
    private String name;

    public Workflow( ) {}

    public Workflow(String name) {
        this.name = name;
    }

    @Relationship(type="hasStart", direction = Relationship.OUTGOING)
    public Set<ScheduledEntity> start;
    /**
     * Add new successor to the ScheduledEntity
     * @param item
     * @return ScheduledEntity for the fluent interface
     */
    public void addStart (ScheduledEntity item) {
        if (start == null) {
            start = new HashSet<>();
        }
        start.add(item);
    }

    public void removeStart (ScheduledEntity item) {
        if (start == null) {
            start = new HashSet<>();
        }
        start.remove(item);
    }

    public Set<ScheduledEntity> getStart () {
        return start;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("workflowID")
    public String getWorkflowID() {
        return workflowID;
    }

    @JsonProperty("workflowID")
    public void setWorkflowID(String workflowID) {
        this.workflowID = workflowID;
    }
}
