package eu.accept.tiekinetix.nexus;

import eu.accept.tiekinetix.nexus.messaging.MessageDispatcher;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;

import java.util.concurrent.CountDownLatch;

/**
 * Created by vchepegin on 14/07/2017.
 */
@Configuration
public class MessagingConfiguration {

    @Value("${redis.hostname}")
    private String redisHostName;

    @Bean
    RedisConnectionFactory redisConnectionFactory() {
        return new LettuceConnectionFactory(redisHostName, 6379);
    }
/*    @Bean
    RedisConnectionFactory redisConnectionFactory() {
        return new LettuceConnectionFactory();
    }
*/
    @Bean
    RedisMessageListenerContainer container(RedisConnectionFactory connectionFactory,
                                            MessageListenerAdapter listenerAdapter) {

        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
//        container.addMessageListener(listenerAdapter, new PatternTopic("CHANGED_TASK"));
//        container.addMessageListener(listenerAdapter, new PatternTopic("CHANGED_ACTIVITY"));
        container.addMessageListener(listenerAdapter, new PatternTopic("COMMANDS"));

        return container;
    }

    @Bean
    MessageListenerAdapter listenerAdapter(MessageDispatcher mDispatcher) {
        return new MessageListenerAdapter(mDispatcher, "receiveMessage");
    }

    @Bean
    MessageDispatcher receiver(CountDownLatch latch) {
        return new MessageDispatcher(latch);
    }

    @Bean
    CountDownLatch latch() {
        return new CountDownLatch(1);
    }

    @Bean
    StringRedisTemplate template(RedisConnectionFactory connectionFactory) {
        return new StringRedisTemplate(connectionFactory);
    }



}
