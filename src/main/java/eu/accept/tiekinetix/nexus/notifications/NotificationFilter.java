package eu.accept.tiekinetix.nexus.notifications;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 *
 * Created by vchepegin on 26/07/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationFilter {

    private  String projectID;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH.mm.ss")
    private Date timestamp;
    private  String receiverID;

    public NotificationFilter(String projectID, Date timestamp, String receiverID) {
        this.projectID = projectID;
        this.timestamp = timestamp;
        this.receiverID = receiverID;
    }

    public  NotificationFilter () {};

    @JsonProperty("projectID")
    public String getProjectID() {
        return projectID;
    }

    @JsonProperty("projectID")
    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    @JsonProperty("dateTime")
    public Date getTimestamp() {
        return timestamp;
    }

    @JsonProperty("dateTime")
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @JsonProperty("receiverID")
    public String getReceiverID() {
        return receiverID;
    }

    @JsonProperty("receiverID")
    public void setReceiverID(String receiverID) {
        this.receiverID = receiverID;
    }
}
