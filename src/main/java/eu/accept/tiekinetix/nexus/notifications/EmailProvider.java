package eu.accept.tiekinetix.nexus.notifications;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 *
 * Created by vchepegin on 12/09/2017.
 */
public final class EmailProvider {

    public static void sendEmail (NotificationProperties properties,
                                  String subject, String body, String... recipients) {
        // Create a mail session
        Properties props = new Properties();
        props.put("mail.smtp.starttls.enable", "true"); //enable
        props.put("mail.smtp.host", "smtp.gmail.com");
//        props.setProperty("mail.user", properties.getUser());
//        props.setProperty("mail.password", properties.getPassword());
        props.put("mail.smtp.port", "587"); //TLS Port
        props.put("mail.smtp.auth", "true"); //enable authentication

        javax.mail.Session session = javax.mail.Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(properties.getUser(), properties.getPassword());
                    }
                });

        //un-comment in case of troubles
//            session.setDebug(true);
        if (recipients.length == 0) {
            return; }
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(properties.getFromEmail()));
            message.setSubject("ACCEPT.EU notification: " + subject);

            for (String recipient : recipients) {
                message.addRecipient(javax.mail.Message.RecipientType.TO,
                        new InternetAddress(recipient));
            }

            message.setText(body);

            Transport transport = session.getTransport("smtp");
//                transport.connect("smtp.gmail.com", fromEmail, password);
            transport.send(message, message.getAllRecipients());
        } catch (MessagingException mex) {
            mex.printStackTrace();
        }

    }



}
