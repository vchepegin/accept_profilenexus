package eu.accept.tiekinetix.nexus.notifications;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.neo4j.ogm.annotation.GraphId;
import org.neo4j.ogm.annotation.NodeEntity;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * Notification class - a VO for notifications.
 */
@NodeEntity
@JsonIgnoreProperties(ignoreUnknown = true)
final public class Notification implements Serializable {

    @GraphId
    private Long id;

    private static final long serialVersionUID = -3170755130324724840L;

    private  String projectID;
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH.mm.ss")
    private  Date timestamp;
    private  String receiverID;
//    private Set<String> receiverID;
    private  String sender;
    private  String subject;
    private  String classification;
//    private  String telephone;
    private  int priority;


    public Notification(String projectID, Date timestamp, String receiverID,
                        String sender, String subject, String classification,
                        /*String telephone,*/ int priority) {
        this.projectID = projectID;
        this.timestamp = timestamp;
        this.receiverID = receiverID;
        this.sender = sender;
        this.subject = subject;
        this.classification = classification;
//        this.telephone = telephone;
        this.priority = priority;
    }

    public  Notification () {};

    @JsonProperty("projectID")
    public String getProjectID() {
        return projectID;
    }

    @JsonProperty("projectID")
    public void setProjectID(String projectID) {
        this.projectID = projectID;
    }

    @JsonProperty("dateTime")
    public Date getTimestamp() {
        return timestamp;
    }

    @JsonProperty("dateTime")
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    @JsonProperty("receiverID")
    public String getReceiverID() {
        return receiverID;
    }

    @JsonProperty("receiverID")
    public void setReceiverID(String receiverID) {
        this.receiverID = receiverID;
    }

    @JsonProperty("sender")
    public String getSender() {
        return sender;
    }

    @JsonProperty("sender")
    public void setSender(String sender) {
        this.sender = sender;
    }

    @JsonProperty("subject")
    public String getSubject() {
        return subject;
    }

    @JsonProperty("subject")
    public void setSubject(String subject) {
        this.subject = subject;
    }

    @JsonProperty("classification")
    public String getClassification() {
        return classification;
    }

    @JsonProperty("classification")
    public void setClassification(String classification) {
        this.classification = classification;
    }
/*
    @JsonProperty("telephone")
    public String getTelephone() {
        return telephone;
    }

    @JsonProperty("telephone")
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
*/
    @JsonProperty("priority")
    public int getPriority() {
        return priority;
    }

    @JsonProperty("priority")
    public void setPriority(int priority) {
        this.priority = priority;
    }
}
