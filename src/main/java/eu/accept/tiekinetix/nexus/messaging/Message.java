package eu.accept.tiekinetix.nexus.messaging;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

/**
 * Immutable class for passing events.
 * Created by vchepegin on 14/07/2017.
 */
final public class Message implements Serializable {

    private static final long serialVersionUID = -8088004241912917338L;

    //message type from Enum? how to write extendible dispatcher then?
    private final String messageType;
    private final String ID;
    private final String message;

    @JsonCreator
    public Message(@JsonProperty("messageType") String messageType,
                   @JsonProperty("ID") String ID,
                   @JsonProperty("message") String message) {
        this.messageType = messageType;
        this.ID = ID;
        this.message = message;
    }

    @JsonProperty("messageType")
    public String getMessageType() {
        return messageType;
    }

    @JsonProperty("ID")
    public String getID() {
        return ID;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "Message{" +
                "taskTpe='" + messageType + '\'' +
                ", ID='" + ID + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Message)) return false;

        Message message1 = (Message) o;

        if (!getMessageType().equals(message1.getMessageType())) return false;
        if (!getID().equals(message1.getID())) return false;
        return getMessage().equals(message1.getMessage());
    }

    @Override
    public int hashCode() {
        int result = getMessageType().hashCode();
        result = 31 * result + getID().hashCode();
        result = 31 * result + getMessage().hashCode();
        return result;
    }
}
