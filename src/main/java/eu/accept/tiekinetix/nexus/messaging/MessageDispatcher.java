package eu.accept.tiekinetix.nexus.messaging;

/**
 * Created by vchepegin on 14/07/2017.
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.accept.tiekinetix.nexus.messaging.handlers.MessageHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Async;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

public class MessageDispatcher {
    private static final Logger LOGGER = LoggerFactory.getLogger(MessageDispatcher.class);
    private static final ObjectMapper mapper = new ObjectMapper();
    private static final String HANDLER_PREFIX = "eu.accept.tiekinetix.nexus.messaging.handlers.";

    private CountDownLatch latch;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    public MessageDispatcher(CountDownLatch latch) {
        this.latch = latch;
    }

    @Async
    public void receiveMessage(String ms) {
        try {
            Message message = mapper.readValue(ms, Message.class);

            //long ct = System.nanoTime();
            MessageHandler mh = instantiate(HANDLER_PREFIX + message.getMessageType() + "Service",
                    MessageHandler.class);
            //System.out.println("Difference is " + (System.nanoTime() - ct)  );
            mh.handle(message, applicationContext);

        } catch (IOException e) {
            LOGGER.error("Cannot deserialize message: " + ms + " due to: "
                    + e.getLocalizedMessage());
        }
        latch.countDown();
    }

    /**
     * Load and instantiate a necessary implementation of the class by its name. Here this function is used for
     * building a concrete implementation of a functional MessageHandler interface
     * @param className
     * @param type
     * @param <T>
     * @return
     */
    private <T> T instantiate(final String className, final Class<T> type){
        try{
            return type.cast(Class.forName(className).newInstance());
        } catch(InstantiationException
                | IllegalAccessException
                | ClassNotFoundException e){
            LOGGER.error("Cannot instantiate class: " + className + " due to: "
                    + e.getLocalizedMessage());
            throw new IllegalStateException(e);
        }
    }
}
