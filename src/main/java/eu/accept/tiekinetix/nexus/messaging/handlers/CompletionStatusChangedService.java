package eu.accept.tiekinetix.nexus.messaging.handlers;

import com.google.common.collect.Sets;
import eu.accept.tiekinetix.nexus.messaging.Message;
import eu.accept.tiekinetix.nexus.notifications.Notification;
import eu.accept.tiekinetix.nexus.notifications.NotificationProperties;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Project;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.ProjectRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.ScheduledEntityRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.TaskRepository;
import eu.accept.tiekinetix.nexus.profiles.repositories.reporting.NotificationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;
import eu.accept.tiekinetix.nexus.profiles.util.TikkiQueue;

import static eu.accept.tiekinetix.nexus.notifications.EmailProvider.*;
import static eu.accept.tiekinetix.nexus.profiles.util.TikkiQueue.sendTikkiMessage;

/**
 * Created by vchepegin on 08/09/2017.
 */
public class CompletionStatusChangedService implements MessageHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(CompletionStatusChangedService.class);

    TaskRepository taskRepository;

    public CompletableFuture<Message> handle(Message msg, ApplicationContext ctx) {
        sendNotification(msg, ctx);
        final CompletableFuture<Message> result = new CompletableFuture<>();
        result.complete(new Message("success", msg.getID(), "done"));
        return result;
    }

    private void sendNotification (Message msg, ApplicationContext ctx) {
        LOGGER.info("Received by CompletionStatusChangedService <" + msg.getID() + ">");

        ProjectRepository projectRepository = ctx.getBean(ProjectRepository.class);
        ScheduledEntityRepository seRepository = ctx.getBean(ScheduledEntityRepository.class);

        Project parentProject = projectRepository.findProjectByChild(msg.getID());
        Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        cal.setTime(new Date()); // sets calendar time/date
        cal.add(Calendar.HOUR_OF_DAY, parentProject.getTimeZoneDifferenceToUTC());

        Notification notification = new Notification();
//        notification.setProjectID(projectRepository.findProjectIdByChild(msg.getID()));
        notification.setProjectID(parentProject.getProjectID());
//        notification.setTimestamp(new Date());
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        Date timestamp = cal.getTime();
        notification.setTimestamp(timestamp);
        notification.setSender(msg.getID());
        notification.setPriority(1);
        notification.setSubject(msg.getMessage());
        notification.setClassification("default class");
        Set<String> emails = seRepository.findOwnersEmails(msg.getID());
        Set<String> receiverIDs = seRepository.findOwnersIDs(msg.getID());
        notification.setReceiverID(receiverIDs.stream().collect(Collectors.joining(";")));
//        notification.setReceiverID(emails.stream().collect(Collectors.joining(";")));

        NotificationRepository notificationRepository  = ctx.getBean(NotificationRepository.class);
        notificationRepository.save(notification);

        NotificationProperties properties = ctx.getBean(NotificationProperties.class);
        ScheduledEntity se = seRepository.findByEntityID(msg.getID());
        String subject = new String("ACCEPT.EU: status change for ") + se.getTaskType() + " " + se.getName();
//        String subject = new String("ACCEPT.EU: status change for entity ID = ") + msg.getID();
        String body = new String("Dear owner, \n\nYour project  ") + parentProject.getProjectName() +
                " has an update: \n\n" + msg.getMessage() + "\n\nYours sincerely,\n ACCEPT Crew";
        sendEmail(properties, subject,body, emails.toArray(new String[emails.size()]));
        String tikkiQueueID = se.getTikkiQueueID();
        try {
            sendTikkiMessage(emails, tikkiQueueID, subject, body);
        }catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
