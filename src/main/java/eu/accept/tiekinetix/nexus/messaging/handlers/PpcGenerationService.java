package eu.accept.tiekinetix.nexus.messaging.handlers;

import eu.accept.tiekinetix.nexus.messaging.Message;
import eu.accept.tiekinetix.nexus.profiles.reporting.vo.PpcActivity;
import eu.accept.tiekinetix.nexus.profiles.reporting.vo.PpcReport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Date;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

/**
 * Created by vchepegin on 27/07/2017.
 */
public class PpcGenerationService implements MessageHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectProcessingService.class);

    public CompletableFuture<Message> handle(Message msg, ApplicationContext ctx) {
        generatePpcReport(ctx);
        final CompletableFuture<Message> result = new CompletableFuture<>();
        result.complete(new Message("success", msg.getID(), ""));
        return result;
    }

    private void generatePpcReport (ApplicationContext ctx) {
        StringRedisTemplate template = ctx.getBean(StringRedisTemplate.class);

        Set<String> members = template.boundSetOps("VALIDATED_ACTIVITIES").members();

        PpcReport report = new PpcReport("projectID",
                new Date(System.currentTimeMillis()));

        PpcActivity ppc = new PpcActivity();

        /*
         * ActivityRepository has new method to find all with id list from members
         * Then
         *
         * PPC_Activity: for each different wbs-code of Activities (these Activities with same wbs-code differ in lbs-code (other location)), which have been monitored and which dynamicDailyGoal > 0, perform loop:
            cumulativeWBSgoalAchieved = SUM(goalAchieved == TRUE) of Activity with scanned wbs-code
            cumulativeWBSscheduled = SUM(monitored Activities with scanned wbs-code
            parameter PPC_Activity: PPC_Activity = (cumulativeWBSgoalAchieved / cumulativeWBSscheduled) * 100; (%)
            for KPI-record wbs-code has to be stored, too
         */

        //flush after procesign is done and everythign is generated
        //TODO: in principle the same ID cld be added in a meanwhile again - so need to lock
        // readWrite operations for it - create utility and lock it
        for (String id : members) {
            template.boundSetOps("VALIDATED_ACTIVITIES").remove(id);
        }
    }

}
