package eu.accept.tiekinetix.nexus.messaging.handlers;

import eu.accept.tiekinetix.nexus.messaging.Message;
import org.springframework.context.ApplicationContext;

import java.util.concurrent.CompletableFuture;

/**
 * This interface has to be implemented by all NEXUS message handlers.
 * Created by vchepegin on 18/07/2017.
 */
public interface MessageHandler {
    /**
     * It returns a promise in case a handler needs to trigger the further propagation such as
     * send back diagnostic message if something went wrong, return result, send partial result back,
     * initiate new recalculations, etc.
     * @param msg
     * @return
     */
    CompletableFuture<Message> handle(Message msg, ApplicationContext ctx);
}
