package eu.accept.tiekinetix.nexus.messaging.mock;

import com.fasterxml.jackson.databind.ObjectMapper;
import eu.accept.tiekinetix.nexus.messaging.Message;
import eu.accept.tiekinetix.nexus.messaging.MessageDispatcher;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Async;

import java.util.concurrent.TimeUnit;

/**
 * Example showing how to produce the messages and send them over the Redis. If uncomment @Service then upon the
 * application start run method will be started and will emit messages.
 *
 * Created by vchepegin on 14/07/2017.
 */
//@Service
public class MessageProducer implements CommandLineRunner {

    private final StringRedisTemplate template;
//    private final MessageDispatcher receiver;
    private final ConfigurableApplicationContext context;

    private final ObjectMapper mapper = new ObjectMapper();

    public MessageProducer(MessageDispatcher receiver, StringRedisTemplate template,
                           ConfigurableApplicationContext context) {
//        this.receiver = receiver;
        this.template = template;
        this.context = context;
    }

    @Async
    @Override
    public void run(String... args) throws Exception {
        try {
            while (true) {
//                System.out.println("MessageDispatcher sending message...");
                template.convertAndSend("CHANGED_ACTIVITY",
                        mapper.writeValueAsString(new Message("SomethingProcessing", "3242234", "")));
                template.convertAndSend("CHANGED_TASK",
                        mapper.writeValueAsString(new Message("ProjectProcessing", "3242234", "")));
//                template.convertAndSend("CHANGED_ACTIVITY", "new changed ID = 213123");
                TimeUnit.SECONDS.sleep(5);
//        receiver.getLatch().await(10000, TimeUnit.MILLISECONDS);
            }
        } catch (InterruptedException e){

        } finally {
            context.close();
        }
    }

}
