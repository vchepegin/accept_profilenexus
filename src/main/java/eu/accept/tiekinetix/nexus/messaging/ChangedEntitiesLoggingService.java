package eu.accept.tiekinetix.nexus.messaging;

import eu.accept.tiekinetix.nexus.profiles.reporting.vo.ChangedObjects;
import eu.accept.tiekinetix.nexus.profiles.repositories.messaging.ChangedObjectsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Set;

/**
 * This whole thing has to be replaced with a queue, e.g. using Redis - see the same package for the examples, e.g.
 * {@code MessageDispatcher}
 *
 * Created by vchepegin on 04/07/2017.
 */
@Deprecated
@Service
public class ChangedEntitiesLoggingService {

    @Autowired
    ChangedObjectsRepository changedObjectsRepository;


    //transaction should be inside the
    @Transactional
    public void logChangedEntity (String entityID) {
        ChangedObjects co = changedObjectsRepository.findCurrent();
        if (co == null) {
            co = new ChangedObjects();
        }
        co.addID(entityID);
        changedObjectsRepository.save(co);
    }

    @Transactional
    public void logChangedEntities (Set<String> entityIDs) {
        ChangedObjects co = changedObjectsRepository.findCurrent();
        co.addIDs(entityIDs);
        changedObjectsRepository.save(co);
    }

    @Transactional
    public ChangedObjects  poll () {
        ChangedObjects co = changedObjectsRepository.findCurrent();
        co.setValidUntil(new Date(System.currentTimeMillis()));
        changedObjectsRepository.save(co);
        ChangedObjects co2 = new ChangedObjects();
        changedObjectsRepository.save(co2);
        return co;
    }
}
