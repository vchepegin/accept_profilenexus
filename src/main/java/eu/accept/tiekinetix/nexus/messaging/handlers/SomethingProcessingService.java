package eu.accept.tiekinetix.nexus.messaging.handlers;

import eu.accept.tiekinetix.nexus.messaging.Message;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.Activity;
import eu.accept.tiekinetix.nexus.profiles.core.model.workflow.impl.ScheduledEntity;
import eu.accept.tiekinetix.nexus.profiles.repositories.core.ScheduledEntityRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.util.concurrent.CompletableFuture;

/**
 * Created by vchepegin on 14/07/2017.
 */
public class SomethingProcessingService implements MessageHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(SomethingProcessingService.class);

    public CompletableFuture<Message> handle(Message msg, ApplicationContext ctx) {
        calcDuration(msg);

        ScheduledEntityRepository repository = ctx.getBean(ScheduledEntityRepository.class);
        ScheduledEntity entity = repository.findByEntityID("Activity2Update1");
        Activity activity = (Activity) entity;
        activity.setComment("I was updated via PN MessageHandler");
        repository.save(activity);

        final CompletableFuture<Message> result = new CompletableFuture<>();
        result.complete(new Message("success", msg.getID(), ""));
        return result;
    }

    private void calcDuration (Message message) {
        LOGGER.info("Received  by SomethingProcessingService <" + message.getID() + ">");
    }
}
