package eu.accept.tiekinetix.nexus;
/**
 * Created by vchepegin on 25/01/2017.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.data.neo4j.repository.config.EnableNeo4jRepositories;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executor;

@SpringBootApplication
@EnableNeo4jRepositories
@EnableTransactionManagement
@EnableAsync
@EnableCaching
@EnableAspectJAutoProxy
public class Application {

//    @Value("${environment}")
//    private String environment;

    private final static Logger log = LoggerFactory.getLogger(Application.class);

    /* Number of threads = number of available cores * 2 (if hyperthreaded). Bear in mind that
     * Spring has it own thread pool. See {@code  @Bean public Executor asyncExecutor() {})
     */
    private static final int NUMBER_OF_WORKERS = 6;

    /**
     * DOCME: Redis server has to be running and if not listening top the standard ports
     * on the localhost then the configuration has to be adjusted.
     * If Redis is running locally including inside the Docker container and uses the
     * default port then no changes to the configuration are needed.
     */
    public static void main(String[] args) throws Exception {
        ApplicationContext ctx = SpringApplication.run(Application.class, args);

        StringRedisTemplate template = ctx.getBean(StringRedisTemplate.class);
        CountDownLatch latch = ctx.getBean(CountDownLatch.class);

        //DOCME: continue the service only after messaging framework is up and running
        latch.await();
    }

    @Bean
    public Executor asyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(NUMBER_OF_WORKERS);
        executor.setMaxPoolSize(NUMBER_OF_WORKERS * 2);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("message-handler-");
        executor.initialize();
        return executor;
    }

//    @Bean
//    public String getInformation() {
//        System.out.println("Build for the " + environment + " environment!");
//        return "OK";
//    }
}